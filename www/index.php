<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Main on front page                                                  #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', './');
define ('CODE', 'home');

require (PATH_TO_ROOT . 'inc/init.inc.php');
$t = new Template();
$t->set_file('main', PATH_TO_TEMPLATE . 'index.html');
$t->set_block('main', 'exp_block', 'exp_block_');
$t->set_block('main', 'block1_img', 'block1_img_');

define ('PAGE_TITLE', '');
define ('META_KEYWORDS', '');
define ('META_DESCRIPTION', '');

$db->Query('SELECT * FROM ru_sitemap WHERE ID = 507');
while($db->NextRecord())
{
	$t->set_var(array(
		"BLOCK1_TITLE" =>	$db->F('Title'),
		"BLOCK1_DESC"  =>	$db->F('Description'),
		"BLOCK1_LINK"  =>	'/content/' . $db->F('ID')
	));
	$image = is_image('img/ru/content/' . $db->F('ID'));
	if ($image){
		
		$t->set_var("BLOCK1_IMG", $image);
		$t->parse('block1_img_', 'block1_img', false);
	}
	
}

$db->Query('SELECT * FROM ru_sitemap WHERE ID = 512');
while($db->NextRecord())
{
	$t->set_var(array(
		"EXPBLOCK_READMORE"		=> '/content/' . $db->F('ID')
	));
}

$db->Query('SELECT * FROM ru_sitemap WHERE Parent_id = 512');
while($db->NextRecord())
{
	$t->set_var(array(
		"EXPBLOCK_TITLE"	   =>	$db->F('Title'),
		"EXPBLOCK_DESC"		   =>	$db->F('Description'),
		"EXPBLOCK_LINK"  	   =>	'/content/' . $db->F('ID')
	));	
	
	$t->parse('exp_block_', 'exp_block', true);
}


$t->set_block('main', 'block2_img', 'block2_img_');
$db->Query('SELECT * FROM ru_sitemap WHERE ID = 508');
while($db->NextRecord())
{
	$t->set_var(array(
		"BLOCK2_TITLE" =>	$db->F('Title'),
		"BLOCK2_DESC"  =>	$db->F('Description'),
		"BLOCK2_LINK"  =>	'/content/' . $db->F('ID')
	));
	$image = is_image('img/ru/content/' . $db->F('ID'));
	if ($image){
		
		$t->set_var("BLOCK2_IMG", $image);
		$t->parse('block2_img_', 'block2_img', false);
	}
	
}

$t->set_block('main', 'news', 'news_');
$date_array = array();
$db->Query('SELECT * FROM ru_news WHERE Hide = 0 ORDER BY Date DESC LIMIT 2');
while($db->NextRecord())
{
	$date_array = explode('.', sql_to_date($db->F('Date')));
	$t->set_var(array(
		"NEWS_TITLE"	=>	$db->F('Title'),
		"NEWS_DESC"	=>	$db->F('Announce'),
		"NEWS_LINK"	=>	'/news/' . $db->F('ID'),
		"NEWS_DAY"	=> $date_array[0],
		"NEWS_MONTH"	=>	get_month_name_simple_eng($date_array[1])
	));
	
	$t->parse('news_', 'news', true);
}
$t->set_block('main', 'coming', 'coming_');
$date_array = array();
$db->Query('SELECT * FROM ru_cs WHERE Hide = 0 ORDER BY Date DESC LIMIT 2');
while($db->NextRecord())
{
	$date_array = explode('.', sql_to_date($db->F('Date')));
	$t->set_var(array(
		"C_TITLE"	=>	$db->F('Title'),
		"C_DESC"	=>	$db->F('Announce'),
		"C_LINK"	=>	'/comingsoon/' . $db->F('ID'),
		"C_DAY"	=> $date_array[0],
		"C_MONTH"	=>	get_month_name_simple_eng($date_array[1])
	));
	
	$t->parse('coming_', 'coming', true);
}
require (PATH_TO_ROOT . 'include/all.inc.php');

?>