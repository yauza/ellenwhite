<?
#########################################################################
#                                                                       #
#   Copyright (c) 2008, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   blocks.inc.php                                                      #
#   functions for print block of information                            #
#                                                                       #
#########################################################################


//-------------------------------------------------------------------------------------------
// $page           -  текущая страница
// $count          -  общее количество записей
// $count_on_page  -  количество записей на странице
function get_block_pages($page, $count, $count_on_page)
{
	global $db;
	$tpl = new Template;
    $tpl->set_file('main', PATH_TO_TEMPLATE . 'unit_pages.html');
    $tpl->set_block('main', 'page', 'page_');
    $tpl->set_block('page', 'page_active', 'page_active_');
    $tpl->set_block('page', 'page_active_no', 'page_active_no_');
    $tpl->set_block('main', 'page_first', 'page_first_');
    $tpl->set_block('main', 'page_last', 'page_last_');

    $count_pages = ceil($count / $count_on_page); // количество страниц
    if ($count_pages < 2) return '';
    
    if ($page > 1) $tpl->parse('page_first_', 'page_first', false);
    if ($page < $count_pages)
	{
		$tpl->set_var('PAGE_LAST', $count_pages);
		$tpl->parse('page_last_', 'page_last', false);
	}
    
    $url = $_SERVER['REQUEST_URI'];
    /*if (isset($_GET['page']))
    {
		$query_str = str_replace('page=' . $_GET['page'] . '&', '', $_SERVER['QUERY_STRING']);
		if ($query_str == $_SERVER['QUERY_STRING'])
		$query_str = str_replace('&page=' . $_GET['page'], '', $_SERVER['QUERY_STRING']);
		if ($query_str == $_SERVER['QUERY_STRING'])
		$query_str = str_replace('page=' . $_GET['page'], '', $_SERVER['QUERY_STRING']);
	}
	if (trim($query_str) != '') $query_str .= '&';*/
    //print_R($_SERVER);
    $page_url = preg_replace('/[\&\?]page\=[0-9]+$/', '', $_SERVER['REQUEST_URI']);
    $page_url = preg_replace('/p[0-9]+\/[0-9]+$/', '', $page_url);
    $page_url = preg_replace('/p[0-9]+$/', '', $page_url);
    $page_url .= strpos($page_url, '?') === false ? '?' : '&';
    $tpl->set_var('PAGE_URL', $page_url);
    //$tpl->set_var('PAGE_URL', str_replace('&', '&amp;', $query_str));
    //$tpl->set_var('THIS_PAGE', $_SERVER['PHP_SELF']);
    
    $first = $page - 5;
    $last = $page + 5;
    
    // получение '<' для перемещения на страницу назад
    if ($page > 1)
    {
		$tpl->set_var(array(
			'PAGE_NUM_TITLE' => '&lt;&nbsp;',
			'PAGE_NUM_LINK'  => $page - 1,
			'SEPARATOR'      => ''
        ));
		$tpl->parse('page_active_no_', 'page_active_no', false);
		$tpl->parse('page_', 'page', true);
	}
    
	//получение блока с активной страницей
	for ($i = $first; $i <= $last; $i++)
    {
		if ($i > 0 && $i <= $count_pages)
		{
			$tpl->set_var(array(
				'page_active_'    => '',
				'page_active_no_' => '',
				'PAGE_NUM_TITLE'  => $i,
				'PAGE_NUM_LINK'   => $i,
				'SEPARATOR'       => '&nbsp;'
        	));
        	if ($i == $page) $tpl->parse('page_active_', 'page_active', false);
        	else $tpl->parse('page_active_no_', 'page_active_no', false);
        	$tpl->parse('page_', 'page', true);
		}
	}
    
    // получение '>' для перемещения на страницу вперед
    if ($page < $count_pages)
    {
		$tpl->set_var(array(
			'PAGE_NUM_TITLE' => '&nbsp;&gt;',
			'PAGE_NUM_LINK'  => $page + 1,
			'SEPARATOR'      => ''
        ));
        $tpl->parse('page_active_no_', 'page_active_no', false);
		$tpl->parse('page_', 'page', true);
	}
    
    return $tpl->parse('_', 'main', false);
}
//-------------------------------------------------------------------------------------------
function get_banner($code)
{
    global $db;
    
	$db->Query('SELECT ID, Width, Height FROM banners_pos WHERE Code = "' . addslashes($code) . '" AND Lang = "' . LANG . '" AND Hide = 0');
	if ($db->NextRecord())
	{
	    $pos = $db->mRecord;
		$db->Query('SELECT ID, Title, Type, Url, Value FROM banners WHERE Position_id = ' . $pos['ID'] . ' AND Hide = 0 ORDER BY RAND() LIMIT 1');
		if ($db->NextRecord())
		{
			switch ($db->F('Type'))
			{
                case 2:
                {
                    return $db->F('Value');
                } break;
				default:
                {
                    $image = is_image(PATH_TO_ROOT . 'img/banners/' . $db->F('ID'));
                    if ($image)
                        return '<a href="' . $db->F('Url') . '"><img src="' . $image . '" width="' . $pos['Width'] . '" height="' . $pos['Height'] . '" alt="' . htmlspecialchars($db->F('Title')) . '"></a>';
                }
			}
		}
		else
			return '';
	}
    else
        return '';
}

?>