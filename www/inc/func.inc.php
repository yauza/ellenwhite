<?
#########################################################################
#                                                                       #
#   Copyright (c) 2008, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   func.inc.php                                                        #
#   functions for navigation, work with content, formating messages     #
#                                                                       #
#########################################################################

/*
  string get_navigation(mixed $add, string $block) - return navigation string 
  string get_codes(string code) - return codes child for element with code=$code
   array get_childs(int $id) - return child elements for element with id=$id
     int get_id_by_code(string $code) - get id by code
   array get_content(string $code) - get content text by code
  string get_formatted_error(string $err)      - return formated error message
    bool is_obj_exists(int $id, string $idField, string $tblName) - check exist record in table "tblName", where "idField = id"
*/

function get_video_stars($id)
{
    global $db;
    
    $db->Query('SELECT AVG(Rating) AS Rating FROM ru_videos_ratings WHERE Video_id = ' . intval($id));
    if ($db->NextRecord())
    {
        if ($db->F('Rating') < 0.25) $img = array(2, 2, 2, 2, 2);
        elseif ($db->F('Rating') < 0.75) $img = array(3, 2, 2, 2, 2);
        elseif ($db->F('Rating') < 1.25) $img = array(1, 2, 2, 2, 2);
        elseif ($db->F('Rating') < 1.75) $img = array(1, 3, 2, 2, 2);
        elseif ($db->F('Rating') < 2.25) $img = array(1, 1, 2, 2, 2);
        elseif ($db->F('Rating') < 2.75) $img = array(1, 1, 3, 2, 2);
        elseif ($db->F('Rating') < 3.25) $img = array(1, 1, 1, 2, 2);
        elseif ($db->F('Rating') < 3.75) $img = array(1, 1, 1, 3, 2);
        elseif ($db->F('Rating') < 4.25) $img = array(1, 1, 1, 1, 2);
        elseif ($db->F('Rating') < 4.75) $img = array(1, 1, 1, 1, 3);
        else $img = array(1, 1, 1, 1, 1);
        
        return array('rating' => round($db->F('Rating'), 2), 'img' => '<img src="img/star' . implode('.png" alt=""><img src="img/star', $img) . '.png" alt="">', 'nums' => implode('', $img));
    }
    else
        return array('rating' => 0, 'img' => '');
}
//----------------------------------------------------------------------------------------
// получение поля типа date
function get_field_date($field, $value, $title, $is_time = false)
{
	global $LA;
	return '<input name="' . $field . '" value="' . $value . '" id="' . $field . '_order" maxlength="' . ($is_time ? '16' : '10') . '"><img src="img/cal.gif" id="tr_' . $field . '" alt="' . $LA['Редактор даты'] . '" title="' . $LA['Редактор даты'] . '" onmouseover="this.style.background=\'red\';" onmouseout="this.style.background=\'\'"><script type="text/javascript">Calendar.setup({inputField:"' . $field . '",ifFormat:"%d.%m.%Y' . ($is_time ? ' %H:%M' : '') . '",showsTime:' . ($is_time ? 'true' : 'false') . ',button:"tr_' . $field . '",timeFormat:"24",align:"Tl",singleClick:false});</script>';
}
//----------------------------------------------------------------------------------------
// получение значения опции по коду
function get_option_by_code($code, $lang = 'ru', $is_html = true)
{
	$db9 = new PslDb;
	$db9->Query('SELECT Value FROM options WHERE Code = "' . addslashes($code) . '" AND Lang = "' . $lang . '"');
	return $db9->NextRecord() ? ($is_html ? htmlspecialchars($db9->F('Value')) : $db9->F('Value')) : '';
}
//----------------------------------------------------------------------------------------
// получение параметров для древоивидных разделов админки
function get_parameters($id, $code)
{
	switch ($code)
	{
		case 'content': $TABLE = LANG . '_sitemap'; break;
		case 'options': $TABLE = 'options_list'; break;
		case 'adm':     $TABLE = $code; break;
		default: $TABLE = $code;
	}
	
	$param = array('level' => 1, 'count_childs' => 0, 'count_childs_all' => 0);
	$db9 = new PslDb;
	$db9->Query('SELECT Parent_id FROM ' . $TABLE . ' WHERE ID = ' . $id);
	while ($db9->NextRecord() && $db9->F('Parent_id') != 1)
	{
		$param['level']++;
		$db9->Query('SELECT Parent_id FROM ' . $TABLE . ' WHERE ID = ' . $db9->F('Parent_id'));
	}
	
	$db9->Query('SELECT Count_childs FROM ' . $TABLE . ' WHERE ID = ' . $id);
	if ($db9->NextRecord()) $param['count_childs'] = $db9->F('Count_childs');
	
	$param['count_childs_all'] = get_count_chils_all($id, $TABLE);
	
	unset($db9);
	return $param; // level - уровень вложенности; count_childs - количество вложенных страних; count_childs_all - общее количество вложенных страниц
}
// полчение общего количества вложенных записей
function get_count_chils_all($id, $TABLE)
{
	$db9 = new PslDb;
	$count = 0; $childs = array();
	$db9->Query('SELECT ID, Count_childs FROM ' . $TABLE . ' WHERE Hide = 0 AND Parent_id = ' . $id);
	while ($db9->NextRecord())
	{
		$count++;
		$childs[] = $db9->F('ID');
	}
	foreach ($childs AS $v) $count += get_count_chils_all($v, $TABLE);
	return $count;
}
//-------------------------------------------------------------------------------------
function get_resized_image_size ($w, $h, $path)
{
	list($width, $height) = getimagesize($path);
	if ($width > $w || $height > $h)
	{
		if ($height > $h)
		{
			$newheight = $h;
			$newwidth = $width * $newheight / $height;
			if ($newwidth > $w)
			{
				$newwidth = $w;
				$newheight = $height * $newwidth / $width;
			}
		}
		elseif ($width > $w)
		{
			$newwidth = $w;
			$newheight = $height * $newwidth / $width;
			if ($newheight > $h)
			{
				$newheight = $h;
				$newwidth = $width * $newheight / $height;
			}
		}
		return array(intval($newwidth), intval($newheight));
	}
	else
		return array(intval($width), intval($height));
}
//-------------------------------------------------------------------------------------

?>