<?
#########################################################################
#                                                                       #
#   Copyright (c) 2008, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   menu.inc.php                                                        #
#   functions for menu generation                                       #
#                                                                       #
#########################################################################

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
function get_menu($template = 'left_menu', $parent_id = 1)
{
	global $g_user, $javascript, $LA;
    $db = new PslDb;
    $r = '';
	
	$tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATE . $template . '.html');
	$tpl->set_block('main', 'first', 'first_');
	$tpl->set_block('first', 'second_out', 'second_out_');
	$tpl->set_block('second_out', 'second_in', 'second_in_');
	$tpl->set_block('second_in', 'third_out', 'third_out_');
	$tpl->set_block('third_out', 'third_in', 'third_in_');
	
	$db2 = new PslDb; $db3 = new PslDb;
	$db->Query('SELECT ID, Title FROM ' . LANG . '_sitemap WHERE Parent_id = ' . $parent_id . ' AND Hide = 0 ORDER BY OrderBy');
	while ($db->NextRecord())
	{
		$tpl->set_var('FIRST_TITLE', htmlspecialchars($db->F('Title')));
		$tpl->set_var('second_out_', '');
		$tpl->set_var('second_in_', '');
		
		$flag_second = false;
		$db2->Query('SELECT ID, Title, Code FROM ' . LANG . '_sitemap WHERE Parent_id = ' . $db->F('ID') . ' AND Hide = 0 ORDER BY OrderBy');
		if ($db2->NumRows() > 0) $flag_second = true;
		while ($db2->NextRecord())
		{
			$href = get_href($db2->F('Code'), $db2->F('ID'));
			$tpl->set_var(array(
				'SECOND_TITLE'  => htmlspecialchars($db2->F('Title')),
				'SECOND_HREF'   => $href,
				'third_out_'    => '',
				'third_in_'     => ''
			));
			if ((($db2->F('Code') == 'content' && isset($_GET['id']) && $_GET['id'] == $db2->F('ID')) || 
			     ($db2->F('Code') != 'content' && empty($_GET))) && CODE == $db2->F('Code'))
			{
				$tpl->set_var(array(
					'SECOND_OPEN'  => '<li class="current"><b>',
					'SECOND_CLOSE' => '</b>'
				));
			}
			else
			{
				$tpl->set_var(array(
					'SECOND_OPEN'  => '<li><a href="' . $href . '">',
					'SECOND_CLOSE' => '</a>',
				));
			}
			
			$flag_third = false;
			if ($db2->F('Code') == 'news') // вывод категорий новостей
			{
				$db3->Query('SELECT ID, Title FROM ' . LANG . '_news_categories WHERE Hide = 0');
				if ($db3->NumRows() > 0) $flag_third = true;
				while ($db3->NextRecord())
				{
					$href = PATH_TO_ROOT . LANG . '/news/?cat=' . $db3->F('ID');
					$tpl->set_var('THIRD_TITLE', htmlspecialchars($db3->F('Title')));
					$tpl->set_var('THIRD_HREF',  $href);
					if (CODE == 'news' && isset($_GET['cat']) && $_GET['cat'] == $db3->F('ID'))
					{
						$tpl->set_var(array(
							'THIRD_OPEN'  => '<li class="current"><b>',
							'THIRD_CLOSE' => '</b>'
						));
					}
					else
					{
						$tpl->set_var(array(
							'THIRD_OPEN'  => '<li><a href="' . $href . '">',
							'THIRD_CLOSE' => '</a>',
						));
					}
					$tpl->parse('third_in_', 'third_in', true);
				}
			}
			else // страницы контента
			{
				$db3->Query('SELECT ID, Title, Code FROM ' . LANG . '_sitemap WHERE Parent_id = ' . $db2->F('ID') . ' AND Hide = 0 ORDER BY OrderBy');
				if ($db3->NumRows() > 0) $flag_third = true;
				while ($db3->NextRecord())
				{
					$href = get_href($db3->F('Code'), $db3->F('ID'));
					$tpl->set_var('THIRD_TITLE', htmlspecialchars($db3->F('Title')));
					$tpl->set_var('THIRD_HREF',  $href);
					if (CODE == 'content' && isset($_GET['id']) && $_GET['id'] == $db3->F('ID'))
					{
						$tpl->set_var(array(
							'THIRD_OPEN'  => '<li class="current"><b>',
							'THIRD_CLOSE' => '</b>'
						));
					}
					else
					{
						$tpl->set_var(array(
							'THIRD_TITLE' => htmlspecialchars($db3->F('Title')),
							'THIRD_OPEN'  => '<li><a href="' . $href . '">',
							'THIRD_CLOSE' => '</a>'
						));
					}
					$tpl->parse('third_in_', 'third_in', true);
				}
			}
			if ($flag_third) $tpl->parse('third_out_', 'third_out', false);
			
			$tpl->parse('second_in_', 'second_in', true);
		}
		if ($flag_second) $tpl->parse('second_out_', 'second_out', false);
		
		$tpl->parse('first_', 'first', true);
	}
	
    return $tpl->parse('_', 'main', false);
}
//---------------------------------------------------------------------------
// событие при нажатии на название
//---------------------------------------------------------------------------
function get_href($code, $id)
{
	switch ($code)
	{
		case 'none': // если нет такой страницы, ставится ссылка на первую дочернюю
		{
			$db1 = new PslDb;
			$db1->Query('SELECT ID, Code FROM ' . LANG . '_sitemap WHERE Parent_id = ' . $id . ' AND Hide = 0 ORDER BY OrderBy LIMIT 1');
			if ($db1->NextRecord())
			{
				switch ($db1->F('Code'))
				{
					case 'content':
					{
						return PATH_TO_ROOT . LANG . '/' . $db1->F('Code') . '/?id=' . $db1->F('ID');
					} break;
					default:
						return PATH_TO_ROOT . LANG . '/' . $db1->F('Code') . '/';
				}
			}
			else
				return '#';
		}
		/*case 'a': // если просто ссылка
		{
			return htmlspecialchars($link);
		}*/
		case 'content':
		{
			if ($id == 2) // если главная страница
				return PATH_TO_ROOT;
			else
				return PATH_TO_ROOT . LANG . '/' . $code . '/?id=' . $id;
		}
		default:
			return PATH_TO_ROOT . LANG . '/' . $code . '/';
	}
}
	
?>