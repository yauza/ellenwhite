<?
#########################################################################
#                                                                       #
#   Copyright (c) 2008, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   init.inc.php                                                        #
#   initilize objects, include library and common functions             #
#                                                                       #
#########################################################################

//$_PHPSITELIB_USER_POSTFIX = '_fe';

require (PATH_TO_ROOT . 'lib/psl_start.inc.php');
require (_PHPSITELIB_PATH . 'psl_constants.inc.php');

// путь к текущей странице
define ('THIS_PAGE', LANG . '/' . (CODE != 'index' ? CODE . '/' : ''));

// путь до папки с шаблонами
define ('PATH_TO_TEMPLATE', PATH_TO_ROOT . 'temp_front/' . (array_key_exists(LANG, $LANGS) ? (LANG . '/') : ''));

require (PATH_TO_ROOT . 'inc/func.inc.php');
require (PATH_TO_ROOT . 'inc/blocks.inc.php');

// get value for parameter RET (address to return)
$ret_link = $_SERVER['REQUEST_URI'];
$ret_addr = isset($_GET['ret']) ? $_GET['ret'] : '';
if ($ret_addr == '' && isset($_POST['ret'])) $ret_addr = $_POST['ret'];
if ($ret_addr == '') $ret_addr = PATH_TO_ROOT;

$g_user->mAutoAcceptGroups = true;
$g_user->mCheckAccessBackEnd = false;
$g_user->mAccessBackEnd = false;
$g_user->mAccessFrontEnd = true;
$g_user->Start();
	
$js = '';

$lang = isset($_GET['lang']) && $_GET['lang'] == 'ru' ? 'ru' : 'en';

// проверка авторизирован ли пользователь
if (isset($_COOKIE['user_id'], $_COOKIE['user_pass']))
{
    $db->Query('SELECT * FROM users WHERE ID = ' . intval($_COOKIE['user_id']) . ' AND Password = "' . addslashes($_COOKIE['user_pass']) . '" AND Hide = 0');
    if ($db->NextRecord()) $user = $db->mRecord;
}

if (empty($user) && isset($_SESSION['user_id'], $_SESSION['user_pass']))
{
    $db->Query('SELECT * FROM users WHERE ID = ' . intval($_SESSION['user_id']) . ' AND Password = "' . addslashes($_SESSION['user_pass']) . '" AND Hide = 0');
    if ($db->NextRecord()) $user = $db->mRecord;
}

if (!empty($user['ID']))
    $db->Query('UPDATE users SET Date_last = NOW() WHERE ID = ' . intval($user['ID']));

?>