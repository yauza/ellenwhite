<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   News page                                                           #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT',  '../../');
define ('CODE', 'news');
define ('TABLE', LANG . '_sitemap');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

require (PATH_TO_ROOT . 'inc/init.inc.php');

$t = new Template;
if ($id > 0){

$t->set_file('main', PATH_TO_TEMPLATE . 'new.html');
$t->set_block('main', 'news', 'news_');
$t->set_block('news', 'img', 'img_');
$db->Query('SELECT * FROM ru_news WHERE Hide = 0 AND ID = ' . $id);
while($db->NextRecord())
{
	    define ('PAGE_TITLE', $db->F('Title') . ' // News // ' . $db->F('Title') . ' / ');
		define ('META_KEYWORDS', $db->F('Meta_keys'));
		define ('META_DESCRIPTION', $db->F('Meta_desc'));
	$t->set_var(array(
		"DATE"	=>	sql_to_date($db->F('Date')),
		"TITLE"	=>	$db->F('Title'),
		"TEXT"	=>	$db->F('Text')
	));
	
	$image = is_image(PATH_TO_ROOT . 'img/ru/news/' . $db->F('ID'));
	if ($image)
	{
		$t->set_var(array(
			"IMG"	=>	$image
		));
		$t->parse('img_', 'img', false);
	}
	$t->parse('news_', 'news', false);
}
}
else
{
$t->set_file('main', PATH_TO_TEMPLATE . 'news.html');
$last_id = 0;
$t->set_block('main', 'last_news', 'last_news_');
$t->set_block('main', 'arch_news', 'arch_news_');
$t->set_block('last_news', 'last_img', 'last_img_');
$db->Query('SELECT * FROM ru_news WHERE Hide = 0 ORDER BY Date DESC LIMIT 1');
while($db->NextRecord())
{
	define ('PAGE_TITLE', 'News // ');
	$last_id = intval($db->F('ID'));
	$t->set_var(array(
		"LAST_DATE"	=>	sql_to_date($db->F('Date')),
		"LAST_TITLE"	=>	$db->F('Title'),
		"LAST_TEXT"	=>	$db->F('Text')
	));
	
	$image = is_image(PATH_TO_ROOT . 'img/ru/news/' . $db->F('ID'));
	if ($image)
	{
		$t->set_var(array(
			"LAST_IMG"	=>	$image
		));
		$t->parse('last_img_', 'last_img', false);
	}
	$t->parse('last_news_', 'last_news', false);
}

$db->Query('SELECT * FROM ru_news WHERE Hide = 0 AND ID != ' . $last_id . ' ORDER BY Date DESC');
while($db->NextRecord())
{
	$t->set_var(array(
		"ARCH_DATE"	=>	sql_to_date($db->F('Date')),
		"ARCH_TITLE"	=>	$db->F('Title'),
		"ARCH_ANNOUNCE"	=>	$db->F('Announce'),
		"ARCH_LINK"	=>	'/news/' . $db->F('ID') . '/'
	));
	$t->parse('arch_news_', 'arch_news', true);
}
}
require PATH_TO_ROOT . 'include/all.inc.php';
?>