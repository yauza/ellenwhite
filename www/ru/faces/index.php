<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Faces page                                                          #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'faces');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

require (PATH_TO_ROOT . 'inc/init.inc.php');
        
$t = new Template;

$type_page = 'block';
if (isset($_GET['block']))
{
    $_SESSION['index'] = 'block';
    setcookie('index', 'block', time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
    $type_page = 'block';
}
elseif (isset($_GET['list']))
{
    $_SESSION['index'] = 'list';
    setcookie('index', 'list', time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
    $type_page = 'list';
}
elseif ((!empty($_COOKIE['index']) && $_COOKIE['index'] == 'block') || (!empty($_SESSION['index']) && $_SESSION['index'] == 'block'))
{
    $type_page = 'block';
}
elseif ((!empty($_COOKIE['index']) && $_COOKIE['index'] == 'list') || (!empty($_SESSION['index']) && $_SESSION['index'] == 'list'))
{
    $type_page = 'list';
}

if ($id)
{
    $t->set_file('main', PATH_TO_TEMPLATE . 'face.html');
    
    $db->Query('SELECT p.FioRus, p.FioOriginal, YEAR(p.Date_birth) AS Year, p.Description, p.Description_eng, p.Text, p.Text_eng, c.Title AS Country, c.Title_eng AS Country_eng FROM ru_peoples AS p LEFT JOIN ru_countries AS c ON p.Country_id = c.ID WHERE p.Hide = 0 AND p.IsPage = 1 AND p.ID = ' . intval($id) . ' GROUP BY p.ID ORDER BY RAND() LIMIT 11');
    if ($db->NextRecord())
    {
        $author = $db->mRecord;
        // print_r($author);exit;   
        $country = $lang == 'ru' ? $author['Country'] : $author['Country_eng'];
        
        $professions = array();
        $db->Query('SELECT p.Title, p.Title_eng FROM ru_professions AS p INNER JOIN ru_peoples_records AS pr ON p.ID = pr.Profession_id WHERE pr.People_id = ' . intval($id));
        while ($db->NextRecord())
            $professions[] = $lang == 'ru' ? htmlspecialchars($db->F('Title')) : htmlspecialchars($db->F('Title_eng'));
        
        $t->set_var(array(
         	'PEOPLE_IMG'        => is_image(PATH_TO_ROOT . 'img/ru/peoples/' . $id . '_big'),
         	'PEOPLE_IMG2'       => is_image(PATH_TO_ROOT . 'img/ru/peoples/' . $id . '_icon', 'img/userpic-null.gif'),
         	'PEOPLE_FIO'        => $lang == 'ru' ? htmlspecialchars($author['FioRus']) : htmlspecialchars($author['FioOriginal']),
         	'PEOPLE_PROFESSION' => implode(', ', $professions),
         	'PEOPLE_YEAR'       => !empty($author['Year']) ? (', ' . $author['Year']) : '',
         	'PEOPLE_COUNTRY'    => $country ? (($lang == 'ru' ? 'Страна' : 'Country') . ': ' . htmlspecialchars($country)) : '',
         	'PEOPLE_GENRES'     => '',
            'PEOPLE_DESCRIPTION' => $lang == 'ru' ? get_title_formated($author['Description'], 50, 200, true) : get_title_formated($author['Description_eng'], 50, 200, true),
            'PEOPLE_TEXT'       => $lang == 'ru' ? $author['Text'] : $author['Text_eng'],
            'LANG_PORTFOLIO'    => $lang == 'ru' ? 'Портфолио' : 'Portfolio'
        ));
    }
    else
        location(PATH_TO_ROOT . '404.php');
    
    //
	$j = 1;  
    $t->set_block('main', 'record', 'record_');
    $db->Query('SELECT v.ID, v.TitleRus, v.TitleOriginal, v.Section_id, v.IsFree, DAY(Date) AS Day, MONTH(Date) AS Month, YEAR(Date) AS Year FROM ru_videos AS v INNER JOIN ru_peoples_records AS pr ON v.ID = pr.Record_id AND pr.Record_code = "videos" WHERE v.Hide = 0 AND pr.People_id = ' . intval($id) . ' ORDER BY RAND() LIMIT 10');
    while ($db->NextRecord())
    {
    	if ($j > 3)
			$j = 1;
    	$t->set_var(array(
			'BLOCK_NUM'		   => $j++,
         	'RECORD_LINK'    => '../'.$lang.'/videos/' . $db->F('ID'),
         	'RECORD_TITLE'   => $lang == 'ru' ? htmlspecialchars($db->F('TitleRus')) : htmlspecialchars($db->F('TitleOriginal')),
         	'RECORD_IMG'     => is_image(PATH_TO_ROOT . 'img/ru/videos/' . $db->F('ID') . '_small'),
         	'RECORD_PEOPLES' => '',
         	'RECORD_DATE'    => $db->F('Day') . ' ' . get_month_name_low($db->F('Month')) . ' ' . $db->F('Year'),
            'RECORD_LOCK'    => !$db->F('IsFree') ? 'f-lock' : 'clear'
        ));
        
        $t->parse('record_', 'record', true);
    }
}
else
{
    $t->set_file('main', PATH_TO_TEMPLATE . 'faces.html');

    $t->set_var('PEOPLES_PAGES_TITLE', $lang == 'ru' ? 'персональные страницы' : 'personal pages');
    
    $t->set_var(array(
        'LINK_BLOCK'  => 'faces/?block',
        'LINK_LIST'   => 'faces/?list',
    	'CLASS_BLOCK' => $type_page == 'block' ? ' class="sel"' : '',
    	'CLASS_LIST'  => $type_page == 'list' ? ' class="sel"' : ''
    ));
    
    $t->set_block('main', 'record_block', 'record_block_');
    $t->set_block('main', 'record_list', 'record_list_');
   $j = 1;   
    $db->Query('SELECT p.ID, p.FioRus, p.FioOriginal, p.Description, p.Description_eng, YEAR(p.Date_birth) AS Year, p2.Title AS Profession, p2.Title_eng AS Profession_eng, c.Title AS Country, c.Title_eng AS Country_eng FROM ru_peoples AS p INNER JOIN ru_peoples_records AS pr ON p.ID = pr.People_id INNER JOIN ru_professions AS p2 ON pr.Profession_id = p2.ID LEFT JOIN ru_countries AS c ON p.Country_id = c.ID WHERE p.Hide = 0 AND p.IsPage = 1 AND p2.ID = 1 GROUP BY p.ID ORDER BY p.FioRus');
    
    //$db->Query('SELECT v.ID, v.TitleRus, v.TitleOriginal, v.Section_id, v.IsFree, DAY(Date) AS Day, MONTH(Date) AS Month, YEAR(Date) AS Year FROM ru_videos AS v WHERE v.Hide = 0 ORDER BY RAND() LIMIT 10');
    while ($db->NextRecord())
    {
	if ($j > 3)
			$j = 1;
    	$t->set_var(array(
			'BLOCK_NUM'		   => $j++,
         	'RECORD_LINK'       => '../'.$lang.'/faces/' . $db->F('ID'),
         	'RECORD_TITLE'      => $lang == 'ru' ? htmlspecialchars($db->F('FioRus')) : htmlspecialchars($db->F('FioOriginal')),
         	'RECORD_IMG'        => is_image(PATH_TO_ROOT . 'img/ru/peoples/' . $db->F('ID') . '_small'),
         	'RECORD_PROFESSION' => $lang == 'ru' ? htmlspecialchars($db->F('Profession')) : htmlspecialchars($db->F('Profession_eng')),
         	'RECORD_DATE'       => $db->F('Year') ? (', ' . $db->F('Year')) : '',
            'RECORD_LOCK'       => /*!$db->F('IsFree') ? 'f-lock' : */'clear',
            'RECORD_DESCRIPTION' => $lang == 'ru' ? ($db->F('Description')) : ($db->F('Description_eng'))
        ));
        
        if ($type_page == 'list')
            $t->parse('record_list_', 'record_list', true);
        else
            $t->parse('record_block_', 'record_block', true);
    }
}

//---------------------------------------------------------------------------

require (PATH_TO_ROOT . 'include/all.inc.php');

?>