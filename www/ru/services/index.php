<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Servicesge                                                          #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'services');

require (PATH_TO_ROOT . 'inc/init.inc.php');

define ('PAGE_TITLE', 'Services // ');
        
$t = new Template;
$t->set_file('main', PATH_TO_TEMPLATE . 'services.html');
$t->set_block('main', 'col1', 'col1_');
$t->set_block('col1', 'col1_items', 'col1_items_');
$t->set_block('main', 'col2', 'col2_');
$t->set_block('col2', 'col2_items', 'col2_items_');
$t->set_block('main', 'col3', 'col3_');
$t->set_block('col3', 'col3_items', 'col3_items_');

$i = 1;
$db->Query('SELECT * FROM ru_services WHERE Hide = 0');
if($db->Nf()>0){
	while($db->NextRecord())
	{
		if ($i % 2 == 0){
			
			$t->set_var(array(
				"COL2_TITLE"	=>	$db->F('Title'),
				"COL2_TEXT"	=>	$db->F('Text')
			));
			$t->parse('col2_items_', 'col2_items', true);
			$t->parse('col2_', 'col2', false);
			$i++;
		}
		elseif ($i % 3 == 0){
			
			$t->set_var(array(
				"COL3_TITLE"	=>	$db->F('Title'),
				"COL3_TEXT"	=>	$db->F('Text')
			));
			$t->parse('col3_items_', 'col3_items', true);
			$t->parse('col3_', 'col3', false);
			$i++;
		}
		else{
			
			$t->set_var(array(
				"COL1_TITLE"	=>	$db->F('Title'),
				"COL1_TEXT"	=>	$db->F('Text')
			));
			$t->parse('col1_items_', 'col1_items', true);
			$t->parse('col1_', 'col1', false);
			$i++;
		}
		if ($i > 3)
			$i = 1;
	}
}

$t->set_var("LIKE", get_option_by_code('site_like'));
$t->set_block('main', 'tm', 'tm_');
$t->set_block('tm', 'tm_img', 'tm_img_');
$db->Query('SELECT * FROM ru_testimonials WHERE Hide = 0 ORDER BY RAND() LIMIT 2');
while($db->NextRecord())
{
	$t->set_var(array(
		"TM_NAME"	=>	$db->F('Name'),
		"TM_ROLE"	=>	$db->F('Role'),
		"TM_COMPANY"	=>	$db->F('Company_name'),
		"TM_DESC"	=>	$db->F('Announce')
	));
	$image = is_image(PATH_TO_ROOT . 'img/ru/testimonials/' . $db->F('ID'));
	if($image){
		$t->set_var("TM_IMG", $image);
		$t->parse('tm_img_', 'tm_img', false);
	}
	$t->parse('tm_', 'tm', true);
	$t->set_var('tm_img_', '');
}
//---------------------------------------------------------------------------

require (PATH_TO_ROOT . 'include/all.inc.php');

?>