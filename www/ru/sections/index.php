<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   About page                                                          #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'sections');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

require (PATH_TO_ROOT . 'inc/init.inc.php');
        
$t = new Template;
$t->set_file('main', PATH_TO_TEMPLATE . 'section.html');

$db->Query('SELECT Title FROM ru_sections WHERE ID = ' . intval($id) . ' AND Hide = 0');
if ($db->NextRecord())
{
    $t->set_var(array(
    	'SECTION_TITLE' => htmlspecialchars($db->F('Title')),
    	'' => ''
    ));	
}
else
    location(PATH_TO_ROOT . '404.php');

//---------------------------------------------------------------------------

require (PATH_TO_ROOT . 'include/all.inc.php');

?>