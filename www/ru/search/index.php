<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   search page                                                          #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'search');

$s = isset($_GET['s']) ? $_GET['s'] : '';

require (PATH_TO_ROOT . 'inc/init.inc.php');
        
define ('PAGE_TITLE', 'Search // ');		
		
$t = new Template;
$t->set_file('main', PATH_TO_TEMPLATE . 'search.html');
$t->set_block('main', 'result', 'result_');
$t->set_block('main', 'no_result', 'no_result_');
$db->Query('SELECT * FROM ru_news WHERE Hide = 0 AND Text LIKE "%' . $s . '%"');
if ($db->Nf() > 0){
while ($db->NextRecord()) 
{
	$t->set_var(array(
		"TITLE"	=>	$db->F('Title'),
		"DESC"	=>	$db->F('Announce'),
		"LINK"	=>	'/news/' . $db->F('ID')
	));
	$t->parse('result_', 'result', true);
	
}
}
else
	$t->parse('no_result_', 'no_result', false);


//---------------------------------------------------------------------------
require PATH_TO_ROOT . 'include/all.inc.php';

?>