<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Main on front page                                                  #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'content');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

require (PATH_TO_ROOT . 'inc/init.inc.php');

$t = new Template;
$t->set_var(array(
    'LANG2'        => $lang == 'ru' ? 'ru' : 'en'
));
//---------------------------------------------------------------------------
if ($id) 
{
$t->set_file('main', PATH_TO_TEMPLATE . 'content.html');
$t->set_block('main', 'news', 'news_');
$t->set_block('news', 'img', 'img_');

$db->Query('SELECT Title, Text FROM ru_sitemap WHERE Hide = 0 AND Code = "content" AND ID = ' . intval($id));
while($db->NextRecord())
{
	define ('PAGE_TITLE', htmlspecialchars($db->F('Title')) . ' // ');
	$t->set_var(array(
		"DATE"	=>	sql_to_date($db->F('Date')),
		"TITLE"	=>	$db->F('Title'),
		"TEXT"	=>	$db->F('Text')
	));
	
	$image = is_image(PATH_TO_ROOT . 'img/ru/content/' . $db->F('ID'));
	if ($image)
	{
		$t->set_var(array(
			"IMG"	=>	$image
		));
		$t->parse('img_', 'img', false);
	}
	$t->parse('news_', 'news', false);
}
}
else
	location('/');
//---------------------------------------------------------------------------

require (PATH_TO_ROOT . 'include/all.inc.php');

?>