<?
#########################################################################
#                                                                       #
#   Copyright (c) 2008, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Content page                                                        #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT',  '../../');
define ('CODE', 'content');
define ('TABLE', LANG . '_sitemap');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

require (PATH_TO_ROOT . 'inc/init.inc.php');

//page title
define ('PAGE_TITLE', '');

if ($id)
{	
	$t = new Template;
	$t->set_file('main', PATH_TO_TEMPLATE . 'content.html');
	
	$db->Query('SELECT Title, Text FROM ' . TABLE . ' WHERE Hide = 0 AND ID = ' . $id);
	if ($db->NextRecord())
	{
		define ('PAGE_TITLE', htmlspecialchars($db->F('Title')) . ' // ');
		$t->set_var(array(
			'TITLE' => htmlspecialchars($db->F('Title')),
			'TEXT'  => $db->F('Text')
        ));
	}
	else
		location(PATH_TO_ROOT . '404.html');
}
else
	location(PATH_TO_ROOT . '404.html');

// set common parameters
$t->set_var(array(
	'PATH_TO_ROOT' => PATH_TO_ROOT,
	'PATH_TO_IMG'  => PATH_TO_ROOT . 'img/'
));

require (PATH_TO_ROOT . 'include/top.inc.php');

$t->pparse('C', 'main', false);

require (PATH_TO_ROOT . 'include/bottom.inc.php');
?>