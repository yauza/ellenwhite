<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Authors page                                                        #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'folio');


require (PATH_TO_ROOT . 'inc/init.inc.php');
require_once('classSimpleImage.php');
$t = new Template;

define ('PAGE_TITLE', 'Folio // ');

$t->set_file('main', PATH_TO_TEMPLATE . 'folio.html');
$t->set_block('main', 'categories', 'categories_');
$t->set_block('main', 'categories_items', 'categories_items_');
$t->set_block('categories_items', 'item', 'item_');
$i = 0;

//categories
$categories = array();
$c = 1;
$db->Query('SELECT * FROM ru_categories WHERE Hide = 0 ORDER BY OrderBy');
while($db->NextRecord()) $categories[] = $db->mRecord;
foreach($categories AS $cat)
{
	$t->set_var(array(
		"SELECTED"	=> $c == 1 ? "class=\"selected\"" : "",
		"CAT_CODE"	=> $cat['Code'],
		"CAT_TITLE"	=> $cat['Title']
	));
	$t->parse('categories_', 'categories', true);
}

foreach($categories AS $cat)
{
	
	$db->Query('SELECT * FROM ru_folio WHERE Hide = 0 AND Cat_id = ' . intval($cat['ID']));
	while($db->NextRecord())
	{
		$img = is_image(PATH_TO_ROOT . 'img/ru/folio/' . $db->F('ID'));
		$img_mini = is_image(PATH_TO_ROOT . 'img/ru/folio/' . $db->F('ID') . '_mini');
		if($img && $img_mini)
		{
		
		$image = new SimpleImage();
		$image->load($img_mini);
		$w = $image->getWidth();
		$h = $image->getHeight();
		//echo $w . "x" . $h . "<br />";
		$i++;
		$t->set_var(array(
			"ITEM_CLASS" => $i == 3 ? 'class="end"' : '',
			"ITEM_IMG" => $img,
			"ITEM_IMG_MINI" => $img_mini,
			"ITEM_MARGIN"	=>	$w < 192 ? 'style="margin-left: ' .(145 - $h). 'px"' : ''
		));
		$t->parse('item_', 'item', true);
		}
	}
		if($i == 3)
			$i = 0;
	
	$t->set_var(array(
		"ITEMS_CODE"	=>	$cat["Code"],
		"ITEMS_TITLE"	=>	$cat['Title']
	));
	
	$t->parse('categories_items_', 'categories_items', true);
	$t->set_var('item_', '');
}
require PATH_TO_ROOT . 'include/all.inc.php';

?>