<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   About page                                                          #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', '../../');
define ('CODE', 'about');

require (PATH_TO_ROOT . 'inc/init.inc.php');

define ('PAGE_TITLE', 'About Me // ');

$t = new Template;
$t->set_file('main', PATH_TO_TEMPLATE . 'about.html');

$t->set_block('main', 'main_about', 'main_about_');
$t->set_block('main_about', 'about_main_img', 'about_main_img_');
$db->Query('SELECT * FROM ru_about WHERE Hide = 0 AND IsMainVis = 1 LIMIT 1');
if ($db->Nf()>0)
{
	while($db->NextRecord())
	{
			$t->set_var(array(
				"MAIN_ABOUT_TITLE"	=>	$db->F('Title'),
				"MAIN_ABOUT_TEXT"	=>	$db->F('Text'),
				"MAIN_ABOUT_LINK"	=>	'/about/' . $db->F('ID')
			));
			$image = is_image(PATH_TO_ROOT . 'img/ru/about/' . $db->F('ID'));

			if ($image)
			{
				$t->set_var(array(
					"MAIN_ABOUT_IMG"	=>	$image
				));
				$t->parse('about_main_img_', 'about_main_img', false);
			}
	}
			$t->parse('main_about_', 'main_about', false);
}


$t->set_block('main', 'bottom_about', 'bottom_about_');
$t->set_block('bottom_about', 'col1', 'col1_');
$t->set_block('bottom_about', 'col2', 'col2_');
$t->set_block('col1', 'bot_main_img1', 'bot_main_img1_');
$t->set_block('col2', 'bot_main_img2', 'bot_main_img2_');
$t->set_block('bottom_about', 'buttons', 'buttons_');
$t->set_block('buttons', 'butt1', 'butt1_');
$t->set_block('buttons', 'butt2', 'butt2_');
$i = 1;
$db->Query('SELECT * FROM ru_about WHERE Hide = 0 AND IsMainVis = 0 LIMIT 2');
if ($db->Nf()>0){
	while($db->NextRecord())
	{
		if($i == 1)
		{
			$t->set_var(array(
				"BOTTOM_ABOUT_TITLE_1"	=>	$db->F('Title'),
				"BOTTOM_ABOUT_TEXT_1"	=>	$db->F('Text'),
				"BOTTOM_ABOUT_ANNOUNCE_1"	=>	$db->F('Announce'),
				"BOTTOM_ABOUT_LINK_1"	=>	'/about/' . $db->F('ID')
			));
			$image = is_image(PATH_TO_ROOT . 'img/ru/about/' . $db->F('ID'));
	
			if ($image)
			{
				$t->set_var(array(
					"BOT_ABOUT_IMG1"	=>	$image
				));
				$t->parse('bot_main_img1_', 'bot_main_img1', false);
			}
			$t->parse('col1_', 'col1', false);
			$i++;
		}
		else
		{
			$t->set_var(array(
				"BOTTOM_ABOUT_TITLE_2"	=>	$db->F('Title'),
				"BOTTOM_ABOUT_TEXT_2"	=>	$db->F('Text'),
				"BOTTOM_ABOUT_ANNOUNCE_2"	=>	$db->F('Announce'),
				"BOTTOM_ABOUT_LINK_2"	=>	'/about/' . $db->F('ID')
			));
			$image = is_image(PATH_TO_ROOT . 'img/ru/about/' . $db->F('ID'));
	
			if ($image)
			{
				$t->set_var(array(
					"BOT_ABOUT_IMG2"	=>	$image
				));
				$t->parse('bot_main_img2_', 'bot_main_img2', false);
			}
			$t->parse('col2_', 'col2', false);
			$i++;
		}
		
	}
			$t->parse('bottom_about_', 'bottom_about', false);
}
//---------------------------------------------------------------------------

require (PATH_TO_ROOT . 'include/all.inc.php');

?>