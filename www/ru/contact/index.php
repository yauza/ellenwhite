<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   News page                                                           #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT',  '../../');
define ('CODE', 'news');
define ('TABLE', LANG . '_sitemap');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

require (PATH_TO_ROOT . 'inc/init.inc.php');

define ('PAGE_TITLE', 'Contact Form // ');

$t = new Template;

$t->set_file('main', PATH_TO_TEMPLATE . 'contact.html');

if (isset($_POST['send'], $_POST['name'], $_POST['email'], $_POST['msg']))
{
	$name = htmlspecialchars($_POST['name']);
	$email = htmlspecialchars($_POST['email']);
	$msg = htmlspecialchars($_POST['msg']);
	$send_to = get_option_by_code('site_feedback_email');
	$message = '';
	$message .= 'Name: ' . $name . "\n";
	$message .= 'Email: ' . $email . "\n";
	$message .= 'Message: ' . wordwrap($msg, 70) . "\n";
	
	mail($send_to, 'E-mail from ellen-white.com', $message);
	
	echo "ok";
	exit;
}

$t->set_var(array(
	"Miscellaneous_text"	=>	get_option_by_code("site_miscellaneous"),
	"Country"	=>	get_option_by_code("site_country"),
	"State"	=>	get_option_by_code("site_state"),
	"City"	=>	get_option_by_code("site_city"),
	"Phone"	=>	get_option_by_code("site_phone"),
	"Email"	=>	get_option_by_code("site_feedback_email"),
));
require PATH_TO_ROOT . 'include/all.inc.php';
?>