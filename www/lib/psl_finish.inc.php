<?php	
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   psl_finish.php                                                      #
#   finishing execute script                                            #
#                                                                       #
#########################################################################

//free mysql resources (generate E_Warning in php >= 5.0.0)
$db->Free();

if (_PHPSITELIB_GENTIME)
    echo '<!-- This page generated in ' . (get_micro_time() - $_phpsitelib_gen) . ' seconds -->';

?>