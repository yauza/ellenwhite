<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Quick site development                                         #
#                                                                              #
#   Copyright (с) 2011, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_config.inc.php                                                         #
#   Library options                                                            #
#                                                                              #
################################################################################

define ("_PHPSITELIB_SITE", 'artsokolova.com');

if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1')
{
    // DB: Access to database
    define ("DB_HOST", 'localhost');
    define ("DB_NAME", 'ellenwhite');
    define ("DB_USER", 'root');
    define ("DB_PWD",  '');
}
else
{
    // DB: Access to database
    define ("DB_HOST", 'localhost');
    define ("DB_NAME", 'hawaiik4_ellen');
    define ("DB_USER", 'hawaiik4_ellen');
    define ("DB_PWD",  'ht8bw-{3B3x@');
}

// директория админки
define ('_ADMIN', 'ew.cms');

// Session: Use session
define ('_PHPSITELIB_USESESS', true);

// Cache: Level of cache ('passive', 'no', 'private', 'public')
define ('_PHPSITELIB_CACHE', 'no');
// Cache: If cache is on, display time out in min.
define ('_PHPSITELIB_CACHEEXP', 1440);

// Debug: Set debug level for system (display all queris and errors)
define ('_PHPSITELIB_DEBUG', false);
// Debug: Display time of generation page
define ('_PHPSITELIB_GENTIME', true);

// Errors: Sel level errors display for object DB:
// report - Display errors and continue execution
// yes    - Display errors and stop
// no     - Ignore errors
define ('_PHPSITELIB_DB_REPORT', 'report');
// Errors: Email, where send all reports with errors, if empty not send
define ('_PHPSITELIB_ERRMAIL', '');
// Errors: Name of index file
define ('_PHPSITELIB_CUTINDEX', 'index.php');
// Errors: Level of errors
define ('_PHPSITELIB_ERRLVL', E_ALL);

// Tables: Count of records in tables by default
define ('_PHPSITELIB_TBL_DEFINPAGE', 25);

// Use additional modules
define ('_PHPSITELIB_USE_TEMPLATE', true);
define ('_PHPSITELIB_USE_USER',     true);
define ('_PHPSITELIB_USE_PAGE',     true);
define ('_PHPSITELIB_USE_OPTIONS',  true);

//-----------------------------------------------------------------------------
// массив значений количества записей на странице
$LIMITS = array(10, 25, 50, 100, 'все');

// разрешения для загрузки изображений
$IMG_ALLOW = array('jpeg', 'jpg', 'gif', 'png');

// переменная, в которую записывается код javascript, который будет выполнен после загрузки страницы
$javascript = '';

// массив подгружаемых javascript-файлов
$scripts = array();

?>