<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Library for quick site development                             #
#                                                                              #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_options.inc.php                                                        #
#   Options. Constants and lists/ Geting values, printing, editing             #
#                                                                              #
################################################################################
/*

  string GetOption(string optionCode)                 - return value constante by it code
   array GetOptionList(string optionCode)             - return list by it code

     int GetErrno()                                   - last error (code)
     int GetError()                                   - last error (message)

*/

define ('E_PSL_OPTIONS_OK',       0);
define ('E_PSL_OPTIONS_CODE',     1);
define ('E_PSL_OPTIONS_NOTFOUND', 2);

class PslOptions {

    # Options
    var $mOptionsTbl      = 'Options';
    var $mOptionsId       = 'ID_Option';
    var $mOptionsParent   = 'ID_Parent';
    var $mOptionsCode     = 'OptionCode';
    var $mOptionsItemCode = 'OptionSubCode';
    var $mOptionsName     = 'OptionName';
    var $mOptionsValue    = 'OptionValue';
    var $mOptionsOrder    = 'OptionOrder';
    
    # public:
    var $mDb;                          // Object for access to DB
    
    # private (don't change it manually!):
    var $_Errno = E_PSL_OPTIONS_OK;
    var $_Fetched = false;
    var $_Options = array();
    var $_OptionsIds = array();
    
    function GetOption($optionCode) {
        $this->_FlushError();
        $this->_FetchOptions();
        $r = $this->GetOptionList($optionCode);
        if (is_array($r) && isset($r[0])) $r = $r[0];
        return $this->GetErrno() ? false : $r;
    }
    
    function GetOptionList($optionCode) {
        $this->_FlushError();
        $this->_FetchOptions();    
        $r = 0;
        if (!string_is_login($optionCode))
            $this->_Errno = E_PSL_OPTIONS_CODE;
        elseif (!isset($this->_OptionsIds[$optionCode]))
            $this->_Errno = E_PSL_OPTIONS_NOTFOUND;
        else
            $r = $this->_Options[$this->_OptionsIds[$optionCode]];
        return $this->GetErrno() ? false : $r;
    }
    
    function GetErrno() {
        return $this->_Errno;
    }
    
    function GetError() {
        global $lang_words;
        $e = array(E_PSL_OPTIONS_OK         => $lang_words['E_PSL_OPTIONS_OK'],
                   E_PSL_OPTIONS_CODE       => $lang_words['E_PSL_OPTIONS_CODE'],
                  );
        return in_array($this->_Errno, array_keys($e)) ? $e[$this->_Errno] : $lang_words['E_PSL_OPTIONS_UNKNOWN'];
    }
    
    # load all options
    function _FetchOptions() {
        $this->_CheckDb();
        $this->_FlushError();
        if (!$this->_Fetched) {
            $q = 'select * from ' . $this->mOptionsTbl . ' order by ' . $this->mOptionsParent . ', ' . $this->mOptionsOrder;
            $this->mDb->Query($q);
            $i = 0;
            while ($this->mDb->NextRecord()) {
               	$k = $this->mDb->F($this->mOptionsItemCode);
                // if ID_Parent is not set, add option, 
                // else this is child, add to child
                if (!isset($this->mDb->mRecord[$this->mOptionsParent]) || !$this->mDb->F($this->mOptionsParent)) {
                	if ($k == '') $k = 0;
                    $this->_OptionsIds[$this->mDb->F($this->mOptionsCode)] = $this->mDb->F($this->mOptionsId);
                    $this->_Options[$this->mDb->F($this->mOptionsId)] = array($k => $this->mDb->F($this->mOptionsValue));
                } else {
   	            	if ($k == '') $k = $i++;
                    $this->_Options[$this->mDb->F($this->mOptionsParent)][$k] = $this->mDb->F($this->mOptionsValue);
                }
            }
            
            $this->_Fetched = true;
        }
    }
    
    # checking turn on DB
    function _CheckDb() {
        global $lang_words;
        if (!isset($this->mDb)) die ($lang_words['E_PSL_OPTIONS_DB']);
    }
    
    # reset code of last error
    function _FlushError() {
        $this->_Errno = E_PSL_OPTIONS_OK;
    }
    
}

?>