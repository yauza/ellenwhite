<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Library for quick site development                             #
#                                                                              #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_page.inc.php                                                           #
#   Class for work with pages                                                  #
#                                                                              #
################################################################################
/*

    bool IsPageAccessible(string pageCode = '')       - accessable page
  
     int GetErrno()                                   - last error (code)
     int GetError()                                   - last error (message)

*/

define ("E_PSL_PAGE_OK",         0);
define ("E_PSL_PAGE_GRPID",      1);
define ("E_PSL_PAGE_RESCODE",    2);
define ("E_PSL_PAGE_GRPCODE",    3);
define ("E_PSL_PAGE_PAGECODE",   4);
define ("E_PSL_PAGE_MENUCODE",   5);

class PslPage {
 
    # options
    var $mThisPage      = '';                // code of current page
    //var $mLang       = LANG;    
    
    var $mPageTbl       = 'pages';        // pages. Table name
    var $mPageId        = 'ID';              // pages. ID
    var $mPageCode      = 'Code';            // pages. Code
    var $mPageName      = '';                // pages. Name
    var $mPagePath      = 'Href';            // pages. Path to page
    var $mPageLang      = 'Lang';            // pages. Lang
    
    var $mAccessTbl     = 'groups_access';// pages. Table name
    var $mAccessGroupId = 'Group_id';        // pages. ID
    var $mAccessPageId  = 'Page_id';         // pages. Code
    
    var $mGrpTbl        = 'groups';          // groups. Table name
    var $mGrpId         = 'id_group';        // groups. ID
    var $mGrpCode       = 'group_code';      // groups. Code
    
    # public:
    var $mDb;                          // Object for access to DB
    var $mUser;                        // Object for work with user PslUser
    
    # private (don't change it manually!):
    var $_Errno = E_PSL_PAGE_OK;
    //var $_Menu = array();
    //var $_MenuLoaded = false;
    
    function IsPageAccessible ($pageCode = '')
    {
		$this->_CheckDb();
		$this->_CheckUser();
        $this->_CheckPage();
        $this->_FlushError();
		
		$group_id = $this->mUser->GetGroupId();
		if ($pageCode == '') $pageCode = $this->mThisPage;
		
		if (!string_is_id($group_id))
		{
            $this->_Errno = E_PSL_PAGE_GRPID;
            return false;
        }
		else
		{
			$q = "SELECT p.$this->mPageCode FROM $this->mPageTbl p 
				  INNER JOIN $this->mAccessTbl AS a ON p.ID = a.$this->mAccessPageId 
				  WHERE a.$this->mAccessGroupId = $group_id AND p.$this->mPageCode = '$pageCode' AND p.$this->mPageLang = '" . LANG . "'";
			$this->mDb->Query($q);
			if ($this->mDb->NextRecord())
	            return true;
	        else
	            return false;
        }
	}

    function GetErrno() {
        return $this->_Errno;
    }
    
    function GetError() {
        global $lang_words;
        $e = array(E_PSL_PAGE_OK         => $lang_words['E_PSL_PAGE_OK'],
                   E_PSL_PAGE_GRPID      => $lang_words['E_PSL_PAGE__GRPID'],
                   E_PSL_PAGE_RESCODE    => $lang_words['E_PSL_PAGE_RESCODE'],
                   E_PSL_PAGE_GRPCODE    => $lang_words['E_PSL_PAGE_GRPCODE'],
                   E_PSL_PAGE_PAGECODE   => $lang_words['E_PSL_PAGE_PAGECODE'],
                   E_PSL_PAGE_MENUCODE   => $lang_words['E_PSL_PAGE_MENUCODE'],                  );
        return in_array($this->_Errno, array_keys($e)) ? $e[$this->_Errno] : $lang_words['E_PSL_PAGE_UNKNOWN'];
    }
    
    # checking turn on DB
    function _CheckDb() {
        global $lang_words;
        if (!isset($this->mDb)) die ('PslPage: ' . $lang_words['E_PSL_PAGE_DB']);
    }
    
    function _CheckUser() {
        global $lang_words;
        if (!isset($this->mUser)) die ('PslPage: ' . $lang_words['E_PSL_PAGE_USER']);
    }
    
    function _CheckPage() {
        global $lang_words;
        if (!string_is_login($this->mThisPage)) die ('PslPage: ' . $lang_words['E_PSL_PAGE_NOCODE']);
    }
    
    function _FlushError() {
        $this->_Errno = E_PSL_PAGE_OK;
    }
    
    function _GetGrpId($grpCode) {
        if (!string_is_login($grpCode)) {
            $this->_Errno = E_PSL_PAGE_GRPCODE;
            return false;
        } else {
            $q = 'SELECT ' . $this->mGrpId . ' FROM ' . $this->mGrpTbl . ' WHERE ' . $this->mGrpCode . ' = "' . $grpCode . '"';
            $this->mDb->Query($q);
            if ($this->mDb->NextRecord()) {
                return $this->mDb->F(0);
            } else {
                $this->_Errno = E_PSL_PAGE_GRPCODE;
                return false;
            }
        }
    }
    
    function _GetPageId($pageCode) {
        if (!string_is_login($pageCode)) {
            $this->_Errno = E_PSL_PAGE_PAGECODE;
            return false;
        } else {
            $q = 'SELECT ' . $this->mPageId . ' FROM ' . $this->mPageTbl . ' WHERE ' . $this->mPageCode . ' = "' . $pageCode . '"';
            $this->mDb->Query($q);
            if ($this->mDb->NextRecord()) {
                return $this->mDb->F(0);
            } else {
                $this->_Errno = E_PSL_PAGE_PAGECODE;
                return false;
            }
        }
    }
    
    /*
    function _GetMenuId($menuCode) {
        if (!string_is_login($menuCode)) {
            $this->_Errno = E_PSL_PAGE_MENUCODE;
            return false;
        } else {
            $q = 'select ' . $this->mMenuId . ' from ' . $this->mMenuTbl . ' where ' . $this->mMenuCode . ' = "' . $menuCode . '"';
            $this->mDb->Query($q);
            if ($this->mDb->NextRecord()) {
                return $this->mDb->F(0);
            } else {
                $this->_Errno = E_PSL_PAGE_MENUCODE;
                return false;
            }
        }
    }
    
    function _IsGrpResExists($grpCode, $resCode) {
        $grp_id = $this->_GetGrpId($grpCode);
        $res_id = $this->_GetResId($resCode);
        if (!$this->GetErrno()) {
            $q = 'select * from ' . $this->mGrpResTbl . ' where ' . $this->mGrpResIdGrp . ' = ' . $grp_id . 
                 ' and ' . $this->mGrpResIdRes . ' = ' . $res_id;
            $this->mDb->Query($q);
            if ($this->mDb->Nf()) $this->_Errno = E_PSL_PAGE_GRXSTS;
        }
        return $this->GetErrno() == E_PSL_PAGE_GRXSTS ? true : false;
    }
    
    function _IsPageResExists($pageCode, $resCode) {
        $page_id = $this->_GetPageId($pageCode);
        $res_id = $this->_GetResId($resCode);
        if (!$this->GetErrno()) {
            $q = 'select * from ' . $this->mPageResTbl . ' where ' . $this->mPageResIdPage . ' = ' . $page_id . 
                 ' and ' . $this->mPageResIdRes . ' = ' . $res_id;
            $this->mDb->Query($q);
            if ($this->mDb->Nf()) $this->_Errno = E_PSL_PAGE_PRXSTS;
        }
        return $this->GetErrno() == E_PSL_PAGE_PRXSTS ? true : false;
    }
    
    function _LoadMenu() {
    
        if (!$this->_MenuLoaded) {
        
            // get all menu from base and split up it by ID_Parent
            $q = 'select distinct m.*, p.' . $this->mPageCode . ', p.' . $this->mPagePath . ' from ' . $this->mMenuTbl . ' m' .
                 ' inner join ' . $this->mLang . $this->mPageTbl . ' p on p.' . $this->mPageId . ' = m.' . $this->mMenuPage .
                 ' inner join ' . $this->mPageResTbl . ' pr on pr.' . $this->mPageResIdPage . ' = p.' . $this->mPageId .
                 ' inner join ' . $this->mGrpResTbl . ' gr on gr.' . $this->mGrpResIdRes . ' = pr.' . $this->mPageResIdRes . 
                 ' and gr.' . $this->mGrpResIdGrp . ' = ' . $this->mUser->GetGroupId() .
                 ' order by m.' . $this->mMenuParent . ', m.' . $this->mMenuOrder;
            $this->mDb->Query($q);
            $menuArray = array();
            $codesArray = array('NULL' => 'NULL');
            while ($this->mDb->NextRecord()) {
                  $menuArray[$this->mDb->F($this->mMenuParent) ? $this->mDb->F($this->mMenuParent) : 'NULL'][] = $this->mDb->mRecord;
                  $codesArray[$this->mDb->F($this->mMenuCode)] = $this->mDb->F($this->mMenuId);
            }
            
            // load 
            $this->_Menu = $this->_LoadMenuLevel($menuArray, $codesArray);
            
            // get list of codes pages linked with founded menu points and later
            $this->_CollectPageCodes($this->_Menu, $a);
            $this->_MenuLoaded = true;
        }
        
    }
    
    function _LoadMenuLevel($menuArray, $codesArray, $parentCode = '') {
        $this->_CheckDb();
        $this->_FlushError();
        
        $r = array();
        if ($parentCode != '' && !$this->_GetMenuId($parentCode)) {
            $this->_Errno = E_PSL_PAGE_MENUCODE;
        } else {
            if (isset($codesArray[$parentCode ? $parentCode : 'NULL']) && 
                isset($menuArray[$codesArray[$parentCode ? $parentCode : 'NULL']]) && 
                is_array($menuArray[$codesArray[$parentCode ? $parentCode : 'NULL']])) {
                
                foreach ($menuArray[$codesArray[$parentCode ? $parentCode : 'NULL']] as $m) $r[] = $m;
                
            }
                
            foreach ($r as $k => $v) $r[$k]['_children'] = $this->_LoadMenuLevel($menuArray, $codesArray, $v[$this->mMenuCode]);
        }
        
        return $r;
    }
    
    # Recursive search on menu tree, search element by code,
    # return array of values, which childs for menu with code $parentCode
    function _FindItem($searchCode, $parentArray) {
        $item = false;
        if (is_array($parentArray) || string_is_login($searchCode))
            foreach ($parentArray as $k => $v)
                if (is_array($v)) {
                    if (isset($v[$this->mMenuCode]) && $v[$this->mMenuCode] == $searchCode)
                        $item = $v['_children'];
                    else
                        $item = $this->_FindItem($searchCode, $v['_children']);
                    if (is_array($item)) break;
                }
        return $item;
    }
    
    # Recursive round menu tree and saves codes all pages linked with menu points
    function _CollectPageCodes(&$menu, &$codesArray) {
        if (!is_array($codesArray)) $codesArray = array();
        if (!is_array($menu)) return $codesArray;
        foreach ($menu as $k => $v) {
            if (isset($v[$this->mPageCode]) && string_is_login($v[$this->mPageCode])) $menu[$k]['_codes'][] = $v[$this->mPageCode];
            $this->_CollectPageCodes($menu[$k]['_children'], $menu[$k]['_codes']);
            $codesArray = array_merge($codesArray, $menu[$k]['_codes']);
        }
    }*/
    
}
?>