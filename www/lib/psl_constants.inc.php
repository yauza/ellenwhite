<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Quick site development                                         #
#                                                                              #
#   Copyright (с) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_constants.inc.php                                                      #
#                                                                              #
#                                                                              #
################################################################################
                          

################################################################################
# Languages
$LANGS = array('ru' => 'Русский');
$ALL_LANGS = $LANGS + array('opt' => 'Настройки');
define ('DEFAULT_LANG', 'ru');

// путь до папки с шаблонами
define ('PATH_TO_TEMPLATES', PATH_TO_ROOT . 'temp_' . (defined('IN_ADMIN') ? 'admin' : 'front') . '/' . (array_key_exists(LANG, $ALL_LANGS) ? (LANG . '/') : ''));

// путь к папке с изображениями
define ('PATH_TO_IMG', PATH_TO_ROOT . 'img/' . (array_key_exists(LANG, $LANGS) ? (LANG . '/') : ''));

# Set default timezone. Generate E_STRICT message if using the system settings or the TZ environment variable. in php > 5.0
if (phpversion() >= '5.0.0')
	date_default_timezone_set($g_user->GetTimezone() ? $g_user->GetTimezone() : 'Europe/Moscow');

?>