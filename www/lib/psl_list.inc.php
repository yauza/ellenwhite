<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Library for quick site development                             #
#                                                                              #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_list.inc.php                                                           #
#   Class for work with list                                                   #
#                                                                              #
################################################################################

class PslList
{
    var $file = 'list.html';          // файл шаблона списка записей
    
    var $code = '';                   // Код страницы
    var $headTitle = 'Список';        // Верхний заголовок
    
    var $backTitle = '';              // Текст ссылки для возврата на предыдущую страницу
    var $backLink = '';               // Адрес ссылки для возврата на предыдущую страницу
    var $backImage = 'back2.gif';     // Картинка для возврата на предыдущую страницу
    
    var $actions = array();           // Ссылки-действия в правом верхнем углу
                                      // array('название' => 'ссылка', 'название' => 'ссылка', ...)
    
    var $addTitle = '';               // Текст ссылки для добавления записи
    var $addLink = '?a=add&id=-1';    // Адрес ссылки для добавления записи (путь от текущей страницы)
    var $addHide = false;             // Условие, при котором не показывается ссылка
    
    var $table = '';                  // Таблица из которой берутся записи для списка
    var $field_id = 'ID';             // Название уникального поля
    var $where = '';                  // Условие для вывода записей
    var $groupBy = '';                // Поле, по которому группируются записи
    var $orderBy = '';                // Поле, по которому сортируются записи (если OrderBy, то сортировка в списке)
    
    var $search = array();            // Данные для формы поиска
    
    /*  Пример $search:
                                                 x - обязательно должно быть какое-то значение
        array(
            'Title' => array(                    x   // поле из таблицы БД ($table), по которому будет поиск
                'title'   => 'Название',             // текст непосредственно перед полем
                                                     // (по умолчанию = '')
                't_width' => 100,                    // ширина области (в px) для текста перед полем
                                                     // (по умолчанию = ширине введенного текста)
                'type'    => 'string',           x   // тип поля - текстовое поле
                'width'   => 320,                    // ширина поля (в px) в блоке поиска на странице
                                                     // (по умолчанию = стандартная шинина <input>)
                'length'  => 32,                     // максимально допустимое кол-во символов в поле
                                                     // (по умолчанию = без ограничения)
                'focus'   => true,                   // если true, то авто-фокус на данное поле при загрузке страницы
                                                     // (по умолчанию = false)
                'auto'    => true                    // если true, то включается авто-заполнение
                                                     // (по умолчанию = false)
            ),
            'Category_id' => array(              x
                'title'   => 'Категория',
                't_width' => 100,
                'type'    => 'select',           x   // тип поля - выпадающий список
                'table'   => 'categories',       x   // таблица БД откуда берутся значения для списка
                'parent'  => 1                       // id для двухуровнего селекта с optiongroup
                'field'   => 'Title',            x   // поле из таблицы для отображения записей
                'orderBy' => '',                     // поле из таблицы для сортировки записей при отображении
                                                     // (по умолчанию = значение из 'field')
                'width'   => 150,
                'focus'   => true
            ),
            'Date' => array(                     x
                'title'   => 'Дата',
                't_width' => 100,
                'type'    => 'date',             x   // тип поля - дата
                'is_time' => false,                  // true - DD.MM.YYYY HH:II   false - DD.MM.YYYY
                                                     // (по умолчанию = false)
                'focus'   => true
            ),
            'Date' => array(                     x   // поле соответствующее начальному значению даты
                'title'   => 'Дата',
                't_width' => 100,
                'type'    => 'date-interval',    x   // тип поля - интервал времени
                'code'    => 'Date_end',         x   // поле соответствующее конечному значению даты 
                'is_time' => false,
                'focus'   => true
            ),
            'Date' => array(                     x   // поле из которого считываются все возможные месяцы
                'title'   => 'Месяц',
                't_width' => 100,
                'type'    => 'months',           x   // тип поля - месяцы
                'focus'   => true
            ),
            'Hide' => array(
                'title'   => 'Заблокировать',
                't_width' => 100,
                'type'    => 'bool',             x  // тип поля - булево значение
                'focus'   => true
            ),
            'Author_ip' => array(                x
                'title'   => 'IP',
                't_width' => 100,
                'type'    => 'ip',               x   // тип поля - ip адрес
                'width'   => 100,
                'length'  => 15,
                'focus'   => true
            )
        )
    */
    
    var $columns = array();        // Данные о выводимых столбцах
    
    /*  Пример $columns:
                                                     x - обязательно должно быть какое-то значение
        array(,
            'Title' => array(                        x   // поле из таблицы БД ($table), из которого берется значение
                'head'        => 'Название',         x   // название столбца
                'type'        => 'string',           x   // тип выводимого значения - текстовое поле
                'max_letters' => 35,                     // обрезать выводимое значение до N букв
                                                         // (по умолчанию = не обрезается)
                'max_words'   => 5,                      // обрезать выводимое значение до N слов
                                                         // (по умолчанию = не обрезается)
                'link'        => true,                   // ссылка с выводимого значения (путь от текущей страницы)
                                                         // если true, то  '?page=' . $page . '&a=edit&id=[ID]'
                                                         // (по умолчанию = false, т.е. сыслки нет)
                'before'      => '',                     // текст, добавляемый до выводимого значения
                                                         // (по умолчанию = '')
                'after'       => ' ([Cost])',            // текст, добавляемый после выводимого значения
                                                         // блоки [..] заменяются на значения из соответствующих полей
                                                         // (по умолчанию = '')
                'style'       => 'width:300px;',         // стили для выводимого значения
                                                         // (по умолчанию = '')
                'inner'       => array(                  // значение в скобках после выводимого значения
                    'code' => 'Author'               x   // поле из таблицы БД ($table), из которого берется значение
                    'type' => ''                     x
                )
            )
            'Date' => array(                         x
                'head'        => 'Дата',
                'type'        => 'date',             x   // тип выводимого значения - дата (DD.MM.YYYY)
                'link'        => true,
                'before'      => '',
                'after'       => '',
                'style'       => '',
                'inner'       => array(
                    'code' => ''
                    'type' => '' 
                )
            ),
            'Date' => array(                         x
                'head'        => 'Дата',
                'type'        => 'datetime',         x   // тип выводимого значения - дата (DD.MM.YYYY HH:II)
                'link'        => true,
                'before'      => '',
                'after'       => '',
                'style'       => '',
                'inner'       => array(
                    'code' => ''
                    'type' => '' 
                )
            ),
            'IP' => array(                           x
                'head'        => 'IP',
                'type'        => 'ip',               x   // тип выводимого значения - ip (XXX.XXX.XXX.XXX)
                'link'        => true,
                'before'      => '',
                'after'       => '',
                'style'       => '',
                'inner'       => array(
                    'code' => ''
                    'type' => '' 
                )
            ),
            'Position_id' => array(                  x   // поле из таблицы 'table', по которому идет связь таблиц
                'head'        => 'Кол-во позиций',
                'type'        => 'count',            x   // тип выводимого значения - кол-во связанных значений
                'title'       => 'всего: [count]',   x   // выводимое значение (вместо [count] подставляется кол-во)
                'table'       => 'banners',          x   // таблица БД, из которой берутся связанные значения
                'link'        => true,
                'style'       => ''
            ),
            'Poll_id' => array(                      x   // поле из таблицы 'table', по которому идет связь таблиц
                'head'        => 'Сумма голосов',
                'type'        => 'count',            x   // тип выводимого значения - сумма связанных значений
                'title'       => 'всего: [sum]',     x   // выводимое значение (вместо [sum] подставляется сумма)
                'table'       => 'answers',          x   // таблица БД, из которой берутся связанные значения
                'field_sum'   => 'Count',            x   // поле, значения которого, суммируются
                'link'        => true,
                'style'       => ''
            ),
            'Image' => array(                        x   // всегда должно быть 'Image'
                'type'        => 'img',              x   // тип выводимого значения - всплывающее изображение
                'src'         => '[ID]'              x   // название файлов изображения (без расширения)
                'before'      => '',
                'after'       => '',
                'style'       => ''
            )
        )
    */
    
    var $r_icon = array();         // Данные о выводимых иконках
    
    /* Пример $r_icon:
                                                  x - обязательно должно быть какое-то значение
       array(
            'Hide' => array(                      x   // поле из таблицы БД ($table), которое будет изменяться
                                                      // иконка для смены значения поля (0/1)
                'image'   => 'eye.gif',           x   // изображение иконки 1
                'image2'  => 'eyesleep.gif',      x   // изображение икноки 2
                'title'   => 'показывать',            // текст, показываемый при навережии мышки на иконку 1
                                                      // (по умолчанию = '')
                'title2'  => 'скрыть'                 // текст, показываемый при навережии мышки на иконку 2
                                                      // (по умолчанию = '')
            ),
            'delete' => array(                    x   // иконка для удаления записей
                'image'   => 'delete.gif',            // изображение иконки (по умолчанию = delete2.gif)
                'title'   => 'удалить',               // текст, показываемый при навережии мышки на иконку
                                                      // (по умолчанию = 'удалить')
                'message' => 'Удалить [Title]?',      // текст подтверждающего сообщения при попытке удаления
                                                      // блоки [..] заменяются на значения из соответствующих полей
                                                      // (по умолчанию = 'Действительно удалить?')
                'hide'    => '[ID] == 1'              // условие, при котором иконка не показывается
                                                      // блоки [..] заменяются на значения из соответствующих полей
                                                      // (по умолчанию = false, т.е. показывается всегда)
            ),
            'delete_and' => array(                x   // иконка для удаления записей и ... (напр., см. раздел Гостевая)
                'image'   => 'delete.gif',
                'title'   => 'удалить',
                'message' => 'Удалить [Title]?',
                'hide'    => '[ID] == 1'
            )
        )
    */
    
    // данные для блока со списком страниц
    var $limits = array(10, 25, 50, 100, 999999999);
    var $pages_title = 'выводить по';
    var $pages_next = 'следующая >';
    var $pages_last = '< предыдущая';
    var $pages_sep = ' | ';        // разделитель между номерами страниц
    var $pages_count = '9';        // количество видимых ссылок на страницы до и после текущей
    
    # public:
    var $db;     // объект для доступа к классу БД
    var $tpl;    // объект для доступа к классу шаблонов
    var $user;   // объект для доступа к классу текущего пользователя
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function setParam($params, $value = '')
    {
        if (is_array($params))
            foreach($params AS $name => $val)
                $this->$name = $val;
        else   
            $this->$params = $value;
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function getHTML()
    {
        $this->_CheckObj();
        $this->_CheckParam();
        global $javascript, $page;
        
        // сохранение текущего пути
        setcookie($this->code . '_query_string', $_SERVER['QUERY_STRING'], time() + 31536000, '/', '.' . $_SERVER['HTTP_HOST']);
        $_SESSION[$this->code . '_query_string'] = $_SERVER['QUERY_STRING'];
        
        $this->tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/' . $this->file);
        //------------------------------------------------------------------
        $this->tpl->set_var(array(
            'THIS_PAGE'   => THIS_PAGE,
    		'HEAD_TITLE'  => htmlspecialchars($this->headTitle),
            'BACK_TITLE'  => htmlspecialchars($this->backTitle),
            'BACK_LINK'   => htmlspecialchars($this->backLink),
            'BACK_IMAGE'  => 'img/' . $this->backImage
    	));
        //------------------------------------------------------------------
        // вывод ссылок-действий
        $this->tpl->set_block('main', 'actions', 'actions_');
        foreach ($this->actions AS $title => $link)
        {
            if (!empty($link))
            {
                $this->tpl->set_var(array(
            		'ACT_TITLE' => htmlspecialchars($title),
                    'ACT_LINK'  => htmlspecialchars($link)
            	));
                $this->tpl->parse('actions_', 'actions', true);
            }
        }
        //------------------------------------------------------------------
        // вывод формы поиска
        $this->tpl->set_block('main', 'search', 'search_');
        $this->tpl->set_block('search', 'field', 'field_');
        $this->tpl->set_block('field', 'string', 'string_');
        $this->tpl->set_block('field', 'date', 'date_');
        $this->tpl->set_block('field', 'months', 'months_');
        $this->tpl->set_block('field', 'select', 'select_');
        $this->tpl->set_block('field', 'bool', 'bool_');
        $f = array();
        foreach ($this->search AS $code => $field)
        {
            if ($field['type'] == false) continue;
            $this->tpl->set_var(array(
                'string_'           => '',
                'date_'             => '',
                'select_'           => '',
                'bool_'             => '',
                'FIELD_TITLE'       => !empty($field['title']) ? htmlspecialchars($field['title']) : '',
                'FIELD_TITLE_CLASS' => !empty($field['t_width']) ? (' class="w' . $field['t_width'] . '"') : ''
            ));
            switch ($field['type'])
            {
                case 'ip':
                case 'string':
                {
                    $this->tpl->set_var(array(
                		'FIELD_CLASS'  => !empty($field['width']) ? (' class="w' . $field['width'] . '"') : '',
                        'FIELD_CODE'   => $code,
                        'FIELD_LENGTH' => !empty($field['length']) ? intval($field['length']) : '',
                        'FIELD_VALUE'  => !empty($_GET['form'][$code]) ? htmlspecialchars($_GET['form'][$code]) : '',
                        'FIELD_TABLE'  => !empty($field['auto']) ? $this->table : ''
                	));
                    $this->tpl->parse('string_', 'string', false);
                } break;
                case 'date':
                case 'date_interval':
                {
                    $this->tpl->set_var('TYPE_DATE', get_field_date(
                        /*field*/$code,
                        /*value*/!empty($_GET['form'][$code]) ? htmlspecialchars($_GET['form'][$code]) : '',
                        /*is required*/false,
                        /*is_time*/!empty($field['is_time']) ? true : false
                    ));
                    $this->tpl->parse('date_', 'date', false);
                } break;
                case 'select':
                case 'select_adv':
                {
                    $this->tpl->set_var('TYPE_SELECT', get_field_select(
                        /*form*/!empty($_GET['form']) ? $_GET['form'] : array(),
                        /*field*/$code,
                        /*table*/$field['table'],
                        /*field for title*/$field['field'],
                        /*parent*/!empty($field['parent']) ? intval($field['parent']) : 0,
                        /*where*/!empty($field['where']) ? $field['where'] : '',
                        /*field for orderBy*/!empty($field['orderBy']) ? $field['orderBy'] : $field['field'],
                        /*width*/!empty($field['width']) ? intval($field['width']) : '',
                        /*is required*/false
                    ));
                    $this->tpl->parse('select_', 'select', false);
                } break;
                case 'bool':
                {
                    $this->tpl->set_var(array(
                        'FIELD_CODE' => $code,
                        'FIELD'      => empty($_GET['form'][$code]) ? '' : ' checked'
                	));
                    $this->tpl->parse('bool_', 'bool', false);
                } break;
                case 'months':
                {
                    $this->tpl->set_var('TYPE_MONTHS', get_field_month(
                        /*form*/!empty($_GET['form']) ? $_GET['form'] : array(),
                        /*field*/$code,
                        /*table*/$this->table,
                        /*is required*/false
                    ));
                    $this->tpl->parse('months_', 'months', false);
                } break;
            }
            
            // получаем условие из формы поиска
            if (!empty($_GET['form'][$code]))
            {
                switch ($field['type'])
                {
                    case 'string':
                    case 'months':
                    {
                        $f[] = $this->getField($code) . ' LIKE "%' . addslashes(trim($_GET['form'][$code])) . '%"';
                    } break;
                    case 'int':
                    case 'select':
                    {
                        $f[] = $this->getField($code) . ' = ' . intval($_GET['form'][$code]);
                    } break;
                    case 'select_adv':
                    {
                        $f[] = $this->getField($code) . ' = ' . intval($_GET['form'][$code]);
                    } break;
                    case 'bool':
                    {
                        $f[] = $this->getField($code) . ' = 1';
                    } break;
                    case 'ip':
                    {
                        $f[] = $this->getField($code) . ' = "' . sprintf("%u", ip2long($_GET['form'][$code])) . '"';
                    } break;
                    case 'date':
                    {
                        $f[] = $this->getField($code) . ' LIKE "' . date_to_sql(trim($_GET['form'][$code])) . '%"';
                    } break;
                    case 'date_interval':
                    {
                        $f[] = $this->getField($code) . ' <= "' . date_to_sql(trim($_GET['form'][$code])) . '" AND ' . $field['code'] . ' >= "' . date_to_sql(trim($_GET['form'][$code])) . '"';
                    } break;
                }
            }
            
            // устанавливаем фокус для элемента формы
            if (!empty($field['focus']))
                $javascript .= "document.forms[0].elements['form[$code]'].focus();";
            
            $this->tpl->parse('field_', 'field', true);
        }
            
        if (count($f) > 0)
            $this->where .= (strpos($this->where, 'WHERE') ? ' AND ' : ' WHERE ') . implode(' AND ', $f);
        
        $this->tpl->parse('search_', 'search', false);
        //------------------------------------------------------------------
        // вывод ссылки добавления записи
        $this->tpl->set_block('main', 'add', 'add_');
        if (!empty($this->addTitle) && (empty($this->addHide) || !eval('return ' . $this->addHide . ' ? true : false;')))
        {
            $this->tpl->set_var(array(
        		'ADD_TITLE' => htmlspecialchars($this->addTitle),
                'ADD_LINK'  => THIS_PAGE . htmlspecialchars($this->addLink)
        	));
            $this->tpl->parse('add_', 'add', false);
        }
        //------------------------------------------------------------------
        // вывод блока со списком страниц
        $this->tpl->set_block('main', 'pages', 'pages_');
        $this->tpl->set_var('PAGES_TITLE', htmlspecialchars($this->pages_title));
        
        $count_on_page = $this->getCountOnPage(); // кол-во записей на странице
        
        $this->db->Query('SELECT COUNT(DISTINCT r.' . $this->field_id . ') AS cnt FROM ' . $this->table . ' AS r' . $this->where);
        $count =  $this->db->NextRecord() ? $this->db->F('cnt') : 0; // общее кол-во записей
        
        $this->tpl->set_var('COUNT_RECORDS', $count);
        
    	if ($count > $this->limits[0])
    	{
            /*switch ($this->code)
            {
        		case 'pages': $this->tpl->set_var('THIS_PAGE', ADM . '/' . $this->code . '/'); break;
        		default: $this->tpl->set_var('THIS_PAGE', LANG . '/' . $this->code . '/');
        	}*/
            
            $this->tpl->set_block('pages', 'change_info', 'change_info_');
            $this->tpl->set_block('change_info', 'limit', 'limit_');
            $this->tpl->set_block('pages', 'page', 'page_');
            $this->tpl->set_block('page', 'page_active', 'page_active_');
            $this->tpl->set_block('page', 'page_active_no', 'page_active_no_');
            
            $this->tpl->set_var('QUERY_STRING', $this->getQueryString());
            $this->tpl->set_var('QUERY_STRING_NO_LIMIT', $this->getQueryString(true));
    		
    		foreach ($this->limits AS $v)
    		{
    			$this->tpl->set_var(array(
    				'LIMIT_TITLE' => $v == 999999999 ? 'все' : $v,
    				'LIMIT_VALUE' => $v,
    				'LIMIT'       => $count_on_page == $v ? ' selected' : ''
    			));
    			$this->tpl->parse('limit_', 'limit', true);
    		}	
    		$this->tpl->parse('change_info_', 'change_info', false);
            
            $count_pages = ceil($count / $count_on_page); // количество страниц
            
            // получение блока для перемещения на страницу назад
            if ($page > 1)
            {
        		$this->tpl->set_var(array(
        			'PAGE_NUM_TITLE' => htmlspecialchars($this->pages_last),
        			'PAGE_NUM_LINK'  => $page - 1,
        			'SEPARATOR'      => htmlspecialchars($this->pages_sep)
                ));
        		$this->tpl->parse('page_active_no_', 'page_active_no', false);
        		$this->tpl->parse('page_', 'page', true);
        	}
        	//получение блока с активной страницей
        	if ($count_pages > 1)
        	for ($i = $page - intval($this->pages_count); $i <= $page + intval($this->pages_count); $i++)
            {
        		if ($i > 0 && $i <= $count_pages)
        		{
        			$this->tpl->set_var(array(
        				'page_active_'    => '',
        				'page_active_no_' => '',
        				'PAGE_NUM_TITLE'  => $i,
        				'PAGE_NUM_LINK'   => $i,
        				'SEPARATOR'       => htmlspecialchars($this->pages_sep)
                	));
                	if ($i == $page) $this->tpl->parse('page_active_', 'page_active', false);
                	else $this->tpl->parse('page_active_no_', 'page_active_no', false);
                	$this->tpl->parse('page_', 'page', true);
        		}
        	}
            // получение блока для перемещения на страницу вперед
            if ($page < $count_pages)
            {
        		$this->tpl->set_var(array(
        			'PAGE_NUM_TITLE' => htmlspecialchars($this->pages_next),
        			'PAGE_NUM_LINK'  => $page + 1,
        			'SEPARATOR'      => ''
                ));
                $this->tpl->parse('page_active_no_', 'page_active_no', false);
        		$this->tpl->parse('page_', 'page', true);
        	}
            $this->tpl->parse('pages_', 'pages', false);
    	}
        //------------------------------------------------------------------
        // вывод записей
        $this->tpl->set_block('main', 'record', 'record_');
        $this->tpl->set_block('main', 'column_title', 'column_title_');
        $this->tpl->set_block('record', 'column', 'column_');
        $this->tpl->set_block('record', 'right_icon', 'right_icon_');
        $this->tpl->set_block('right_icon', 'bool', 'bool_');
        $this->tpl->set_block('right_icon', 'delete', 'delete_');
        $this->tpl->set_block('right_icon', 'none', 'none_');
        
        $this->tpl->set_var('IS_SORT', $this->orderBy == 'OrderBy' ? (' id="sort_lines" table="' . $this->table . '"') : '');
        
        // выводим заголовки столбцов
        $request_uri = str_replace('/' . _ADMIN . '/', '', $_SERVER['REQUEST_URI']);
        foreach ($this->columns AS $code => $column)
        {
            if (isset($_GET['sort'], $_GET['order']) && $_GET['sort'] == $code)
                $link = preg_replace('/order\=[a-z_]+/i', 'order=' . ($_GET['order'] == 'asc' ? 'desc' : 'asc'), mb_substr($request_uri, 0, 999, 'utf-8'));
            elseif (isset($_GET['sort'], $_GET['order']) && $_GET['sort'] != $code)
            {
                $link = preg_replace('/sort\=[a-z_\.]+/i', 'sort=' . $code, mb_substr($request_uri, 0, 999, 'utf-8'));
                $link = preg_replace('/order\=[a-z_]+/i', 'order=asc', mb_substr($link, 0, 999, 'utf-8'));
            }
            else
                $link = $request_uri . (strpos($request_uri, '?') !== false ? '&' : '?') . 'sort=' . $code . '&order=asc';
            preg_match('/width\:([0-9]+)px\;/i', $column['style'], $width);
            $this->tpl->set_var(array(
                'COLUMN_STYLE' => !empty($width[1]) ? (' style="width:' . $width[1] . 'px;"') : '',
                'COLUMN_TITLE' => empty($column['head']) ? '' : (empty($column['is_sort']) ? $column['head'] : ('<a href="' . $link . '">' . $column['head'] . '</a>' . (isset($_GET['sort'], $_GET['order']) && $_GET['sort'] == $code ? ($_GET['order'] == 'asc' ? ' &dArr;' : ' &uArr;') : '')))
        	));
            $this->tpl->parse('column_title_', 'column_title', true);
        }
        // ---
        
        // получаем массив полей для запроса, необходимых для корректной работы
        $fields = array($this->getField($this->field_id));
        foreach ($this->columns AS $code => $column)
        {
            if ($column['type'] == 'count' || $column['type'] == 'sum-count' || $column['type'] == 'img') continue;
            
            $fields[] = $this->getField($code, true);
            
            if ($column['type'] == 'date-interval' && !empty($column['code']))
                $fields[] = $this->getField($column['code']);
                
            if (!empty($column['inner']['code']))
                $fields[] = $this->getField($column['inner']['code']);
            
            if (!empty($column['after']) && preg_match_all('/\[([0-9a-z\.]+)\]/ie', str_replace('_', '.', $column['after']), $fld))
            {
                foreach ($fld[1] AS $f)
                    $fields[] = $this->getField($f, true);
            }
                
        }
        foreach ($this->r_icon AS $code => $icon)
            if ($code != 'delete' && $code != 'delete_and')
                $fields[] = $this->getField($code);
        // ---
        
        $this->db->Query('SELECT ' . implode(', ', $fields) . ' 
                          FROM ' . $this->table . ' AS r' . $this->where . 
                          ($this->groupBy ? (' GROUP BY ' . $this->getField($this->groupBy)) : '') . ' 
                          ORDER BY ' . (isset($_GET['sort'], $_GET['order']) ? ($_GET['sort'] . ' ' . $_GET['order']) : $this->getField($this->orderBy)) . ' 
                          LIMIT ' . (($page - 1) * $count_on_page) . ', ' . $count_on_page);
        while ($this->db->NextRecord())
        {
            $r = $this->db->mRecord;
        	$this->tpl->set_var(array(
                'column_'     => '',
                'right_icon_' => '',
        		'ID'          => $r[$this->field_id],
                'IS_CURSOR'   => $this->orderBy == 'OrderBy' ? ' style="cursor:move;"' : ''
        	));
            
            // выводим столбцы с информацией о записи
            foreach ($this->columns AS $code => $column)
            {
                if ($column['type'] == 'img')
                {
                    $image = is_image(PATH_TO_IMG . $this->code . '/' . str_replace('[ID]', $r[$this->field_id], $column['src']));
                    if ($image)
                    {
                        $image_size = getimagesize($image);
            	    	$image_info = pathinfo($image);
                        
                        $title = $image ? ('<div class="popup_image">' . str_replace('[ID]', $r[$this->field_id], $column['src']) . '.' . $image_info['extension'] . '<div><img src="' . $image . '" ' . $image_size[3] . ' alt="свернуть" title="свернуть"><br>' . round(filesize($image) / 1024) . ' Кб.</div></div>') : '';
                    }
                    else
                        $title = '';
                }
                elseif ($column['type'] == 'count')
                {
                    if (!isset($db_temp)) $db_temp = new PslDb;
                    $db_temp->Query('SELECT COUNT(*) AS cnt 
                                     FROM ' . $column['table'] . ' 
                                     WHERE ' . $code . ' = ' . $r[$this->field_id] . ($this->code == 'groups' ? ' AND ID != 1' : ''));
                    if (isset($column["href"]) && $column["href"] === false)
                        $title = preg_replace('/\[count\]/ie', '$db_temp->NextRecord() ? $db_temp->F("cnt") : 0', $column['title']);
                    elseif (!isset($column["href"]))
                        $title = preg_replace('/\[count\]/ie', '$db_temp->NextRecord() ? ("<a href=\"" . LANG . "/" . str_replace(LANG . "_", "", $column["table"]) . "/?form[" . $code . "]=" . $r[$this->field_id] . "\">" . $db_temp->F("cnt") . "</a>") : 0', $column['title']);
                    else
                        $title = preg_replace('/\[count\]/ie', '$db_temp->NextRecord() ? ("<a href=\"" . $column["href"] . $r[$this->field_id] . "\">" . $db_temp->F("cnt") . "</a>") : 0', $column['title']);
                    
                    if (empty($r['is_cnt'])) // для скрытия иконки удаления
                        $r['is_cnt'] = $db_temp->F('cnt');
                }
                elseif ($column['type'] == 'sum-count')
                {
                    if (!isset($db_temp)) $db_temp = new PslDb;
                    $db_temp->Query('SELECT SUM(' . $column['field_sum'] . ') AS sum 
                                     FROM ' . $column['table'] . ' 
                                     WHERE ' . $code . ' = ' . $r[$this->field_id]);
                    $title = preg_replace('/\[sum\]/ie', '$db_temp->NextRecord() ? $db_temp->F("sum") : 0', $column['title']);
                }
                elseif ($column['type'] != 'count' && $column['type'] != 'sum-count')
                {
                    $code = str_replace('.', '_', $code);
                    if ($column['type'] == 'string' && !empty($column['max_letters']) && !empty($column['max_words']))
                        $title = get_title_formated($r[$code], intval($column['max_words']), intval($column['max_letters']));
                    else
                        $title = $this->getModifiedValue($column['type'], $r[$code]);
                }
                    
                if (!empty($column['inner']['code']) && !empty($column['inner']['type']))
                    $title .= ' (' . $this->getModifiedValue($column['inner']['type'], $r[$column['inner']['code']]) . ')';
                if (!empty($column['link']) && $column['link'] === true) // ссылка по умолчанию
                    $column_title = '<a href="' . THIS_PAGE . '?page=' . $page . '&amp;a=edit&amp;id=' . $r[$this->field_id] . '">' . $title . '</a>';
                elseif (!empty($column['link'])) // введенная ссылка
                    $column_title = !empty($r[$code]) ? ((!empty($column['link']) ? '<a href="' . THIS_PAGE . htmlspecialchars(preg_replace('/\[([a-z]+)\]/ie', '$r["$1"]', $column['link'])) . '">' : '') . $title . (!empty($column['link']) ? '</a>' : '')) : '';
                else // нет ссылки
                    $column_title = $title;
                
                $this->tpl->set_var('COLUMN_TITLE', (!empty($column['before']) ? $column['before'] : '') . $column_title . (!empty($column['after']) ? preg_replace('/\[([0-9a-z_]+)\]/ie', '$r["$1"]', $column['after']) : ''));
                $this->tpl->set_var('COLUMN_STYLE', !empty($column['style']) ? (' style="' . $column['style'] . '"') : '');
                $this->tpl->parse('column_', 'column', true);
            }
            
            // выводим иконки в правых столбцах списка
            foreach (array_reverse($this->r_icon, true) AS $code => $icon)
            {
                $this->tpl->set_var(array('bool_' => '', 'delete_' => '', 'none_' => ''));
                switch ($code)
                {
                    case 'delete':
                	case 'delete_and':
                	{
                       if (empty($r['is_cnt']) && (empty($icon['hide']) || !eval(preg_replace('/\[([a-z_]+)\]/i', '$r["$1"]', 'return ' . $icon['hide'] . ' ? true : false;'))))
                       {
                            $this->tpl->set_var(array(
                                'ICON_IMAGE'   => 'img/' . (!empty($icon['image']) ? $icon['image'] : 'delete2.gif'),
                        		'ICON_TITLE'   => !empty($icon['title']) ? htmlspecialchars($icon['title']) : 'удалить',
                                'ICON_ATTR_ID' => $code == 'delete_and' ? (' _id="-' . $r[$this->field_id] . '"') : '',
                                'ICON_MESSAGE' => !empty($icon['message']) ? preg_replace('/\[([a-z_]+)\]/ie', '$r["$1"]', $icon['message']) : 'Действительно удалить?',
                        	));
                            $this->tpl->parse('delete_', 'delete', false);
                       }
                       else
                            $this->tpl->parse('none_', 'none', false);
                	} break;
                	default:
                	{
                	    if (empty($icon['hide']) || !eval(preg_replace('/\[([a-z]+)\]/i', '$r["$1"]', 'return ' . $icon['hide'] . ' ? true : false;')))
                        {
                            $this->tpl->set_var(array(
                                'ICON_IMAGE'  => 'img/' . ($r[$code] ? $icon['image2'] : $icon['image']),
                                'ICON_IMAGE2' => 'img/' . (!$r[$code] ? $icon['image2'] : $icon['image']),
                                'ICON_FIELD'  => $code,
                        		'ICON_TITLE'  => !empty($icon['title']) && !empty($icon['title2']) ? ($r[$code] ? htmlspecialchars($icon['title']) : htmlspecialchars($icon['title2'])) : '',
                                'ICON_TITLE2' => !empty($icon['title']) && !empty($icon['title2']) ? (!$r[$code] ? htmlspecialchars($icon['title']) : htmlspecialchars($icon['title2'])) : ''
                        	));
                            $this->tpl->parse('bool_', 'bool', false);
                        }
                        else
                            $this->tpl->parse('none_', 'none', false);
                	}
                }
                $this->tpl->parse('right_icon_', 'right_icon', true);
            }
            $this->tpl->parse('record_', 'record', true);
        }
        //------------------------------------------------------------------
        return $this->tpl->parse('_', 'main', true);
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function getCountOnPage()
    {
    	if (isset($_GET['limit']) && in_array($_GET['limit'], $this->limits))
    	{
    		setcookie($this->code . '_count_on_page', intval($_GET['limit']), time() + 31536000, '/', '.' . $_SERVER['HTTP_HOST']);
            $_SESSION[$this->code . '_count_on_page'] = intval($_GET['limit']);
    		return intval($_GET['limit']);
    	}
    	elseif (isset($_COOKIE[$this->code . '_count_on_page']) && in_array($_COOKIE[$this->code . '_count_on_page'], $this->limits))
    		return intval($_COOKIE[$this->code . '_count_on_page']);
    	elseif (isset($_SESSION[$this->code . '_count_on_page']) && in_array($_SESSION[$this->code . '_count_on_page'], $this->limits))
    		return intval($_SESSION[$this->code . '_count_on_page']);
    	else
    		return $this->limits[0];
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function getQueryString($no_limit = false)
    {
        $query_str = $_SERVER['QUERY_STRING'];
        $var = $no_limit ? 'limit' : 'page';
        
        if (isset($_GET[$var]))
        {
    		$query_str = str_replace($var . '=' . $_GET[$var] . '&', '', $_SERVER['QUERY_STRING']);
    		if ($query_str == $_SERVER['QUERY_STRING'])
    		$query_str = str_replace('&' . $var . '=' . $_GET[$var], '', $_SERVER['QUERY_STRING']);
    		if ($query_str == $_SERVER['QUERY_STRING'])
    		$query_str = str_replace($var . '=' . $_GET[$var], '', $_SERVER['QUERY_STRING']);
    	}
    	if (trim($query_str) != '') $query_str .= '&';
        
        if ($this->code == 'pages' && !strpos(' ' . $query_str, 'lang'))
            $query_str .= 'lang=' . (isset($_GET['lang']) && preg_match('/^[a-z_]+$/i', $_GET['lang']) ? $_GET['lang'] : 'ru') . '&';
        
        return str_replace('&', '&amp;', $query_str);
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function getModifiedValue($type, $value)
    {
        switch ($type)
        {
        	case 'string': return htmlspecialchars($value);
        	case 'date': return sql_to_date($value) ? sql_to_date($value) : date('d.m.Y', $value);
        	case 'datetime': return sql_to_datetime_nosec($value) ? sql_to_datetime_nosec($value) : date('d.m.Y H:i', $value);
        	case 'ip': return long2ip($value);
            default: return '';
        }
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function getField($field, $withAs = false)
    {
        if (!strpos($field, '.'))
            $field = 'r.' . $field;
        elseif ($withAs)
            $field .= ' AS ' . str_replace('.', '_', $field);
        return $field;
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function _CheckObj()
    {
        if (!isset($this->db))
            die ('PslList: Не определен объект для доступа к классу БД ($this->db)');
        if (!isset($this->tpl))
            die ('PslList: Не определен объект для доступа к классу шаблонов ($this->tpl)');
        //if (!isset($this->user))
        //    die ('PslList: Не определен объект для доступа к классу текущего пользователя ($this->user)');
    }
    //----------------------------------------------------------------------
    //----------------------------------------------------------------------
    function _CheckParam()
    {
        foreach($this->actions AS $title => $link)
        {
            if (empty($title))
                die ('PslList: Неверное значение переменной $this->actions');
        }
        foreach ($this->search AS $code => $field)
        {
            if (!isset($field['type']))
                die ('PslList: Не существует переменной $this->search[\'' . $code . '\'][\'type\']');
            
            switch ($field['type'])
            {
                case 'ip':
                case 'bool':
                case 'date':
                case 'months':
                case 'string':
                case 'date-interval':
                {
                    
                } break; 
                case 'select_adv':
                {
                    if (empty($field['t_collecting']))
                        die ('PslList: Не существует переменной $this->search[\'' . $code . '\'][\'t_collecting\']');
                }
                case 'select':
                {
                    if (empty($field['table']))
                        die ('PslList: Не существует переменной $this->search[\'' . $code . '\'][\'table\']');
                    if (empty($field['field']))
                        die ('PslList: Не существует переменной $this->search[\'' . $code . '\'][\'field\']');
                }break;
                case 'date_interval':
                {
                    if (empty($field['code']))
                        die ('PslList: Не существует переменной $this->search[\'' . $code . '\'][\'code\']');
                } break;
                case false: break;
                default:
                    die ('PslList: Неизвестная переменная $this->search[\'' . $code . '\']');
            }
        }
        foreach ($this->columns AS $code => $column)
        {
            switch ($column['type'])
            {
                case 'ip':
                case 'date':
                case 'string':
                case 'datetime':
                {
                    if (empty($column['head']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'head\']');
                } break;
                case 'img':
                {
                    if (empty($column['src']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'src\']');
                } break;
                case 'count':
                {
                    if (empty($column['title']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'title\']');
                    if (empty($column['table']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'table\']');
                } break;
                case 'sum-count':
                {
                    if (empty($column['title']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'title\']');
                    if (empty($column['table']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'table\']');
                    if (empty($column['field_sum']))
                        die ('PslList: Не существует переменной $this->columns[\'' . $code . '\'][\'field_sum\']');
                } break;
                default:
                    die ('PslList: Неизвестная переменная $this->columns[\'' . $code . '\']');
            }
        }
        foreach (array_reverse($this->r_icon, true) AS $code => $icon)
        {
            switch ($code)
            {
                case 'delete':
            	case 'delete_and':
            	{
                    if (isset($icon['image']) && !is_file(PATH_TO_ADMIN . 'img/' . $icon['image']))
                        die ('PslList: Неверное значение переменной $this->r_icon[\'' . $code . '\'][\'image\']');
            	} break;
            	default:
            	{
            	    if (empty($icon['image']))
                        die ('PslList: Не существует переменной $this->r_icon[\'' . $code . '\'][\'image\']');
                    if (empty($icon['image2']))
                        die ('PslList: Не существует переменной $this->r_icon[\'' . $code . '\'][\'image2\']');
                    if (!is_file(PATH_TO_ADMIN . 'img/' . $icon['image']))
                        die ('PslList: Неверное значение переменной $this->r_icon[\'' . $code . '\'][\'image\']');
                    if (!is_file(PATH_TO_ADMIN . 'img/' . $icon['image2']))
                        die ('PslList: Неверное значение переменной $this->r_icon[\'' . $code . '\'][\'image2\']');
            	}
            }
        }
    }
    //----------------------------------------------------------------------
}

?>