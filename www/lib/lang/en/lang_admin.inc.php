<?
/* ------------------- общие ------------------- */
$LA = array(
	'управление сайтом'                => 'website content management system',
	'Страницы сайта'                   => 'Pages',
	'Настройки'                        => 'Settings',
	'Администрирование'                => 'Admin tools',
	'Выход'                            => 'Logout',
	'Перейти на сайт'                  => 'Front end',
	'Код страницы'                     => 'Page code',
	'Новости'                          => 'News',
	'настройки'                        => 'settings',
	'да'                               => 'yes',
	'нет'                              => 'no',
	'скрыть'                           => 'hide',
	'показывать'                       => 'show',
	'показывать на главной'            => 'show on the front page',
	'убрать с главной'                 => 'hide from the front page',
	'сортировать'                      => 'sort',
	'Сохранить'                        => 'Save',
	'удалить'                          => 'delete',
	'Искать'                           => 'Search',
	'предыдущая'                       => 'prev',
	'следующая'                        => 'next',
	'выводить по'                      => 'records per page',
	'Редактор даты'                    => 'Date editor',
	'Последнее обновление'             => 'Last updated',
	'вернуться к списку страниц сайта' => 'back to pages list',
	'вернуться к списку настроек'      => 'back to settings list',
	'Были внесены изменения в следующие поля' => 'The following fields were changed',
	'Система управления сайтом разработана в' => 'Content management system by',
	'вернуться к списку страниц<br>без сохранения изменений'  => 'back to pages list<br>discard changes',
	'вернуться к списку настроек<br>без сохранения изменений' => 'back to settings list<br>discard changes'
);
/* ------------------- языки ------------------- */
$LA['langs'] = array(
	'Русский'    => 'Russian',
	'Английский' => 'English'
);
/* ----------------- страница логина ----------------- */
$LA['login'] = array(
	'Вход в систему' => 'Sign in',
	'Логин'          => 'Login',
	'Пароль'         => 'Password',
	'Войти'          => 'Enter',
	'Забыли пароль'  => 'Forgot your password',
	'E-mail'         => 'E-mail',
	'Получить'       => 'Get your password',
	'Код'            => 'Code',
	'и выше'         => 'or higher',
	'Внимание! У вас выключен JavaScript!<br>Включите JavaScript и перезагрузите страницу'     => 'Your browser doesn\'t support JavaScript.<br>JavaScript must be enabled to view these pages',
	'Извините, ваша версия браузера не поддерживается!<br><br>Список поддерживаемых браузеров' => 'Your browser is not supported.<br><br>Currently suppported browsers'
);
/* ---------------- страницы контента ---------------- */
$LA['content'] = array(
	'добавить подраздел'      => 'add a subsection',
	'удалить страницу'        => 'delete page',
	'и все вложенные'         => 'and all subpages',
	'календарь'               => 'calendar',
	/* добавление/редактирование */
	'Добавление страницы'     => 'Add page',
	'Редактирование страницы' => 'Edit page',
	'Новая страница'          => 'New page',
	'Название страницы'       => 'Page title',
	'Скрыть'                  => 'Hide',
	'Текст'                   => 'Text',
	'сортировать вложенные страницы' => 'sort subpages'
);
/* --------------------- новости --------------------- */
$LA['news'] = array(
	'Новости'                     => 'News',
	'Список новостей'             => 'News list',
	'Добавить новость'            => 'Add news item',
	'Категории новостей'          => 'News categories',
	'удалить новость'             => 'delete news item',
	/* форма поиска */
	'Поиск новости'               => 'News search',
	'Дата'                        => 'Date',
	/* добавление/редактирование */
	'Добавление новости'          => 'Add news item',
	'Редактирование новости'      => 'Edit news item',
	'Новая новость'               => 'New news item',
	'Показывать'                  => 'Show',
	'с'                           => 'from',
	'по'                          => 'to',
	'На главной'                  => 'On the front page',
	'Скрыть'                      => 'Hide',
	'Категория'                   => 'Category',
	'или добавить новую'          => 'or add new',
	'Заголовок'                   => 'Title',
	'Изображения'                 => 'Images',
	'большое'                     => 'large',
	'маленькое'                   => 'small',
	'мини-копия (только для jpg)' => 'thumbnail (jpg only)',
	'Анонс'                       => 'Teaser',
	'Текст'                       => 'Text',
	'вернуться к списку новостей<br>без сохранения изменений' => 'back to news list<br>discard changes'
);
/* ---------------- категории новостей --------------- */
$LA['news_categories'] = array(
	'Категории'                     => 'Categories',
	'Список категорий для новостей' => 'News categories list',
	'Добавить категорию'            => 'Add a category',
	'удалить категорию'             => 'delete category',
	/* форма поиска */
	'Поиск категории'               => 'Search categories',
	/* добавление/редактирование */
	'Добавление категории'          => 'Add a news category',
	'Редактирование категории'      => 'Edit a category',
	'Новая категория'               => 'New category',
	'Название'                      => 'Category name',
	'Скрыть'                        => 'Hide',
	'вернуться к списку категорий<br>без сохранения изменений' => 'back to categories list<br>discard changes'
);
/* -------------------- подписчики -------------------- */
$LA['subscribers'] = array(
	'Подписчики'                => 'Subscribers',
	'Список подписчиков'        => 'Subscribers list',
	'Добавить подписчика'       => 'Add a new subscriber',
	'удалить подписчика'        => 'delete a subscriber',
	/* форма поиска */
	'Поиск по e-mail'           => 'E-mail search',
	/* добавление/редактирование */
	'Добавление подписчика'     => 'Add a new subscriber',
	'Редактирование подписчика' => 'Edit subscriber details',
	'Новый подписчик'           => 'New subscriber',
	'E-mail'                    => 'E-mail',
	'Заблокировать'             => 'Lock',
	'вернуться к списку подписчиков<br>без сохранения изменений' => 'back to subscribers list<br>discard changes'
);
/* ----------------------- фото ----------------------- */
$LA['foto'] = array(
	'список фотографий'           => 'pictures list',
	'Добавить фото'               => 'add a picture',
	'добавить подраздел'          => 'add a subcategory',
	'Название фотоальбома'        => 'Photo album name',
	'Описание фотоальбома'        => 'Photo album description',
	'редактировать'               => 'edit',
	'удалить фотографию'          => 'delete a picture',
	'удалить фотоальбом'          => 'delete entire album',
	'и все вложенные'             => 'and all sub-albums',
	'Новая фотография'            => 'New picture',
	'Новый подраздел'             => 'New subcategory',
	'Добавление фотографии'       => 'Add a picture',
	'Редактирование фотографии'   => 'Picture edit',
	'Название'                    => 'Name',
	'Изображения'                 => 'Images',
	'большое'                     => 'large',
	'маленькое'                   => 'small',
	'мини-копия (только для jpg)' => 'thumbnail (jpg only)',
	'Описание'                    => 'Description',
	'Скрыть'                      => 'Hide',
	'отменить изменения'          => 'cancel changes',
	'сортировать фото'            => 'sort pictures',
	'сортировать вложенные подразделы' => 'sort subcategories',
	'изменить название<br>и описание<br>фотоальбома' => 'edit<br>album name<br>or description',
	'вернуться к списку фотографий<br>без сохранения изменений' => 'back to pictures list<br>discard changes'
);
/* --------------------- гостевая --------------------- */
$LA['guestbook'] = array(
	'Гостевая'                  => 'Guestbook',
	'Список сообщений гостевой' => 'Postings',
	'удалить сообщение'         => 'delete a posting',
	/* форма поиска */
	'Дата'                      => 'Date',
	/* добавление/редактирование */
	'Редактирование сообщения'  => 'Edit a posting',
	'Дата добавления'           => 'Date posted',
	'Автор'                     => 'Username',
	'IP'                        => 'IP address',
	'E-mail'                    => 'E-mail',
	'Текст'                     => 'Text',
	'Скрыть'                    => 'Hide',
	'удалить и добавить автора в бан-лист'                     => 'delete and ban',
	'вернуться к списку сообщений<br>без сохранения изменений' => 'back to guestbook list<br>discard changes'
);
/* ---------------------- бан ---------------------- */
$LA['ban'] = array(
	'Забаненные IP' => 'Banned IP addresses',
	'Поиск IP'      => 'Search for IP address',
	'удалить IP'    => 'delete IP address from the list'
);
/* ---------------------- опросы ---------------------- */
$LA['polls'] = array(
	'Опросы'                => 'Polls',
	'Список опросов'        => 'Polls list',
	'Добавить опрос'        => 'Add a poll',
	'удалить опрос'         => 'delete a poll',
	'кол-во ответов'        => 'vote count',
	/* форма поиска */
	'Поиск опроса'          => 'Polls search',
	'Дата'                  => 'Date',
	/* добавление/редактирование */
	'Добавление опроса'     => 'Add a poll',
	'Редактирование опроса' => 'Edit a poll',
	'Новый опрос'           => 'New poll',
	'Показывать'            => 'Show',
	'с'                     => 'from',
	'по'                    => 'to',
	'Вопрос'                => 'Topic',
	'Скрыть'                => 'Hide',
	'Варианты ответов'      => 'Options',
	'вернуться к списку опросов<br>без сохранения изменений' => 'back to polls list<br>discard changes'
);
/* ------------------ обратная связь ------------------ */
$LA['feedback'] = array(
	'Обратная связь'                         => 'Feedback',
	'Редактирование страницы обратной связи' => 'Edit feedback page',
	'E-mail для сообщений обратной связи'    => 'E-mail to send feedback to',
	'Контактная информация'                  => 'Contact info',
	'Контактный телефон'                     => 'Contact phone'
);

/* -------------------- настройки -------------------- */
$LA['options'] = array(
	'Редактирование настроек' => 'Change settings'
);
//Редактирование настроек
/* ------------------- пользователи ------------------- */
$LA['users'] = array(
	'Пользователи'                => 'Users',
	'Список пользователей'        => 'User list',
	'Добавить пользователя'       => 'Add user',
	'список групп'                => 'group list',
	'удалить пользователя'        => 'delete user',
	/* форма поиска */
	'Пользователь'                => 'User',
	'E-mail'                      => 'E-mail',
	'Группа'                      => 'Group',
	'Заблокирован'                => 'Locked',
	/* добавление/редактирование */
	'Добавление пользователя'     => 'Add user',
	'Редактирование пользователя' => 'Edit user details',
	'Новый пользователь'          => 'New user',
	'Группа'                      => 'Group',
	'Язык'                        => 'Language',
	'Заблокировать'               => 'Lock',
	'Логин'                       => 'Login name',
	'Пароль'                      => 'Password',
	'Ф.И.О.'                      => 'Real name',
	'Телефон'                     => 'Phone',
	'E-mail'                      => 'E-mail',
	'Должность'                   => 'Position',
	'Организация'                 => 'Organisation',
	'Часовой пояс'                => 'Time zone',
	'Комментарии'                 => 'Comments',
	'вернуться к списку пользователей<br>без сохранения изменений' => 'back to user list<br>discard changes'
);
/* --------------------- группы --------------------- */
$LA['groups'] = array(
	'Группы'                              => 'Groups',
	'Список групп'                        => 'Groups list',
	'Добавить группу'                     => 'Add a group',
	'список групп'                        => 'groups list',
	'кол-во пользователей'                => 'user count',
	'удалить группу'                      => 'delete a group',
	'удалить группу и всех пользователей' => 'delete a group and all its users',
	/* форма поиска */
	'Поиск группы'                        => 'Group search',
	/* добавление/редактирование */
	'Добавление группы'                   => 'Add a group',
	'Редактирование группы'               => 'Edit a group',
	'Новая группа'                        => 'New group',
	'Название'                            => 'Group name',
	'Права'                               => 'Access rights',
	'вернуться к списку групп<br>без сохранения изменений' => 'back to group list<br>discard changes'
);
/* --------------------- баннеры --------------------- */
$LA['banners'] = array(
	'Баннеры'                => 'Banner ads',
	'Список баннеров'        => 'List of banner ads',
	'Добавить баннер'        => 'Add a banner',
	'удалить баннер'         => 'delete a banner ad',
	/* форма поиска */
	'Поиск баннера'          => 'Banner ads search',
	/* добавление/редактирование */
	'Добавление баннера'     => 'Add a banner',
	'Редактирование баннера' => 'Edit a banner',
	'Новый баннер'           => 'New banner ad',
	'Название'               => 'Name',
	'Изображение'            => 'Image',
	'Скрыть'                 => 'Hide',
	'Код'                    => 'Code',
	'Ширина'                 => 'Width',
	'Высота'                 => 'Height',
	'Язык'                   => 'Language',
	'Список'                 => 'List',
	'добавить'               => 'add',
	'удалить'                => 'delete',
	'Текст'                  => 'Text',
	'Тип баннера'            => 'Ad type',
	'картинка'               => 'image',
	'текст'                  => 'text',
	'вернуться к списку баннеров<br>без сохранения изменений' => 'back to banner ads list<br>discard changes'
);
/* ---------------------- счетчики ---------------------- */
$LA['counters'] = array(
	'Счетчики'                 => 'Counters',
	'Редактирование счетчиков' => 'Change counter settings',
	'Код вверху страницы'      => 'Top-of-the-page code',
	'Код счетчиков'            => 'Counter code'
);
/* ---------------------- логи ---------------------- */
$LA['logs'] = array(
	'Логи'            => 'Logs',
	'Поиск по месяцу' => 'Search by month',
	'Пользователь'    => 'User'
);

/* -------------------- сортировка -------------------- */
$LA['sort'] = array(
	'Сортировка страниц раздела' => 'Page order sort',
	'Для сортировки объектов достаточно установить<br>на нужную иконку курсор мыши и, зажав левую<br>кнопку мыши, перетащить значок на новое место' => 'Drag and drop to sort'
);
?>
