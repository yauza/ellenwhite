<?

//Errors

//for class for work with pages PslPage
$lang_words['E_PSL_PAGE_OK'] = 'Excute with out errors';
$lang_words['E_PSL_PAGE_GRPID'] = 'Wrong ID group for current user';
$lang_words['E_PSL_PAGE_RESCODE'] = 'Wrong code of resource';
$lang_words['E_PSL_PAGE_GRPCODE'] = 'Wrong code of group';
$lang_words['E_PSL_PAGE_PAGECODE'] = 'Wrong code of page';
$lang_words['E_PSL_PAGE_MENUCODE'] = 'Wrong code of menu';
$lang_words['E_PSL_PAGE_UNKNOWN'] = 'Unkonwn error';
$lang_words['E_PSL_PAGE_DB'] = 'PslPage: You forget turn DB!';
$lang_words['E_PSL_PAGE_USER'] = 'PslPage: You forget turn PslUser!';
$lang_words['E_PSL_PAGE_NOCODE'] = 'PslPage: Not found code of current page';

//for class for work with lists PslOptions
$lang_words['E_PSL_OPTIONS_OK'] = 'Excute with out errors';
$lang_words['E_PSL_OPTIONS_CODE'] = 'Code contains disabled simbols';
$lang_words['E_PSL_OPTIONS_UNKNOWN'] = 'Unknown error';
$lang_words['E_PSL_OPTIONS_DB'] = 'PslOptions: You forget turn DB!';

//for class for work with users PslUser
$lang_words['E_PSL_USER_OK'] = 'Excute with out errors';
$lang_words['E_PSL_USER_STRLOGIN'] = 'Login contains disabled simbols';
$lang_words['E_PSL_USER_STRPWD'] = 'Password contains disabled simbols'; 
$lang_words['E_PSL_USER_NOTFOUND'] = 'Record is not found'; 
$lang_words['E_PSL_USER_NOLOGIN'] = 'Login is not found'; 
$lang_words['E_PSL_USER_NOTLOGGED'] = 'User is not autorized'; 
$lang_words['E_PSL_USER_NOGP'] = 'Not found parametres for work with groups';
$lang_words['E_PSL_USER_UNKNOWN'] = 'Unknown error';
$lang_words['E_PSL_USER__DB'] = 'PslUser: You forget turn DB!';

?>