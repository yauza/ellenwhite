<?

$LEA = array(
	'Запись не найдена'               => 'Record not found',
	'Ошибка'                          => 'Error',
	'Ошибка при удалении записи'      => 'Record delete error occured',
	'Ошибка при удалении директории'  => 'Folder delete error occured',
	'Ошибка при создании директории'  => 'Folder create error occured',
	'Ошибка при удалении изображения' => 'Image delete error occured',
	'Ошибка при загрузке изображения' => 'Image load error occured',
	'Расширение'                      => 'File type',
	'является недопустимым'           => 'is not allowed'
);

/* --------------- страница авторизации --------------- */
$LEA['login'] = array(
	'Введите логин'                        => 'Please enter your login name',
	'Логин содержит недопустимые символы'  => 'Login contains invalid characters',
	'Доступ запрещен'                      => 'Access denied',
	'Введите e-mail'                       => 'Please enter your e-mail address',
	'Введен некорректный e-mail'           => 'Email address is incorrect',
	'Данного e-mail нет в базе'            => 'Email address not found',
	'Пароль отправлен на указанный e-mail' => 'Your password has been sent to the e-mail you entered',
	'Вы превысили количество попыток входа!<br>Введите код изображенный на картинке!' => 'Too many login attempts.<br>Please enter the code from the picture to proceed'
);
/* ---------------- страницы контента ---------------- */
$LEA['content'] = array(
	'Введите название' => 'Enter the title'
);
/* --------------------- новости --------------------- */
$LEA['news'] = array(
	'Введите название' => 'Enter the title',
	'Введите анонс'    => 'Enter the teaser',
	'Введите текст'    => 'Enter the text',
	'Ошибка при загрузке изображения (большого)'   => 'Large image load error',
	'Ошибка при загрузке изображения (маленького)' => 'Small image load error',
	'Ошибка при удалении изображения (большого)'   => 'Large image delete error',
	'Ошибка при удалении изображения (маленького)' => 'Small image delete error',
	'Неверное значение начальной даты (Дата должна быть в формате DD.MM.YYYY)' => 'Start date should be in DD.MM.YYYY format',
	'Неверное значение конечной даты (Дата должна быть в формате DD.MM.YYYY)'  => 'End date should be in DD.MM.YYYY format',
	'Неверное значение начальной даты (Дата должна быть в формате DD.MM.YYYY HH:MM)' => 'Start date should be in DD.MM.YYYY HH:MMformat',
	'Неверное значение конечной даты (Дата должна быть в формате DD.MM.YYYY HH:MM)'  => 'End date should be in DD.MM.YYYY HH:MMformat'
);
/* ---------------- категории новостей --------------- */
$LEA['news_categories'] = array(
	'Введите название' => 'Enter category name'
);
/* -------------------- подписчики -------------------- */
$LEA['subscribers'] = array(
	'Введите e-mail' => 'Enter e-mail address'
);
/* ----------------------- фото ----------------------- */
$LEA['foto'] = array(
	'Введите название' => 'Enter image title',
	'Ошибка при загрузке изображения (большого)'   => 'Large image load error',
	'Ошибка при загрузке изображения (маленького)' => 'Small image load error',
	'Ошибка при удалении изображения (большого)'   => 'Large image delete error',
	'Ошибка при удалении изображения (маленького)' => 'Small image delete error',
	'Неверное значение начальной даты (Дата должна быть в формате DD.MM.YYYY)' => 'Date should be in DD.MM.YYYY format'
);
/* --------------------- гостевая --------------------- */
$LEA['guestbook'] = array(
	'Введите автора'             => 'Enter the user name',
	'Введите текст'              => 'Enter the text',
	'Введен некорректный e-mail' => 'Incorrect e-mail address'
);
/* ---------------------- опросы ---------------------- */
$LEA['polls'] = array(
	'Введите название' => 'Enter the poll title',
	'Неверное значение начальной даты (Дата должна быть в формате DD.MM.YYYY)' => 'Start date should be in DD.MM.YYYY format',
	'Неверное значение конечной даты (Дата должна быть в формате DD.MM.YYYY)'  => 'End date should be in DD.MM.YYYY format',
	'Неверное значение начальной даты (Дата должна быть в формате DD.MM.YYYY HH:MM)' => 'Start date should be in DD.MM.YYYY HH:MMformat',
	'Неверное значение конечной даты (Дата должна быть в формате DD.MM.YYYY HH:MM)'  => 'End date should be in DD.MM.YYYY HH:MMformat'
);
/* ------------------ обратная связь ------------------ */
$LEA['feedback'] = array(
	'Введите e-mail'             => 'Enter the e-mail address',
	'Введен некорректный e-mail' => 'Incorrect e-mail address'
);
/* ------------------- пользователи ------------------- */
$LEA['users'] = array(
	'Выберите группу'           => 'Select the user group',
	'Введите логин'             => 'Enter a login name',
	'Введите пароль'            => 'Enter the password',
	'Введите корректный e-mail' => 'Incorrect e-mail',
	'Пользователь с таки логином или e-mail уже существует' => 'User with the login name or e-mail you entered already exists'
);
/* --------------------- группы --------------------- */
$LEA['groups'] = array(
	'Введите название' => 'Enter group title'
);
/* --------------------- баннеры --------------------- */
$LEA['banners'] = array(
	'Введите название'                   => 'Enter a banner name',
	'Файл с таким именем уже существует' => 'File with this name already exists',
	'Введите текст'                      => 'Enter the text',
);

?>
