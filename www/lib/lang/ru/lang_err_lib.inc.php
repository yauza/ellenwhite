<?

//Ошибки

//Для класса работы со страницами PslPage
$lang_words['E_PSL_PAGE_OK'] = 'Выполнено без ошибок';
$lang_words['E_PSL_PAGE_GRPID'] = 'Неверный ID группы текущего пользователя';
$lang_words['E_PSL_PAGE_RESCODE'] = 'Неверный код ресурса';
$lang_words['E_PSL_PAGE_GRPCODE'] = 'Неверный код группы';
$lang_words['E_PSL_PAGE_PAGECODE'] = 'Неверный код страницы';
$lang_words['E_PSL_PAGE_MENUCODE'] = 'Неверный код меню';
$lang_words['E_PSL_PAGE_UNKNOWN'] = 'Неизвестная ошибка';
$lang_words['E_PSL_PAGE_DB'] = 'PslPage: Вы забыли подключить БД!';
$lang_words['E_PSL_PAGE_USER'] = 'PslPage: Вы забыли подключить PslUser!';
$lang_words['E_PSL_PAGE_NOCODE'] = 'PslPage: Не установлен код текущей страницы';

//Для класса работы со списками PslOptions
$lang_words['E_PSL_OPTIONS_OK'] = 'Выполнено без ошибок';
$lang_words['E_PSL_OPTIONS_CODE'] = 'Код содержит недопустимые символы';
$lang_words['E_PSL_OPTIONS_UNKNOWN'] = 'Неизвестная ошибка';
$lang_words['E_PSL_OPTIONS_DB'] = 'PslOptions: Вы забыли подключить БД!';

//Для класса работы с пользователями PslUser
$lang_words['E_PSL_USER_OK'] = 'Выполнено без ошибок';
$lang_words['E_PSL_USER_STRLOGIN'] = 'Логин содержит недопустимые символы';
$lang_words['E_PSL_USER_STRPWD'] = 'Пароль содержит недопустимые символы'; 
$lang_words['E_PSL_USER_NOTFOUND'] = 'Запись не найдена'; 
$lang_words['E_PSL_USER_NOLOGIN'] = 'Логин не определен'; 
$lang_words['E_PSL_USER_NOTLOGGED'] = 'Не авторизован'; 
$lang_words['E_PSL_USER_NOGP'] = 'Не определены параметры для работы с группами';
$lang_words['E_PSL_USER_UNKNOWN'] = 'Неизвестная ошибка';
$lang_words['E_PSL_USER__DB'] = 'PslUser: Вы забыли подключить БД!';

?>