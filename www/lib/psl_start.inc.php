<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Quick site development                                         #
#                                                                              #
#   Copyright (с) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_start.inc.php                                                          #
#   Create needed objects, include libraries and config files                  #
#                                                                              #
################################################################################

header('Content-type: text/html; charset=utf-8');                       

################################################################################
# DB: path to catalog with this library
define ('_PHPSITELIB_PATH', PATH_TO_ROOT . 'lib/'); 

# check exist variable $path_to_root
if (!defined('PATH_TO_ROOT') || PATH_TO_ROOT == '') die('Not found constant PATH_TO_ROOT. Continue is not available.');

# set procedure processing scripts
ini_set('magic_quotes_runtime', 0);

//setlocale(LC_CTYPE, 'ru_RU.CP1251');

# load options
require (_PHPSITELIB_PATH . 'psl_config.inc.php');  

# current page
$this_page = $_SERVER['PHP_SELF'];		

if (_PHPSITELIB_USESESS) session_start();

// postfix for session variables, which contain autorize parameters (for object PslUser)
if (!isset($_PHPSITELIB_USER_POSTFIX)) $_PHPSITELIB_USER_POSTFIX = '';

if (phpversion() >= '5.0.0') {
    $ver = '_p5';
}

################################################################################
# include files libraries
require (_PHPSITELIB_PATH . "psl_mysql$ver.inc.php");
require (_PHPSITELIB_PATH . 'psl_utils.inc.php');
if (_PHPSITELIB_USE_TEMPLATE) require (_PHPSITELIB_PATH . 'template.inc.php');
if (_PHPSITELIB_USE_USER)     require (_PHPSITELIB_PATH . "psl_user$ver.inc.php");
if (_PHPSITELIB_USE_PAGE)     require (_PHPSITELIB_PATH . 'psl_page.inc.php');
if (_PHPSITELIB_USE_OPTIONS)  require (_PHPSITELIB_PATH . 'psl_options.inc.php');


################################################################################
# if show time of generaion page, save current time
if (_PHPSITELIB_GENTIME) $_phpsitelib_gen = get_micro_time();

# cut index file from name of currnet page
$this_page = preg_replace('/'._PHPSITELIB_CUTINDEX.'/', '', $this_page);

set_error_handler('mail_bug_report');

error_reporting(_PHPSITELIB_ERRLVL);

cache_control(_PHPSITELIB_CACHE, _PHPSITELIB_CACHEEXP);

################################################################################
# inheritance classes for their customing /customazing/ Наследование классов для их кастомизации
class PslDb extends PslMySql {
  var $mHost        = DB_HOST;
  var $mDatabase    = DB_NAME;
  var $mUser        = DB_USER;
  var $mPassword    = DB_PWD;
  var $mDebug       = _PHPSITELIB_DEBUG;
  var $mHaltOnError = _PHPSITELIB_DB_REPORT;
}

# function for send message of error in DB by email
function mail_bug_report_db($text) {
    global $this_page;
    if (_PHPSITELIB_ERRMAIL) {
        $text = "Страница: $this_page\n\n".$text;
        @mail(_PHPSITELIB_ERRMAIL, "Database bug report", $text);
    }
}

# function for send message of error by email
function mail_bug_report($errno, $errstr, $errfile, $errline) {
    switch ($errno) {
      case E_USER_ERROR:
        $text = "FATAL [$errno] $errstr<br>\n".
                "  Fatal error in line ".$errline." of file ".$errfile.
                ", PHP ".PHP_VERSION." (".PHP_OS.")\n".
                "Aborting...\n";
        break;
      case E_USER_WARNING:
        $text = "ERROR [$errno] $errstr in line $errline of file $errfile\n";
        break;
      case E_USER_NOTICE:
        $test = "WARNING [$errno] $errstr in line $errline of file $errfile\n";
        break;
      default:
        $errors = array(  1 => "E_ERROR",            2 => "E_WARNING",           4 => "E_PARSE", 
                          8 => "E_NOTICE",          16 => "E_CORE_ERROR",       32 => "E_CORE_WARNING", 
                         64 => "E_COMPILE_ERROR",  128 => "E_COMPILE_WARNING", 256 => "E_USER_ERROR",
						512 => "E_USER_WARNING ", 1024 => "E_USER_NOTICE",    2047 => "E_ALL",
                       2048 => "E_STRICT",        8192 => "E_DEPRECATED");
        $text = "[".(isset($errors[$errno])?$errors[$errno]:$errno)."] $errstr in line $errline of file $errfile\n";
        break;
    }
    if (_PHPSITELIB_ERRMAIL) 
        @mail(_PHPSITELIB_ERRMAIL, 'Bug report', $text);
    else 
        print nl2br($text);
}

# set level of cache (from PHPLib)
function cache_control($allowcache, $expire)
{
    switch ($allowcache) {

      case 'passive':
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', getlastmod()) . ' GMT');
        # possibly ie5 needs the pre-check line. This needs testing.
        header('Cache-Control: post-check=0, pre-check=0');
      break;

      case 'public':
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expire * 60) . ' GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', getlastmod()) . ' GMT');
        header('Cache-Control: public');
        header('Cache-Control: max-age=' . $expire * 60);
      break;
 
      case 'private':
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', getlastmod()) . ' GMT');
        header('Cache-Control: private');
        header('Cache-Control: max-age=' . $expire * 60);
        header('Cache-Control: pre-check=' . $expire * 60);
      break;

      default:
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-cache');
        header('Cache-Control: post-check=0, pre-check=0');
        header('Pragma: no-cache');
      break;
    }
}

################################################################################
# create objects
$db = new PslDb;
$db->mErrHandler = 'mail_bug_report_db';

if (_PHPSITELIB_USE_USER)
{
    $g_user = new PslUser;
    $g_user->mDb = $db;
    if (_PHPSITELIB_USE_PAGE)
    {
		$g_page = new PslPage;
	    $g_page->mDb = $db;
	    $g_page->mUser = $g_user;
	}
}
if (_PHPSITELIB_USE_OPTIONS)
{
    $g_options = new PslOptions;
    $g_options->mDb = $db;
}



//if mysql 5 and problems with encoding
$db->Query('SET NAMES UTF8');

?>