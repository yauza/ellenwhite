<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Library for quick site development                             #
#                                                                              #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_utils.inc.php (String, Date and Time utilities)                        #
#                                                                              #
################################################################################

function string_is_email($s) {
    return preg_match('/^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}$/i', $s) ? true : false;
}
function string_is_login($s) {
    return preg_match('/^[a-z0-9_]+$/i', $s) ? true : false;
}
function string_is_int($s) {
    return preg_match('/^[-+]?\d+$/', $s) ? true : false;
}
function string_is_float($s) {
    return preg_match('/^[-+]?\d*\.?\d+(e[-+]?\d+)?$/i', $s) ? true : false;
}
function string_is_id($s) { 
    return string_is_int($s) && $s > 0; 
}
function string_is_date($s) {
    return preg_match('/^(\d{1,2})\.(\d{1,2})\.(\d{1,4})$/', $s, $m) && checkdate($m[2], $m[1], $m[3]) ? true : false;
}
function string_is_time($s) {
    return preg_match('/^([0-1]?\d|2[0-3]):([0-5]?\d)(:([0-5]?\d))?$/', $s) ? true : false;
}
function string_is_datetime($s) {
    return preg_match('/^(\d{1,2})\.(\d{1,2})\.(\d{1,4})\s+([0-1]?\d|2[0-3]):([0-5]?\d)(:([0-5]?\d))?$/', $s, $m) && 
           checkdate($m[2], $m[1], $m[3]) ? true : false;
}
function sql_to_date ($s) {
    return preg_match('/^(\d+)-(\d+)-(\d+)/', $s, $m) ? $m[3].'.'.$m[2].'.'.$m[1] : '';
}
function sql_to_datestr ($s) {
    return preg_match('/^(\d+)-(\d+)-(\d+)/', $s, $m) ? $m[3].' '.get_month_name($m[2]).' '.$m[1] : '';
}

function sql_to_datestr_noyear ($s) {
    return preg_match('/^(\d+)-(\d+)-(\d+)/', $s, $m) ? ($m[3]+0) . ' ' . get_month_name($m[2]) : '';
}

function sql_to_datestr_noyear_low ($s) {
    return preg_match('/^(\d+)-(\d+)-(\d+)/', $s, $m) ? ($m[3]+0) . ' ' . get_month_name_low($m[2]) : '';
}

function sql_to_time ($s, $withSeconds = true) {
    return preg_match('/(\d+):(\d+):(\d+)/', $s, $m) ? $m[1].':'.$m[2].($withSeconds ? ':'.$m[3] : '') : '';
}
function sql_to_datetime ($s) {
    return preg_match('/^(\d+)-(\d+)-(\d+)\s+((\d+):(\d+):(\d+))$/', $s, $m) ? $m[3].'.'.$m[2].'.'.$m[1].' '.$m[4] : '';
}
function sql_to_datetime_nosec ($s) {
    return preg_match('/^(\d+)-(\d+)-(\d+)\s+(\d+):(\d+):(\d+)$/', $s, $m) ? $m[3].'.'.$m[2].'.'.$m[1].' '.$m[4].':'.$m[5] : '';
}
function sql_to_ts ($s) {
    if (preg_match('/^(\d+)-(\d+)-(\d+)\s+((\d+):(\d+):(\d+))$/', $s, $m)) {
        return mktime($m[5], $m[6], $m[7], $m[2], $m[3], $m[1]);
    } elseif (preg_match('/^(\d+)-(\d+)-(\d+)/', $s, $m)) {
        return mktime(0, 0, 0, $m[2], $m[3], $m[1]);
    } else {
        return 0;
    }
}
function date_to_sql ($s) {
    return preg_match('/^(\d{1,2})\.(\d{1,2})\.(\d{1,4})$/', $s, $m) && checkdate($m[2], $m[1], $m[3]) 
           ? $m[3].'-'.$m[2].'-'.$m[1] : false;
}
function datetime_to_sql ($s) {
    return preg_match('/^(\d{1,2})\.(\d{1,2})\.(\d{1,4})(\s+([0-1]?\d|2[0-3]):([0-5]?\d)(:([0-5]?\d))?)?$/', $s, $m) && 
           checkdate($m[2], $m[1], $m[3]) ? trim($m[3].'-'.$m[2].'-'.$m[1].' '.$m[4]) : false;
}
function ts_to_date($ts) {
    return date('d.m.Y', $ts);
}
function ts_to_time($ts) {
    return date('H:i:s', $ts);
}
function ts_to_datetime($ts){
    return date('d.m.Y H:i:s', $ts);
}
function ts_to_sql($ts) {
    return date('Y-m-d H:i:s', $ts);
}
function get_word_form($v, $w) {
    $s = '';
    if (is_array($w)) { 
        $last = substr($v, ($l = strlen($v)) - 1, 1);
        if ($l >= 2) $last2 = substr($v, $l - 2, 2);
        if ((isset($last2) && $last2 >= 11 && $last2 <= 14) || $last >= 5 || $last == 0) $s = $w[2];
        elseif ($last == 1) $s = $w[0];
        elseif ($last > 1 && $last < 5) $s = $w[1];
    }
    return $s;
}
function get_month_name($m) {
    $a = array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');
    return $m > 0 && $m < 13 ? $a[$m - 1] : '';
}
function get_month_name_simple($m) {
    $a = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    return $m > 0 && $m < 13 ? $a[$m - 1] : '';
}
function get_month_name_simple_eng($m) {
    $a = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    return $m > 0 && $m < 13 ? $a[$m - 1] : '';
}
function get_month_name_low($m) {
    global $lang;
    if ($lang == 'ru')
        $a = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
    else
        $a = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    return $m > 0 && $m < 13 ? $a[$m - 1] : '';
}
function get_micro_time() { 
    list($usec, $sec) = explode(' ', microtime()); 
    return ((float)$usec + (float)$sec); 
} 
function get_sql_days($date_start, $date_end) {
    return (sql_to_ts($date_end) - sql_to_ts($date_start)) / 86400;
}
function get_sql_months($date_start, $date_end) {
    if (preg_match('/^(\d+)-(\d+)-(\d+)/', $date_start, $m1) && preg_match('/^(\d+)-(\d+)-(\d+)/', $date_end, $m2)) {
        $sy = $m1[1]; $sm = $m1[2]; $sd = $m1[3];
        $ey = $m2[1]; $em = $m2[2]; $ed = $m2[3];
        return ($em - $sm) + ($ey - $sy) * 12 + ($ed - $sd) / 30;
    } else {
        return 0;
    }
}

function string_to_html($s) {
    return str_replace('}', '&#125;', str_replace('{', '&#123;', htmlspecialchars($s)));
}
function string_to_html_special($s, $tags = '<a><b><i><u><p>', $donl2br = true) {
	$s = str_replace('{', '&#123;', strip_tags($s, $tags));
	return $donl2br ? nl2br($s) : $s;
}
function string_to_money($s) {
	return string_is_float($s) ? number_format(round($s, 2), 2, '.', ' ') : $s;
}

function get_user_ip() {
	return isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : '';
}

function get_user_host() {
	return gethostbyaddr(get_user_ip());
}

function send_html_mail($mailto, $subj, $body, $headers = '', $charset = 'KOI8-R') {
	if ($charset == '') $charset = 'KOI8-R';
	return @mail($mailto, iconv('UTF-8', 'KOI8-R', $subj), iconv('UTF-8', 'KOI8-R//IGNORE', $body), $headers . "MIME-Version: 1.0\nContent-Type: text/html; charset=$charset");
}

function is_post() {
	return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function psl_xslt_process($xmlText, $xsltFile, $xsltPath = '') {
	$xh = xslt_create();
	if ($xsltPath == '') $xsltPath = getcwd();
	$fileBase = 'file://' . $xsltPath . '/';
	xslt_set_base($xh, $fileBase);
	$result = xslt_process($xh, 'arg:/_xml', $xsltFile, NULL, array('_xml' => $xmlText));
	if ($result == '') {
		print "<p>XML/XSLT error #" . xslt_errno($xh) . ": " . xslt_error($xh) . "</p>";
	}
	xslt_free($xh);
	return $result;
}

function location($locationUrl) {
	header("Location: $locationUrl");
	exit();
}

function is_image($file, $no_file = false)
{
	global $IMG_ALLOW;
	foreach ($IMG_ALLOW AS $v)
	{
		$filename = $file . '.' . $v;
		if (@is_file($filename)) return $filename;
	}
	return $no_file;
}

//---------------------------------------------------------------------------
//возвращает список файлов удовлетворяющих шаблону.
//$delete_patterns - то что нужно удалить из пути к файлу
function get_file_list_by_glob($pattern , $delete_patterns = Array())
{
         $file_list = Array();

         foreach ((Array)glob($pattern) as $filename)
         {
                  if (sizeof($delete_patterns))
                           $filename = str_replace(array_keys($delete_patterns), array_values($delete_patterns), $filename);

                  if ($filename)
                      $file_list[] = trim($filename);
         }

         return $file_list;
}
//---------------------------------------------------------------------------
// загрузка изображений на сервер
function uploading_img($img, $dir/*куда загружать*/, $name/*под каким именем сохранять*/, $size_x, $size_y, $auto_mini = array())
{
	global $db, $IMG_ALLOW; $r = array();
	
    if ($img['error'] != 0)
        $r[] = file_upload_error_message($img['error']);
    
    if (empty($r))
    {
        $fileinfo = pathinfo($img['name']);
        if (!(in_array(strtolower($fileinfo['extension']), $IMG_ALLOW)))
		  $r[] = 'Ошибка при загрузке изображения "' . $fileinfo['basename'] . '" (расширение ' . $fileinfo['extension'] . ' является недопустимым)';
          
        $full_path = $dir . $name . '.' . strtolower($fileinfo['extension']);
          
        list($width, $height) = getimagesize($img['tmp_name']);
        if ($width > 2000 || $height > 1600)
            $r[] = 'Размеры загружаемого изображения слишком велики.<br><br>Изображение должно быть не больше 2000 x 1600 пикселей.';
    }
    
	if (empty($r) && !is_dir($dir) && !@mkdir($dir, 0777))
        $r[] = 'Ошибка при создании директории "' . $dir . '"';
	
	if (empty($r))
        $r = array_merge($r, delete_image($dir . $name));
	
    //print_r($r); exit;
    
	if (empty($r))
    {
        if (!@is_uploaded_file($img['tmp_name']) || !@move_uploaded_file($img['tmp_name'], $full_path))
            $r[] = 'Ошибка при загрузке изображения "' . $fileinfo['basename'] . '" (' . file_upload_error_message($img['error']) . ')';
        else
        {
            // если размеры загружаемого изображения больше допустимых, то уменьшаем изображение
    		if ($width > $size_x || $height > $size_y)
    		{
    		    list($a, $b, $new_x, $new_y) = get_image_size(array($width, $height), array($size_x, $size_y));
    			$thumb1 = imagecreatetruecolor($new_x, $new_y);
    			$source = imagecreatefrom_ext($fileinfo['extension'], $full_path);
    			if (!$source) $r[] = 'Ошибка при загрузке изображения "' . $fileinfo['basename'] . '" (функция imagecreatefrom_ext не выполнилась)';
    			else
    			{
    				imagecopyresampled($thumb1, $source, 0, 0, 0, 0, $new_x, $new_y, $width, $height);
    				image_ext($fileinfo['extension'], $thumb1, $full_path);
                    imagedestroy($thumb1);
    			}
    		}
        }
    }
	
    // автоматическое создание мини-копий загружаемого изображения
    foreach ($auto_mini AS $mini)
    {
        if (!empty($r) || empty($mini['x']) || empty($mini['y']) || empty($mini['pref'])) continue;
        $r = array_merge($r, delete_image($dir . $name . $mini['pref']));
		if (empty($r))
		{
		    list($a, $b, $new_x, $new_y) = get_image_size(array($width, $height), array($mini['x'], $mini['y']));
			$thumb2 = imagecreatetruecolor($new_x, $new_y);
            if (empty($source)) $source = imagecreatefrom_ext($fileinfo['extension'], $full_path);
			imagecopyresampled($thumb2, $source, 0, 0, 0, 0, $new_x, $new_y, $width, $height);
			image_ext($fileinfo['extension'], $thumb2, str_replace($name . '.' . $fileinfo['extension'], $name . $mini['pref'] . '.' . $fileinfo['extension'], $full_path));
			imagedestroy($thumb2);
		}
    }
	
	return empty($r) ? $full_path : $r;
}
//---------------------------------------------------------------------------
// получение размеров изображения
function get_image_size($source, $max)
{
	if ($source[0] > $max[0])
	{
		$new[0] = $max[0];
		$new[1] = ($source[1] * $new[0]) / $source[0];
		if ($new[1] > $max[1])
		{
			$temp = $new;
			$new[1] = $max[1];
			$new[0] = ($temp[0] * $new[1]) / $temp[1];
		}
	}
	elseif ($source[1] > $max[1])
	{
		$new[1] = $max[1];
		$new[0] = ($source[0] * $new[1]) / $source[1];
	}
	else
	{
		$new = $source;
	}
	
	return array($source[0], $source[1], intval($new[0]), intval($new[1]));
}
//---------------------------------------------------------------------------
function file_upload_error_message($error_code)
{
    switch ($error_code)
	{
        case UPLOAD_ERR_INI_SIZE:
            return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        case UPLOAD_ERR_FORM_SIZE:
            return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        case UPLOAD_ERR_PARTIAL:
            return 'The uploaded file was only partially uploaded';
        case UPLOAD_ERR_NO_FILE:
            return 'No file was uploaded';
        case UPLOAD_ERR_NO_TMP_DIR:
            return 'Missing a temporary folder';
        case UPLOAD_ERR_CANT_WRITE:
            return 'Failed to write file to disk';
        case UPLOAD_ERR_EXTENSION:
            return 'File upload stopped by extension';
        default:
            return 'Unknown upload error';
    }
}
//----------------------------------------------------------------------------------------
// обрезание строки
// title - форматируемая строка
// word - максимальное количество оставляемых слов
// length - максимальное количество оставляемых символов
function get_title_formated($title, $word = 4, $lenght = 40, $noHTML = false)
{
    //echo $title . ' -- ' . $word . ' -- ' . $lenght . '<br><br>';
	$title = preg_replace('/[ ]+/', ' ', trim($title));
	$titles = explode(' ', $title);
    
    if (count($titles) == 1 && mb_strlen($title, 'utf-8') > $lenght)
        return mb_substr($title, 0, $lenght, 'utf-8') . '...';
    elseif (count($titles) == 1)
        return $title;
    
	$title_formated = '';
	foreach ($titles AS $k => $v)
	{
		if (mb_strlen($v, 'utf-8') > 16 && $pos = mb_strpos($v, '-', 0, 'utf-8'))
			$v = mb_substr($v, 0, $pos, 'utf-8') . ' - ' . mb_substr($v, $pos + 1, 16, 'utf-8');
		
		if ($k < $word && mb_strlen($title_formated . ' ' . $v, 'utf-8') < $lenght)
			$title_formated .= ($k != 0 ? ' ' : '') . $v;
	}
	
	$title_formated = ($noHTML ? $title_formated : htmlspecialchars($title_formated)) . ((mb_strlen($title_formated, 'utf-8') < mb_strlen($title, 'utf-8')) ? '...' : '');
		
	return $title_formated;
}
//----------------------------------------------------------------------------------------
// удаление директории вместе со всеми поддиректориями и файлами
function del_dir($dir)
{
	if (!is_dir($dir)) return '';
	if ($handle = opendir($dir))
	{
	    while (false !== ($file = readdir($handle)))
	        if ($file != "." && $file != "..")
	        {
				if (is_dir($dir . $file))
					del_dir($dir . $file . '/');
				elseif (!@unlink($dir . $file))
					return 'Ошибка при удалении директории "' . $dir . '"';
			}
	    closedir($handle);
	    //chmod($dir, 0777);
	    if (!@rmdir($dir)) return 'Ошибка при удалении директории "' . $dir . '"';
	    return '';
	}
}
//---------------------------------------------------------------------------
// удаление изображения
function delete_image($path)
{
	$r = array();
    $image = is_image($path);
    while ($image)
    {
        if (!@unlink($image))
		  $r[] = 'Ошибка при удалении изображения "' . $image . '"';
        $image = is_image($path);
    }
	return $r;
}
//---------------------------------------------------------------------------
// вспомогательные функции
function imagecreatefrom_ext($ext, $full_path)
{
	switch (strtolower($ext))
	{
		case 'jpg':
		case 'jpeg': return @imagecreatefromjpeg($full_path);
		case 'gif': return @imagecreatefromgif($full_path);
		case 'png': return @imagecreatefrompng($full_path);
	}
}
function image_ext($ext, $thumb, $path)
{
	switch (strtolower($ext))
	{
		case 'jpg':
		case 'jpeg': imagejpeg($thumb, $path, 100); return;
		case 'gif': imagegif($thumb, $path); return;
		case 'png': imagepng($thumb, $path, 9); return;
	}
}
//---------------------------------------------------------------------------
function send_mail($email, $from, $subject, $msg)
{
    require_once(PATH_TO_ROOT . 'include/phpmailer/class.phpmailer.php');
    $mail = new PHPMailer();
	$mail->SetFrom('noreply@' . _PHPSITELIB_SITE . '.ru', _PHPSITELIB_SITE);
	$mail->Subject = $subject;
    
    if (is_array($email))
    {
        foreach ($email AS $e)
            $mail->AddAddress($e, '');
    }
    else
	    $mail->AddAddress($email, htmlspecialchars($from));
    
    preg_match_all('/\<img src=\"([^\"]*)\"/i', $msg, $images);
    $i = 0;
    foreach ($images[1] AS $img)
    {
        $i++;
        $msg = str_replace($img, 'cid:image' . $i, $msg);
        $mail->AddEmbeddedImage(PATH_TO_ROOT . $img, 'image' . $i);
    }
    
	$mail->MsgHTML($msg);
	$mail->Send();
}
//---------------------------------------------------------------------------
function get_random_string($length = 8)
{
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $numChars = mb_strlen($chars, 'utf-8');
    $string = '';
    for ($i = 0; $i < $length; $i++)
    {
        $string .= mb_substr($chars, rand(1, $numChars) - 1, 1, 'utf-8');
    }
    return $string;
}
//---------------------------------------------------------------------------
if(!function_exists('mb_ucwords'))
{
    function mb_ucwords($str)
    {
          return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
    /*
    function mb_ucwords($s, $charset)
    {
        $s = mb_strtolower(trim($s), $charset);
        $w = explode(' ', $s);
        
        $return = '';
        foreach ($w as $val)
            $return .= ' ' . mb_strtoupper(mb_substr($val, 0, 1, $charset), $charset) . mb_substr($val, 1, mb_strlen($val, $charset) - 1, $charset);
        
        return trim($return);
    }*/
}
//---------------------------------------------------------------------------
function get_alphabet($lang = 'ru', $upper = true)
{
    if ($lang == 'ru' && $upper)
        return array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я');
    elseif ($lang == 'ru' && !$upper)
        return array('а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
    elseif ($lang == 'en' && $upper)
        return array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    elseif ($lang == 'en' && !$upper)
        return array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    else
        return array();
}
//---------------------------------------------------------------------------
function get_filesize($file)
{
    if (is_file($file))
    {
        $size = filesize($file);
        if ($size > 1024 * 1024 * 1024) // больше 1 гигабайта
            $size = round($size / 1024 * 1024 * 1024, 2) . ' Гб.';
        if ($size > 1024 * 1024) // больше 1 мегабайта
            $size = round($size / 1024 * 1024, 2) . ' Мб.';
        elseif ($size > 1024) // больше 1 килобайта
            $size = round($size / 1024, 2) . ' Кб.';
        return $size;
    }
    else
        return 0;
}
//---------------------------------------------------------------------------

?>