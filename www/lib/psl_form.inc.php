<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Library for quick site development                             #
#                                                                              #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_form.inc.php                                                           #
#   Class for work with form                                                   #
#                                                                              #
################################################################################

class PslForm
{
    var $file = 'form.html';                             // файл шаблона формы редактирования записей
    
    var $code = '';                                      // Код страницы
    var $headTitleAdd = 'Add record';             // Верхний заголовок
    var $headTitleEdit = 'Edit record';        // Верхний заголовок
    
    var $thisPage = '';                                  // Путь к странице списка записей
    
    var $is_chain = true;                                // функция get_chain для вывода цепочки страниц
    var $chainLastTitle = 'New record';                // Последний пункт в цепочке страниц
    var $chainParentId = '';                             // 1 пераметр ($parent_id) функции get_chain
    var $chainCode = '';                                 // 2 пераметр ($code) функции get_chain
    
    var $backTitle = 'return to list<br>without saving changes'; // Текст ссылки для возврата на пред. стр.
    var $backLink = '';                                  // Адрес ссылки для возврата на пред. стр.
    var $backImage = 'back.gif';                         // изображение рядом с ссылкой на пред. стр.
    
    var $actions = array();                              // Ссылки-действия в правом верхнем углу
                                                         // array('название' => 'ссылка', 'название' => 'ссылка', ...)
    
    var $lu_title = 'Last update';              // Текст в блоке с информацией о последнем обновлении записи
    
    var $table = '';                                     // Таблица, в которой находится запись
    var $id = -1;                                        // id записи (если -1, значит идет добавление новой записи)
    var $fieldForChain = 'Title';                        // Поле из таблицы, которое используется для отображения 
                                                         // последнего пункта в цепочке страниц
    
    var $error = '';                                     // Текст сообщения об ошибке
    
    var $btnSaveTitleAdd = 'Save';                  // Текст кнопки сохранения (в случае добавления новой записи)
    var $btnSaveTitleEdit = 'Save';                 // Текст кнопки сохранения (в случае редактирования записи)
    
    var $data = array();                                 // массив всех полей записи
    
    var $fields = '';                                    // html-код c формой для добавления/редактирования записи
    
        
    # public:
    var $db;     // объект для доступа к классу БД
    var $user;   // объект для доступа к классу текущего пользователя
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    function setParam($params, $value = '')
    {
        if (is_array($params))
            foreach($params AS $name => $val)
                $this->$name = $val;
        else   
            $this->$params = $value;
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    function getHTML()
    {
        $this->_CheckObj();
        $this->_CheckParam();
        global $javascript, $page;
        //----------------------------------------------------------------------
        $tpl = new Template;
        $tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/' . $this->file);
        $tpl->set_block('main', 'is_last_update', 'is_last_update_');
        $tpl->set_block('main', 'btn_save', 'btn_save_');
        //----------------------------------------------------------------------
        $tpl->set_var(array(
            // вывод ссылки для возвращения на предыдущую страницу
            'BACK_TITLE'  => $this->backTitle,
            'BACK_LINK'   => htmlspecialchars($this->backLink),
            'BACK_IMAGE'  => 'img/' . $this->backImage,
            // путь для возврата на туже страницу, с которой вошли
            'QUERY_STRING' => $this->id != -1 ? (isset($_COOKIE[$this->code . '_query_string']) ? ('&' . $_COOKIE[$this->code . '_query_string']) : (isset($_SESSION[$this->code . '_query_string']) ? ('&' . $_SESSION[$this->code . '_query_string']) : '')) : '',
            // вывод цепочки страниц
            'CHAIN'       => $this->is_chain ? get_chain($this->chainParentId, $this->chainCode) : '',
            // сообщение об ошибке
            'ERROR'       => $this->error
    	));
        //----------------------------------------------------------------------
        // продолжение вывода цепочки страниц
        $tpl->set_block('main', 'chain', 'chain_');
        foreach ($this->chain AS $title => $link)
        {
            $tpl->set_var('CHAIN_TITLE', !empty($link) ? ('<a href="' . $link . '">' . htmlspecialchars($title) . '</a>') : htmlspecialchars($title));
            $tpl->parse('chain_', 'chain', true);
        }
        //----------------------------------------------------------------------
        // вывод ссылок-действий
        $tpl->set_block('main', 'actions', 'actions_');
        foreach ($this->actions AS $title => $link)
        {
            if (!empty($link))
            {
                $tpl->set_var(array(
            		'ACT_TITLE' => htmlspecialchars($title),
                    'ACT_LINK'  => htmlspecialchars($link)
            	));
                $tpl->parse('actions_', 'actions', true);
            }
        }
        //----------------------------------------------------------------------
        // если происходит добавление записи
        if ($this->id == -1)
        {
            $tpl->set_var(array(
                'THIS_PAGE'        => THIS_PAGE,
                'ACTION'           => 'add_proc',
                'ID'               => -1,
                'CHAIN_TITLE_LAST' => htmlspecialchars($this->сhainLastTitle),
        		'HEAD_TITLE'       => htmlspecialchars($this->headTitleAdd)
        	));
            //------------------------------------------------------------------
            // вывод кнопки для сохранения
            if (!empty($this->btnSaveTitleAdd))
            {
                $tpl->set_var('BTN_SAVE_TITLE', htmlspecialchars($this->btnSaveTitleAdd));
                $tpl->parse('btn_save_', 'btn_save', false);
            }
        }
        // иначе, если происходит редактирование записи
        elseif (!empty($this->data))
        {
            $tpl->set_var(array(
                'THIS_PAGE'        => THIS_PAGE . '?page=' . $page,
                'ACTION'           => 'edit_proc',
                'ID'               => intval($this->data['ID']),
                'CHAIN_TITLE_LAST' => htmlspecialchars($this->data[$this->fieldForChain]),
        		'HEAD_TITLE'       => htmlspecialchars(preg_replace('/\[([a-z]+)\]/ie', '$this->data["$1"]', $this->headTitleEdit))
                
        	));
            //------------------------------------------------------------------
            // вывод кнопки для сохранения
            if (!empty($this->btnSaveTitleEdit))
            {
                $tpl->set_var('BTN_SAVE_TITLE', htmlspecialchars($this->btnSaveTitleEdit));
                $tpl->parse('btn_save_', 'btn_save', false);
            }
            //------------------------------------------------------------------
            // вывод информации о последнем обновлении записи
            if (!empty($this->data['Last_update_user']) && !empty($this->data['Last_update_date']) && !empty($this->data['Last_update_ip']))
            {
                $tpl->set_var(array(
            		'LAST_UPDATE_TITLE'   => htmlspecialchars($this->lu_title),
        		    'LAST_UPDATE_USER_ID' => get_login_by_id($this->data['Last_update_user']),
        		    'LAST_UPDATE_DATE'    => sql_to_datetime_nosec($this->data['Last_update_date']),
        		    'LAST_UPDATE_IP'      => long2ip($this->data['Last_update_ip'])
            	));
                $tpl->parse('is_last_update_', 'is_last_update', false);
            }
        }
        else
        {
            $tpl->set_var(array(
                'THIS_PAGE'        => $this->thisPage,
                'ACTION'           => 'edit_proc',
                'ID'               => intval($this->id),
                'CHAIN_TITLE_LAST' => htmlspecialchars($this->chainLastTitle),
        		'HEAD_TITLE'       => htmlspecialchars($this->headTitleEdit)
        	));
            if (!empty($this->btnSaveTitleEdit))
            {
                $tpl->set_var('BTN_SAVE_TITLE', htmlspecialchars($this->btnSaveTitleEdit));
                $tpl->parse('btn_save_', 'btn_save', false);
            }
        }
        //----------------------------------------------------------------------
        $tpl->set_var('FIELDS', $this->fields);
        //----------------------------------------------------------------------
        return $tpl->parse('_', 'main', true);
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    function getData($field = 'form', $code = '')
    {
        $r = array();
        
        if ($field != 'form')
        {
            if (!empty($_POST[$code]) && is_array($_POST[$code]))
            {
                $r = $_POST[$code];
            }
            else
            {
                $this->db->Query('SELECT * FROM ' . $this->table . '_' . $code . ' WHERE ' . $field . ' = ' . $this->id);
                for ($i = 1; $this->db->NextRecord(); $i++)
                    $r[$i] = $this->db->F(1);
            }
        }
        elseif (empty($_POST[$field]))
        {
            if (string_is_id($this->id))
            {
                $this->db->Query('SELECT * FROM ' . $this->table . ' WHERE ID = ' . $this->id);
                if ($this->db->NextRecord())
                    $r = $this->db->mRecord;
                else
                    location(PATH_TO_ADMIN . LANG . '/' . CODE . '/');
            }
        }
    	else
    	{
    		$r = $_POST[$field];
            if (!empty($_FILES))
            {
                foreach ($_FILES AS $image => $f)
                    if ($f['error'] != 4) $r[$image] = $_FILES[$image];
            }
    	}
    		
        return $r;
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    function _CheckObj()
    {
        if (!isset($this->db))
            die ('PslForm: Не определен объект для доступа к классу БД ($this->db)');
        //if (!isset($this->tpl))
        //    die ('PslForm: Не определен объект для доступа к классу шаблонов ($this->tpl)');
        //if (!isset($this->user))
        //    die ('PslForm: Не определен объект для доступа к классу текущего пользователя ($this->user)');
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    function _CheckParam()
    {
        foreach($this->actions AS $title => $link)
        {
            if (empty($title))
                die ('PslForm: Неверное значение переменной $this->actions');
        }
    }
    //--------------------------------------------------------------------------
}

?>