<?php
################################################################################
#                                                                              #
#   PhpSiteLib. Library for quick site development                             #
#                                                                              #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)                  #
#                                                                              #
#   psl_user.inc.php                                                           #
#   object for work with current user                                          #
#                                                                              #
################################################################################
/*

    bool Start(void)                                  - initialize current user. If TRUE successfully
  
    bool SetLogin(string login)                       - set current login. If TRUE successfully
  string GetLogin()                                   - return current login
  string GetGroup()                                   - return code of group (if groups is on)
     int GetGroupId()                                 - return id of group user
    bool SetGroup(string group_code)                  - set code of group (if groups is on)
     int GetId()                                      - get id current user
    bool IsAuthorized(void)                           - is user logged on?
  
   mixed Get(string paramName = '')                   - get value of parameter or hash with all parameters
    bool Set(mixed paramName, string paramValue = '') - set parameter or hash with all parameters. If TRUE successfully
  
    bool Login(string login, string pwd)              - try logged on. If TRUE successfully
    void Logout()                                     - logout from system

     int GetErrno()                                   - last error (code)
     int GetError()                                   - last error (message)

*/

define ('_PHPSITELIB_USER_LN', '_phpsitelibuser_login' . $_PHPSITELIB_USER_POSTFIX);
define ('_PHPSITELIB_USER_AN', '_phpsitelibuser_auth' . $_PHPSITELIB_USER_POSTFIX);
define ('E_PSL_USER_OK',        0);
define ('E_PSL_USER_STRLOGIN',  1);
define ('E_PSL_USER_STRPWD',    2);
define ('E_PSL_USER_NOTFOUND',  3);
define ('E_PSL_USER_NOLOGIN',   4);
define ('E_PSL_USER_NOTLOGGED', 5);
define ('E_PSL_USER_NOGP',      6);

class PslUser
{
    # options
    public $mUsersTable   = 'users_a';      // table with users
    public $mIdField      = 'ID';    // field with ID user
    public $mLoginField   = 'Login';  // field with login
    public $mPwdField     = 'Password';    // field with password
    public $mTimezone     = 'Timezone';  // Timezone
    public $mLang         = 'Lang';  // language
    public $mDeletedField = 'Hide';// field with flag "deleted", if empty not use
    public $mLastPageField = 'LastVisitPage';
    public $mOpenMenuId   = 'OpenMenuId';
    public $mPwdCrypt     = false;        // coding password with function PASSWORD
    public $mSecureDelay  = 3;            // count seconds delay if wrong login. If 0, not delay
    
    public $mGroupsTable  = 'groups';     // table with groups of users, if empty groups is not use
    public $mUGroupField  = 'Group_id';   // field in table users with link on group
    public $mGIdField     = 'ID';   // field ID in table with groups (which linked with users table)
    public $mGCodeField   = 'Code';  // field with code group (save in options)
    public $mGCodeAlias   = 'Code';  // name code of group in options
    public $mGAccessBackEnd  = 'Access_back_end'; // flag access to admin part
    public $mGAccessFrontEnd = 'Access_front_end'; // flag access to site
    
    public $mAcceptGroups = array();      // list accessable groups for autorization
    
    public $mAutoAcceptGroups = false;    // automatically select groups for autorization (use flag GroupAccessBackEnd)
    public $mAccessBackEnd = false;       // if use automatically selet groups, select groups in which
    public $mAccessFrontEnd = false;      // set fields GroupAccessBackEnd and GroupAccessFrontEnd
    public $mCheckAccessBackEnd = true;   // check access to admin
    public $mCheckAccessFrontEnd = true;  // check access to site
    
    # public:
    public $mDb;                          // object access DB
    
    # private (don't change it manually!):
    private $_Fetched = false;
    private $_Auth = false;
    private $_Login = '';    
    private $_Id = 0;
    private $_Props = array();
    private $_Errno = E_PSL_USER_OK;
    
    function GetLastPagePath()
    {
    	$this->_CheckDb();
    	
		$q = "SELECT $this->mLastPageField FROM $this->mUsersTable 
			  WHERE $this->mLoginField = '$this->_Login'";
		$this->mDb->Query($q);

		return $this->mDb->NextRecord() && $this->mDb->F($this->mLastPageField) ? $this->mDb->F($this->mLastPageField) : false;
	}
	
	function UpdateLastPagePath($code)
	{
		$this->_CheckDb();
		
		$q = "UPDATE $this->mUsersTable SET $this->mLastPageField = '$code' 
			  WHERE $this->mLoginField = '$this->_Login'";
		$this->mDb->Query($q);

		return true;
	}
    
    function Start()
    {
        if (isset($_SESSION[_PHPSITELIB_USER_LN], $_SESSION[_PHPSITELIB_USER_AN]) && $_SESSION[_PHPSITELIB_USER_AN])
            $this->_Auth = $this->SetLogin($_SESSION[_PHPSITELIB_USER_LN]);
        return $this->_Auth;
    }

    function SetLogin($login)
    {

        if ($login != $this->_Login) return $this->_FetchProps(false, $login);
    }
    
    function GetLogin()
    {
        return $this->_Login;
    }
    
    function SetGroup($code)
    {
        return $this->Set($this->mGCodeAlias, $code);
    }
    
    function GetGroup()
    {
        return $this->Get($this->mGCodeAlias);
    }
    
    function GetGroupId()
    {
        return $this->Get($this->mUGroupField);
    }
    
    function GetLang()
    {
        return $this->Get($this->mLang);
    }
    
    function GetTimezone()
    {
        return $this->Get($this->mTimezone);
    }
    
    function GetId()
    {
        return $this->_Id;
    }
    
    function IsAuthorized()
    {
        return $this->_Auth;
    }
    
    function GetErrno()
    {
        return $this->_Errno;
    }
    
    function GetError()
    {
        global $lang_words;
        $e = array(E_PSL_USER_OK         => $lang_words['E_PSL_USER_OK'],
                   E_PSL_USER_STRLOGIN   => $lang_words['E_PSL_USER_STRLOGIN'],
                   E_PSL_USER_STRPWD     => $lang_words['E_PSL_USER_STRPWD'], 
                   E_PSL_USER_NOTFOUND   => $lang_words['E_PSL_USER_NOTFOUND'], 
                   E_PSL_USER_NOLOGIN    => $lang_words['E_PSL_USER_NOTLOGIN'], 
                   E_PSL_USER_NOTLOGGED  => $lang_words['E_PSL_USER_NOTLOGGED'], 
                   E_PSL_USER_NOGP       => $lang_words['E_PSL_USER_NOGR'], #!
                  );
        return in_array($this->_Errno, array_keys($e)) ? $e[$this->_Errno] : $lang_words['E_PSL_USER_UNKNOWN'];
    }
    
    function Get($paramName = '')
    {
        if (!$this->_Fetched)
            if (!$this->_FetchProps())
                return false;
        return $paramName != '' ? $this->_Props[$paramName] : $this->_Props;
    }
    
    function Set($paramName, $paramValue = '')
    {
        $this->_CheckDb();
        
        // Lock all tables, which can use
        $this->mDb->Lock($this->mGroupsTable != '' ? array($this->mUsersTable, $this->mGroupsTable) : $this->mUsersTable);
        
        // check existing current user
        if (!$this->_Exists($this->_Login)) return false;
        
        // if entry parameter isn't array, transform it to array for unversaly
        if (!is_array($paramName)) $paramName = array($paramName => $paramValue);
        
        // form string for update and execute it
        $a = array();
        foreach ($paramName as $k => $v)
            if (string_is_login($k) && $k != $this->mGCodeAlias) {
                $v = addslashes($v);
                $a[] = $k == $this->mPwdField && $this->mPwdCrypt ? "$k = password('$v')" : "$k = '$v'";
            }
                    
        if (count($a)) {
            $q = 'update ' . $this->mUsersTable . 
                 ' set ' . implode(', ', $a) . 
                 ' where ' . $this->mLoginField . ' = "' . $this->_Login . '"';
            $this->mDb->Query($q);
        }
        
        // if in parameters was specify group, then update it:
        // get ID group and update user
        if ($this->mGroupsTable != '' && in_array($this->mGCodeAlias, array_keys($paramName)) && string_is_login($paramName[$this->mGCodeAlias])) {
        
            $q = 'select ' . $this->mGIdField . 
                 ' from ' . $this->mGroupsTable . 
                 ' where ' . $this->mGCodeField . ' = "' . $paramName[$this->mGCodeAlias] . '"';
            $this->mDb->Query($q);
            
            if ($this->mDb->NextRecord())
			{
                $q = 'update ' . $this->mUsersTable . 
                     ' set ' . $this->mUGroupField . ' = ' . $this->mDb->F(0) . 
                     ' where ' . $this->mLoginField . ' = "' . $this->_Login . '"';
                $this->mDb->Query($q);
            }
        }
        
        $this->mDb->Unlock();
        
        $this->_Fetched = false;
        return true;
    }
    
    # checking login and password, if ok then logged on, else turn off
    function Login($login, $pwd)
    {
        eval('global $' . _PHPSITELIB_USER_LN .', $' . _PHPSITELIB_USER_AN . ';');
        $this->Logout();
        if ($this->_FetchProps(true, $login, $pwd))
		{
            $this->_Auth  = true;
            eval('$' . _PHPSITELIB_USER_LN . ' = "' . $this->_Login . '";');
            eval('$' . _PHPSITELIB_USER_AN . ' = "' . $this->_Auth . '";');

	     	$_SESSION[_PHPSITELIB_USER_LN] = $this->_Login;
	     	$_SESSION[_PHPSITELIB_USER_AN] = $this->_Auth;
        }
		elseif ($this->mSecureDelay)
        {
            sleep($this->mSecureDelay);
        }
        return $this->_Errno == E_PSL_USER_OK;
    }
    
    # reset all statuses in zero and clear session
    function Logout()
    {
        if (!$this->_Auth)
        {
            $this->_Errno = E_PSL_USER_NOTLOGGED;
        }
        else
        {
        	$this->UpdateLastPagePath('');
            eval('global $' . _PHPSITELIB_USER_LN .', $' . _PHPSITELIB_USER_AN . ';');
            unset($_SESSION[_PHPSITELIB_USER_LN], $_SESSION[_PHPSITELIB_USER_AN]);
            $this->_Fetched = false;
            $this->_Auth = false;
            $this->_Login = '';
            $this->_Id = 0;
            $this->_Errno = E_PSL_USER_OK;
        }
        return $this->_Errno == E_PSL_USER_OK;
    }
    
    # if $login, then check login with password. If not fount return false. If ok sets current login and id
    function _FetchProps($check_pwd = false, $login = '', $pwd = '')
    {
        $this->_CheckDb();
        $this->_Fetched = false;
        $this->_Errno = E_PSL_USER_OK;
        $this->_FetchAcceptGroups();
        if ($login == '') $login = $this->_Login;
        
        if ($login == '')                         $this->_Errno = E_PSL_USER_NOLOGIN;
        if (!string_is_login($login))             $this->_Errno = E_PSL_USER_STRLOGIN;
        if ($pwd != '' && !string_is_login($pwd)) $this->_Errno = E_PSL_USER_STRPWD;
        if ($this->_Errno != E_PSL_USER_OK) return false;

        if ($this->mGroupsTable != '')
        {
            if ($this->mUGroupField == '' || $this->mGroupsTable == '' || $this->mGIdField == '' || 
                $this->mGCodeField == '' || $this->mGCodeAlias == '')
            {
                $this->_Errno = E_PSL_USER_NOGP;
                return false;
            }
			else
			{
                $gtbl  = $this->mGroupsTable; $ugrpid = $this->mUGroupField; $gid = $this->mGIdField;     
                $gcode = $this->mGCodeField;  $alias  = $this->mGCodeAlias;
            }
        }
        
        // forming string for query, executeing query
        $pwds = $this->mPwdCrypt ? 'password("' . $pwd . '")' : '"' . $pwd . '"';
                                        $q = 'SELECT u.*';
        if ($this->mGroupsTable != '')  $q .= ", g.$gcode AS $alias";
                                        $q .= ' FROM ' . $this->mUsersTable . ' u';
        if ($this->mGroupsTable != '')  $q .= " INNER JOIN $gtbl g on g.$gid = u.$ugrpid";
                                        $q .= ' WHERE u.' . $this->mLoginField . ' = "' . $login . '"';
        if ($check_pwd && $pwds != '')  $q .= ' AND u.' . $this->mPwdField . ' = ' . $pwds;
        if ($this->mDeletedField != '') $q .= ' AND u.' . $this->mDeletedField . ' = 0';
        
        /*if ($this->mGroupsTable != '')
        {
            $ag = array();
            foreach ($this->mAcceptGroups as $g) $ag[] = '"' . addslashes($g) . '"';
            $q .= ' AND g.' . $this->mGCodeField . ' in (' . (count($this->mAcceptGroups) ? implode(', ', $ag) : '""') . ')';
        }*/
        //echo $q; exit;
        $this->mDb->Query($q);
        
        // get parameters
        if (!$this->mDb->NextRecord())
        {
            $this->_Errno = E_PSL_USER_NOTFOUND;
        }
        else
        {
            $this->_Props   = $this->mDb->mRecord;
            $this->_Login   = $login;
            $this->_Id      = $this->mDb->F($this->mIdField);
            $this->_Fetched = true;
        }
        return $this->_Errno == E_PSL_USER_OK;
    }
    
    # checking turn on DB
    function _CheckDb()
    {
        global $lang_words;
        if (!isset($this->mDb)) die ($lang_words['E_PSL_USER_DB']);
    }
    
    # check existing user by login
    function _Exists($login)
    {
        $this->_CheckDb();
        if (!string_is_login($login)) {
            $this->_Errno = E_PSL_USER_NOLOGIN;
            return false;
        }
        $this->mDb->Query('select * from ' . $this->mUsersTable . ' where ' . $this->mLoginField . ' = "' . $login . '"');
        $this->_Errno = $this->mDb->Nf() ?  E_PSL_USER_OK : E_PSL_USER_NOTFOUND;
        return $this->_Errno == E_PSL_USER_OK;
    }
    
    # if set flag $mAutoAcceptGroups, then array $mAcceptGroups load automatically by fag GroupAccessAdmin
    
    function _FetchAcceptGroups()
    {
        $this->_CheckDb();
        if ($this->mAutoAcceptGroups)
        {
            $q = 'select ' . $this->mGCodeField . ' from ' . $this->mGroupsTable;
            if (($this->mCheckAccessBackEnd || $this->mCheckAccessFrontEnd) && ($this->mAccessFrontEnd || $this->mAccessBackEnd)) $q .= ' where ';
            if ($this->mCheckAccessBackEnd && $this->mAccessBackEnd) $q .= $this->mGAccessBackEnd . ' = ' . ($this->mAccessBackEnd ? '1' : '');
            if ($this->mCheckAccessBackEnd && $this->mCheckAccessFrontEnd && $this->mAccessFrontEnd && $this->mAccessBackEnd) $q .= ' and ';
            if ($this->mCheckAccessFrontEnd && $this->mAccessFrontEnd) $q .= $this->mGAccessFrontEnd . ' = ' . ($this->mAccessFrontEnd ? '1' : '');

            $this->mDb->Query($q);
            $this->mAcceptGroups = array();
            while ($this->mDb->NextRecord()) $this->mAcceptGroups[] = $this->mDb->F(0);
        }
    }
}

?>