//---------------------------------------------------------------------------------
$(document).ready(function(){//----------------------------------------------------
//---------------------------------------------------------------------------------
$("#container").clickCarousel({margin: 0, shiftOnly:true});

$("a[rel='photo']").fancybox();

$('#container img').click(function()
{
    $('#photo_main_img').attr('src', $(this).attr('src'));
});

$('#top-menu li').mouseover(function()
{
    var top_menu_id = $(this).attr('top-menu-id');
    var left = getElementPosition($(this).get(0)).left;
    $('#top-submenu-' + top_menu_id).css('left', left).show().siblings('div.submenu-block').hide();
});
$('div.submenu-block').mouseover(function()
{
    $(this).show();
});
$('div.submenu-block').mouseout(function()
{
    $(this).hide();
});

$('#top_search').focus(function()
{
    if($(this).val()=='Поиск...')
        $(this).val('');
});
$('#top_search').blur(function()
{
    if($(this).val()=='')
        $(this).val('Поиск...');
});

// ссылки для английской версии
var lang = $('#lang').attr('href') == 'en/' ? 'ru' : 'en';
var lang2 = $('#lang').attr('href') == 'en/' ? 'en' : 'ru';
updateLinks();
if (location.pathname != '/')
    $('#lang').attr('href', location.pathname.replace(lang + '/', lang2 + '/') + location.search);

//


//---------------------------------------------------------------------------------
});//------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
function updateLinks()
{
    // ссылки для английской версии
    var lang = $('#lang').attr('href') == 'en/' ? 'ru' : 'en';
    var lang2 = $('#lang').attr('href') == 'en/' ? 'en' : 'ru';
    
    $('a').each(function()
    {
        if (!($(this).attr('id') == 'lang' || $(this).attr('id') == 'back_to_top' || $(this).attr('href') == undefined || $(this).attr('href').substring(0, 10) == 'javascript'))
        {
            var temp = $(this).attr('href').substring(0, 3);
            if (temp != 'ru/' && temp != 'en/')
            {
                var href = $(this).attr('href').substring(0, 1) == '/' ? lang : (lang + '/');
                $(this).attr('href', href + $(this).attr('href'));
            }   
        }
    });
}

function ajax_get_objects(section_id)
{
    //сколько объектов уже загружено
    var cnt = $('div.record_ajax').length;
    //url по котрому доступен get_objects и передаем ему page
    var url = '/actions.php?add_index_records&count=' + cnt + '&section_id=' + section_id;
    $.ajax(
    {
        type: "GET",
        url: url,
        dataType: "json",
        success: function (data)
        {
            //alert(data.records);
            $('div.record_ajax').eq(cnt-1).after(data.records);
            updateLinks();
        }
    });
}


(function(){
var special = jQuery.event.special,
uid1 = 'D' + (+new Date()),
uid2 = 'D' + (+new Date() + 1);


special.scrollstart = {
setup: function() {


var timer,
handler = function(evt) {

var _self = this,
_args = arguments;


if (timer) {
clearTimeout(timer);
} else {
evt.type = 'scrollstart';
jQuery.event.handle.apply(_self, _args);
}


timer = setTimeout( function(){
timer = null;
}, special.scrollstop.latency);


};


jQuery(this).bind('scroll', handler).data(uid1, handler);


},
teardown: function(){
jQuery(this).unbind( 'scroll', jQuery(this).data(uid1) );
}
};


special.scrollstop = {
latency: 300,
setup: function() {


var timer,
handler = function(evt) {


var _self = this,
_args = arguments;


if (timer) {
clearTimeout(timer);
}


timer = setTimeout( function(){


timer = null;
evt.type = 'scrollstop';
jQuery.event.handle.apply(_self, _args);


}, special.scrollstop.latency);


};


jQuery(this).bind('scroll', handler).data(uid2, handler);


},
teardown: function() {
jQuery(this).unbind( 'scroll', jQuery(this).data(uid2) );
}
};


})();



function togglePeoplesList()
{
    $('#peoples_list').toggle();
}

