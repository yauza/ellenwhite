//---------------------------------------------------------------------------------
function ltrim(txt)
{
  var spacers = " \t\r\n";
  while (txt.length>0 && spacers.indexOf(txt.charAt(0)) != -1)
  {
    txt = txt.substr(1);
  }
  return(txt);
}
function rtrim(txt)
{
  var spacers = " \t\r\n";
  while (txt.length>0 && spacers.indexOf(txt.charAt(txt.length-1)) != -1)
  {
    txt = txt.substr(0,txt.length-1);
  }
  return(txt);
}
function trim(txt)
{
  return(ltrim(rtrim(txt)));
}
function strip_tags(str)
{
	return str.replace(/<\/?[^>]+>/gi, '');
}
function addslashes(str)
{  
    return str.replace('/(["\'\])/g', "\\$1").replace('/\0/g', "\\0");  
}
function rand(min, max)
{
    if(max)
        return Math.floor(Math.random() * (max - min + 1)) + min;
    else
        return Math.floor(Math.random() * (min + 1));
}
//---------------------------------------------------------------------------------
// координаты и размеры элемента на странице
function getElementPosition(elm)
{
    var elem = typeof(elm) == 'string' ? document.getElementById(elm) : elm;
	
    var w = elem.offsetWidth;
    var h = elem.offsetHeight;
	
    var l = 0;
    var t = 0;
	
    while (elem)
    {
        l += elem.offsetLeft;
        t += elem.offsetTop;
        elem = elem.offsetParent;
    }

    return {"left":l, "top":t, "width": w, "height":h};
}
//---------------------------------------------------------------------------------
// координаты указателя мыши
function getMousePosition(event) {
    var x = y = 0;
    if (document.attachEvent != null) { // Internet Explorer & Opera
        x = window.event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        y = window.event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    } else if (!document.attachEvent && document.addEventListener) { // Gecko
        x = event.clientX + window.scrollX;
        y = event.clientY + window.scrollY;
    } else {
        // Do nothing
    }
    return {x:x, y:y};
}
//---------------------------------------------------------------------------------
// предварительная загрузка изображений
function preload(images)
{
    $('<div></div>').css('display', 'none').appendTo('body').html('<img src="' + images.join('"><img src="') + '" />');
}
//---------------------------------------------------------------------------------
// кодирование данных формы для отсылки на сервер
// var sBody = getRequestBody(oForm);
function getRequestBody(form)
{
	var params = new Array();
	for (var i = 0; i < form.elements.length; i++)
	{
		//alert(form.elements[i].attributes.getNamedItem('type').value);
		var param = encodeURIComponent(form.elements[i].name);
		param += '=';
		if (form.elements[i].attributes && form.elements[i].getAttribute('type') == 'checkbox')
		{
			if (form.elements[i].checked) param += '1'; else param += '0';
		}
		else
		{
			param += encodeURIComponent(form.elements[i].value);
		}
		params.push(param);
	}
	return params.join('&');
}
//---------------------------------------------------------------------------------
// насколько прокручена странца вниз
function getBodyScrollTop()
{
  return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}
// насколько прокручена странца вправо
function getBodyScrollLeft()
{
  return self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft);
}
//---------------------------------------------------------------------------------
function getClientWidth()
{
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
}
//---------------------------------------------------------------------------------
function getClientHeight()
{
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}
//---------------------------------------------------------------------------------
function sizeOf(Array)
{
	var count = 0;
	
	for (var i in Array)
		count++;
	
	return count;
}
