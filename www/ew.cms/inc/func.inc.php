<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   common functions                                                    #
#                                                                       #
#########################################################################


//---------------------------------------------------------------------------
function get_peoples_form($record_id, $record_code)
{
    global $db;

    $t = new Template;
    $t->set_file('main', PATH_TO_TEMPLATES . 'block_peoples.html');
    $t->set_block('main', 'people', 'people_');

    $peoples = array();
    if (isset($_POST['peoples']) && is_array($_POST['peoples']))
    {
        foreach ($_POST['peoples'] AS $i => $people)
            $peoples[$i] = array('Profession_id' => intval($_POST['peoples_prof'][$i]), 'People' => $people);
        $i++;
    }
    else
    {
        $db->Query('SELECT pr.Profession_id, p.FioRus FROM ru_peoples AS p INNER JOIN ru_peoples_records AS pr ON p.ID = pr.People_id WHERE pr.Record_id = ' . $record_id . ' AND pr.Record_code = "' . $record_code . '"');
        for ($i = 1; $db->NextRecord(); $i++)
            $peoples[$i] = array('Profession_id' => $db->F('Profession_id'), 'People' => $db->F('FioRus'));
    }

    if (empty($peoples))
        $peoples[$i++] = array('Profession_id' => 0, 'People' => '');

    $t->set_var('NUM', $i);

    foreach ($peoples AS $num => $people)
    {
        $t->set_var(array(
            'Num'         => $num,
            'People'      => htmlspecialchars($people['People']),
            'Professions' => get_field_select(array($num => $people['Profession_id'])/*form*/, $num/*field*/, LANG . '_professions'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'OrderBy'/*orderBy*/, 220/*width*/, false/*is required*/, 'peoples_prof'/*field_name*/)
        ));
        $t->parse('people_', 'people', true);
    }

    return $t->parse('_', 'main', false);
}
//---------------------------------------------------------------------------
//
function item_props($item_id)
{
    global $db;
	$tpl = new Template;
    $tpl->set_file('main', PATH_TO_TEMPLATES . 'block_store_item_props.html');
    
    $tpl->set_block('main', 'prop', 'prop_');
    $tpl->set_block('main', 'item_prop', 'item_prop_');
    $tpl->set_block('item_prop', 'value', 'value_');
    $tpl->set_block('item_prop', 'edit_value', 'edit_value_');
    $tpl->set_block('main', 'add_prop', 'add_prop_');
    
    $tpl->set_var('ITEM_ID', $item_id);
    
    // sp1 - items, sp2 - props or count, sp3 - values
    
    $props = array();
    $db->Query('SELECT sp2.Value FROM ru_store_props AS sp1 INNER JOIN ru_store_props AS sp2 ON sp1.ID = sp2.Parent_id WHERE sp1.Code = "items" AND sp2.Code = "props" AND sp1.Value = "' . $item_id . '" GROUP BY sp2.Value ORDER BY sp2.Value');
    while ($db->NextRecord())
    {
        $props[] = $db->F('Value');
        $tpl->set_var('PROP_TITLE', htmlspecialchars($db->F('Value')));
        $tpl->parse('prop_', 'prop', true);
        $tpl->parse('add_prop_', 'add_prop', true);
    }
    
    $items = array();
    $db->Query('SELECT ID FROM ru_store_props WHERE Code = "items" AND Value = "' . $item_id . '"');
    while ($db->NextRecord()) $items[] = $db->F('ID');
    foreach ($items AS $item)
    {
        $tpl->set_var('ITEM', $item);
        
        $db->Query('SELECT sp2.Value FROM ru_store_props AS sp2 WHERE sp2.Code = "cost" AND sp2.Parent_id = ' . intval($item) . '');
        $tpl->set_var('COST', $db->NextRecord() ? intval($db->F('Value')) : 0);
        
        $db->Query('SELECT sp2.Value FROM ru_store_props AS sp2 WHERE sp2.Code = "count" AND sp2.Parent_id = ' . intval($item) . '');
        $tpl->set_var('COUNT', $db->NextRecord() ? intval($db->F('Value')) : 0);
        
        $values = array();
        $db->Query('SELECT sp2.Value AS Prop, sp3.Value FROM ru_store_props AS sp2 INNER JOIN ru_store_props AS sp3 ON sp2.ID = sp3.Parent_id WHERE sp2.Code = "props" AND sp3.Code = "values" AND sp2.Parent_id = "' . $item . '" ORDER BY sp2.Value');
        while ($db->NextRecord()) $values[$db->F('Prop')] = $db->F('Value');
        foreach ($props AS $prop)
        {
            $tpl->set_var(array(
                'PROP'  => $prop,
                'VALUE' => isset($values[$prop]) ? htmlspecialchars($values[$prop]) : ''
            ));
            $tpl->parse('value_', 'value', true);
            $tpl->parse('edit_value_', 'edit_value', true);
        }
        
        $tpl->parse('item_prop_', 'item_prop', true);
        $tpl->set_var('value_', '');
        $tpl->set_var('edit_value_', '');
    }
    
    return $tpl->parse('_', 'main', false);
}
//---------------------------------------------------------------------------
// добаление действия в лог
function add_to_log($code, $_id)
{
/* 
	global $g_user, $db;
	if ($g_user->GetLogin() == 'yauza') return;
	$file = PATH_TO_ROOT . '../logs/' . date('ym') . '.log';
	if (strpos(' '.$code , '_del'))
	{
		$code = str_replace('_del', $code == 'ban_del' ? '' : '_list', $code);
		$flag = true;
	}
	else
		$flag = false;
	$db->Query('SELECT ID FROM pages WHERE Code = "' . addslashes($code) . '"');
	$str = date('j.H:i:s') . ' ' . $g_user->GetId() . ' ' . ($db->NextRecord() ? (($flag ? '-' : '') . $db->F('ID')) : '0') . ' ' . $_id . "\n";
	
    if (fileowner($file) != getmyuid()) chmod($file, 0777);
	$f = fopen($file, 'a'); fputs($f, $str); fclose($f);
*/
}
//---------------------------------------------------------------------------
// удаление записей
function record_del($table, $id, $images = array(), $and_delete = array())
{
	global $db;
    
    foreach ($images AS $img)
    {
        if (is_dir($img))
            del_dir($img);
        else
        {
            $image = is_image(PATH_TO_IMG . CODE . '/' . $img);
            if ($image)
                unlink($image);
        }
    }
    
	if ($db->Query('DELETE FROM ' . $table . ' WHERE ID = ' . $id))
    {
        if (in_array($table, array('ru_videos', 'ru_photos', 'ru_news', 'ru_rightholders', 'ru_sitemap', 'ru_peoples')))
            add_to_log(CODE . '_del', $id);
        
        foreach ($and_delete AS $del)
            $db->Query('DELETE FROM ' . $del['table'] . ' WHERE ' . $del['field'] . ' = ' . $id);
        return '';
    }
    else
        return 'Error delete.';
	//add_to_log(CODE . '_del', $id);
}
//-------------------------------------------------------------------------------------------
// редактирование bool поля
function record_change_bool_field($table, $id, $field)
{
    global $db;
    
	$db->Query('SELECT ' . $field . ' FROM ' . $table . ' WHERE ID = ' . $id);
	if ($db->NextRecord())
		$db->Query('UPDATE ' . $table . ' SET ' . $field . ' = "' . ($db->F($field) == '1' ? '0' : '1') . '" WHERE ID = ' . $id);
    
	return 'ok';
}
//-------------------------------------------------------------------------------------------
// получение списка страниц
// $page           -  текущая страница
// $count          -  общее количество записей
// $count_on_page  -  количество записей на странице
function get_block_pages($page, $count, $count_on_page, $code)
{
	global $db, $LIMITS;
	$tpl = new Template;
    $tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/block_pages.html');
    
    switch ($code)
    {
		case 'pages': $tpl->set_var('THIS_PAGE', ADM . '/' . $code . '/'); break;
		default: $tpl->set_var('THIS_PAGE', LANG . '/' . $code . '/');
	}
    
    //-------------------------------------------------------------------
    $tpl->set_block('main', 'change_info', 'change_info_');
    
    $query_str = $_SERVER['QUERY_STRING'];
    if (isset($_GET['limit']))
    {
		$query_str = str_replace('limit=' . $_GET['limit'] . '&', '', $_SERVER['QUERY_STRING']);
		if ($query_str == $_SERVER['QUERY_STRING'])
		$query_str = str_replace('&limit=' . $_GET['limit'], '', $_SERVER['QUERY_STRING']);
		if ($query_str == $_SERVER['QUERY_STRING'])
		$query_str = str_replace('limit=' . $_GET['limit'], '', $_SERVER['QUERY_STRING']);
	}
	if (trim($query_str) != '') $query_str .= '&';
	
	if ($code == 'pages' && !strpos(' '.$query_str, 'lang')) $query_str .= 'lang=' . (isset($_GET['lang']) && preg_match('/^[a-z_]+$/i', $_GET['lang']) ? $_GET['lang'] : 'ru') . '&';
    
    $tpl->set_var('QUERY_STRING_NO_LIMIT', str_replace('&', '&amp;', $query_str));
    
	if ($count > $LIMITS[0])
	{
		$tpl->set_block('change_info', 'limit', 'limit_');
		foreach ($LIMITS AS $v)
		{
			$tpl->set_var(array(
				'LIMIT'       => ($count_on_page == $v || ($count_on_page == 999999999 && $v == 'все')) ? ' selected' : '',
				'LIMIT_TITLE' => $v,
				'LIMIT_VALUE' => $v == 'все' ? 999999999 : $v
			));
			$tpl->parse('limit_', 'limit', true);
		}	
		$tpl->parse('change_info_', 'change_info', false);
	}
	else
		return '';
    //-------------------------------------------------------------------
    
    $tpl->set_block('main', 'page', 'page_');
    $tpl->set_block('page', 'page_active', 'page_active_');
    $tpl->set_block('page', 'page_active_no', 'page_active_no_');

    $count_pages = ceil($count / $count_on_page); // количество страниц
    //if ($count_pages < 2) return '';
    
    //print_r($GLOBALS);
    $query_str = $_SERVER['QUERY_STRING'];
    if (isset($_GET['page']))
    {
		$query_str = str_replace('page=' . $_GET['page'] . '&', '', $_SERVER['QUERY_STRING']);
		if ($query_str == $_SERVER['QUERY_STRING'])
		$query_str = str_replace('&page=' . $_GET['page'], '', $_SERVER['QUERY_STRING']);
		if ($query_str == $_SERVER['QUERY_STRING'])
		$query_str = str_replace('page=' . $_GET['page'], '', $_SERVER['QUERY_STRING']);
	}
	if (trim($query_str) != '') $query_str .= '&';
    
    if ($code == 'pages' && !strpos(' '.$query_str, 'lang')) $query_str .= 'lang=' . (isset($_GET['lang']) && preg_match('/^[a-z_]+$/i', $_GET['lang']) ? $_GET['lang'] : 'ru') . '&';
    
    $tpl->set_var('QUERY_STRING', str_replace('&', '&amp;', $query_str));
    
    $first = $page - 9;
    $last = $page + 9;
    
    // получение '<' для перемещения на страницу назад
    if ($page > 1)
    {
		$tpl->set_var(array(
			'PAGE_NUM_TITLE' => '&lt;&nbsp;prev',
			'PAGE_NUM_LINK'  => $page - 1,
			'SEPARATOR'      => '&nbsp;| '
        ));
		$tpl->parse('page_active_no_', 'page_active_no', false);
		$tpl->parse('page_', 'page', true);
	}
    
	//получение блока с активной страницей
	if ($count_pages > 1)
	for ($i = $first; $i <= $last; $i++)
    {
		if ($i > 0 && $i <= $count_pages)
		{
			$tpl->set_var(array(
				'page_active_'    => '',
				'page_active_no_' => '',
				'PAGE_NUM_TITLE'  => $i,
				'PAGE_NUM_LINK'   => $i,
				'SEPARATOR'       => '&nbsp;| '
        	));
        	if ($i == $page) $tpl->parse('page_active_', 'page_active', false);
        	else $tpl->parse('page_active_no_', 'page_active_no', false);
        	$tpl->parse('page_', 'page', true);
		}
	}
    
    // получение '>' для перемещения на страницу вперед
    if ($page < $count_pages)
    {
		$tpl->set_var(array(
			'PAGE_NUM_TITLE' => 'next&nbsp;&gt;',
			'PAGE_NUM_LINK'  => $page + 1,
			'SEPARATOR'      => ''
        ));
        $tpl->parse('page_active_no_', 'page_active_no', false);
		$tpl->parse('page_', 'page', true);
	}
    
    return $tpl->parse('_', 'main', false);
}
//----------------------------------------------------------------------------------------
// получение цепочки страниц
function get_chain($parent_id = '', $code = '')
{
	global $db;
	$table = strpos(CODE, 'store') === 0 ? (LANG . '_store') : (LANG == 'adm' || defined('ADM') ? 'adm' : (LANG == 'opt' ? 'options_list' : LANG . '_sitemap'));
	
	$tpl = new Template;
	$tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/block_chain.html');
	$tpl->set_block('main', 'chain', 'chain_');
    
    if (strpos(CODE, 'store') === 0)
        $back_to_main = LANG . '/store/';
    elseif (LANG != 'opt' && LANG != 'adm')
        $back_to_main = LANG . '/content/';
    else
        $back_to_main = LANG . '/';
    
	$tpl->set_var(array(
		'BACK_TO_MAIN' => $back_to_main,
		'MAIN_TITLE'   => strpos(CODE, 'store') === 0 ? 'Магазин' : (LANG == 'adm' || defined('ADM') ? 'Adm' : (LANG == 'opt' ? 'Options' : _PHPSITELIB_SITE))
	));
	$chain = array();
	if (!$parent_id && !(LANG == 'adm' || defined('ADM')))
	{
		$db->Query('SELECT Parent_id FROM ' . $table . ' WHERE Code = "' . (!$code ? CODE : $code) . '"');
		$parent_id = $db->NextRecord() ? intval($db->F('Parent_id')) : 0;
	}
	
	for ($i = 1; $db->Query('SELECT Parent_id, Title, Code FROM ' . $table . ' WHERE ID = ' . $parent_id) && $db->NextRecord(); $i++) // считывание цепочки из базы (в обратном порядке)
	{
		if ($db->F('Parent_id') == 0) break;
		switch ($db->F('Code'))
		{
			case 'none': $chain[$i]['Href'] = $back_to_main; break;
			case 'store_items': $chain[$i]['Href'] = LANG . '/store_items/'; break;
			case 'foto': $chain[$i]['Href'] = LANG . '/foto/?foto=' . $parent_id; break;
			default: $chain[$i]['Href'] = LANG . '/' . $db->F('Code') . '/?a=edit&amp;id=' . $parent_id; break;
		}
		$chain[$i]['Title'] = htmlspecialchars($db->F('Title'));
		$parent_id = $db->F('Parent_id');
	}
	for ($j = $i - 1; $j != 0; $j--) // вывод цепочки задом-наперед
	{
		$tpl->set_var(array(
			'CHAIN_HREF'  => $chain[$j]['Href'],
			'CHAIN_TITLE' => $chain[$j]['Title']
		));
		$tpl->parse('chain_', 'chain', true);
	}
	
	return $tpl->parse('_', 'main', false);
}
//----------------------------------------------------------------------------------------
// получение количества записей на странице
function get_count_on_page()
{
	global $LIMITS;
	if (isset($_GET['limit']) && intval($_GET['limit']))
	{
		setcookie(CODE.'_count_on_page', intval($_GET['limit']), time() + 31536000, '/', '.' . $_SERVER['HTTP_HOST']);
		return intval($_GET['limit']);
	}
	elseif (isset($_COOKIE[CODE.'_count_on_page']))
		return intval($_COOKIE[CODE.'_count_on_page']);
	else
		return $LIMITS[0];
}
//----------------------------------------------------------------------------------------
// получение поля типа date
function get_field_date($field, $value, $is_required = true, $is_time = false)
{
	if ($is_time)
		return '<input class="field w' . ($is_time ? '110' : '70') . ($is_required ? (' required_date' . ($is_time ? 'time' : '')) : '') . '" name="form[' . $field . ']" value="' . $value . '" id="' . $field . '" maxlength="' . ($is_time ? '16' : '10') . '"><img src="include/calendar/cal.gif" id="tr_' . $field . '" alt="editor-date" title="editor-date" onmouseover="this.style.background=\'red\';" onmouseout="this.style.background=\'\'"><script type="text/javascript">Calendar.setup({inputField:"' . $field . '",ifFormat:"%d.%m.%Y' . ($is_time ? ' %H:%M' : '') . '",showsTime:' . ($is_time ? 'true' : 'false') . ',button:"tr_' . $field . '",timeFormat:"24",align:"Tl",singleClick:false});</script>';
	else
		return '<script type="text/javascript">$(function(){$(\'#' . str_replace('.', '_', $field) . "').datepicker({ dateFormat: 'dd.mm.yy', firstDay: 1, dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat'], monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'Jule', 'August', 'September', 'October', 'November', 'December'], nextText: 'next', prevText: 'prev' });});</script><input class=\"field w70" . ($is_required ? ' required_date' : '') . '" name="form[' . $field . ']" value="' . $value . '" id="' . str_replace('.', '_', $field) . '" maxlength="10">';
}
//----------------------------------------------------------------------------------------
// получение поля типа select
function get_field_select($form, $field, $table, $title, $parent, $where, $orderBy, $width = '', $is_required = false, $field_name = 'form')
{
	global $db;
    if ($width == '') $width = 150;
	
    $str = array();
   	$str[] = "\n  " . '<select class="w' . $width . ($is_required ? ' required' : '') . '" name="' . $field_name . '[' . $field . ']">';
    
    if ($parent == 0)
    {
    	$str[] = '<option value="0"></option>';
    	
        //echo '<br><br>' . 'SELECT ID, ' . $title . ' FROM ' . $table . ($where ? (' WHERE ' . $where) : '') . ' ORDER BY ' . $orderBy;
    	$db->Query('SELECT ID, ' . $title . ' FROM ' . $table . ($where ? (' WHERE ' . $where) : '') . ' ORDER BY ' . $orderBy);
        while ($db->NextRecord())
        	$str[] = '   <option value="' . $db->F('ID') . '"' . (isset($form[$field]) && $form[$field] == $db->F('ID') ? ' selected' : '') . '>' . htmlspecialchars($db->F($title)) . "</option>";
    }
    else
    {
    	$str[] = '<option value="0"></option>';
    	
        $optiongroups = array();
        $db->Query('SELECT ID, ' . $title . ' FROM ' . $table . ' WHERE Parent_id = ' . $parent . ($where ? (' AND ' . $where) : '') . ' ORDER BY ' . $orderBy);
        while ($db->NextRecord()) $optiongroups[] = $db->mRecord;
        
        foreach ($optiongroups AS $optiongroup)
        {
            $str[] = '<optgroup label="' . htmlspecialchars($optiongroup[$title]) . '">';
            $db->Query('SELECT ID, ' . $title . ' FROM ' . $table . ' WHERE Parent_id = ' . $optiongroup['ID'] . ($where ? (' AND ' . $where) : '') . ' ORDER BY ' . $orderBy);
            while ($db->NextRecord())
            	$str[] = '<option value="' . $db->F('ID') . '"' . (isset($form[$field]) && $form[$field] == $db->F('ID') ? ' selected' : '') . '>' . htmlspecialchars($db->F($title)) . "</option>";
             $str[] = '</optgroup>';
        }
    }
    $str[] = '  </select>';
	
	return implode("\n", $str) . "\n";
}
//----------------------------------------------------------------------------------------
// получение поля типа months
function get_field_month($form, $field, $table, $is_required = false)
{
	global $db;
	
    $str = array();
   	$str[] = "\n  " . '<select class="w120' . ($is_required ? ' required' : '') . '" name="form[' . $field . ']">';

	$str[] = '<option value="0"></option>';
	
	$db->Query('SELECT MONTH(MIN(' . $field . ')) AS min_month, YEAR(MIN(' . $field . ')) AS min_year, MONTH(MAX(' . $field . ')) AS max_month, YEAR(MAX(' . $field . ')) AS max_year FROM ' . $table);
    if ($db->NextRecord())
    {
        $year = intval($db->F('min_year'));
        $month = intval($db->F('min_month'));
        $max_year = intval($db->F('max_year'));
        $max_month = intval($db->F('max_month'));
        
        while ($year < $max_year || ($year == $max_year && $month <= $max_month))
        {
            $mmonth = $month < 10 ? ('0' . $month) : $month;
            
            $str[] = '   <option value="' . $year . '-' . $mmonth . '"' . (isset($form[$field]) && $form[$field] == $year . '-' . $mmonth ? ' selected' : '') . '>' . $year . ' ' . get_month_name_simple($month) . "</option>";
            
            $year = $month == 12 ? ($year + 1) : $year;
            $month = $month == 12 ? 1 : ($month + 1);
        }
    }
        
    $str[] = '  </select>';
	
	return implode("\n", $str) . "\n";
}
//----------------------------------------------------------------------------------------
//
function get_field_adv_select($data, $code, $table, $table_collecting, $width, $is_autocomplete = false, $is_required = false)
{
    global $db;
    $str = array(); $num = 1;
    
    if ($is_autocomplete)
    {
        if (count($data) > 0) // если уже есть какие-то связи
        {
            $records = array();
            $db->Query('SELECT t1.' . $table['field'] . ' FROM ' . $table['table'] . ' AS t1 INNER JOIN ' . $table_collecting['table'] . ' AS t2 ON t1.ID = t2.' . $table_collecting['field2'] . ' WHERE t2.' . $table_collecting['field1'] . ' = ' . $table_collecting['field1_id'] . (!empty($table_collecting['where']) ? (' AND ' . $table_collecting['where']) : '') . ' ORDER BY t1.' . $table['orderBy']);
            while ($db->NextRecord()) $records[] = $db->F($table['field']);
            
            foreach ($records AS $r)
            {
                $str[] = '  <input class="w' . $width . ($is_required ? ' required' : '') . '" name="' . $code . '[' . $num++ . ']" value="' . htmlspecialchars($r) . '" table="' . $table['table'] . '" field="' . $table['field'] . '"><br>';
            }
        }
        else // если связей нет
        {
            $str[] = '  <input class="w' . $width . ($is_required ? ' required' : '') . '" name="' . $code . '[' . $num++ . ']" value="" table="' . $table['table'] . '" field="' . $table['field'] . '"><br>';
        }
        
        $str[] = '  <input type="hidden" name="' . $code . '_auto" value="1">';
    }
    else
    {
        $width += 8;
        
        $records = array();
        $db->Query('SELECT ID, ' . $table['field'] . ' FROM ' . $table['table'] . $table['where'] . ' ORDER BY ' . $table['orderBy']);
        while ($db->NextRecord()) $records[] = $db->mRecord;
       	
        if (count($data) > 0) // если уже есть какие-то связи
        {
            $fields = array(); 
            foreach ($data AS $v)
            {
                $temp = array(); $temp_key = $num;
                
                $temp[] = '  <select class="w' . $width . ($is_required ? ' required' : '') . '" name="' . $code . '[' . $num++ . ']">';
                $temp[] = '   <option value="0"></option>';
                
                foreach ($records AS $r)
                {
                    if ($v == $r['ID'])
                    {
                        $temp_key = htmlspecialchars($r[$table['field']]);
                        $temp[] = '   <option value="' . $r['ID'] . '" selected>' . htmlspecialchars($r[$table['field']]) . '</option>';
                    }
                    else
                        $temp[] = '   <option value="' . $r['ID'] . '">' . htmlspecialchars($r[$table['field']]) . '</option>';
                }
                
                $temp[] = '  </select><br>';
                
                $fields[$temp_key] = implode("\n", $temp);
            }
            ksort($fields); // для вывода по алфавиту
            $str[] = implode("\n", $fields);
        }
        else // если связей нет
        {
            $str[] = '  <select class="w' . $width . ($is_required ? ' required' : '') . '" name="' . $code . '[' . $num++ . ']">';
            $str[] = '   <option value="0"></option>';
            
            foreach ($records AS $r)
                $str[] = '   <option value="' . $r['ID'] . '">' . htmlspecialchars($r[$table['field']]) . '</option>';
            
            $str[] = '  </select>';
        }
        
        $str[] = '  <input type="hidden" name="' . $code . '_auto" value="0">';
    }
    
    $str[] = '  <a class="add_adv_select" num="' . $num . '">add &gt;</a>';
    
    return "\n" . implode("\n", $str) . "\n ";
}
//----------------------------------------------------------------------------------------
// получение информации о последнем обновлении
function get_last_update($form)
{
	global $tpl;
	$tpl->set_var(array(
		'LAST_UPDATE_USER_ID' => $form['Last_update_user'],
		'LAST_UPDATE_DATE'    => $form['Last_update_date'],
		'LAST_UPDATE_IP'      => long2ip($form['Last_update_ip'])
	));
	return 'Last update ' .  $form['Last_update_date'] . '<br>' . get_login_by_id($form['Last_update_user']) . ' ' . '(' . long2ip($form['Last_update_ip']) . ')';
}
//----------------------------------------------------------------------------------------
// получение логина пользователя по ID
function get_login_by_id($id)
{
	global $db;
	$db->Query('SELECT Login FROM users_a WHERE ID = ' . $id);
	return $db->NextRecord() ? htmlspecialchars($db->F('Login')) : '';
}
//----------------------------------------------------------------------------------------
// получение значения опции по коду
function get_option_by_code($code, $lang = '')
{
	$db9 = new PslDb;
	$db9->Query('SELECT Value FROM options WHERE Code = "' . addslashes($code) . '"' . ($lang ? 'AND Lang = "' . $lang . '"' : ''));
	return $db9->NextRecord() ? htmlspecialchars($db9->F('Value')) : '';
}
//----------------------------------------------------------------------------------------
// получение параметров для древоивидных разделов админки
function get_parameters($id, $code, $first_id = 1)
{
	switch ($code)
	{
		case 'content': $TABLE = LANG . '_sitemap'; break;
		case 'options': $TABLE = 'options_list'; break;
		case 'adm':     $TABLE = $code; break;
		default: $TABLE = LANG . '_' . $code;
	}
	
	$param = array('level' => 1, 'count_childs' => 0, 'count_childs_all' => 0);
	$db9 = new PslDb;
	$db9->Query('SELECT Parent_id FROM ' . $TABLE . ' WHERE ID = ' . $id);
	while ($db9->NextRecord() && $db9->F('Parent_id') != $first_id)
	{
		$param['level']++;
		$db9->Query('SELECT Parent_id FROM ' . $TABLE . ' WHERE ID = ' . $db9->F('Parent_id'));
	}
	
	$db9->Query('SELECT Count_childs FROM ' . $TABLE . ' WHERE ID = ' . $id);
	if ($db9->NextRecord()) $param['count_childs'] = $db9->F('Count_childs');
	
	$param['count_childs_all'] = get_count_chils_all($id, $TABLE, $db9);
	
	unset($db9);
	return $param; // level - уровень вложенности; count_childs - количество вложенных страних; count_childs_all - общее количество вложенных страниц
}
// полчение общего количества вложенных записей
function get_count_chils_all($id, $TABLE, $db9)
{
	$count = 0; $childs = array();
	$db9->Query('SELECT ID, Count_childs FROM ' . $TABLE . ' WHERE Parent_id = ' . $id);
	while ($db9->NextRecord())
	{
		$count++;
		$childs[] = $db9->F('ID');
	}
	foreach ($childs AS $v) $count += get_count_chils_all($v, $TABLE, $db9);
	return $count;
}
//---------------------------------------------------------------------------
// обработка значений из формы поиска
function processing_form_search ($form, $string, $int, $bool)
{
	$f = array();
	
	foreach ($string as $v) {
		if (isset($form[$v]) && trim($form[$v]) != '') $f[] = 'r.' . $v . ' LIKE "%' . addslashes(trim($form[$v])) . '%"';
	}
	foreach ($int as $v) {
		if (isset($form[$v]) && intval($form[$v]) > 0) $f[] = 'r.' . $v . ' = ' . intval($form[$v]);
	}
	foreach ($bool as $v) {
		$f[] = 'r.' . $v . ' = ' . (isset($form[$v]) ? $form[$v] : '0');
	}
	
	return $f;
}
//---------------------------------------------------------------------------
function to_replace_fields($tpl, $form, $fields)
{
	global $id;
    foreach ($fields['string'] as $v) {
        $tpl->set_var($v, isset($form[$v]) ? htmlspecialchars($form[$v]) : '');
    }
    foreach (array_merge($fields['int'], array('ID')) AS $v) {
        $tpl->set_var($v, isset($form[$v]) ? intval($form[$v]) : '');
    }
    foreach ($fields['bool'] AS $k => $v) {
        $tpl->set_var($v, isset($form[$v]) && $form[$v] == 1 ? ' checked' : ($k == 99 && $id == -1 ? ' checked' : ''));
    }
    foreach ($fields['text'] as $v) {
        $tpl->set_var($v, isset($form[$v]) ? $form[$v] : '');
    }
}
//---------------------------------------------------------------------------
function processing_edit_record($data, $fields, $is_last_update = true)
{
    global $g_user;
	$f = array();
    
    if ($is_last_update)
    {
        $f[] = 'Last_update_user = ' . intval($g_user->GetId()) . '';
	    $f[] = 'Last_update_date = NOW()';
	    $f[] = 'Last_update_ip = "' . ip2long($_SERVER['REMOTE_ADDR']) . '"';
    }
    
    foreach ($fields['string'] AS $k => $v) //string
    {
        if ($k < 100 && isset($data[$v])) $f[] = $v . ' = "' . addslashes($data[$v]) . '"';
	}
	foreach ($fields['int'] AS $k => $v) { //int
		if ($k < 100 && isset($data[$v])) $f[] = $v . ' = ' . intval($data[$v]);
	}
	foreach ($fields['bool'] AS $v) { //bool
		$f[] = $v . ' = ' . (isset($data[$v]) ? 1 : 0);
	}
    foreach ($fields['text'] AS $v) { //text
		if (isset($data[$v])) $f[] = $v . ' = "' . addslashes($data[$v]) . '"';
	}
    
    return $f;
}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
function is_obj_exists($id, $idField, $tblName)
{
    global $db;
    $r = false;
    if (string_is_id($id) || (strpos($id, '"') == 0 && strrpos($id, '"') == (strlen($id) - 1))) {
        $db->Query('SELECT ' . $idField . ' FROM ' . $tblName . ' WHERE ' . $idField . ' = ' . $id . ' LIMIT 1');
        if ($db->Nf()) $r = true;
    }
    return $r;
}

function translite($str) {
    $search = array('й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', ' ', 'ё', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю', 'Ё');
    $replace = array('i', 'ts', 'u', 'k', 'e', 'n', 'g', 'sh', 'chsh', 'z', 'h', '', 'f', 'i', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'zh', 'e', 'ya', 'ch', 's', 'm', 'i', 't', '', 'b', 'yu', '_', 'yo', 'I', 'Ts', 'U', 'K', 'E', 'N', 'G', 'Sh', 'Chsh', 'Z', 'H', '', 'F', 'I', 'V', 'A', 'P', 'R', 'O', 'L', 'D', 'Zh', 'E', 'Ya', 'Ch', 'S', 'M', 'I', 'T', '', 'B', 'Yu', 'Yo');
    $str = str_replace($search, $replace, $str);
    return ereg_replace('[^a-zA-Z0-9\-\_\.]', '', $str);
}

?>