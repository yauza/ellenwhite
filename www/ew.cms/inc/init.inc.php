<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   init.inc.php                                                        #
#   initilize objects, include library                                  #                 
#                                                                       #
#########################################################################

require(PATH_TO_ROOT . 'lib/psl_start.inc.php');
require(PATH_TO_ADMIN . 'inc/func.inc.php');
if (is_file(PATH_TO_ADMIN . 'inc/func_' . CODE . '.inc.php'))
    require(PATH_TO_ADMIN . 'inc/func_' . CODE . '.inc.php');

$g_user->mAutoAcceptGroups = true;
$g_user->mAccessBackEnd = true;
$g_user->mAccessFrontEnd = false;

$g_user->Start();
$g_page->mUser = $g_user;

// путь к текущей странице
define ('THIS_PAGE', substr(PAGE_PATH, 0, strrpos(PAGE_PATH, '/') + 1));

$g_page->mThisPage = defined('PAGE_CODE') ? PAGE_CODE : '';

if (CODE == 'foto' && isset($_FILES['photo'], $_POST['key']) && $_POST['key'] == 'hdLufREk')
{
    // если идет загрузка нескольких фото с помощью uploadify
}
elseif (CODE != 'login')
{
    // if user is not autorize, then try login with data from cookie
    if (!$g_user->IsAuthorized() && isset($_COOKIE['admin_login']))
	{
		if ($g_user->Login($_COOKIE['admin_login'], isset($_COOKIE['admin_pwd']) ? $_COOKIE['admin_pwd'] : ''))
			$g_page->mUser = $g_user;
    }
    
    if (!$g_user->IsAuthorized())
    {
    	if (isset($_SERVER['REQUEST_URI']))
			$_SESSION['start_page'] = str_replace('/' . _ADMIN . '/', '', $_SERVER['REQUEST_URI']);
		location(PATH_TO_ADMIN);
	}
	elseif (!$g_page->IsPageAccessible() && $g_user->GetLogin() != 'yauza' && $g_user->GetLogin() != 'admin')
	{
		if (LANG == 'opt')
        	location(PATH_TO_ADMIN . 'opt/');
        else
        	location(PATH_TO_ADMIN);
    }
    
    $g_user->UpdateLastPagePath(PAGE_PATH);
}


$lang_admin = $g_user->GetLang() && is_dir(_PHPSITELIB_PATH . 'lang/' . $g_user->GetLang()) ? $g_user->GetLang() : (isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'ru');
setcookie('lang', $lang_admin, time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
//$lang_front = 'ru';

# Load language
if (defined('IN_ADMIN'))
{
    require (_PHPSITELIB_PATH . 'lang/' . $lang_admin . '/lang_err_lib.inc.php');
    require (_PHPSITELIB_PATH . 'lang/' . $lang_admin . '/lang_err_admin.inc.php');
    require (_PHPSITELIB_PATH . 'lang/' . $lang_admin . '/lang_admin.inc.php');
}

require (_PHPSITELIB_PATH . 'psl_constants.inc.php');

?>