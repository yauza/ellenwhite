<?
define ('LANG', 'adm');
define ('IN_ADMIN', 1);
define ('PATH_TO_ROOT', PATH_TO_ADMIN . '../');

define ('ACT_LIST',      'list');
define ('ACT_ADD',       'add');
define ('ACT_EDIT',      'edit');
define ('ACT_ADD_PROC',  'add_proc');
define ('ACT_EDIT_PROC', 'edit_proc');

$action = isset($_POST['a']) && preg_match('/^[a-z_]+$/i', $_POST['a']) ? $_POST['a'] : (isset($_GET['a']) && preg_match('/^[a-z_]+$/i', $_GET['a']) ? $_GET['a'] : ACT_LIST);

$id = isset($_GET['id']) && intval($_GET['id']) >= -1 ? intval($_GET['id']) : 0;

$page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;

define ('CODE', substr(getcwd(), strrpos(str_replace("\\", '/', getcwd()), '/') + 1));

?>