<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   options menu block admin                                            #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', 'options_list');

$fields = array(
    'string' => array('Title', 'Code'),
    'int'    => array('OrderBy', 'Parent_id'),
    'bool'   => array(),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		/*if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		else*/if ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = opt_menu_del($id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Меню настроек',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку администрирования',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'   => 'Поиск',
                'type'    => 'string',
                'width'   => 440,
                'length'  => 32,
                'focus'   => true
            ),
            'Code' => array(
                'title'   => 'Код',
                'type'    => 'string',
                'width'   => 250,
                'length'  => 16
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить запись в список',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' LEFT JOIN ' . TABLE . ' AS o ON r.Parent_id = o.ID WHERE r.ID != 1',
        'orderBy'   => 'OrderBy',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'    => 'Название',
                'type'    => 'string',
                'link'    => true,
                //'is_sort' => true,
                'style'   => 'width:400px;'
            ),
            
            'Code' => array(
                'head'    => 'Код',
                'type'    => 'string',
                //'is_sort' => true,
                'style'   => 'width:200px;'
            ),
            
            'o.Title' => array(
                'head'    => 'Родитель',
                'type'    => 'string',
                //'is_sort' => true,
                'style'   => 'width:300px;'
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'удалить запись',
                'message' => "Удалить запись: '[Title]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление пункта меню',
        'headTitleEdit' => 'Редактирование пункта меню',
        // данные для цепочки страниц
        'is_chain'      => false,
        'chain'         => array(
            'Администрирование' => LANG . '/',
            'Меню настроек'     => THIS_PAGE
        ),
        'сhainLastTitle' => 'Новый пункт меню',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку настроек<br>без сохранения изменений',
        'backLink'      => THIS_PAGE,
        // ссылки-действия
        'actions'       => array(
            'удалить пункт меню' => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . LANG . '/' . CODE . '_' . ACT_EDIT . '.html');
	//-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var('Parents', get_field_select($form->data/*form*/, 'Parent_id'/*field*/, 'options_list'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'Title'/*orderBy*/, 428/*width*/, true/*is required*/));
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{//print_r($data); echo '<br>'; exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data, $code == 'add' ? true : false);
    
    if ($r == '')
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields, false);
		
		if ($code == 'add')
			$db->Query('UPDATE ' . TABLE . ' SET Count_childs = Count_childs + 1 WHERE ID = ' . $data['Parent_id']);
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $q = $q1 . implode(', ', $f) . $q2;
        $db->Query($q);
        //$insert_id = $db->GetInsertId();
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function opt_menu_del($id)
{
	global $db;
	
	$db->Query('SELECT Parent_id FROM ' . TABLE . ' WHERE ID = ' . $id);
	if ($db->NextRecord())
	{
		$db->Query('UPDATE ' . TABLE . ' SET Count_childs = Count_childs - 1 WHERE ID = ' . $db->F('Parent_id'));
		$db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $id);
		return '';
	}
	else
		return 'Запись не найдена';
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{ //print_r($data);
    $r = '';
    
	if (empty($data['Title'])) $r = '<br>Введите название';
    if (empty($data['Code'])) $r .= '<br>Введите код';
	if (empty($data['OrderBy']) || intval($data['OrderBy']) < 1) $r .= '<br>Введите порядок вывода (число > 0)';
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
?>