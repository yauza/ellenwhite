<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   link-actions block admin                                            #
#                                                                       #
#########################################################################

define ('IN_ADMIN', 1);
define ('PATH_TO_ADMIN', '../../');
define ('PATH_TO_ROOT', PATH_TO_ADMIN . '../');
define ('LANG', isset($_GET['lang']) && preg_match('/^[a-z_]+$/i', $_GET['lang']) ? $_GET['lang'] : 'ru');
define ('ADM', 'adm');

define ('CODE', 'actions');
switch (LANG)
{
	case 'ru':
	{
		define ('TABLE', 'ru_sitemap_actions');
		define ('TABLE_MAIN', 'ru_sitemap');
		define ('PATH_TO_MAIN', 'ru/content/');
	} break;
	case 'en':
	{
		define ('TABLE', 'en_sitemap_actions');
		define ('TABLE_MAIN', 'en_sitemap');
		define ('PATH_TO_MAIN', 'en/content/');
	} break;
	case 'opt':
	{
		define ('TABLE', 'options_actions');
		define ('TABLE_MAIN', 'options_list');
		define ('PATH_TO_MAIN', 'opt/');
	} break;
}

define ('ACT_LIST',      'list');
define ('ACT_EDIT',      'edit');
define ('ACT_EDIT_PROC', 'edit_proc');

$is_ajax = isset($_POST['ajax']) && $_POST['ajax'] == '' ? true : (isset($_GET['ajax']) && $_GET['ajax'] == '' ? true : false);
if ($is_ajax) header("Content-Type: text/plain; charset=utf-8");

$action = isset($_POST['a']) && preg_match('/^[a-z_]+$/i', $_POST['a']) ? $_POST['a'] : (isset($_GET['a']) && preg_match('/^[a-z_]+$/i', $_GET['a']) ? $_GET['a'] : ACT_LIST);

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

$err = '';

// получение кода страницы
switch ($action)
{
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', ADM . '/' . CODE . '/' . ($id ? '&amp;id=' . $id : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

$a_orderBy = GetData($id, 'OrderBy');
$a_title = GetData($id, 'Title');
$a_page_id = GetData($id, 'Page_id');
$a_hide = GetData($id, 'Hide');

switch ($action)
{
	case ACT_EDIT_PROC:
	{
		if (!$err = record_add_edit($id, $a_orderBy, $a_title, $a_page_id, $a_hide)) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
			$action = ACT_EDIT;
	} break;
}
//------------------
$tpl = new Template;
$tpl->set_var(array(
	'LANG'  => LANG,
	'ERROR' => $err
));
//------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0) // редактирование записи
		{
			//array_push($scripts, '');
			$container = get_form($id, $a_orderBy, $a_title, $a_page_id, $a_hide);
		}
	} break;
	default:
		location(PATH_TO_ADMIN . PATH_TO_MAIN);
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $a_orderBy, $a_title, $a_page_id, $a_hide)
{
	global $tpl, $db, $g_user, $LA, $ALL_LANGS;
	
	$tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/' . ADM . '/' . CODE . '_' . ACT_EDIT . '.html');
	
	$db->Query('SELECT Title FROM ' . TABLE_MAIN . ' WHERE ID = '. $id);
	if (!$db->NextRecord()) location(PATH_TO_ADMIN . ADM . '/');
	
	$tpl->set_var(array(
		'THIS_PAGE'    => THIS_PAGE . '?lang=' . LANG . '&amp;id=' . intval($id),
		'SITE_TITLE'   => _PHPSITELIB_SITE,
		'PAGE_TITLE'   => htmlspecialchars($db->F('Title')),
		'BACK_HREF'    => PATH_TO_MAIN,
		'BACK_TO_MAIN' => PATH_TO_MAIN
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	//----------------------------------------------------------------------------------- 
	$tpl->set_block('main', 'link-actions', 'link-actions_');
	$tpl->set_block('link-actions', 'group_pages', 'group_pages_');
	$tpl->set_block('group_pages', 'pages', 'pages_');
	foreach ($a_orderBy AS $k => $v)
	{
		$tpl->set_var(array(
			'group_pages_' => '',
			'NUM'          => $k,
			'OrderBy'      => intval($v),
			'Title'        => $a_title[$k],
			'Hide'         => isset($a_hide[$k]) && $a_hide[$k] == 1 ? ' checked' : ''
		));
		
		foreach ($ALL_LANGS AS $lang => $title)
		{
			$tpl->set_var(array('pages_' => '', 'GROUP_PAGES_TITLE' => $title));
			$db->Query('SELECT ID, Title FROM pages WHERE Lang = "' . $lang . '" ORDER BY Title');
			while ($db->NextRecord())
			{
				$tpl->set_var(array(
					'pages_ID'     => $db->F('ID'),
					'pages_Title'  => htmlspecialchars($db->F('Title')),
					'pages_Active' => isset($a_page_id[$k]) && $a_page_id[$k] == $db->F('ID') ? ' selected' : ''
				));
				$tpl->parse('pages_', 'pages', true);
			}
			$tpl->parse('group_pages_', 'group_pages', true);
		}
		
		$tpl->parse('link-actions_', 'link-actions', true);
	}
	
	$i = count($a_orderBy);
	while (++$i <= 5)
	{
		$tpl->set_var(array('group_pages_' => '', 'NUM' => $i, 'OrderBy' => $i, 'Title' => '', 'Hide' => ''));
		
		foreach ($ALL_LANGS AS $lang => $title)
		{
			$tpl->set_var(array('pages_' => '', 'GROUP_PAGES_TITLE' => $title));
			$db->Query('SELECT ID, Title FROM pages WHERE Lang = "' . $lang . '" ORDER BY Title');
			while ($db->NextRecord())
			{
				$tpl->set_var(array(
					'pages_ID'     => $db->F('ID'),
					'pages_Title'  => htmlspecialchars($db->F('Title')),
					'pages_Active' => ''
				));
				$tpl->parse('pages_', 'pages', true);
			}
			$tpl->parse('group_pages_', 'group_pages', true);
		}
		
		$tpl->parse('link-actions_', 'link-actions', true);
	}
	
	return $tpl->parse('C', 'main', false);
}
#----------------------------------------------------------------------------------------
function GetData($id, $field)
{
    if (empty($_POST['OrderBy']) && empty($_POST['Title']) && empty($_POST['Page_id']))
		$r = GetDbData($id, $field);
	else
		if ($field == 'Hide' && empty($_POST['Hide'])) $r = 0; else $r = $_POST[$field];
		
    return $r;
}
#----------------------------------------------------------------------------------------
function GetDbData($id, $field)
{
    global $db; $i = 1;
    $r = array();
    if (string_is_id($id)) {
        $db->Query('SELECT ' . $field . ' FROM ' . TABLE . ' WHERE Sitemap_id = ' . $id . ' ORDER BY OrderBy');
        while ($db->NextRecord()) $r[$i++] = $db->F($field);
    }
    return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($id, $a_orderBy, $a_title, $a_page_id, $a_hide) 
{ //print_r($a_title);
    global $db ,$g_user;
    _record_prepare($a_title, $a_page_id); $r = _record_check($a_orderBy, $a_title, $a_page_id);
    
    if ($r == '')
	{
		$db->Query('DELETE FROM ' . TABLE . ' WHERE Sitemap_id = ' . intval($id));
        $q1  = 'INSERT INTO ' . TABLE . ' SET ';
        foreach($a_title AS $k => $v)
	    {
	    	$f = array();
	    	$f[] = 'Sitemap_id = ' . intval($id) . '';
	    	$f[] = 'Title = "' . addslashes($v) . '"';
	    	$f[] = 'Page_id = ' . intval($a_page_id[$k]) . '';
	    	$f[] = 'OrderBy = ' . intval($a_orderBy[$k]) . '';
	    	$f[] = 'Hide = ' . (isset($a_hide[$k]) ? $a_hide[$k] : '0') . '';
	    	//echo $q1 . implode(', ', $f);
	    	$db->Query($q1 . implode(', ', $f));
		}
        //$insert_id = $db->GetInsertId();
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($a_orderBy, $a_title, $a_page_id)
{
    $r = '';
    
    foreach($a_title AS $k => $v)
    {
    	if (empty($a_orderBy[$k]) || $a_orderBy[$k] < 1) $r .= '<br>Введите номер ( > 0 )';
		if (empty($v)) $r .= '<br>Введите название';
		if (empty($a_page_id[$k]) || $a_page_id[$k] < 1) $r .= '<br>Выберите страницу';
	}
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$a_title, &$a_page_id)
{
	foreach($a_title AS $k => $v) { if (trim($v) == '') unset($a_title[$k]); else $a_title[$k] = trim($v); }
	foreach($a_page_id AS $k => $v) $a_page_id[$k] = intval($v);
}
?>