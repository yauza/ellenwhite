<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   administration block admin                                          #
#                                                                       #
#########################################################################

if (!defined('IN_ADMIN')) define ('IN_ADMIN', 1);
if (!defined('PATH_TO_ADMIN')) define ('PATH_TO_ADMIN', '../');
if (!defined('PATH_TO_ROOT')) define ('PATH_TO_ROOT', PATH_TO_ADMIN . '../');
define ('LANG', 'adm');

define ('CODE', 'adm');
define ('TABLE', CODE);

define ('ACT_LIST', 'list');
define ('ACT_ADD',  'add');
define ('ACT_EDIT', 'edit');

$is_ajax = isset($_POST['ajax']) && $_POST['ajax'] == '' ? true : (isset($_GET['ajax']) && $_GET['ajax'] == '' ? true : false);
if ($is_ajax) header("Content-Type: text/plain; charset=utf-8");

$action = isset($_POST['a']) && preg_match('/^[a-z_]+$/i', $_POST['a']) ? $_POST['a'] : (isset($_GET['a']) && preg_match('/^[a-z_]+$/i', $_GET['a']) ? $_GET['a'] : ACT_LIST);

//$id = isset($_GET['id']) && intval($_GET['id']) >= -1 ? intval($_GET['id']) : 0;

$err = '';

// получение кода страницы
switch ($action)
{
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', CODE . '/' . ($action ? '?a=' . $action : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

//------------------
switch ($action)
{
	default:
	{
		$db->Query('SELECT Count_childs FROM ' . TABLE . ' WHERE Parent_id = 0');
		if ($db->NextRecord())
		{
			array_push($scripts, 'js/menu.js');
			$container = get_list(1, $db->F('Count_childs'));
		}
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($parent_id = 1, $count = 0)
{
	global $javascript;
    $db = new PslDb;
    $r = '';
	
	$tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . LANG . '/' . CODE . '.html');
	$tpl->set_block('main', 'head', 'head_');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_block('record', 'lines', 'lines_');
	$tpl->set_block('record', 'line_plus', 'line_plus_');
	$tpl->set_block('record', 'actions', 'actions_');
	
	if ($r == '' && $parent_id == 1)
	{
		$tpl->set_var('TITLE', 'Администрирование');
		$r .= $tpl->parse('head_', 'head', false);
	}
	
	$db->Query('SELECT * FROM ' . TABLE . ' WHERE Parent_id = ' . $parent_id . ' ORDER BY OrderBy');
	for ($i = 1; $db->NextRecord(); $i++)
	{
		$tpl->set_var(array('lines_' => '', 'line_plus_' => '', 'actions_' => ''));
		
		$isClilds = $db->F('Count_childs') ? true : false;
		
		$param = get_parameters($db->F('ID'), CODE);
		for ($j = 1; $j <= $param['level']; $j++)
		{
			if ($j < $param['level']) $code = '0';
			elseif ($isClilds) break;
			elseif ($i == $count) $code = '2';
			else $code = '1';
			$tpl->set_var('LINE_CODE', $code);
			$tpl->parse('lines_', 'lines', true);
		}
		
		$tpl->set_var(array(
			'LINE_CLASS' => $parent_id == 1 ? 'line' : 'line_hide',
			'ICON'       => $isClilds ? 'folder' : 'file',
			'ICON_CLICK' => $isClilds ? 'plus_minus_click(' . $param['count_childs_all'] . ', this.previousSibling.previousSibling);' : '',
			'ID'         => $db->F('ID'),
			'TITLE'      => htmlspecialchars($db->F('Title')),
			'HREF'       => htmlspecialchars($db->F('Href'))
		));
		
		if ($isClilds)
		{
			$tpl->set_var('LINE_CODE', $i == $count ? '2' : '1');
			$tpl->set_var('COUNT_CHILDS', $db->F('Count_childs'));
			$tpl->parse('line_plus_', 'line_plus', false);
			if (strpos(' '.$db->F('Href') , 'pages'))
				$javascript .= "plus_minus_click(" . $param['count_childs_all'] . ", document.getElementById('line_" . $db->F('ID') . "'));";
		}
				
		$r .= $tpl->parse('record_', 'record', true);
		
		if ($isClilds) $r .= get_list($db->F('ID'), $db->F('Count_childs'));
	}
	
    return $r;
}

?>