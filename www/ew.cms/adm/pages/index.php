<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   pages block admin                                                   #
#                                                                       #
#########################################################################

define ('IN_ADMIN', 1);
define ('PATH_TO_ADMIN', '../../');
define ('PATH_TO_ROOT', PATH_TO_ADMIN . '../');
define ('LANG', isset($_GET['lang']) && preg_match('/^[a-z_]+$/i', $_GET['lang']) ? $_GET['lang'] : (isset($_POST['lang']) && preg_match('/^[a-z_]+$/i', $_POST['lang']) ? $_POST['lang'] : 'ru'));
define ('ADM', 'adm');

define ('CODE', 'pages');
define ('TABLE', CODE);

define ('ACT_LIST',      'list');
define ('ACT_ADD',       'add');
define ('ACT_EDIT',      'edit');
define ('ACT_ADD_PROC',  'add_proc');
define ('ACT_EDIT_PROC', 'edit_proc');

$is_ajax = isset($_POST['ajax']) && $_POST['ajax'] == '' ? true : (isset($_GET['ajax']) && $_GET['ajax'] == '' ? true : false);
if ($is_ajax) header("Content-Type: text/plain; charset=utf-8");

$action = isset($_POST['a']) && preg_match('/^[a-z_]+$/i', $_POST['a']) ? $_POST['a'] : (isset($_GET['a']) && preg_match('/^[a-z_]+$/i', $_GET['a']) ? $_GET['a'] : ACT_LIST);

$id = isset($_GET['id']) && intval($_GET['id']) >= -1 ? intval($_GET['id']) : 0;

$page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;

$fields = array(
    'string' => array('Title', 'Code', 'Type', 'Href', 'Lang'),
    'int'    => array(),
    'bool'   => array(),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', ADM . '/' . CODE . '/?lang=' . LANG . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '&a=' . $action : '') . ($id ? '&id=' . $id : '') . ($page != 1 ? '&page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: //добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page . '&lang=' . LANG);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db, $page, $ALL_LANGS;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список страниц админки [' . mb_strtolower($ALL_LANGS[LANG], 'utf-8') . ']',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку администрирования',
        'backLink'  => ADM . '/',
        // ссылки-действия
        'actions'   => array(
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'   => 'Поиск страницы',
                'type'    => 'string',
                'width'   => 370,
                'length'  => 64,
                'focus'   => true
            ),
            'Code' => array(
                'title'   => 'Код',
                'type'    => 'string',
                'width'   => 260,
                'length'  => 32
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить страницу',
        'addLink'   => '?lang=' . LANG . '&a=add&id=-1',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' WHERE r.Lang = "' . LANG . '"',
        'orderBy'   => 'Title',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'        => 'Название',
                'type'        => 'string',
                'link'        => '?lang=' . LANG . '&page=' . $page . '&a=edit&id=[ID]',
                'is_sort'     => true,
                'style'       => 'width:440px;',
                'max_letters' => 50,
                'max_words'   => 6,
            ),
            'Code' => array(
                'head'        => 'Код',
                'type'        => 'string',
                'is_sort'     => true,
                'style'       => 'width:180px;'
            ),
            'Href' => array(
                'head'        => 'Ссылка',
                'type'        => 'string',
                'style'       => 'width:240px;'
            ),
            'Type' => array(
                'head'        => 'Тип',
                'type'        => 'string',
                'is_sort'     => true,
                'style'       => 'width:40px;'
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'удалить страницу',
                'message' => "Удалить страницу: '[Title]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $ALL_LANGS, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'thisPage'      => THIS_PAGE . '?lang=' . LANG . '&page=' . $page,
        'headTitleAdd'  => 'Добавление страницы',
        'headTitleEdit' => 'Редактирование страницы',
        // данные для цепочки страниц
        'is_chain'      => false,
        'chain'         => array(
            'Администрирование' => ADM . '/',
            'Список страниц админки [' . mb_strtolower($ALL_LANGS[LANG], 'utf-8') . ']' => ADM . '/' . CODE . '/?lang=' . LANG
        ),
        'сhainLastTitle' => 'Новая страница',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку страниц<br>без сохранения изменений',
        'backLink'      => THIS_PAGE . '?lang=' . LANG,
        // ссылки-действия
        'actions'       => array(
            'удалить страницу' => $id != -1 ? (THIS_PAGE . "?page=$page&lang=".LANG."a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/' . ADM . '/' . CODE . '_' . ACT_EDIT . '.html');
	$tpl->set_var('LANG', LANG);
    //-----------------------------------------------------------------------------------
	to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var(array(
		'ADD_CHECKED'  => isset($form->data['Type']) && $form->data['Type'] == 'add' ? ' selected' : '',
		'EDIT_CHECKED' => isset($form->data['Type']) && $form->data['Type'] == 'edit' ? ' selected' : '',
		'LIST_CHECKED' => isset($form->data['Type']) && $form->data['Type'] == 'list' ? ' selected' : ''
	));
    //-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{//print_r($data); echo '<br>'; exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data);
    
    if ($r == '')
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields, false);
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $q = $q1 . implode(', ', $f) . $q2;
        $db->Query($q);
        //$insert_id = $db->GetInsertId();
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{
    $r = '';
    
    if (empty($data['Title'])) $r = '<br>Введите название';
    if (empty($data['Code'])) $r .= '<br>Введите код';
    if (empty($data['Type']) || !in_array($data['Type'], array('add', 'edit', 'list'))) $r .= '<br>Выберите тип';
    if (empty($data['Href'])) $r .= '<br>Введите путь от корня админки';
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
?>