<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   global options block admin                                          #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', 'options');

$fields = array(
    'string' => array('Title', 'Code', 'Value', 'Lang'),
    'int'    => array('Length', 'Type'),
    'bool'   => array(),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: //  добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		/*if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		else*/if ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список настроек для сайта',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку администрирования',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'   => 'Поиск',
                'type'    => 'string',
                'width'   => 350,
                'length'  => 64,
                'focus'   => true
            ),
            'Code' => array(
                'title'   => 'Код',
                'type'    => 'string',
                'width'   => 240,
                'length'  => 32
            ),
            'Lang' => array(
                'title'   => 'Язык',
                'type'    => 'string',
                'width'   => 30,
                'length'  => 2
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить настройку',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => '',
        'orderBy'   => 'Title',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'        => 'Название',
                'type'        => 'string',
                'link'        => true,
                'is_sort'     => true,
                'style'       => 'width:400px;'
            ),
            'Code' => array(
                'head'        => 'Код',
                'type'        => 'string',
                'is_sort'     => true,
                'style'       => 'width:230px;'
            ),
            'Lang' => array(
                'head'        => 'Язык',
                'type'        => 'string',
                'is_sort'     => true,
                'style'       => 'width:50px;'
            ),
            'Value' => array(
                'head'        => 'Значение',
                'type'        => 'string',
                'max_letters' => 15,
                'max_words'   => 3,
                'style'       => 'width:200px;'
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'удалить настройку',
                'message' => "Удалить настройку: '[Title]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $LANGS, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление настройки',
        'headTitleEdit' => 'Редактирование настройки',
        // данные для цепочки страниц
        'is_chain'      => false,
        'chain'         => array(
            'Администрирование' => LANG . '/',
            'Настройки'         => THIS_PAGE
        ),
        'сhainLastTitle' => 'Новая настройка',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку настроек<br>без сохранения изменений',
        'backLink'      => THIS_PAGE,
        // ссылки-действия
        'actions'       => array(
            'удалить настройку' => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . LANG . '/' . CODE . '_' . ACT_EDIT . '.html');
		//'BACK_TO_MAIN' => LANG . '/'
	//-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_block('main', 'lang', 'lang_');
    foreach (array_keys($LANGS) AS $lang)
    {
		$tpl->set_var(array(
			'LANG'        => $lang,
			'LANG_ACTIVE' => isset($form->data['Lang']) && $form->data['Lang'] == $lang ? ' selected' : ''
		));
		$tpl->parse('lang_', 'lang', true);
	}
    //-----------------------------------------------------------------------------------
    $vArr = array(1 => 'int', 2 => 'string', 3 => 'bool', 4 => 'text', 5 => 'file'); //types
    foreach ($vArr AS $k => $v)
        $tpl->set_var($v, isset($form->data['Type']) && $form->data['Type'] == $k ? ' selected' : '');
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); echo '<br>'; exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data, $code == 'add' ? true : false);
    
    if ($r == '')
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields, false);
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        //$insert_id = $db->GetInsertId();
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{ //print_r($data);
    $r = '';
    
	if (empty($data['Title'])) $r = '<br>Введите название';
    if (empty($data['Code'])) $r .= '<br>Введите код';
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
?>