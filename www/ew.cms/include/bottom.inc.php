<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   bottom.inc.php                                                      #
#   print bottom of pages                                               #
#                                                                       #
#########################################################################

$t_bot = new Template;
$t_bot->set_file('main', PATH_TO_ROOT . 'temp_admin/bottom.html');

$t_bot->set_var(array(
));

$t_bot->pparse('OUT', 'main', false);

?>