<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   top.inc.php                                                         #
#   print top of pages                                                  #
#                                                                       #
#########################################################################

$t_top = new Template;
$t_top->set_file('main', PATH_TO_ROOT . 'temp_admin/top.html');
$t_top->set_block('main', 'script', 'script_');
$t_top->set_block('main', 'lang', 'lang_');
$t_top->set_block('main', 'pages', 'pages_');
$t_top->set_block('main', 'opt', 'opt_');
$t_top->set_block('main', 'adm', 'adm_');
$t_top->set_block('main', 'exit', 'exit_');
//$t_top->set_block('main', 'phpmyadmin', 'phpmyadmin_');

foreach ($scripts AS $v)
{
	$t_top->set_var('SRC', $v);
	$t_top->parse('script_', 'script', true);
}

if ($g_user->IsAuthorized())
{
    $ru_pages = array('about' => 'About', 'news' => 'News', 'services' => 'Services', 'folio'=>'Folio','coming' => 'Coming soon', 'testimonials' => 'Testimonials');
	foreach ($LANGS AS $lang => $v) // разные разделы
	{
        $array = $lang . '_pages';
        if (isset($$array) && is_array($$array))
        {
            foreach ($$array AS $code => $title)
            {
                $t_top->set_var(array(
        			'PAGE_IS_ACT'   => LANG == $lang && strpos(CODE, $code) === 0 ? '_active' : '',
        			'PAGE_TITLE'    => $title
        		));
        		if (LANG != $lang || (CODE != $code || (CODE == $code && $action != ACT_LIST)))
        		{
                    $t_top->set_var(array(
                        'PAGE_A_OPEN'  => '<a href="' . $lang . '/' . $code . '/">',
                        'PAGE_A_CLOSE' => '</a>'
                    ));
        		}
                else
                {
                    $t_top->set_var(array(
                        'PAGE_A_OPEN'  => '',
                        'PAGE_A_CLOSE' => ''
                    ));
                }
        		$t_top->parse('pages_', 'pages', true);
            }
        }
	}
    
    $_pages = array_keys($ru_pages);
	foreach ($LANGS AS $lang => $v) // разделы языков
	{
	    $is_active = true;
        foreach (array_keys($ru_pages) AS $_code)
            if (strpos(CODE, $_code) === 0)
                $is_active = false;
        
		$t_top->set_var(array(
			'LANG_IS_ACT'   => LANG == $lang && $is_active ? '_active' : '',
			'LANG_TITLE'    => $v
		));
		if (LANG != $lang || (CODE != 'content' || (CODE == 'content' && $action != ACT_LIST)))
		{
            $t_top->set_var(array(
                'LANG_A_OPEN'  => '<a href="' . $lang . '/content/">',
                'LANG_A_CLOSE' => '</a>'
            ));
		}
        else
        {
            $t_top->set_var(array(
                'LANG_A_OPEN'  => '',
                'LANG_A_CLOSE' => ''
            ));
        }
		$t_top->parse('lang_', 'lang', true);
	}
    
    $adm_pages = array('actions', 'adm', 'adm_opt', 'adm_opt_menu', 'pages');
	if (LANG == 'adm' || in_array(CODE, $adm_pages)) // если находимся в разделе "Администрирование"
	{
		$t_top->set_var(array('OPT_IS_ACT' => '', 'ADM_IS_ACT' => '_active'));
        
        $t_top->set_var(array(
            'OPT_A_OPEN'  => '<a href="opt/">',
            'OPT_A_CLOSE' => '</a>',
            'ADM_A_OPEN'  => CODE != 'adm' ? ('<a href="adm/">') : '',
            'ADM_A_CLOSE' => CODE != 'adm' ? '</a>' : ''
        ));
	}
	elseif (LANG == 'opt') // если находимся в разделе "Настройки"
	{
		$t_top->set_var(array('OPT_IS_ACT' => '_active', 'ADM_IS_ACT' => ''));
        
        $t_top->set_var(array(
            'OPT_A_OPEN'  => CODE != 'options' || (CODE == 'options' && isset($_GET['a']) && $_GET['a'] != 'list') ? ('<a href="opt/">') : '',
            'OPT_A_CLOSE' => CODE != 'options' || (CODE == 'options' && isset($_GET['a']) && $_GET['a'] != 'list') ? '</a>' : '',
            'ADM_A_OPEN'  => '<a href="adm/">',
            'ADM_A_CLOSE' => '</a>'
        ));
	}
	else // если находимся в разделе "Страницы сайта (к-либо язык)"
	{
		$t_top->set_var(array('OPT_IS_ACT' => '', 'ADM_IS_ACT' => ''));
		
        $t_top->set_var(array(
            'OPT_A_OPEN'  => '<a href="opt/">',
            'OPT_A_CLOSE' => '</a>',
            'ADM_A_OPEN'  => '<a href="adm/">',
            'ADM_A_CLOSE' => '</a>'
        ));
	}
	
	$t_top->parse('opt_', 'opt', false);
	if ($g_user->GetLogin() == 'yauza')
    {
        $t_top->parse('adm_', 'adm', false);
    //    $t_top->parse('phpmyadmin_', 'phpmyadmin', false);
    }
	$t_top->parse('exit_', 'exit', false);
}

if (isset($_GET['id']) && $_GET['id'] == -1) // если страница добавление чего-либо
	$javascript .= "$('form.edit_form input, form.edit_form textarea').keyup(); $('form.edit_form select').change()";

$t_top->set_var(array(
    'BASE_HREF'     => 'http://' . $_SERVER['HTTP_HOST'] . '/' . _ADMIN . '/',
	'LANG_PAGE'     => LANG,
	'LANG_ADMIN'    => $lang_admin,
    'PATH_TO_ROOT'  => PATH_TO_ROOT,
    'SITE_TITLE'    => _PHPSITELIB_SITE,
    'EXIT_LINK'     => '?logout',
    'DATABASE'      => DB_NAME,
    'ON_LOAD'       => $javascript
));

$t_top->pparse('OUT', 'main', false);

?>