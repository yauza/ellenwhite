<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   login page                                                          #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('IN_ADMIN', 1);
define ('PATH_TO_ADMIN', './');
define ('PATH_TO_ROOT', '../');
define ('CODE', 'login');

// получение путя к странице
define ('PAGE_PATH', '');

require(PATH_TO_ADMIN . 'inc/init.inc.php');

$err = ''; $form_login = false; 

// номер попытки входа
if (isset($_SESSION['Input_attempt']))
	$_SESSION['Input_attempt'] = $_SESSION['Input_attempt'] + 1;
else
	$_SESSION['Input_attempt'] = 1;

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (isset($_POST['code']))
	{
		include_once(PATH_TO_ROOT . 'include/formvalidator/SPAF_FormValidator.class.php');
		$obj = new SPAF_FormValidator();
		if ($obj->validRequest($_POST['code']))
		{
			$obj->destroy();
			$_SESSION['Input_attempt'] = 0;
			location(PATH_TO_ADMIN);
		}
	}
	elseif (isset($_POST['login'], $_POST['pwd']))
	{
		if ($_POST['login'] == '')
	        $err = 'Введите логин';
	    elseif (!string_is_login($_POST['login']))
	        $err = 'Логин содержит недопустимые символы';
	    elseif (!$g_user->Login($_POST['login'], md5($_POST['pwd'])))
	        $err = 'Доступ запрещен';
	    else
		{
	    	setcookie('admin_login', $_POST['login'], time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
	    	setcookie('admin_pwd', md5($_POST['pwd']), time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
	    }
	    if (!$err)
			unset($_SESSION['Input_attempt']);
	}
	elseif (isset($_POST['email']))
	{
		if ($_POST['email'] == '')
			$err = 'Введите e-mail';
		elseif (!string_is_email($_POST['email']))
			$err = 'Введен некорректный e-mail';
		elseif (!($db->Query('SELECT Email FROM users_a WHERE Email = "' . addslashes($_POST['email']) . '"') && $db->NextRecord()))
			$err = 'Данного e-mail нет в базе';
		else
		{
			$pass = mt_rand();
			$db->Query('UPDATE users_a SET Password = "' . md5($pass) . '" WHERE Email = "' . addslashes($_POST['email']) . '"');
            //send_mail($_POST['email'], '', 'Новый пароль для доступа в административный интефейс ' . _PHPSITELIB_SITE, 'Новый пароль: ' . $pass);
			send_html_mail($_POST['email'], 'Новый пароль для доступа в административный интефейс ' . _PHPSITELIB_SITE, iconv('utf-8', 'koi8-r', 'Новый пароль: ' . $pass));
			$err = 'Пароль отправлен на указанный e-mail';
		}
	}
}
elseif (isset($_GET['logout']))
{
	setcookie('admin_login', '', time() - 3600, '/', '.' . $_SERVER['HTTP_HOST']);
	setcookie('admin_pwd', '', time() - 3600, '/', '.' . $_SERVER['HTTP_HOST']);
	$g_user->Logout();
}

if ($g_user->IsAuthorized())
{
	if (isset($_SESSION['start_page']))
	{
		header('location: ' . PATH_TO_ADMIN . $_SESSION['start_page']);
		unset($_SESSION['start_page']);
		exit;
	}
	else
	{
		$db->Query('SELECT Value, Lang FROM options WHERE Code = "start_page"');
		if ($db->NextRecord())
		{
			$db->Query('SELECT Href FROM pages WHERE ID = ' . intval($db->F('Value')));
			if ($db->NextRecord()) location(PATH_TO_ADMIN . $db->F('Href'));
		}
	}
}

$tpl = new Template;
$tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/login.html');
$tpl->set_block('main', 'login', 'login_');
$tpl->set_block('main', 'code', 'code_');

if (isset($_SESSION['Input_attempt']) && $_SESSION['Input_attempt'] > 3)
{
	$javascript = "document.forms[0].elements['code'].focus();";
	$tpl->set_var('ERROR', 'Вы превысили количество попыток входа!<br>Введите код изображенный на картинке!');
	$tpl->parse('code_', 'code', false);
}
else
{
	$javascript = "document.forms[0].elements['login'].focus();";
	$tpl->set_var('ERROR', $err);
	$tpl->parse('login_', 'login', false);
}

require PATH_TO_ADMIN . 'include/top.inc.php';

$tpl->pparse('C', 'main', false);

require PATH_TO_ADMIN . 'include/bottom.inc.php';

?>