<?

#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   viewers block admin                                                 #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

$fields = array(
    'string' => array('TitleRus', 'TitleOriginal', 'Description', 'Description_eng', 'Meta_keys', 'Meta_desc'),
    'int'    => array('Duration', 'Dues', 'Price_view', 'Price_download', 'Section_id', 'Rightholder_id', 'Proportion_id', 'Geopolitics_id', 'Viewers'),
    'bool'   => array('Hide', 'IsRec', 'IsMainVis', 'IsFree'),
    'text'   => array('Text', 'Text_eng')
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';


//---------------------------------------------------------------------------------------
switch ($action)
{
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Statistics viewers',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'back to list pages',
        'backLink'  => LANG . '/content/',
        // ссылки-действия
        'actions'   => array(
            //'список стран' => LANG . '/countries/'
        ),
        // форма поиска
        'search'    => array(
            'Date' => array(
                'title'        => 'Date',
                't_width'      => 40,
                'type'         => 'date_interval2'
            ),
            'v.Section_id OR p.Section_id' => array(
                'title'       => 'Section',
                'type'        => 'select',
                'table'       => 'ru_sections',
                'field'       => 'Title',
                'orderBy'     => 'OrderBy',
                'width'       => 260
            ),
            'v.Rightholder_id OR p.Rightholder_id' => array(
                'title'       => 'Rightholder',
                'type'        => 'select',
                'table'       => 'ru_rightholders',
                'field'       => 'Title',
                'orderBy'     => 'OrderBy',
                'width'       => 320
            ),
            'User_id' => array(
                'title'       => 'User',
                't_width'      => 40,
                'type'        => 'select',
                'table'       => 'users',
                'field'       => 'Nick',
                'orderBy'     => 'Nick',
                'width'       => 156
            ),
            'Record_id' => array(
                'title'       => 'Video or Photo ID',
                'type'        => 'int',
                'width'       => 40,
                'length'      => 8
            ),
            'Record_type' => array(
                'title'       => 'Record type ("videos" or "photos")',
                'type'        => 'string',
                'width'       => 50,
                'length'      => 16
            )
        ),
        // ссылка для добавления
        //'addTitle'  => 'Add ',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' LEFT JOIN users AS u ON r.User_id = u.ID LEFT JOIN ru_videos AS v ON r.Record_id = v.ID AND r.Record_type = "videos" LEFT JOIN ru_photos AS p ON r.Record_id = p.ID AND r.Record_type = "photos" LEFT JOIN ru_sections AS s1 ON v.Section_id = s1.ID LEFT JOIN ru_sections AS s2 ON p.Section_id = s2.ID LEFT JOIN ru_rightholders AS r1 ON v.Rightholder_id = r1.ID LEFT JOIN ru_rightholders AS r2 ON p.Rightholder_id = r2.ID',
        'orderBy'   => 'Date DESC',
        'groupBy'   => 'r.ID',
        // столбцы таблицы
        'columns'   => array(
            'Date' => array(
                'head'        => 'Date',
                'type'        => 'datetime',
                'is_sort'     => true,
                'style'       => 'width:110px;'
            ),
            'u.Nick' => array(
                'head'        => 'User',
                'type'        => 'string',
                'is_sort'     => true,
                'max_letters' => 18,
                'max_words'   => 10,
                'style'       => 'width:150px;'
            ),
            's1.Title' => array(
                'head'        => 'Section',
                'type'        => 'string',
                'is_sort'     => true,
                'after'       => '[s2_Title]',
                'style'       => 'width:100px;'
            ),
            'v.TitleRus' => array(
                'head'        => 'Video',
                'type'        => 'string',
                'is_sort'     => true,
                'max_letters' => 45,
                'max_words'   => 10,
                'style'       => 'width:270px;'
            ),
            'p.TitleRus' => array(
                'head'        => 'Photo',
                'type'        => 'string',
                'is_sort'     => true,
                'max_letters' => 45,
                'max_words'   => 10,
                'style'       => 'width:270px;'
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'del record',
                'message' => "Del record: '[Date]'?"
            )
        )
    ));
    
    return $list->getHTML();
}

#----------------------------------------------------------------------------------------
?>