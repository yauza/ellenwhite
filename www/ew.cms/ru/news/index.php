<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   news block admin                                                    #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

$fields = array(
    'string' => array('Title', 'Announce'),
    'int'    => array(),
    'bool'   => array('Hide'),
    'text'   => array('Text')
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id, array($id, $id . '_mini'), array(array('table' => TABLE, 'field' => 'ID')))) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js', 'include/calendar/calendar.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'News list',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'back to list pages',
        'backLink'  => LANG . '/content/',
        // ссылки-действия
        'actions'   => array(
          //  'sections list' => LANG . '/sections/'
        ),
        // форма поиска
        'search'    => array(
            'Title_eng' => array(
                'title'        => 'Title',
                'type'         => 'string',
                'width'        => 530,
                'length'       => 64,
                'focus'        => true,
                'auto'         => true
            ),
            'Date' => array(
                'title'        => 'Date',
                'type'         => 'date'
            ),/*
            'Section_id' => array(
                'title'        => 'Section',
                'type'         => 'select',
                'table'        => LANG . '_sections',
                'field'        => 'Title',
                'orderBy'      => 'OrderBy',
                'width'        => 160
            ),
            'IsMainVis' => array(
                'title'        => 'Main',
                'type'         => 'bool'
            ),*/
            'Hide' => array(
                'title'        => 'Hide',
                'type'         => 'bool'
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Add new',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => '',
        'orderBy'   => 'Date DESC',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'        => 'Title',
                'type'        => 'string',
                'link'        => true,
                'is_sort'     => true,
                'max_letters' => 70,
                'max_words'   => 10,
                'style'       => 'width:470px;'
            ),
            'Date' => array(
                'head'        => 'Date',
                'type'        => 'datetime',
                'is_sort'     => true,
                'style'       => 'width:120px;'
            ),
            'Image' => array(
                'type'        => 'img',
                'src'         => '[ID]',
                'style'       => 'width:60px;'
            )
        ),
        'r_icon'    => array(
            /*'IsMainVis' => array(
                'image'   => 'starno.gif',
                'image2'  => 'star.gif',
                'title'   => 'del to main',
                'title2'  => 'add to main'
            ),*/
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'показывать',
                'title2'  => 'скрыть'
            ),
            'delete' => array(
                'title'   => 'delete record',
                'message' => "Delete record: '[Title]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Add record',
        'headTitleEdit' => 'Edit record',
        // данные для цепочки страниц
        'chain'         => array(
            'News' => LANG . '/' . CODE . '/'
        ),
        'fieldForChain' => 'Title',
        'сhainLastTitle' => 'New record',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'return to records list<br>without saving changes',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
            //'добавить новость' => $id != -1 ? (THIS_PAGE . '?a=add&id=-1') : false,
            //'удалить новость'  => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    //$form->data['authors'] = $form->getData('News_id', 'authors');
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
    $tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var(array(
		'Date'     => get_field_date('Date'/*field*/, isset($form->data['Date']) ? (sql_to_datetime_nosec($form->data['Date']) ? sql_to_datetime_nosec($form->data['Date']) : $form->data['Date']) : date('d.m.Y H:i:s')/*value*/, true/*is_required*/, true/*is_time*/),
	//	'Date_end' => get_field_date('Date_end'/*field*/, isset($form->data['Date_end']) ? (sql_to_datetime_nosec($form->data['Date_end']) ? sql_to_datetime_nosec($form->data['Date_end']) : $form->data['Date_end']) : ''/*value*/, false/*is_required*/, true/*is_time*/)
	));
    //-----------------------------------------------------------------------------------
   // $tpl->set_var('Sections', get_field_select($form->data/*form*/, 'Section_id'/*field*/, LANG . '_sections'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'OrderBy'/*orderBy*/, 360/*width*/, false/*is required*/));
    //-----------------------------------------------------------------------------------
    // изображения
	$tpl->set_block('main', 'image_del', 'image_del_');
//    $tpl->set_block('main', 'image2_del', 'image2_del_');
    if (!empty($form->data['ID']))
    {
        $image = is_image(PATH_TO_IMG . CODE . '/' . $form->data['ID']);
	    if ($image) //big image
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image1Title' => $form->data['ID'] . '.' . $image_info['extension'],
				'Image1Src'   => $image,
				'Image1Sizes' => $image_size[3],
				'Image1Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image_del_', 'image_del', false);
		}
/*        $image = is_image(PATH_TO_IMG . CODE . '/' . $form->data['ID'] . '_mini');
	    if ($image) //small image
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image2Title' => $form->data['ID'] . '_mini.' . $image_info['extension'],
				'Image2Src'   => $image,
				'Image2Sizes' => $image_size[3],
				'Image2Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image2_del_', 'image2_del', false);
		}
*/
    }
    //-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data, $code);
    
    if (empty($r))
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
            
        $f = processing_edit_record($data, $fields);
		
		$f[] = 'Date = "' . (!empty($data['Date']) ? datetime_to_sql($data['Date']) : date('d.m.Y H:i:s')) . '"';
	//	$f[] = 'Date_end = "' . (!empty($data['Date_end']) && string_is_datetime($data['Date_end']) ? datetime_to_sql($data['Date_end']) : '9999-12-31 23:59:59') . '"';
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $q = $q1 . implode(', ', $f) . $q2;
		
        $db->Query($q);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        
        // Загрузка изображений на сервер
        if (isset($_FILES['Image']) && empty($r))
        {
           $size_big = array(350, 480);
           $size_mini = array(656, 415);
           
           $upload = uploading_img(
                $_FILES['Image'],
                PATH_TO_IMG . CODE . '/',
                $insert_id,
                $size_big[0],
                $size_big[1],
                array(
                   array()
				   // isset($data['auto_mini_image']) ? array('x' => $size_mini[0], 'y' => $size_mini[1], 'pref' => '_mini') : array()
                )
            );
            
            if (is_array($upload)) array_merge($r, $upload);
        }
        	
        if (isset($_FILES['Image2']) && empty($r))
        {
            $size_mini = array(656, 415);
            
            $upload = uploading_img(
                $_FILES['Image2'],
                PATH_TO_IMG . CODE . '/',
                $insert_id . '_mini',
                $size_mini[0],
                $size_mini[1],
                array()
            );
            
            if (is_array($upload)) array_merge($r, $upload);
        }
        
        if (empty($r))
        {
            add_to_log(CODE . '_' . $code, $insert_id);
        }
        elseif ($code == 'add')
        {
            $db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $insert_id);
        }
    }
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_check($data, $code)
{ //print_r($data);
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Enter to title';
	if (empty($data['Announce'])) $r[] = 'Enter to announce';
	if (empty($data['Text']) || trim(strip_tags($data['Text'])) == '') $r[] = 'Enter to text';
	
	if (!string_is_datetime($data['Date'])) $r[] = 'Invalid value of the initial date (date must be in the format DD.MM.YYYY HH: MM)';
//	if ($data['Date_end'] && !string_is_datetime($data['Date_end'])) $r[] = 'Wrong value of the end date (date must be in the format DD.MM.YYYY HH: MM)';
	
	// Удаление изображений с сервера
	if (empty($r) && isset($data['ImageDel']))
	{
		$image = is_image(PATH_TO_IMG . CODE . '/' . $data['ID']);
		if ($image && !@unlink($image))
			$r[] = 'Error while deleting picture';
	}
	if (empty($r) && isset($data['Image2Del']))
	{
		$image = is_image(PATH_TO_IMG . CODE . '/' . $data['ID'] . '_mini');
		if ($image && !@unlink($image))
			$r[] = 'Error while deleting picture';
	}
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
#----------------------------------------------------------------------------------------
?>