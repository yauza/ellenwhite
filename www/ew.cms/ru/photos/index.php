<?

#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   photos block admin                                                  #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');
define ('TABLE', LANG . '_' . CODE);

$fields = array(
    'string' => array('TitleRus', 'TitleOriginal'/*, 'Description', 'Description_eng', 'Meta_keys', 'Meta_desc'*/),
    'int'    => array('Duration', 'Dues', 'Section_id', 'Style_id'),
    'bool'   => array('Hide'),
    'text'   => array('Text', 'Text_eng')
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id, array($id, $id . '_mini'), array())) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js', 'include/calendar/calendar.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Объекты',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку страниц',
        'backLink'  => LANG . '/content/',
        // ссылки-действия
        'actions'   => array(
            //'список стран' => LANG . '/countries/',
            //'список правообладателей' => LANG . '/rightholders/',
            //'список пропорций' => LANG . '/proportions/',
            //'список типов видеофайлов' => LANG . '/videos_types/',
            //'список геополитик' => LANG . '/geopolitics/',
            //'настройки'     => 'opt/?a=' . CODE
        ),
        // форма поиска
        'search'    => array(
            'TitleOriginal' => array(
                'title'        => 'Название(eng)',
                't_width'      => 80,
                'type'         => 'string',
                'width'        => 120,
                'length'       => 256,
                'auto'         => true
            ),
            'TitleRus' => array(
                'title'        => 'Название',
                't_width'      => 80,
                'type'         => 'string',
                'width'        => 120,
                'length'       => 256,
                'auto'         => true
            ),
            'Section_id' => array(
                'title'       => 'Тип',
                'type'        => 'select',
                'table'       => 'ru_sections',
                'field'       => 'Title',
                'orderBy'     => 'OrderBy',
                'width'       => 148
            ),
            'Style_id' => array(
                'title'       => 'Стиль',
                'type'        => 'select',
                'table'       => 'ru_styles',
                'field'       => 'Title',
                'orderBy'     => 'OrderBy',
                'width'       => 160
            ),
            'Hide' => array(
                'title'        => 'Скрыто',
                'type'         => 'bool',
				
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить объект',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' LEFT JOIN ru_sections AS s ON r.Section_id = s.ID',
        'orderBy'   => 'OrderBy',
        'groupBy'   => '',
        // столбцы таблицы
        'columns'   => array(
            'TitleOriginal' => array(
                'head'        => 'Название',
                'type'        => 'string',
                'link'        => true,
                'is_sort'     => true,
                'max_letters' => 50,
                'max_words'   => 10,
                'after'       => ' ([TitleRus])',
                'style'       => 'width:600px;'
            ),
            's.Title' => array(
                'head'        => 'Тип',
                'type'        => 'string',
                'style'       => 'width:200px;'
            ),
            'Image' => array(
                'type'        => 'img',
                'src'         => '[ID]_big',
                'style'       => 'width:80px;'
            )
        ),
        'r_icon'    => array(
           /* 'IsRec' => array(
                'image'   => 'goodno.gif',
                'image2'  => 'good.gif',
                'title'   => 'del is rec',
                'title2'  => 'add to rec'
            ),*/
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'show',
                'title2'  => 'hide'
            ),
            'delete' => array(
                'title'   => 'удалить объект',
                'message' => "Удалить объект: '[TitleOriginal]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавить объект',
        'headTitleEdit' => 'Редактировать объект',
        // данные для цепочки страниц
        'chain'         => array(
            'Объекты' => LANG . '/' . CODE . '/'
        ),
        'fieldForChain' => 'TitleRus',
        'сhainLastTitle' => 'Новый объект',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку объектов<br>без сохранения изменений',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
     //       'statistics viewers' => $id != -1 ? ('ru/viewers/?form[Record_id]=' . $id . '&form[Record_type]=photos') : false,
  //          'Просмотр изображений'  => $id != -1 ? ('ru/foto/?foto=' . $id) : false,
            'Сортировка изображений'  => $id != -1 ? ('ru/foto/?foto=' . $id . '&a=sort') : false,
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
    $tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
    $tpl->set_var('PHOTOS_LINK', 'ru/foto/?foto=' . $id);
    //-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var(array(
		'Date'       => get_field_date('Date'/*field*/, isset($form->data['Date']) ? (string_is_date(sql_to_date($form->data['Date'])) ? sql_to_date($form->data['Date']) : $form->data['Date']) : ''/*value*/, true/*is_required*/, false/*is_time*/),
		'Date_start' => get_field_date('Date_start'/*field*/, isset($form->data['Date_start']) ? (string_is_date(sql_to_date($form->data['Date_start'])) ? sql_to_date($form->data['Date_start']) : ($form->data['Date_start'] != '0000-00-00' ? $form->data['Date_start'] : '')) : ''/*value*/, false/*is_required*/, false/*is_time*/),
		'Date_end'   => get_field_date('Date_end'/*field*/, isset($form->data['Date_end']) ? (string_is_date(sql_to_date($form->data['Date_end'])) ? sql_to_date($form->data['Date_end']) : ($form->data['Date_end'] != '0000-00-00' ? $form->data['Date_end'] : '')) : ''/*value*/, false/*is_required*/, false/*is_time*/)
	));
    //-----------------------------------------------------------------------------------
    $tpl->set_var('Sections', get_field_select($form->data/*form*/, 'Section_id'/*field*/, LANG . '_sections'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'OrderBy'/*orderBy*/, 180/*width*/, true/*is required*/));
    //-----------------------------------------------------------------------------------
    $tpl->set_var('Styles', get_field_select($form->data/*form*/, 'Style_id'/*field*/, LANG . '_styles'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'OrderBy'/*orderBy*/, 150/*width*/, false/*is required*/));
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    // изображения
	$tpl->set_block('main', 'image_del', 'image_del_');
    if (!empty($form->data['ID']))
    {
        $image = is_image(PATH_TO_IMG . 'photos/' . $form->data['ID'] . '_big');
	    if ($image) // image for carousel
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image1Title' => $form->data['ID'] . '_big.' . $image_info['extension'],
				'Image1Src'   => $image,
				'Image1Sizes' => $image_size[3],
				'Image1Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image_del_', 'image_del', false);
		}
    }
    // изображения
	$tpl->set_block('main', 'image2_del', 'image2_del_');
    if (!empty($form->data['ID']))
    {
        $image = is_image(PATH_TO_IMG . 'photos/moz/' . $form->data['ID'] . '/'. $form->data['ID'] . '_1');
	    if ($image) // image for carousel
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image2Title' => $form->data['ID'] . '_1.' . $image_info['extension'],
				'Image2Src'   => $image,
				'Image2Sizes' => $image_size[3],
				'Image2Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image2_del_', 'image2_del', false);
		}
    }
	$tpl->set_block('main', 'image3_del', 'image3_del_');
    if (!empty($form->data['ID']))
    {
        $image = is_image(PATH_TO_IMG . 'photos/moz/' . $form->data['ID'] . '/'. $form->data['ID'] . '_2');
	    if ($image) // image for carousel
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image3Title' => $form->data['ID'] . '_2.' . $image_info['extension'],
				'Image3Src'   => $image,
				'Image3Sizes' => $image_size[3],
				'Image3Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image3_del_', 'image3_del', false);
		}
    }
	$tpl->set_block('main', 'image4_del', 'image4_del_');
    if (!empty($form->data['ID']))
    {
        $image = is_image(PATH_TO_IMG . 'photos/moz/' . $form->data['ID'] . '/'. $form->data['ID'] . '_3');
	    if ($image) // image for carousel
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image4Title' => $form->data['ID'] . '_3.' . $image_info['extension'],
				'Image4Src'   => $image,
				'Image4Sizes' => $image_size[3],
				'Image4Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image4_del_', 'image4_del', false);
		}
    }
    
    //-----------------------------------------------------------------------------------  
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $fields, $g_user;
    _record_prepare($data); $r = _record_check($data, $code);
    //echo count($_FILES['Image_moz1']['name'])."<br><pre>";print_r($_FILES['Image_moz1']);echo "</pre>";exit;
    if (empty($r))
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
            
        $f = processing_edit_record($data, $fields);
		if ($code == 'add')
		{
			$db->Query('SELECT OrderBy FROM ru_photos ORDER BY OrderBy DESC LIMIT 1');
			while($db->NextRecord())
				$ord = $db->F('OrderBy') +1;
			$f[] = 'OrderBy = "' . $ord . '"';
		}
		$f[] = 'Date = "' . (!empty($data['Date']) ? date_to_sql($data['Date']) : '0000-00-00') . '"';
        
		$f[] = 'Date_start = "' . (!empty($data['Date_start']) ? date_to_sql($data['Date_start']) : '0000-00-00') . '"';
		$f[] = 'Date_end = "' . (!empty($data['Date_end']) ? date_to_sql($data['Date_end']) : '0000-00-00') . '"';
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $q = $q1 . implode(', ', $f) . $q2;
		
        $db->Query($q);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        
        if (empty($r))
        {
            add_to_log(CODE . '_' . $code, $insert_id);
            

            //print_r($_FILES); exit;
            // Загрузка изображений на сервер
            if (!empty($_FILES['Image']['name']) && empty($r))
            {
               $size = array(228, 160);
               
               $upload = uploading_img(
                    $_FILES['Image'],
                    PATH_TO_IMG . 'photos/',
                    $insert_id . '_big',
                    $size[0],
                    $size[1],
                    array(
                        array()
                    )
                );
                
                if (is_array($upload)) $r = array_merge($r, $upload);
            }
            // Загрузка изображений для мозаики на сервер
			$folder = PATH_TO_ADMIN . '../img/ru/photos/moz/' . $insert_id.'/';
				  require_once('classSimpleImage.php');
				if (!empty($_FILES['Image_moz1']['name']) && empty($r))
				{
				if (!is_dir($folder) && !@mkdir($folder, 0777))
					$r[] = 'Ошибка при создании директории "' . $folder . '"';
				  $image = new SimpleImage();
				  $image->load($_FILES['Image_moz1']['tmp_name']);
				  $image->resize(228, 160);
				  $image->save($folder.'/'.$insert_id.'_1.jpg');
				}
				if (!empty($_FILES['Image_moz2']['name']) && empty($r))
				{
				if (!is_dir($folder) && !@mkdir($folder, 0777))
					$r[] = 'Ошибка при создании директории "' . $folder . '"';
				  $image = new SimpleImage();
				  $image->load($_FILES['Image_moz2']['tmp_name']);
				  $image->resize(228, 160);
				  $image->save($folder.'/'.$insert_id.'_2.jpg');

				}
				if (!empty($_FILES['Image_moz3']['name']) && empty($r))
				{
				if (!is_dir($folder) && !@mkdir($folder, 0777))
					$r[] = 'Ошибка при создании директории "' . $folder . '"';
				  $image = new SimpleImage();
				  $image->load($_FILES['Image_moz3']['tmp_name']);
				  $image->resize(228, 160);
				  $image->save($folder.'/'.$insert_id.'_3.jpg');
				}
			
        }
        elseif ($code == 'add')
        {
            $db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $insert_id);
        }
    }
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_check($data, $code)
{ //print_r($data);
    $r = array();
    
    if (empty($data['TitleRus'])) $r[] = 'Введите название';
    //if (empty($data['TitleOriginal'])) $r[] = 'Введите оригинальное название';
	//if (empty($data['Description'])) $r[] = 'Enter a brief description of';
//	if (empty($data['Text']) || trim(strip_tags($data['Text'])) == '') $r[] = 'Введите описание';
    
	if (empty($data['Section_id'])) $r[] = 'Выберите тип';
	
//	if (!string_is_date($data['Date'])) $r[] = 'Неверный формат даты (дата должна быть в формате ДД.ММ.ГГГГ)';
	
	// Удаление изображений с сервера
	if (empty($r) && isset($data['ImageDel']))
	{
		$image = is_image(PATH_TO_IMG . 'photos/' . $data['ID'] . '_big');
		if ($image && !@unlink($image))
			$r[] = 'Ошибка удаления изоображения';
	}
	if (empty($r) && isset($data['ImageDel2']))
	{
		$image = is_image(PATH_TO_IMG . 'photos/moz/' . $data['ID'] . '/' . $data['ID'] . '_1');
		if ($image && !@unlink($image))
			$r[] = 'Ошибка удаления изоображения';
	}
	if (empty($r) && isset($data['ImageDel3']))
	{
		$image = is_image(PATH_TO_IMG . 'photos/moz/' . $data['ID'] . '/' . $data['ID'] . '_2');
		if ($image && !@unlink($image))
			$r[] = 'Ошибка удаления изоображения';
	}
	if (empty($r) && isset($data['ImageDel4']))
	{
		$image = is_image(PATH_TO_IMG . 'photos/moz/' . $data['ID'] . '/' . $data['ID'] . '_3');
		if ($image && !@unlink($image))
			$r[] = 'Ошибка удаления изоображения';
	}
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
#----------------------------------------------------------------------------------------
function resize($cur_dir, $cur_file, $newwidth, $output_dir)
{
    $dir_name = $cur_dir;
    $olddir = getcwd();
    $dir = opendir($dir_name);
    $filename = $cur_file;
    $format='';
    if(preg_match("/.jpg/i", "$filename"))
    {
        $format = 'image/jpeg';
    }
    if (preg_match("/.gif/i", "$filename"))
    {
        $format = 'image/gif';
    }
    if(preg_match("/.png/i", "$filename"))
    {
        $format = 'image/png';
    }
    if($format!='')
    {
        list($width, $height) = getimagesize($filename);
        $newheight=$height*$newwidth/$width;
        switch($format)
        {
            case 'image/jpeg':
            $source = imagecreatefromjpeg($filename);
            break;
            case 'image/gif';
            $source = imagecreatefromgif($filename);
            break;
            case 'image/png':
            $source = imagecreatefrompng($filename);
            break;
        }
        $thumb = imagecreatetruecolor($newwidth,$newheight);
        imagealphablending($thumb, false);
        $source = @imagecreatefromjpeg("$filename");
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        $filename="$output_dir/$filename";
        @imagejpeg($thumb, $filename);
    }
}
?>