<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   photos block admin                                                  #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------

$id     = (isset($_GET['id']) and intval($_GET['id'])) ? intval($_GET['id']) : 0;
$action      = (isset($_GET['action']) and !empty($_GET['action'])) ? $_GET['action'] : '';
$file_name   = (isset($_GET['file_name']) and !empty($_GET['file_name'])) ? $_GET['file_name'] : '';

if ($id == -1 || $id == 0)
{
	$db->Query('SELECT ID FROM ru_photos ORDER BY ID DESC LIMIT 1');
	while ($db->NextRecord())
		$id = $db->F('ID') + 1;
}

$folder = PATH_TO_ADMIN . '../img/ru/photos/' . $id;

if ($id)
{
    switch ($action)
    {
        case 'add_file':
        {
            if (isset($_FILES['my_file']) and $_FILES['my_file']['error'] == 0)
            {
                $content = explode(',',file_get_contents($_FILES['my_file']['tmp_name']));

                if (!file_exists($folder))
                    mkdir($folder, 0777);

                //echo $user_folder . '/' . translite($_FILES['my_file']['name']);

                file_put_contents( $folder . '/' . translite($_FILES['my_file']['name']), base64_decode($content[1]));
                
                $db->Query('INSERT INTO ru_photos_photo SET Photos_id = ' . intval($id) . ', File = "' . addslashes(translite($_FILES['my_file']['name'])) . '"');
                
                if (!is_dir($folder . '/mini/'))
                    mkdir($folder . '/mini/', 0777);
                if (!is_dir($folder . '/icon/'))
                    mkdir($folder . '/icon/', 0777);
                
                // ресайзинг
                $full_path = $folder . '/' . translite($_FILES['my_file']['name']);
                
                $fileinfo = pathinfo($full_path);
                
                list($width, $height) = getimagesize($full_path);
                
                $new_x = 228;
                $new_y = 160;
                
                if ($width > $height)
                {
                    $coord_x = ($width - $height) / 2;
                    $coord_y = 0;
                    $width = $height;
                }
                else
                {
                    $coord_x = 0;
                    $coord_y = ($height - $width) / 2;
                    $height = $width;
                }
                
                //list($a, $b, $new_x, $new_y) = get_image_size(array($width, $height), array($size_x, $size_y));
    			$thumb1 = imagecreatetruecolor($new_x, $new_y);
    			$source = imagecreatefrom_ext($fileinfo['extension'], $full_path);
    			
				imagecopyresampled($thumb1, $source, 0, 0, intval($coord_x), intval($coord_y), $new_x, $new_y, $width, $height);
				image_ext($fileinfo['extension'], $thumb1, $folder . '/mini/' . translite($_FILES['my_file']['name']));
                imagedestroy($thumb1);
                
                $new_x = 94;
                $new_y = 94;
                
                $thumb1 = imagecreatetruecolor($new_x, $new_y);
    			$source = imagecreatefrom_ext($fileinfo['extension'], $full_path);
    			
				imagecopyresampled($thumb1, $source, 0, 0, intval($coord_x), intval($coord_y), $new_x, $new_y, $width, $height);
				image_ext($fileinfo['extension'], $thumb1, $folder . '/icon/' . translite($_FILES['my_file']['name']));
                imagedestroy($thumb1);
            }

            break;
        }

        case 'delete_file':
        {
            if ($file_name)
            {
                if (file_exists($folder . '/' . $file_name))
                {
                    unlink($folder . '/' . $file_name);
                    
                    $db->Query('DELETE FROM ru_photos_photo WHERE Photos_id = ' . intval($id) . ' AND File = "' . addslashes($file_name) . '"');
                }
            }

            break;
        }

        case 'list_files':
        {
            $filelist = Array();

            if (file_exists($folder))
            {
                $templist = get_file_list_by_glob($folder . '/*.*', array($folder . '/' => ''));

                foreach ($templist as $file)
                {
                    $filelist[] = Array('name' => $file);
                }

            }

            echo json_encode($filelist);
            break;
        }
    }
}


?>