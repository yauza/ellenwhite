<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   polls block admin                                                   #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

$fields = array(
    'string' => array('Title'),
    'int'    => array(),
    'bool'   => array('Hide'),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id, array(), array(array('table' => TABLE . '_answers', 'field' => 'Poll_id')))) // если все правильно
				location(THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js', 'include/calendar/calendar.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список опросов',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку страниц сайта',
        'backLink'  => LANG . '/content/',
        // ссылки-действия
        'actions'   => array(
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'  => 'Поиск опроса',
                'type'   => 'string',
                'width'  => 500,
                'length' => 64,
                'focus'  => true,
                'auto'   => true
            ),
            'Date' => array(
                'title'  => 'Дата',
                'type'   => 'date_interval',
                'code'   => 'Date_end'
            ),
            'Hide' => array(
                'title'  => 'Скрыты',
                'type'   => 'bool'
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить опрос',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => '',
        'orderBy'   => 'Date DESC',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'        => 'Вопрос',
                'type'        => 'string',
                'max_letters' => 80,
                'max_words'   => 14,
                'link'        => true,
                'is_sort'     => true,
                'style'       => 'width:580px;'
            ),
            'Poll_id' => array(
                'head'        => '',
                'type'        => 'sum-count',
                'title'       => 'кол-во ответов: [sum]',
                'table'       => TABLE . '_answers',
                'field_sum'   => 'Count',
                'style'       => 'width:135px;'
            ),
            'Date' => array(
                'head'        => 'Дата c',
                'type'        => 'date',
                'is_sort'     => true,
                'style'       => 'width:70px;'
            ),
            'Date_end' => array(
                'head'        => 'Дата по',
                'type'        => 'date',
                'is_sort'     => true,
                'before'      => '- ',
                'style'       => 'width:80px;'
            )
        ),
        'r_icon'    => array(
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'показывать',
                'title2'  => 'скрыть'
            ),
            'delete' => array(
                'title'   => 'удалить опрос',
                'message' => "Удалить опрос: '[Title]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление опроса',
        'headTitleEdit' => 'Редактирование опроса',
        // данные для цепочки страниц
        'chain'         => array(
            'Опросы' => LANG . '/' . CODE . '/'
        ),
        'сhainLastTitle' => 'Новый опрос',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку опросов<br>без сохранения изменений',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
            'добавить опрос' => $id != -1 ? (THIS_PAGE . '?a=add&id=-1') : false,
            'удалить опрос'  => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
	//-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var('Date', get_field_date('Date', isset($form->data['Date']) ? sql_to_date($form->data['Date']) : date('d.m.Y')));
    $tpl->set_var('Date_end', get_field_date('Date_end', isset($form->data['Date_end']) ? sql_to_date($form->data['Date_end']) : '', false));
    //-----------------------------------------------------------------------------------
    // answers
    $tpl->set_block('main', 'answers', 'answers_');
    if ($id != -1) // если редактирование
    {
		$db->Query('SELECT ID, Title, Count FROM ' . TABLE . '_answers WHERE Poll_id = ' . $form->data['ID'] . ' ORDER BY OrderBy');
		for ($i = 1; $db->NextRecord(); $i++)
		{
			$cnt = intval($db->F('Count'));
			$tpl->set_var(array(
				'ANSWER_NUM'         => $i,
				'ANSWER_TITLE'       => htmlspecialchars($db->F('Title')),
				'ANSWER_COUNT'       => $cnt,
				'ANSWER_COUNT_TITLE' => $cnt == 1 ? 'голос' : ($cnt > 1 && $cnt < 5 ? 'голоса' : 'голосов')
			));
			$tpl->parse('answers_', 'answers', true);
		}
		$tpl->set_var('ANSWER_NEXT_NUM', $i);
	}
	else
	{
		$tpl->set_var(array(
			'ANSWER_NUM'         => 1,
			'ANSWER_TITLE'       => '',
			'ANSWER_COUNT'       => 0,
			'ANSWER_COUNT_TITLE' => 'голосов',
			'ANSWER_NEXT_NUM'    => 2
		));
		$tpl->parse('answers_', 'answers', false);
	}
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); echo '<br>'; exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data);
    
    if (empty($r))
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields);
		
		$f[] = 'Date = "' . (isset($data['Date']) ? date_to_sql($data['Date']) : date('d.m.Y')) . '"';
		$f[] = 'Date_end = "' . (isset($data['Date_end']) && string_is_date($data['Date_end']) ? date_to_sql($data['Date_end']) : '9999-12-31') . '"';
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        add_to_log(CODE . '_' . $code, $insert_id);
        
        //связывание голосований и вариантов ответа
        if ($code == 'edit')
        	$db->Query('DELETE FROM ' . TABLE . '_answers WHERE Poll_id = ' . $data['ID']);
		
		foreach ($_POST['answers_title'] AS $num => $title)
    	{
    		if (empty($title)) continue;
    		$db->Query('INSERT INTO ' . TABLE . '_answers SET Poll_id = ' . $insert_id . ', Title = "' . addslashes($title) . '", Count = ' . (isset($_POST['answers_count'][$num]) ? intval($_POST['answers_count'][$num]) : 0) . ', OrderBy = ' . $num);
    	}
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Введите название';
	
	if (!string_is_date($data['Date'])) $r[] = 'Неверное значение начальной даты (Дата должна быть в формате DD.MM.YYYY)';
	if ($data['Date_end'] && !string_is_date($data['Date_end'])) $r[] = 'Неверное значение конечной даты (Дата должна быть в формате DD.MM.YYYY)';
	if (empty($_POST['answers_title'])) $r[] = 'Введите варианты ответов';
	else
	{
		$i = 0;
		foreach ($_POST['answers_title'] AS $v)
		{
			if (!empty($v)) $i++;
		}
		if ($i < 2) $r[] = 'Введите минимум 2 варианта ответов';
	}
	
    return implode('<br>', $r);;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
#----------------------------------------------------------------------------------------
?>