<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   content block admin                                                 #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------

$id     = (isset($_GET['id']) and intval($_GET['id'])) ? intval($_GET['id']) : 0;
$action      = (isset($_GET['action']) and !empty($_GET['action'])) ? $_GET['action'] : '';
$file_name   = (isset($_GET['file_name']) and !empty($_GET['file_name'])) ? $_GET['file_name'] : '';
$folder = PATH_TO_ADMIN . '../img/ru/content/' . $id;

if ($id)
{
    switch ($action)
    {
        case 'add_file':
        {
            if (isset($_FILES['my_file']) and $_FILES['my_file']['error'] == 0)
            {
                $content = explode(',',file_get_contents($_FILES['my_file']['tmp_name']));

                if (!file_exists($folder))
                    mkdir($folder, 0777);

                //echo $user_folder . '/' . translite($_FILES['my_file']['name']);

                file_put_contents( $folder . '/' . translite($_FILES['my_file']['name']), base64_decode($content[1]));
            }

            break;
        }

        case 'delete_file':
        {
            if ($file_name)
            {
                if (file_exists($folder . '/' . $file_name))
                    unlink($folder . '/' . $file_name);
            }

            break;
        }

        case 'list_files':
        {
            $filelist = Array();

            if (file_exists($folder))
            {
                $templist = get_file_list_by_glob($folder . '/*.*', array($folder . '/' => ''));

                foreach ($templist as $file)
                {
                    $filelist[] = Array('name' => $file);
                }

            }

            echo json_encode($filelist);
            break;
        }
    }
}


?>