<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   content block admin                                                 #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require('../inc.php');

define ('TABLE', LANG . '_sitemap');

$fields = array(
    'string' => array('Title', 'Description'),
    'int'    => array(),
    'bool'   => array('Hide'),
    'text'   => array('Text')
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action != ACT_LIST ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$db->Query('SELECT ID FROM ' . TABLE . ' WHERE ID = ' . $id);
		if (!$db->NextRecord()) location(PATH_TO_ADMIN . LANG . '/' . CODE . '/');
        $parent_id = $id;        
		$id = -1;
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = content_del($id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, empty($parent_id) ? '' : $parent_id, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		$db->Query('SELECT ID, Count_childs FROM ' . TABLE . ' WHERE Parent_id = 0');
		if ($db->NextRecord())
		{
			array_push($scripts, 'js/menu.js');
			$container = get_list($db->F('ID'), $db->F('Count_childs'));
		}
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($parent_id = 1, $count = 0)
{
	global $tpl, $g_user;
    $r = '';
        
	$tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'head', 'head_');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_block('record', 'lines', 'lines_');
	$tpl->set_block('record', 'line_plus', 'line_plus_');
	$tpl->set_block('record', 'actions', 'actions_');
	
	$tpl->set_var(array(
        'THIS_PAGE'       => THIS_PAGE,
		'CODE'            => CODE,
		'MAIN_SORT_CLICK' => 'window.location=\'/' . _ADMIN . '/sort/?lang=' . LANG . '&amp;id=1\'',
		'MAIN_ACT_CLICK'  => LANG . '/content/?a=add&amp;id=1'
	));
	
	if ($r == '')
	{
		$tpl->set_var('TITLE', _PHPSITELIB_SITE);
		$r .= $tpl->parse('head_', 'head', false);
	}
	
	$open_menues = isset($_COOKIE['open_' . CODE]) ? explode('|', $_COOKIE['open_' . CODE]) : array();
	
	$r .= get_list_2($parent_id, $count, $open_menues, $parent_id);
	
    return $r;
}

function get_list_2($parent_id = 1, $count = 0, $open_menues = array(), $first_id = 1)
{
	global $tpl, $g_user, $javascript; $db = new PslDb; $r = '';
	
	$db->Query('SELECT * FROM ' . TABLE . ' WHERE Parent_id = ' . intval($parent_id) . ' AND ID !=7 AND ID !=8 AND ID !=27 AND ID != 6 AND Code != "publications" ORDER BY OrderBy');
	for ($i = 1; $db->NextRecord(); $i++)
	{
		$content = $db->mRecord;
		$tpl->set_var(array('lines_' => '', 'line_plus_' => '', 'actions_' => ''));
		
		$isClilds = $content['Count_childs'] ? true : false;
		
		$param = get_parameters($content['ID'], CODE, $first_id);
		for ($j = 1; $j <= $param['level']; $j++)
		{
			if ($j < $param['level']) $code = '0';
			elseif ($isClilds) break;
			elseif ($i == $count) $code = '2';
			else $code = '1';
			$tpl->set_var('LINE_CODE', $code);
			$tpl->parse('lines_', 'lines', true);
		}
		
		$tpl->set_var(array(
			'LINE_CLASS'   => $parent_id == $first_id ? 'line' : 'line_hide',
			'ICON'         => $isClilds ? 'folder' : 'file',
			'ICON_CLICK'   => $isClilds ? 'plus_minus_click(' . $param['count_childs_all'] . ", this.previousSibling.previousSibling, true, '" . CODE . "');" : '',
			'ID'           => $content['ID'],
			'TITLE'        => htmlspecialchars($content['Title']),
			'COUNT_CHILDS' => $param['count_childs_all'],
			'ICON_1_SRC'   => 'img/' . ($param['count_childs'] > 1 ? 'sort' : '_none') . '.gif',
			'ICON_2_SRC'   => 'img/' . ($content['IsHide'] ? ('eye' . ($content['Hide'] ? 'sleep' : '')) : '_none') . '.gif',
			'ICON_2_SRC2'  => 'img/' . ($content['IsHide'] ? ('eye' . (!$content['Hide'] ? 'sleep' : '')) : '_none') . '.gif',
			'ICON_3_SRC'   => 'img/' . ($content['IsDelete'] ? 'delete' : '_none') . '.gif',
			'ICON_1_CLICK' => $param['count_childs'] > 1 ? ('window.location=\'/' . _ADMIN . '/sort/?lang=' . LANG . '&amp;id=' . $content['ID'] . '\'') : '',
			'ICON_1_ALT'   => $param['count_childs'] > 1 ? 'сортировать' : '',
			'ICON_2_ALT'   => $content['IsHide'] ? ($content['Hide'] ? 'показать' : 'скрыть') : '',
			'ICON_2_ALT2'  => $content['IsHide'] ? (!$content['Hide'] ? 'показать' : 'скрыть') : '',
			'ICON_3_ALT'   => $content['IsDelete'] ? 'удалить' : ''
		));
		
		// событие при нажатии на название
		switch ($content['Code'])
		{
			case 'none':
			{
				$tpl->set_var('HREF', $isClilds ? "javascript:plus_minus_click(" . $param['count_childs_all'] . ", document.getElementById('line_" . $content['ID'] . "'), true);" : '');
			} break;
			case 'content':
			{
				$tpl->set_var('HREF', LANG . '/' . $content['Code'] . '/?a=edit&amp;id=' . $content['ID']);
			} break;
			case 'foto':
			{
				$tpl->set_var('HREF', LANG . '/' . $content['Code'] . '/?foto=' . $content['ID']);
			} break;
			default:
                //$tpl->set_var('HREF', 'javascript:;');
				$tpl->set_var('HREF', LANG . '/' . $content['Code'] . '/');
		}
		
		if ($isClilds)
		{
			$tpl->set_var('_LINE_CODE', $i == $count ? '2' : '1');
			$tpl->parse('line_plus_', 'line_plus', false);
			if (in_array($content['ID'], $open_menues))
				$javascript .= "plus_minus_click(" . $param['count_childs_all'] . ", document.getElementById('line_" . $db->F('ID') . "'), false);";
		}
		
		// получение ссылок-действий
		$db1 = new PslDb;
		$db1->Query('SELECT sa.Title, p.Code, p.Href, p.Type FROM ' . TABLE . '_actions AS sa 
					INNER JOIN pages AS p ON sa.Page_id = p.ID 
					WHERE sa.Sitemap_id = ' . intval($content['ID']) . ' AND sa.Hide = 0 ORDER BY OrderBy');
		while ($db1->NextRecord())
		{
			$tpl->set_var('ACT_TITLE', htmlspecialchars($db1->F('Title')));
			switch ($db1->F('Type'))
			{
				case 'list':
				{
					$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')) . ($db1->F('Code') == 'foto_list' ? ('?foto=' . $content['ID'] . '&amp;a=add') : ''));
				} break;
				case 'add':
				{
					$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')) . '&amp;id=' . (strpos(' '.$db1->F('Code'), CODE) ? $content['ID'] : '-1') . ($db1->F('Code') == 'foto_add' ? ('&amp;foto=' . $content['ID']) : ''));
				} break;
				case 'edit':
				{
					if (strpos(' '.$db1->F('Code'), 'opt_') > 0)
					{
						$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')));
					}
					elseif ($content['Code'] == 'polls') // для ссылки на текущий опрос
					{
						$db2 = new PslDb;
						$db2->Query('SELECT ID FROM ' . LANG . '_polls WHERE Date <= "' . date('Y-m-d') . '" AND Date_end >= "' . date('Y-m-d') . '" LIMIT 1');
						if ($db2->NextRecord())
							$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')) . '&amp;id=' . $db2->F('ID'));
						else
							$tpl->set_var('ACT_CLICK', LANG . '/' . $content['Code'] . '/');
					}
					else
						$tpl->set_var('ACT_CLICK', '');
				} break;
				default:
					$tpl->set_var('ACT_CLICK', '');
			}
			$tpl->parse('actions_', 'actions', true);
		}
		//ссылка на редактирование ссылок-действий (видима только для пользователя - yauza)
		if ($g_user->GetLogin() == 'yauza')
		{
			$tpl->set_var(array(
				'ACT_TITLE' => 'edit',
				'ACT_CLICK' => 'adm/actions/?lang=' . LANG . '&amp;a=edit&amp;id=' . $content['ID']
			));
			$tpl->parse('actions_', 'actions', true);
		}
		
		$r .= $tpl->parse('record_', 'record', true);
		
		if ($isClilds) $r .= get_list_2($content['ID'], $content['Count_childs'], $open_menues, $first_id);
	}
	
	return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $parent_id, $err)
{
	global $db, $g_user, $fields;
    //-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $param = get_parameters($id, CODE);
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление страницы',
        'headTitleEdit' => 'Редактирование страницы'
    ));
    $form->db = $db;
    $form->data = $form->getData();
    $form->setParam(array(
        // данные для цепочки страниц
        'chain'          => array(
        ),
        'сhainLastTitle' => 'New page',
        'chainParentId'  => !empty($parent_id) ? $parent_id : $form->data['Parent_id'],
        // ссылка для возврата на предыдущую страницу
        'backTitle'      => 'вернуться к списку страниц<br>без сохранения изменений',
        'backLink'       => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'        => array(
            //'добавить подраздел' => $id != -1 ? (THIS_PAGE . '?a=add&id=-1') : false,
            //'удалить страницу' . ($param['count_childs'] > 0 ? (' и все вложенные (' . $param['count_childs']  . ')') : '')
            //                     => $id > 2 ? (THIS_PAGE . "?a=edit&id=$id&delete") : false,
            //'сортировать вложенные страницы (' . $param['count_childs']  . ')'
            //                     => $param['count_childs'] > 1 ? ('sort/?lang=' . LANG . '&id=' . $id) : false
        ),
        // сообщение об ошибке
        'error'          => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    $tpl->set_block('main', 'code', 'code_');
	//----------------------------------------------------------------------------------- 
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var(array(
    	'TYPE1' => isset($form->data['Type_id']) && $form->data['Type_id'] == 1 ? ' selected' : '',
    	'TYPE2' => isset($form->data['Type_id']) && $form->data['Type_id'] == 2 ? ' selected' : '',
    	'TYPE3' => isset($form->data['Type_id']) && $form->data['Type_id'] == 3 ? ' selected' : ''
    ));
    //-----------------------------------------------------------------------------------
    //$tpl->set_var('Contacts', get_field_select($form->data/*form*/, 'Contact_id'/*field*/, LANG . '_contacts'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'Title'/*orderBy*/, 200/*width*/, false/*is required*/));
    //----------------------------------------------------------------------------------- 
    $tpl->set_var('Code', empty($form->data['Code']) ? 'content' : $form->data['Code']);
    $tpl->set_var('Parent_id', $parent_id);
    //-----------------------------------------------------------------------------------
    // Видеофайлы
    if (!empty($form->data['ID']))
    {
        $dir = PATH_TO_ROOT . 'files/content_videos/' . $form->data['ID'] . '/';
        if (is_dir($dir) && $handle = opendir($dir))
        {
            while (false !== ($file = readdir($handle)))
            { 
                if ($file != "." && $file != "..")
                {
                    $tpl->set_var('Videofile', $file);
                    break;
                } 
            }
            closedir($handle); 
        }
    }
	
	// изображения
	$tpl->set_block('main', 'image_del', 'image_del_');
//    $tpl->set_block('main', 'image2_del', 'image2_del_');
    if (!empty($form->data['ID']))
    {
        $image = is_image(PATH_TO_IMG . CODE . '/' . $form->data['ID']);
	    if ($image) //big image
		{
			$image_size = getimagesize($image);
	    	$image_info = pathinfo($image);
			$tpl->set_var(array(
				'Image1Title' => $form->data['ID'] . '.' . $image_info['extension'],
				'Image1Src'   => $image,
				'Image1Sizes' => $image_size[3],
				'Image1Size'  => round(filesize($image) / 1024) . ' Кб.'
			));
			$tpl->parse('image_del_', 'image_del', false);
		}
	}
	//-----------------------------------------------------------------------------------
    if ($g_user->GetLogin() == 'yauza')
        $tpl->parse('code_', 'code', false);
	//-----------------------------------------------------------------------------------
	$form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); echo '<br>'; exit;
    global $db, $g_user, $fields;
    _record_prepare($data); $r = _record_check($data);
    
    if ($r == '')
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
        $f = processing_edit_record($data, $fields);
        
        if ($g_user->GetLogin() == 'yauza')
            $f[] = 'Code = "' . addslashes($data['Code']) . '"';
		
		if ($code == 'add')
        {
            $f[] = 'Parent_id = ' . $data['Parent_id'];
            $db->Query('SELECT MAX(OrderBy) AS mx FROM ' . TABLE . ' WHERE Parent_id = ' . $data['Parent_id']);
            $f[] = 'OrderBy = ' . ($db->NextRecord() ? $db->F('mx') + 1 : 1);
        }
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        add_to_log(CODE . '_' . $code, $insert_id);
        
        if ($code == 'add')
		{
			// увеличение на 1 количества вложенных страниц
			$db->Query('UPDATE ' . TABLE . ' SET Count_childs = Count_childs + 1 WHERE ID = ' . $data['Parent_id']);
			
			// добавление ссылок-действий (добавление подраздела, ...)
			//$db->Query('SELECT ID FROM pages WHERE Code = "' . CODE . '_add" AND Lang = "' . LANG . '"');
			//if ($db->NextRecord()) $page_id = $db->F('ID'); // id типа страницы - 'добавляние контента'
			//$db->Query('INSERT INTO ' . TABLE . '_actions SET Sitemap_id = ' . $insert_id . ', Title = "добавить подраздел", Page_id = ' . $page_id . ', OrderBy = 1'); 
			
		}
		//print_r($_FILES['Image']);
		// Загрузка изображений на сервер
        if (isset($_FILES['Image']) && $_FILES['Image']['name'] !='' && empty($r))
        {
           $size_big = array(350, 480);
           $size_mini = array(656, 415);
           
           $upload = uploading_img(
                $_FILES['Image'],
                PATH_TO_IMG . CODE . '/',
                $insert_id,
                $size_big[0],
                $size_big[1],
                array(
                   array()
				   // isset($data['auto_mini_image']) ? array('x' => $size_mini[0], 'y' => $size_mini[1], 'pref' => '_mini') : array()
                )
            );
            
            if (is_array($upload)) array_merge($r, $upload);
        }
        	
        if (isset($_FILES['Image2']) && empty($r))
        {
            $size_mini = array(656, 415);
            
            $upload = uploading_img(
                $_FILES['Image2'],
                PATH_TO_IMG . CODE . '/',
                $insert_id . '_mini',
                $size_mini[0],
                $size_mini[1],
                array()
            );
            
            if (is_array($upload)) array_merge($r, $upload);
        }
        
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function content_del($id)
{
	global $db;
	
	$db->Query('DELETE FROM ' . TABLE . '_actions WHERE Sitemap_id = ' . $id);
	
	$db->Query('SELECT Code, Parent_id FROM ' . TABLE . ' WHERE ID = ' . $id);
	if ($db->NextRecord())
	{
		// уменьшение на 1 количества вложенных страних
		$db->Query('UPDATE ' . TABLE . ' SET Count_childs = Count_childs - 1 WHERE ID = ' . $db->F('Parent_id'));
		
		if ($db->F('Code') == 'foto') // если удаляется фотоальбом
		{
			$dir = PATH_TO_IMG . LANG . '/' . $db->F('Code') . '/' . $id . '/';
			if (is_dir($dir)) del_dir($dir);
		}
		
		content_del_all($id, $db->F('Code'));
		
		return '';
	}
	else
		return 'Record not found';
}
#----------------------------------------------------------------------------------------
function content_del_all($id, $code)
{
	global $db;
    $ids = array();
	$db->Query('SELECT ID FROM ' . TABLE . ' WHERE Parent_id = ' . $id);
	while ($db->NextRecord()) $ids[] = $db->F('ID');
	foreach ($ids AS $v) content_del_all($v, $code);
	
	if ($code == 'foto')
		$db->Query('DELETE FROM ' . LANG . '_' . $code . ' WHERE Sitemap_id = ' . $id);
	$db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $id);
	$db->Query('DELETE FROM ' . TABLE . '_actions WHERE Sitemap_id = ' . $id);
	add_to_log($code . '_del', $id);
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Enter title';
  
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
#----------------------------------------------------------------------------------------
?>