<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   platforms block admin                                               #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

$fields = array(
    'string' => array('Title'),
    'int'    => array(),
    'bool'   => array('Hide'),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список рекламных площадок',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку страниц сайта',
        'backLink'  => LANG . '/content/',
        // ссылки-действия
        'actions'   => array(
            //'список видеороликов' => LANG . '/news/'
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'  => 'Поиск площадки',
                'type'   => 'string',
                'width'  => 610,
                'length' => 8,
                'focus'  => true,
                'auto'   => true
            ),
            'Hide' => array(
                'title'  => 'Скрыты',
                'type'   => 'bool'
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить площадку',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => '',
        'orderBy'   => 'OrderBy',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'  => 'Название',
                'type'  => 'string',
                'link'  => true,
                'style' => 'width:640px;'
            ),
            'Platform_id' => array(
                'head'  => '',
                'type'  => 'count',
                'title' => 'кол-во рекламных материалов: [count]',
                'table' => LANG . '_ads',
                'style' => 'width:250px;'
            )
        ),
        'r_icon'    => array(
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'показывать',
                'title2'  => 'скрыть'
            ),
            'delete' => array(
                'title'   => 'удалить площадку',
                'message' => 'Удалить площадку [Title]?'
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление рекламной площадки',
        'headTitleEdit' => 'Редактирование рекламной площадки',
        // данные для цепочки страниц
        'chain'         => array(
            'Студии' => LANG . '/' . CODE . '/'
        ),
        'fieldForChain' => 'Title',
        'сhainLastTitle' => 'Новая площадка',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку площадок<br>без сохранения изменений',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
            //'добавить площадку' => $id != -1 ? (THIS_PAGE . '?a=add&id=-1') : false,
            'удалить площадку'  => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); echo '<br>'; exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data, $code == 'add' ? true : false);
    
    if ($r == '')
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields);
        
        if ($code == 'add')
        {
            $db->Query('SELECT MAX(OrderBy) AS mx FROM ' . TABLE);
            $f[] = 'OrderBy = ' . ($db->NextRecord() ? (intval($db->F('mx')) + 1) : 0);
        }
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        //add_to_log(CODE . '_' . $code, $insert_id);
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{ //print_r($data);
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Введите название';
	
    implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
	
}
#----------------------------------------------------------------------------------------
?>