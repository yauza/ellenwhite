<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   foto block admin                                                    #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', 'ru_photos_photo');

$fields = array(
    'string' => array('TitleRus', 'TitleOriginal'),
    'int'    => array('Photos_id'),
    'bool'   => array('Hide'),
    'text'   => array()
);

define ('ACT_ADD_FEW',      'add_few');
define ('ACT_ADD_FEW_PROC', 'add_few_proc');
define ('ACT_ADD_FOTO',     'add_foto');
define ('ACT_EDIT_FOTO',    'edit_foto');
define ('ACT_SORT',         'sort');
define ('ACT_SORT_PROC',    'sort_proc');

$foto = isset($_GET['foto']) && intval($_GET['foto']) >= -1 ? intval($_GET['foto']) : 0; // id фотоальбома

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC:
	case ACT_ADD_FEW_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC:
	case ACT_SORT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	case ACT_ADD_FEW:
	case ACT_ADD_FOTO:
	case ACT_EDIT_FOTO:
	case ACT_SORT: define ('PAGE_CODE', CODE . '_' . ACT_LIST); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . ($foto ? '?foto=' . $foto : '') . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '&amp;a=' . $action : '') . ($id ? '&amp;id=' . $id : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

$form = $action != ACT_ADD ? GetData($id) : array();

switch ($action)
{
	case ACT_ADD_FEW_PROC:
	{
		record_add_few($foto);
	} exit;
	case ACT_ADD:
	{
		if ($id == -1) // если добавление фото (иначе, добавление подраздела)
		{
			$form['ID'] = $id;
			$action = ACT_EDIT;
		}
	} break;
	case ACT_ADD_FOTO: //добавление фотоальбома
	{
		if (!$err = foto_add_edit($foto, $form, 'add')) // если все правильно
		{
			$foto = $db->GetInsertId();
			$action = ACT_LIST;
		}
		else
			$action = ACT_ADD;
	} break;
	case ACT_EDIT_FOTO: //редактирование фотоальбома
	{
		$err = foto_add_edit($foto, $form, 'edit');
		$action = ACT_LIST;
	} break;
	case ACT_ADD_PROC: // добавление фото
	case ACT_EDIT_PROC: // редактирование фото
	{
		if (!$err = record_add_edit($form, substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $form['ID'];
			$action = ACT_EDIT;
		}
	} break;
	case ACT_SORT_PROC: // обновление порядка сортировки
	{
		if (isset($_GET['f']) && is_array($_GET['f']))
		{
			$mes = 'ok_foto';
			foreach($_GET['f'] AS $id => $order)
			{
				if ($db->Query('UPDATE LOW_PRIORITY ' . TABLE . ' SET OrderBy  = ' . $order . ' WHERE ID = ' . $id))
					continue;
				else { echo $mes = 'Ошибка!'; break; }
			}
			//add_to_log(CODE . '_content', 0);
			echo $mes;
		}
		else
			echo 'Ошибка!';
	} exit;
}
//------------------
$tpl = new Template;
$tpl->set_var(array(
	'THIS_PAGE' => THIS_PAGE,
	'LANG'      => LANG,
	'ERROR'     => empty($err) ? '' : '<br>' . $err
));
//------------------
switch ($action)
{
	case ACT_ADD_FEW:
	{
		array_push($scripts, 'js/foto.js');
		$container =  get_add_few($foto, $form);
	} break;
	case ACT_EDIT:
	{
		if ($foto != 0 && $id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($foto != 0 && $id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id, array(PATH_TO_IMG . CODE . '/' . $foto . '/' . $id, PATH_TO_IMG . CODE . '/' . $foto . '/' . $id . '_mini', PATH_TO_IMG . CODE . '/' . $foto . '/' . $id . '_icon'))) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?foto=' . $foto);
		}
		elseif ($foto != 0 && $id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container =  get_form($foto, $id, $form);
			break;
		}
	}
	case ACT_SORT:
	{
		array_push($scripts, 'js/sort.js');
		$container =  get_sort($foto);
	} break;
	default:
	{
		array_push($scripts, 'js/foto.js');
		$container = get_list($foto, $action, $form);
	}
}
#----------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($foto, $action, $form)
{
	global $tpl, $db, $javascript, $err;

	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'foto', 'foto_');
	$tpl->set_block('foto', 'record', 'record_');
	$tpl->set_block('foto', 'record_empty', 'record_empty_');
	
	$db->Query('SELECT TitleRus, TitleOriginal, Text FROM ' . LANG . '_photos WHERE ID = ' . $foto);
	if (!$db->NextRecord()) location(PATH_TO_ADMIN . LANG .'/content/');
	
	$tpl->set_var(array(
		//'ADD_HREF'     => LANG . '/' . CODE . '/?foto=' . $foto . '&amp;a=add&amp;id=-1',
		//'ADD_FEW_HREF' => LANG . '/' . CODE . '/?foto=' . $foto . '&amp;a=add_few',
		//'SORT_HREF'    => LANG . '/' . CODE . '/?foto=' . $foto . '&amp;a=sort',
		'BACK_HREF'    => LANG . '/content/',
		'ACTION'       => $action == ACT_LIST ? 'edit' : 'add',
		'ID'           => $foto,
		'PAGE_TITLE'   => $action == ACT_LIST ? htmlspecialchars($db->F('TitleRus') . ' (' . $db->F('TitleOriginal') . ')') : 'Новый подраздел',
		'LAST_UPDATE'  => '',
		'ERROR'        => $err
	));
    
    //
    /*$adds = array();
    $dir = PATH_TO_ROOT . 'img/ru/photos/' . $foto . '/';
    if ($handle = opendir($dir))
    {
        while (false !== ($file = readdir($handle)))
        { 
            if ($file != "." && $file != "..")
            {
                $db->Query('SELECT ID FROM ru_photos_photo WHERE Photos_id = ' . intval($foto) . ' AND File = "' . addslashes($file) . '"');
                if (!$db->NextRecord()) $adds[] = $file;
            } 
        }
        closedir($handle); 
    }
    foreach ($adds AS $ad)
    {
        $db->Query('INSERT INTO ru_photos_photo SET Photos_id = ' . intval($foto) . ', File = "' . addslashes($ad) . '"');
    }*/
	
	if ($action == ACT_LIST)
	{
		$db->Query('SELECT ID, TitleRus, TitleOriginal, Hide, File FROM ru_photos_photo WHERE Photos_id = ' . intval($foto) . ' ORDER BY OrderBy');
		for ($i = 0; $db->NextRecord(); $i++)
		{
			$r = $db->mRecord;
			$tpl->set_var(array(
				'FOTO_ID'     => $foto,
				'NUM'         => $i,
				'TITLE'       => get_title_formated($r['TitleRus'], 5, 35),
				'HREF'        => THIS_PAGE . '?foto=' . $foto . '&amp;a=edit&amp;id=' . $r['ID'],
				'SRC'         => PATH_TO_IMG . 'photos/' . $foto . '/' . $r['File'],
				'ICON_2_SRC'  => 'img/' . ($r['Hide'] ? 'eyesleep' : 'eye') . '.gif',
				'ICON_2_SRC2' => 'img/' . (!$r['Hide'] ? 'eyesleep' : 'eye') . '.gif',
				'ICON_3_SRC'  => 'img/' . 'delete2.gif',
				'ICON_2_ALT'  => $r['Hide'] ? 'показывать' : 'скрыть',
				'ICON_2_ALT2' => !$r['Hide'] ? 'показывать' : 'скрыть',
				'ICON_3_ALT'  => 'удалить фотографию'
			));
			$tpl->parse('record_', 'record', true);
		}
		
		$cnt =  $i;
		$cnt_all = $cnt;
		while (!is_int($cnt_all / 6)) $cnt_all++; //$cnt_all--; echo $cnt_all;
		for ($i = $cnt + 1; $i <= $cnt_all; $i++)
		{
			$tpl->set_var('NUM', $i);
			$tpl->parse('record_empty_', 'record_empty', true);
		}
		$tpl->parse('foto_', 'foto', false);
	}
	
	//$db->Query('SELECT MAX(OrderBy) AS mx FROM ' . LANG . '_sitemap WHERE Parent_id = ' . $foto);
    //$tpl->set_var('OrderBy', $db->NextRecord() ? $db->F('mx') : 1);
	
	$param = get_parameters($foto, 'content');
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	$tpl->set_var(array(
		'ACT_HREF'  => LANG.'/foto/?foto=' . $foto . '&a=sort',
		'ACT_TITLE' => 'сортировать изображения'
	));
	$tpl->parse('actions_', 'actions', true);
	//----------------------------------------------------------------------------------- 
	
    return $tpl->parse('_', 'main', true);
}
#----------------------------------------------------------------------------------------
#------------------------------------------sort------------------------------------------
#----------------------------------------------------------------------------------------
function get_sort($id)
{
    global $db;
    $r = '';
	
	$tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_sort.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_block('main', 'record_empty', 'record_empty_');
	
	$tpl->set_var(array(
		'BACK_HREF' => THIS_PAGE . '?foto=' . $id,
		'ACT_HREF'  => LANG . '/' . CODE . '/?foto=' . $id . '&amp;a=sort_proc&amp;'
	));
	
	$db->Query('SELECT COUNT(*) AS cnt FROM ' . TABLE . ' WHERE Photos_id = ' . $id);
	$cnt = $db->NextRecord() ? $db->F('cnt') : 0;
	if ($cnt < 1) return 'ошибка';
	$db->Query('SELECT TitleRus FROM ' . LANG . '_photos WHERE ID = ' . $id);
	$tpl->set_var('MAIN_TITLE', $db->NextRecord() ? htmlspecialchars($db->F('TitleRus')) : '');
	
	$cnt_all = $cnt + 5;
	while (!is_int($cnt_all / 5)) $cnt_all++;
	
	$db->Query('SELECT ID, TitleRus, File FROM ' . TABLE . ' WHERE Photos_id = ' . $id . ' ORDER BY OrderBy');
	for ($i = 1; $i <= $cnt && $db->NextRecord(); $i++)
	{
		$tpl->set_var(array(
			'NUM'   => $i,
			'ID'    => $db->F('ID'),
			'FOTO_ID'=> $id,
			'TITLE' => get_title_formated($db->F('Text'), 3, 16),
			'ICON'  => /*is_image(PATH_TO_IMG . CODE . '/' . $id . '/' . $db->F('ID') . '_icon')*/ PATH_TO_IMG . 'photos/' . $id . '/icon/' .$db->F('File')
		));
		$tpl->parse('record_', 'record', true);
	}
	for ($i = $cnt + 1; $i <= $cnt_all; $i++)
	{
		$tpl->set_var('NUM', $i);
		$tpl->parse('record_empty_', 'record_empty', true);
	}
	
	return $tpl->parse('C', 'main', false);
}
#----------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($foto, $id, $form)
{
	global $tpl, $db, $fields;
	
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
	$tpl->set_var(array(
		'FOTO'         => $foto,
		'ACTION'       => $id == -1 ? 'add' : 'edit',
		'HEAD_TITLE'   => $id == -1 ? 'Add photo' : 'Edit photo',
		'PAGE_TITLE'   => $id == -1 ? 'New photo' : htmlspecialchars($form['TitleRus']),
		'BACK_HREF'    => THIS_PAGE . '?foto=' . $foto,
		'LAST_UPDATE'  => '',
        'Photos_id'    => intval($form['Photos_id'])
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	//----------------------------------------------------------------------------------- 
    to_replace_fields($tpl, $form, $fields);
    
    //---------------------------------- изображения ------------------------------------
    $tpl->set_var('PHOTO_IMG', PATH_TO_ROOT . 'img/ru/photos/' . $form['Photos_id'] . '/' . $form['File']);
    //-----------------------------------------------------------------------------------
    
	return $tpl->parse('C', 'main', false);
}
#----------------------------------------------------------------------------------------
# get parameters as array from post or from base
function GetData($id)
{
    if (empty($_POST['form']))
		$form = GetDbData($id);
	else
	{
		$form = $_POST['form'];
		if (isset($_FILES['Image']) && $_FILES['Image']['error'] != 4) $form['Image'] = $_FILES['Image'];
		if (isset($_FILES['Image2']) && $_FILES['Image2']['error'] != 4) $form['Image2'] = $_FILES['Image2'];
	}
		
    return $form;
}
#----------------------------------------------------------------------------------------
# get parameters as array from base
function GetDbData($id)
{
    global $db;
    $r = array();
    if (string_is_id($id)) {
        $db->Query('SELECT * FROM ' . TABLE . ' WHERE ID = ' . $id);
        if ($db->NextRecord()) $r = $db->mRecord;
        else location('../../'.LANG . '/' . CODE . '/');
    }
    return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
// добавление\редактирование фотографии
function record_add_edit($data, $code) 
{ //print_r($data); echo '<br>'; exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data, $code);
    
    if (empty($r))
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
        $f = processing_edit_record($data, $fields);
		
		//if ($code == 'add') $f[] = 'OrderBy = ' . intval($data['OrderBy']);
		//$f[] = 'Date = "' . (isset($data['Date']) ? date_to_sql($data['Date']) : date('d.m.Y')) . '"';
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $q = $q1 . implode(', ', $f) . $q2;
		//echo "<br>$q<br>";
        $db->Query($q);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        
        // Загрузка изображений на сервер
        if (isset($_FILES['Image']) && empty($r))
        {
           $size_big = explode('|', get_option_by_code(CODE . '_big_image_size', LANG));
           $size_mini = explode('|', get_option_by_code(CODE . '_small_image_size', LANG));
           $size_icon = array(0 => 60, 1 => 60);
           
           $upload = uploading_img(
                $_FILES['Image'],
                PATH_TO_IMG . CODE . '/' . $data['Sitemap_id'] . '/',
                $insert_id,
                $size_big[0],
                $size_big[1],
                array(
                    isset($data['auto_mini_image']) ? array('x' => $size_mini[0], 'y' => $size_mini[1], 'pref' => '_mini') : array(),
                    array('x' => $size_icon[0], 'y' => $size_icon[1], 'pref' => '_icon')
                )
            );
            
            if (is_array($upload)) array_merge($r, $upload);
        }
        	
        if (isset($_FILES['Image2']) && empty($r))
        {
            $size_mini = explode('|', get_option_by_code(CODE . '_small_image_size', LANG));
            
            $upload = uploading_img(
                $_FILES['Image2'],
                PATH_TO_IMG . CODE . '/' . $data['Sitemap_id'] . '/',
                $insert_id . '_mini',
                $size_mini[0],
                $size_mini[1],
                array()
            );
            
            if (is_array($upload)) array_merge($r, $upload);
        }
        
        if (empty($r))
        {
            add_to_log(CODE . '_' . $code, $insert_id);
        }
        elseif ($code == 'add')
        {
            $db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $insert_id);
        }
    }
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{ //print_r($data);
    $r = array();
    
    if (empty($data['TitleRus'])) $r[] = 'Enter rus title';
    if (empty($data['TitleOriginal'])) $r[] = 'Enter original title';
	
    return $r;
}
#----------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
    
}
#----------------------------------------------------------------------------------------
?>