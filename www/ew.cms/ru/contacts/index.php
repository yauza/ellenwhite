<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   contacts block admin                                                #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', LANG . '_' . CODE);

$fields = array(
    'string' => array('Title', 'Title_eng', 'Email'),
    'int'    => array(),
    'bool'   => array('Hide', 'Field_name', 'Field_site', 'Field_email', 'Field_text', 'Field_img1', 'Field_img2'),
    'text'   => array('Message', 'Message_eng')
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id, array($id, $id . '_mini'), array(array('table' => TABLE . '_authors', 'field' => 'News_id')))) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js', 'include/calendar/calendar.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Contacts pages',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'back to list pages',
        'backLink'  => LANG . '/content/',
        // ссылки-действия
        'actions'   => array(
            //'список разделов' => LANG . '/sections/'
        ),
        // форма поиска
        'search'    => array(
            'Title_eng' => array(
                'title'        => 'Title',
                'type'         => 'string',
                'width'        => 740,
                'length'       => 64,
                'focus'        => true,
                'auto'         => true
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Add page',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => '',
        'orderBy'   => 'Title_eng',
        // столбцы таблицы
        'columns'   => array(
            'Title_eng' => array(
                'head'        => 'Title',
                'type'        => 'string',
                'link'        => true,
                'is_sort'     => true,
                'max_letters' => 70,
                'max_words'   => 10,
                'style'       => 'width:900px;'
            ),
            'Image' => array(
                'type'        => 'img',
                'src'         => '[ID]_mini',
                'style'       => 'float:right;'
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'delete page',
                'message' => "Delete page: '[Title_eng]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Add page',
        'headTitleEdit' => 'Edit page',
        // данные для цепочки страниц
        'chain'         => array(
            'Contacts pages' => LANG . '/' . CODE . '/'
        ),
        'fieldForChain' => 'Title',
        'сhainLastTitle' => 'New page',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'return to pages list<br>without saving changes',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
            //'добавить страницу' => $id != -1 ? (THIS_PAGE . '?a=add&id=-1') : false,
            //'удалить страницу'  => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
    $tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $fields;
    _record_prepare($data); $r = _record_check($data, $code);
    
    if (empty($r))
	{
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
            
        $f = processing_edit_record($data, $fields);
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $q = $q1 . implode(', ', $f) . $q2;
		
        $db->Query($q);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        
        if (empty($r))
        {
            //add_to_log(CODE . '_' . $code, $insert_id);
        }
        elseif ($code == 'add')
        {
            $db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $insert_id);
        }
    }
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_check($data, $code)
{ //print_r($data);
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Enter title';
//	if (empty($data['Email']) || !string_is_email($data['Email'])) $r[] = 'Enter e-mail';
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
#----------------------------------------------------------------------------------------
?>