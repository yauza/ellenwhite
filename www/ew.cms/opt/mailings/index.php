<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   mailings block admin                                                #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);

$fields = array(
    'string' => array('Subject'),
    'int'    => array(),
    'bool'   => array(),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = mailing_del($id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list($page);
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список рассылок',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку настроек',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
            'пользователи сайта' => LANG . '/users/'
        ),
        // форма поиска
        'search'    => array(
            'Date' => array(
                'title'   => 'Дата',
                'type'    => 'date'
            ),
            'Subject' => array(
                'title'   => 'Тема',
                'type'    => 'string',
                'width'   => 300,
                'length'  => 128,
                'focus'   => true,
                'auto'    => true
            ),
            's.Email' => array(
                'title'   => 'E-mail',
                'type'    => 'string',
                'width'   => 250,
                'length'  => 64
            )
        ),
        // ссылка для добавления
        'addTitle'  => '',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' INNER JOIN mailings_users AS ms ON r.ID = ms.Mailing_id INNER JOIN users AS s ON ms.User_id = s.ID',
        'orderBy'   => 'Date DESC',
        'groupBy'   => 'ID',
        // столбцы таблицы
        'columns'   => array(
            'Date' => array(
                'head'        => 'Дата',
                'type'        => 'datetime',
                'style'       => 'width:120px;',
                'link'        => true,
                'is_sort'     => true
            ),
            'Subject' => array(
                'head'        => 'Тема',
                'type'        => 'string',
                'style'       => 'width:600px;',
                'max_letters' => 80,
                'max_words'   => 14
            ),
            'Mailing_id' => array(
                'head'        => '',
                'type'        => 'count',
                'title'       => 'кол-во получателей: [count]',
                'table'       => TABLE . '_users',
                'href'        => false,
                'style'       => 'width:180px;'
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'удалить рассылку',
                'message' => "Удалить рассылку: '[Subject]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'             => CODE,
        'table'            => TABLE,
        'id'               => $id,
        'headTitleEdit'    => 'Просмотр рассылки за [Date]',
        // данные для цепочки страниц
        'chain'            => array(
            'Архив рассылок' => THIS_PAGE
        ),
        'fieldForChain'    => 'Date',
        // ссылка для возврата на предыдущую страницу
        'backTitle'        => 'вернуться к списку рассылок',
        'backLink'         => THIS_PAGE,
        'backImage'        => 'back2.gif',
        // ссылки-действия
        'actions'          => array(
            'пользователи'     => LANG . '/users/',
            'удалить рассылку' => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'            => $err,
        // кнопка сохранения
        'btnSaveTitleEdit' => false
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
	//----------------------------------------------------------------------------------- 
    to_replace_fields($tpl, $form->data, $fields);
	//----------------------------------------------------------------------------------- 
	$tpl->set_var('Body', $form->data['Body']);
	$tpl->set_var('Date', sql_to_datetime_nosec($form->data['Date']));
	//----------------------------------------------------------------------------------- 
	$tpl->set_block('main', 'emails', 'emails_');
	$db->Query('SELECT s.Email FROM users AS s INNER JOIN mailings_users AS ms ON s.ID = ms.User_id WHERE ms.Mailing_id = ' . $form->data['ID']);
	while ($db->NextRecord())
	{
		$tpl->set_var('Email', htmlspecialchars($db->F('Email')));
		$tpl->parse('emails_', 'emails', true);
	}
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
function mailing_del($id)
{
	global $db;
	if (!_is_record_exists($id))
		return 'Запись не найдена';
	elseif ($db->Query('DELETE FROM ' . TABLE . ' WHERE ID = "' . $id . '"'))
	{
		//add_to_log(CODE . '_del', $id);
		return '';
	}
	else
		return 'Ошибка при удалении записи';
}

?>