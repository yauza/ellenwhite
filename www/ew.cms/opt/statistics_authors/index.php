<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   statistics block admin                                              #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

// получение кода страницы
define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));

// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/');

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------

array_push($scripts, 'js/menu.js');
$container = get_list($page);
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($page)
{
	global $db, $g_user, $javascript;
    
    $tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_var(array(
    	'THIS_PAGE'   => THIS_PAGE,
		'BACK_HREF'   => LANG . '/'
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	$tpl->set_var(array(
		'ACT_HREF'  => LANG . '/mailings/',
		'ACT_TITLE' => 'архив рассылки'
	));
	//$tpl->parse('actions_', 'actions', true);
	//----------------------------------------------------------------------------------- 
	
	// ------------------------------ форма поиска ------------------------------
	if (isset($_GET['form'])) $form = $_GET['form'];
	$tpl->set_block('main', 'search_form', 'search_form_');
	$tpl->set_var(array(
		'Author'        => isset($form['Author']) ? htmlspecialchars($form['Author']) : '',
		'Date_start'    => get_field_date(/*field*/'Date_start', /*value*/!empty($_GET['form']['Date_start']) ? htmlspecialchars($_GET['form']['Date_start']) : '', /*is required*/false, /*is_time*/true),
		'Date_end'      => get_field_date(/*field*/'Date_end', /*value*/!empty($_GET['form']['Date_end']) ? htmlspecialchars($_GET['form']['Date_end']) : '', /*is required*/false, /*is_time*/true)
	));
	$tpl->parse('search_form_', 'search_form', false);
	
	$javascript .= "document.forms[0].elements['form[Author]'].focus();";
	
	if (empty($_GET['form']['Date_start']) || !string_is_datetime($_GET['form']['Date_start']) || empty($_GET['form']['Date_end']) || !string_is_datetime($_GET['form']['Date_end']))
    {
        $tpl->set_var('ERROR', '<div style="color:red;">Select a start and end date value</div><div class="space10"></div>');
    }
    else
    {
        $tpl->set_var('ERROR', '');
    }
	// -------------------------------------------------------------------------
    
    if (isset($_GET['form']['Date_start'], $_GET['form']['Date_end'], $_GET['form']['Author']))
    {
        if (!empty($_GET['form']['Author']))
        {
            $db->Query('SELECT ID FROM ru_peoples WHERE FioOriginal LIKE "%' . addslashes($_GET['form']['Author']) . '%"');
            $people_id = $db->NextRecord() ? $db->F('ID') : 0;
        }
        
        $db->Query('SELECT COUNT(*) FROM ru_viewers AS v LEFT JOIN ru_peoples_records AS pr ON v.Record_id = pr.Record_id AND v.Record_type = pr.Record_code WHERE Date <= "' . date_to_sql(trim($_GET['form']['Date_end'])) . '" AND Date >= "' . date_to_sql(trim($_GET['form']['Date_start'])) . '"' . (!empty($people_id) ? (' AND pr.People_id = ' . intval($people_id)) : ''));
        $all_viewers = $db->NextRecord() ? $db->F(0) : 0; // все просмотры за период времени
        //
        $date_start = trim($_GET['form']['Date_start']);
        $start_time = mktime(substr($date_start, 11, 2), substr($date_start, 14, 2), 0, substr($date_start, 3, 2), substr($date_start, 0, 2), substr($date_start, 6, 4));
        $date_end = trim($_GET['form']['Date_end']);
        $end_time = mktime(substr($date_end, 11, 2), substr($date_end, 14, 2), 0, substr($date_end, 3, 2), substr($date_end, 0, 2), substr($date_end, 6, 4));
        
        $sub_time = $end_time - $start_time;
        
        $all_cost = 0; // все деньги за период времени
        $db->Query('SELECT Date, Cost, Days, DAY(Date) AS Day, MONTH(Date) AS Month, YEAR(Date) AS Year, HOUR(Date) AS Hour, MINUTE(Date) AS Minute, SECOND(Date) AS Second FROM ru_payments AS p WHERE Date <= "' . date_to_sql(trim($_GET['form']['Date_end'])) . '" AND Date >= "' . date_to_sql(trim($_GET['form']['Date_start'])) . '"');
        while ($db->NextRecord())
        {
        	$current_time_start = mktime($db->F('Hour'), $db->F('Minute'), $db->F('Second'), $db->F('Month'), $db->F('Day'), $db->F('Year'));
            
        	$current_time_end = mktime($db->F('Hour'), $db->F('Minute'), $db->F('Second'), $db->F('Month'), $db->F('Day') + $db->F('Days'), $db->F('Year'));
            
            if ($current_time_end > $end_time)
                $all_cost += 100 * ($end_time -$current_time_start) / ($current_time_end - $current_time_start);
            else
                $all_cost += $db->F('Cost');
        }
        
        $peoples = array();
        $db->Query('SELECT p.ID, p.FioOriginal FROM ru_peoples AS p' . (!empty($people_id) ? (' WHERE p.ID = ' . intval($people_id)) : ''));
        while ($db->NextRecord()) $peoples[] = $db->mRecord;
        foreach ($peoples AS $people)
        {
        	$tpl->set_var('PEOPLE', htmlspecialchars($people['FioOriginal']));
            
            $db->Query('SELECT COUNT(*) FROM ru_viewers AS v LEFT JOIN ru_peoples_records AS pr ON v.Record_id = pr.Record_id AND v.Record_type = pr.Record_code WHERE pr.People_id = ' . intval($people['ID']));
            $viewers = $db->NextRecord() ? $db->F(0) : 0; // просмотры автора за период времени
            $tpl->set_var('VIEWERS', $viewers);
            
            $percents = $viewers * 100 / $all_viewers;
            $tpl->set_var('PERCENTS', number_format($percents, 2));
            
            $money = $all_cost * $percents / 100; // деньги автора за период времени
            $tpl->set_var('MONEY', number_format($money, 2));
            
            $payable = $money / 2;
            $tpl->set_var('PAYABLE', number_format($payable, 2));
            
            $tpl->parse('record_', 'record', true);
        }
        
        $tpl->set_var(array(
        	'ALL_VIEWERS' => $all_viewers,
        	'ALL_MONEY'   => number_format($all_cost, 2),
        	'ALL_PAYABLE' => number_format($all_cost / 2, 2)
        ));
    }
	// -------------------------------------------------------------------------
	
    return $tpl->parse('_', 'main', true);
}

?>