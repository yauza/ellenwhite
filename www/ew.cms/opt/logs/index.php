<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   logs block admin                                                    #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

// получение кода страницы
switch ($action)
{
	default: define ('PAGE_CODE', CODE);
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . ($action ? '?a=' . $action : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

//------------------
$tpl = new Template;
$tpl->set_var(array(
	'THIS_PAGE' => THIS_PAGE,
	'LANG'      => LANG,
	'ERROR'     => empty($err) ? '' : '<br>' . $err
));
//------------------
switch ($action)
{
	default:
	{
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $tpl, $db, $javascript;
    
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_var(array(
		'BACK_HREF' => LANG . '/'
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	//----------------------------------------------------------------------------------- 
	
	if (isset($_GET['form'])) $form = $_GET['form'];
	// ------------------------------ форма поиска ------------------------------
	$tpl->set_block('main', 'search_form', 'search_form_');
	
	$block = 'month'; // месяцы
    $tpl->set_block('search_form', $block, $block.'_');
    if ($dir = opendir(PATH_TO_ROOT . '../logs/'))
	{
		$files = array();
		while (false !== ($f = readdir($dir)))
			if ($f != '.' && $f != '..' && intval($f) <= intval(date('ym'))) $files[] = substr($f, 0, 4);
		
		arsort($files);
		foreach($files AS $k => $v)
		{
			$tpl->set_var(array(
				$block.'_ID'     => $v,
				$block.'_Title'  => (intval(substr($v, 0, 2)) > 70 ? '19' : '20') . substr($v, 0, 2) . 'г.&nbsp;&nbsp;&nbsp;' . get_month_name_simple_eng(intval(substr($v, 2, 2))),
				$block.'_Active' => isset($form['Month']) && $form['Month'] == $v ? ' selected' : ''
			));
			$tpl->parse($block.'_', $block, true);
		}
		closedir($dir); 
	}
		
	$block = 'users'; // пользователи
    $tpl->set_block('search_form', $block, $block.'_');
    $db->Query('SELECT ID, Login FROM users_a WHERE ID != 1 ORDER BY Login');
    while ($db->NextRecord())
    {
		$tpl->set_var(array(
			$block.'_ID'     => $db->F('ID'),
			$block.'_Title'  => htmlspecialchars($db->F('Login')),
			$block.'_Active' => isset($form['User_id']) && $form['User_id'] == $db->F('ID') ? ' selected' : ''
		));
		$tpl->parse($block.'_', $block, true);
	}
    
    $tpl->set_var('Record_id', isset($form['Record_id']) ? intval($form['Record_id']) : '');
    $tpl->set_var('Record_title', isset($form['Record_title']) ? htmlspecialchars($form['Record_title']) : '');
	
	$tpl->parse('search_form_', 'search_form', false);
	
	//----------------------------------------------------------------------------
	// структура файла:  дата id_юзера id_страницы id_записи
	$file = PATH_TO_ROOT . '../logs/' . date('ym') . '.log';
	if (!is_file($file)) { $f = fopen($file, 'x'); fclose($f); chmod($file, 0777); }
	$file = PATH_TO_ROOT . '../logs/' . (isset($form['Month']) && strlen($form['Month']) == 4 ? $form['Month'] : date('ym')) . '.log';
	if (!is_file($file)) location (THIS_PAGE);
	if (fileowner($file) != getmyuid()) chmod($file, 0777);
	$f = fopen($file, 'r');
	$s = array();
	while (!feof($f)) $s[] = fgets($f, 4096);
	krsort($s);
	foreach ($s AS $v)
	{
	    if (trim($v == '')) continue;
		$data = explode(' ', $v);
		$data[2] = intval($data[2]);
	    if (isset($form['User_id']) && intval($form['User_id']) != 0 && intval($form['User_id']) != intval($data[1])) continue;
	    if ($data[2] < 0)
	    {
			$data[2] *= -1;
			$flag = true;
		}
		else
			$flag = false;
        
        $record_title = ''; $page = '';
        $db->Query('SELECT Code, Title FROM pages WHERE ID = ' . $data[2]);
        if ($db->NextRecord())
        {
        	$page = $flag ? ('Delete (' . ($db->F('Code') == 'foto_list' ? 'Фото' : htmlspecialchars($db->F('Title'))) . ')') : htmlspecialchars($db->F('Title'));
            
            if (mb_strpos($db->F('Code'), 'videos', 0, 'utf-8') !== false)
            {
                $db->Query('SELECT TitleRus, TitleOriginal FROM ru_videos WHERE ID = ' . intval($data[3]));
                if ($db->NextRecord())
                	$record_title = $db->F('TitleRus');
            }
            elseif (mb_strpos($db->F('Code'), 'photos', 0, 'utf-8') !== false)
            {
                $db->Query('SELECT TitleRus, TitleOriginal FROM ru_photos WHERE ID = ' . intval($data[3]));
                if ($db->NextRecord())
                	$record_title = $db->F('TitleRus');
            }
            elseif (mb_strpos($db->F('Code'), 'news', 0, 'utf-8') !== false)
            {
                $db->Query('SELECT Title, Title_eng FROM ru_news WHERE ID = ' . intval($data[3]));
                if ($db->NextRecord())
                	$record_title = $db->F('Title');
            }
            elseif (mb_strpos($db->F('Code'), 'peoples', 0, 'utf-8') !== false)
            {
                $db->Query('SELECT FioRus, FioOriginal FROM ru_peoples WHERE ID = ' . intval($data[3]));
                if ($db->NextRecord())
                	$record_title = $db->F('FioRus');
            }
            elseif (mb_strpos($db->F('Code'), 'rightholders', 0, 'utf-8') !== false)
            {
                $db->Query('SELECT Title FROM ru_rightholders WHERE ID = ' . intval($data[3]));
                if ($db->NextRecord())
                	$record_title = $db->F('Title');
            }
            elseif (mb_strpos($db->F('Code'), 'content', 0, 'utf-8') !== false)
            {
                $db->Query('SELECT Title, Title_eng FROM ru_sitemap WHERE ID = ' . intval($data[3]));
                if ($db->NextRecord())
                	$record_title = $db->F('Title');
            }
        }
        
        if (!empty($form['Record_id']))
        {
            if (mb_strpos($data[3], $form['Record_id'], 0, 'utf-8') === false)
                continue;
        }
        if (!empty($form['Record_title']))
        {
            if (mb_strpos($record_title, $form['Record_title'], 0, 'utf-8') === false)
                continue;
        }
			
	    $date_pos = strpos($data[0], '.');
	    $tpl->set_var(array(
			'DATE'   => substr($data[0], 0, $date_pos) . ' ' . get_month_name_low(isset($form['Month']) && strlen($form['Month']) == 4 ? intval(substr($form['Month'], 2)) : date('n')) . ' ' . substr($data[0], $date_pos + 1),
			'USER'   => $db->Query('SELECT Login FROM users_a WHERE ID = ' . intval($data[1])) && $db->NextRecord() ? htmlspecialchars($db->F('Login')) : '',
			'PAGE'   => $page,
			'RECORD' => rtrim($data[3]),
            'RECORD_TITLE' => htmlspecialchars($record_title)
		));
        
		$tpl->parse('record_', 'record', true);
	}
	fclose($f);
	
    return $tpl->parse('_', 'main', false);
}

?>