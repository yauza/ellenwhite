<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   banners block admin                                                 #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);
define ('TABLE_POS', TABLE . '_pos');

define ('ACT_ADD_BAN',   'add_ban');
define ('ACT_EDIT_BAN',  'edit_ban');

$fields = array(
    'string' => array('Title', 'Code'),
    'int'    => array('Width', 'Height'),
    'bool'   => array(),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_BAN: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_BAN: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '&amp;page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

$banner = in_array($action, array(ACT_ADD_BAN, ACT_EDIT_BAN)) ? GetData($id, 'banner') : array();
$form = $action != ACT_ADD ? GetData($id, 'form') : array();
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$form['ID'] = -1;
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!$err = record_add_edit($form, substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $form['ID'];
			$action = ACT_EDIT;
		}
	} break;
	case ACT_ADD_BAN: // добавление баннера
	case ACT_EDIT_BAN: // редактирование баннера
	{
		list($err, $banner['ID']) = banner_add_edit($banner, substr($action, 0, strpos($action, '_')));
		if (!$err)
			$err = '<br>Сохранено!';
		else
			$flag = false;
		$id = $banner['Position_id'];
		$action = ACT_EDIT;
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE_POS, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE_POS, $id, array(), array(array('table' => TABLE, 'field' => 'Position_id')))) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0 && isset($_GET['ban_id']) && isset($_GET['bdelete'])) // удаление записи
		{
			if (!$err = banner_del(intval($_GET['ban_id']))) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page . '&amp;a=edit&amp;id=' . $id);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $form, $page, $banner, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list($page);
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db, $g_user;
	
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->user = $g_user;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Item list for banners',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'return to options list',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
            //'статистика баннеров' => LANG . '/banners_stats/',
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'  => 'Banner position',
                'type'   => 'string',
                'width'  => 650,
                'length' => 32,
                'focus'  => true,
                'auto'   => true
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Ass position',
        //'addLink'   => '',
        'addHide'   => '$this->user->GetLogin() != "yauza"',
        // данные для вывода записей
        'table'     => TABLE_POS,
        'where'     => '',
        'orderBy'   => 'Title',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'    => 'Position',
                'type'    => 'string',
                'style'   => 'width:640px;',
                'link'    => true,
                'is_sort' => true
            ),
            'Position_id' => array(
                'head'   => '',
                'type'  => 'count',
                'title'  => 'varieties: [count]',
                'table'  => 'banners',
                'style'  => 'width:160px;',
                'href'   => false
            ),
            'Width' => array(
                'head'   => 'width',
                'type'   => 'string',
                'style'  => 'width:25px;'
            ),
            'Height' => array(
                'head'   => 'height',
                'before' => 'x&nbsp;&nbsp;&nbsp;',
                'type'   => 'string',
                'style'  => 'width:35px;'
            )
        ),
        'r_icon'    => array(
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'показывать',
                'title2'  => 'скрыть'
            ),
            'delete' => array(
                'title'   => 'detele banner position',
                'message' => "Delete position '[Title]'",
                'hide'    => '$this->user->GetLogin() != "yauza"'
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $form, $page, $banner, $err)
{
	global $db, $g_user, $LANGS, $javascript, $fields;
	
    $tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
	$tpl->set_var(array(
    	'THIS_PAGE'   => THIS_PAGE,
    	'ERROR'       => $err,
		'PAGE_ID'     => $page,
		'ACTION'      => $id == -1 ? 'add' : 'edit',
		'HEAD_TITLE'  => $id == -1 ? 'Add banner' : 'Edit banner',
		'PAGE_TITLE'  => $id == -1 ? 'New banner' : (htmlspecialchars($form['Title']) . ' (' . intval($form['Width']) . ' x ' . intval($form['Height']) . ')'),
		'BACK_HREF'   => THIS_PAGE,
		'LAST_UPDATE' => '',
		'CHAIN'       => get_chain()
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	//----------------------------------------------------------------------------------- 
    
    $tpl->set_block('main', 'banner_form', 'banner_form_');
    if ($g_user->GetLogin() == 'yauza')
    {
    	$tpl->set_block('banner_form', 'lang', 'lang_');
	    foreach (array_keys($LANGS) AS $lang)
	    {
			$tpl->set_var(array(
				'LANG'        => $lang,
				'LANG_ACTIVE' => isset($form['Lang']) && $form['Lang'] == $lang ? ' selected' : ''
			));
			$tpl->parse('lang_', 'lang', true);
		}
		$tpl->parse('banner_form_', 'banner_form', true);
	}
	
    to_replace_fields($tpl, $form, $fields);
	
	if (isset($form['Width'], $form['Height']))
	{
		if ($form['Width'] > 200) list($_width, $_height) = array(200, $form['Height'] * 200 / $form['Width']);
		else list($_width, $_height) = array($form['Width'], $form['Height']);
		
		$tpl->set_var(array('Width_' => $_width, 'Height_' => $_height));
	}
	
    //----------------------------------------------------------------------------
    $tpl->set_block('main', 'banner_list', 'banner_list_');
	$tpl->set_block('banner_list', 'ban_title', 'ban_title_');
	$tpl->set_block('banner_list', 'banner_img', 'banner_img_');
	$tpl->set_block('banner_list', 'banner_txt', 'banner_txt_');
	if ($id != -1) // если редактирование
	{
		$tpl->set_var(array(
			'bAddTitle' => isset($banner['ID']) && $banner['ID'] == -1 && isset($banner['Title']) ? htmlspecialchars($banner['Title']) : '',
			'bAddUrl'   => isset($banner['ID']) && $banner['ID'] == -1 && isset($banner['Url']) ? htmlspecialchars($banner['Url']) : '',
			'bAddValue' => isset($banner['ID']) && $banner['ID'] == -1 && isset($banner['Value']) ? htmlspecialchars($banner['Value']) : '',
			'bAddHide'  => isset($banner['ID']) && $banner['ID'] == -1 && isset($banner['Hide']) && $banner['Hide'] == 1 ? ' checked' : ''
		));
		
		$db->Query('SELECT * FROM ' . TABLE . ' WHERE Position_id = ' . $form['ID']);
		while ($db->NextRecord())
		{
			$tpl->set_var(array(
				'bID'      => $db->F('ID'),
				'bTitle'   => htmlspecialchars($db->F('Title')),
				'bUrl'     => htmlspecialchars($db->F('Url')),
				'bHide'    => $db->F('Hide') == 1 ? ' checked' : '',
				'bType'    => intval($db->F('Type')),
				'DEL_HREF' => THIS_PAGE . '?page=' . $page . '&amp;a=edit&amp;id=' . $id . '&amp;ban_id=' . $db->F('ID') . '&amp;bdelete'
			));
			$tpl->parse('ban_title_', 'ban_title', true);
			
			if (empty($flag))
			{
				if (isset($banner['ID']) && $banner['ID'] == -1) // ошибка при добавлении баннера
				{
					$javascript .= "$('#banner_add').click();";
					if (isset($banner['Type']))
						$javascript .= "document.getElementById('ban_type').value = '" . intval($banner['Type']) . "'; change_type_banner(" . intval($banner['Type']) . ");";
					$flag = true;
				}
				elseif (isset($banner['ID']) && $banner['ID'] > 0) // после редактирования или добавления баннера
				{
					$javascript .= "$('#banner_" . intval($banner['ID']) . "').click();";
					$flag = true;
				}
				else // просто открытие (активация первого баннера в списке)
				{
					$javascript .= "$('#banner_" . $db->F('ID') . "').click();";
					$flag = true;
				}
			}
			
			switch (intval($db->F('Type')))
			{
				case 1: // если баннер-картинка
				{
					$tpl->set_var('bImage_src', is_image(PATH_TO_IMG . CODE . '/' . $db->F('ID')));
					$tpl->parse('banner_img_', 'banner_img', true);
				} break;
				case 2: // если баннер-текст
				{
					$tpl->set_var('bValue', $db->F('Value'));
					$tpl->parse('banner_txt_', 'banner_txt', true);
				} break;
			}
		}
		if (empty($flag))
			$javascript .= "$('#banner_add').click();";
		
		$tpl->parse('banner_list_', 'banner_list', false);
	}
	
	return $tpl->parse('C', 'main', false);
}
#----------------------------------------------------------------------------------------
function GetData($id, $field)
{
    if (isset($_POST[$field]) && is_array($_POST[$field]) && count($_POST[$field]) > 0)
    {
    	$form = $_POST[$field];
		if (isset($_FILES['Image']) && $_FILES['Image']['error'] != 4) $form['Image'] = $_FILES['Image'];
	}
	elseif ($field == 'form')
		$form = GetDbData($id);
	else
		$form = array();
	
	return $form;
}
#----------------------------------------------------------------------------------------
function GetDbData($id)
{
    global $db;
    $r = array();
    if (string_is_id($id)) {
        $db->Query('SELECT * FROM ' . TABLE_POS . ' WHERE ID = ' . $id);
        if ($db->NextRecord()) $r = $db->mRecord;
        else location(PATH_TOLANG . '/' . CODE . '/');
    }
    return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function banner_add_edit($data, $code)
{ //print_r($data); exit;
    global $db, $g_user;
    _banner_prepare($data); $r = _banner_check($data, $code);
    
    if (empty($r))
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
		
		if ($data['Type'] == 1 && isset($data['Image'])) // если баннер-картинка
			$data['Value'] = $data['Image']['name'];
        
		$vArr = array('Title', 'Value', 'Url'); //string
		foreach ($vArr as $v) {
			if (isset($data[$v])) $f[] = $v . ' = "' . addslashes($data[$v]) . '"';
		}
		$vArr = array(); //int
		foreach ($vArr as $v) {
			if (isset($data[$v])) $f[] = $v . ' = ' . intval($data[$v]);
		}
		
		$vArr = array('Hide'); //bool
		foreach ($vArr as $v) {
			$f[] = $v . ' = ' . (isset($data[$v]) ? 1 : 0);
		}
		
		if ($code == 'add')
		{
			$f[] = 'Position_id = ' . intval($data['Position_id']);
			$f[] = 'Type = ' . intval($data['Type']);
		}
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        
        // Загрузка изображений на сервер
        if (isset($data['Image']) && empty($r))
        {
            $db->Query('SELECT Width, Height FROM ' . TABLE_POS . ' WHERE ID = ' . $data['Position_id']);
            if ($db->NextRecord())
            {
                $upload = uploading_img(
                    $data['Image'],
                    PATH_TO_IMG . CODE . '/',
                    $insert_id,
                    intval($db->F('Width')),
                    intval($db->F('Height')),
                    array()
                );
                
                if (is_array($upload)) array_merge($r, $upload);
            }
        }
        
        if (empty($r))
        {
            //add_to_log(CODE . '_' . $code, $insert_id);
        }
        elseif ($code == 'add')
        {
            $db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $insert_id);
        }
    }
    
    if ($code == 'add')
    	return array($r, isset($insert_id) ? $insert_id : -1);
    else
    	return array($r, $data['ID']);
}
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $g_user;
    _record_prepare($data); $r = ''; //$r = _record_check($data, $code);
    
    if (empty($r))
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE_POS . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE_POS . ' SET ';
        
		$vArr = array('Title', 'Code', 'Width', 'Height', 'Lang'); //string
		foreach ($vArr as $v) {
			if (isset($data[$v])) $f[] = $v . ' = "' . addslashes($data[$v]) . '"';
		}
		$vArr = array(); //int
		foreach ($vArr as $v) {
			if (isset($data[$v])) $f[] = $v . ' = ' . intval($data[$v]);
		}
		
		$vArr = array('Hide'); //bool
		foreach ($vArr as $v) {
			$f[] = $v . ' = ' . (isset($data[$v]) ? 1 : 0);
		}
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        add_to_log(CODE . '_' . $code, $insert_id);
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function banner_del($id)
{
	global $db;
    
	$image = is_image(PATH_TO_IMG . CODE . '/' . $id);
	if ($image && !@unlink($image))
		return 'Error while deleting picture';
		
	if ($db->Query('DELETE FROM ' . TABLE . ' WHERE ID = ' . $id))
		add_to_log(CODE . '_del', $id);
}
#----------------------------------------------------------------------------------------
function _banner_check($data, $code)
{
	global $db;
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Enter to name';
	
    return $r;
}
#----------------------------------------------------------------------------------------
function _banner_prepare(&$data)
{
	$vArr = array('Title', 'Value', 'Url');
    foreach ($vArr AS $val) { if (isset($data[$val])) $data[$val] = trim($data[$val]); }
}
#----------------------------------------------------------------------------------------
function _record_check($data, $code)
{
	global $db;
    $r = array();
	
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	$vArr = array('Title', 'Code', 'Width', 'Height', 'Lang');
    foreach ($vArr AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}

?>