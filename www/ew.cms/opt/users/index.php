<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   users block admin                                                   #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);

$fields = array(
    'string' => array('Fio', 'Nick', 'Email', 'City', 'Phone', 'Interests', 'Sex'),
    'int'    => array('Rightholder_id', 'Money'),
    'bool'   => array('Hide', 99 => 'IsSubscribe', 'IsFree'),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id, array(), array(array('table' => 'mailings_' . TABLE, 'field' => 'User_id')))) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list($page);
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($page)
{
	global $db, $g_user, $javascript;
    
    $tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_var(array(
    	'THIS_PAGE'   => THIS_PAGE,
		'ADD_HREF'    => THIS_PAGE . '?a=add&amp;id=-1',
		'BACK_HREF'   => LANG . '/',
        'TABLE'       => TABLE
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	$tpl->set_var(array(
		'ACT_HREF'  => LANG . '/mailings/',
		'ACT_TITLE' => 'архив рассылки'
	));
	//$tpl->parse('actions_', 'actions', true);
	//----------------------------------------------------------------------------------- 
	
	// ------------------------------ форма поиска ------------------------------
	if (isset($_GET['form'])) $form = $_GET['form'];
	$tpl->set_block('main', 'search_form', 'search_form_');
	$tpl->set_var(array(
		'Fio'        => isset($form['Fio']) ? htmlspecialchars($form['Fio']) : '',
		'Email'      => isset($form['Email']) ? htmlspecialchars($form['Email']) : '',
		'Date_start' => get_field_date(/*field*/'Date_start', /*value*/!empty($_GET['form']['Date_start']) ? htmlspecialchars($_GET['form']['Date_start']) : '', /*is required*/false, /*is_time*/false),
		'Date_end'   => get_field_date(/*field*/'Date_end', /*value*/!empty($_GET['form']['Date_end']) ? htmlspecialchars($_GET['form']['Date_end']) : '', /*is required*/false, /*is_time*/false)
	));
	$tpl->parse('search_form_', 'search_form', false);
	
	$javascript .= "document.forms[0].elements['form[Fio]'].focus();";
	
	$where = '';
	// -------------------------------------------------------------------------
	if (isset($form)) //условие из формы поиска
	{ //print_r($form);
		$f = processing_form_search($form, array('Fio', 'Email')/*string*/, array()/*int*/, array()/*bool*/);
        
        $f[] = 'Date_reg <= "' . date_to_sql(trim($_GET['form']['Date_end'])) . '" AND Date_reg >= "' . date_to_sql(trim($_GET['form']['Date_start'])) . '"';
        
        if (count($f) > 0) $where = ' WHERE ' . implode(' AND ', $f);
	}
	// -------------------------------------------------------------------------
	
	// ------------------ выбор кол-ва страниц и сами страницы -----------------
	$count_on_page = get_count_on_page();
	$lim_begin = ($page - 1) * $count_on_page;
	$limit = "LIMIT $lim_begin, $count_on_page";
	
	$db->Query('SELECT COUNT(*) AS cnt FROM ' . TABLE . ' AS r' . $where);
	$count =  $db->NextRecord() ? $db->F('cnt') : 0;
	
	$tpl->set_var('COUNT_RECORDS', $count);
	$tpl->set_var('PAGES', get_block_pages($page, $count, $count_on_page, CODE));
	// -------------------------------------------------------------------------
	
	$db->Query('SELECT r.* FROM ' . TABLE . ' AS r' . $where . ' ORDER BY Fio ' . $limit);
	while ($db->NextRecord())
	{
		$r = $db->mRecord;
		$tpl->set_var(array(
			'ID'          => $r['ID'],
			'FIO'         => get_title_formated($r['Fio'], 3, 36),
			'EMAIL'       => get_title_formated($r['Email'], 1, 36),
            'DATE_REG'    => sql_to_date($r['Date_reg']),
			'HREF'        => THIS_PAGE . '?page=' . $page . '&amp;a=edit&amp;id=' . $r['ID'],
			'ICON_2_SRC'  => 'img/' . (($r['Hide'] ? 'eyesleep' : 'eye')) . '.gif',
			'ICON_2_SRC2' => 'img/' . ((!$r['Hide'] ? 'eyesleep' : 'eye')) . '.gif',
			'ICON_3_SRC'  => 'img/' . 'delete2.gif',
			'ICON_2_ALT'  => $r['Hide'] ? 're-block' : 'block',
			'ICON_2_ALT2' => !$r['Hide'] ? 're-block' : 'block',
			'ICON_3_ALT'  => 'delete user'
		));
		$tpl->parse('record_', 'record', true);
	}
	
	// ------------------------------- рассылка -------------------------------
	if (!empty($_POST['body']))
	{
		if (isset($_POST['users']) && is_array($_POST['users']) && count($_POST['users']) > 0)
		{
			$i = 0;
			$db->Query('INSERT INTO mailings SET Date = NOW(), Subject = "' . (!empty($_POST['subject']) ? addslashes($_POST['subject']) : '') . '", Body = "' . addslashes($_POST['body']) . '", Last_update_user = ' . intval($g_user->GetId()) . ', Last_update_date = "' . date("Y-m-d H:i:s") . '", Last_update_ip = "' . ip2long($_SERVER['REMOTE_ADDR']) . '"');
			$insert_id = $db->GetInsertId();
			
			require_once(PATH_TO_ROOT . 'include/phpmailer/class.phpmailer.php');
			
			foreach($_POST['users'] AS $id => $v)
			{
				$db->Query('SELECT Fio, Email FROM ' . TABLE . ' WHERE ID = ' . $id);
				if ($db->NextRecord())
				{
					$i++;
					$mail = new PHPMailer();
				
					$mail->SetFrom('noreply@nuart.ru', 'Nu-art');
					$mail->AddAddress(htmlspecialchars($db->F('Email')), iconv('utf-8', 'KOI8-R', htmlspecialchars($db->F('Fio'))));
					
                    $mail->Subject = !empty($_POST['subject']) ? $_POST['subject'] : '';
					//$mail->Subject = !empty($_POST['subject']) ? iconv('utf-8', 'KOI8-R', $_POST['subject']) : '';
					
					preg_match_all("/img\/opt\/mailings\/([^\"]*)/i", $_POST['body'], $images, PREG_SET_ORDER);
					foreach($images AS $img) $mail->AddAttachment(PATH_TO_ROOT . $img[0]);
					$body = preg_replace("/\/img\/opt\/mailings\/([^\"]*)/i", "$1", $_POST['body']);
					//$mail->MsgHTML(iconv('utf-8', 'KOI8-R', $body));
                    $mail->MsgHTML($body);
					
					if($mail->Send())
						$db->Query('INSERT INTO mailings_users SET Mailing_id = ' . $insert_id . ', User_id = ' . $id);
					else
					{
						$javascript .= "_alert('Error!', '" . $mail->ErrorInfo . "');";
						$db->Query('DELETE mailings WHERE ID = ' . $insert_id);
						$db->Query('DELETE mailings_users WHERE Mailing_id = ' . $insert_id);
						$i = -1;
						break;
					}
				}
			}
			
			if ($i > 0)
				$javascript .= "_alert('Result', 'Subscription success!');";
			elseif ($i = 0)
			{
				$javascript .= "_alert('Error!', 'Select users');";
				$db->Query('DELETE mailings WHERE ID = ' . $insert_id);
			}
		}
		else
			$javascript .= "_alert('Error!', 'Select users');";
	}
	
	$tpl->set_var('BODY', '');
	define ('TEST', true);
	
	// -------------------------------------------------------------------------
	
    return $tpl->parse('_', 'main', true);
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Add user',
        'headTitleEdit' => 'Edit user',
        // данные для цепочки страниц
        'chain'         => array(
            'Users' => THIS_PAGE
        ),
        'fieldForChain' => 'Nick',
        'сhainLastTitle' => 'New user',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'back to user list<br>without saving changes',
        'backLink'      => THIS_PAGE,
        // ссылки-действия
        'actions'       => array(
            //'архив рассылки'       => LANG . '/mailings/',
            //'удалить пользователя' => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
	to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var(array(
		'Date_birth' => get_field_date('Date_birth'/*field*/, isset($form->data['Date_birth']) ? (string_is_date(sql_to_date($form->data['Date_birth'])) ? sql_to_date($form->data['Date_birth']) : '') : ''/*value*/, true/*is_required*/),
        'Date_reg'   => isset($form->data['Date_reg']) ? sql_to_date($form->data['Date_reg']) : '',
        'Date_last'  => isset($form->data['Date_reg']) ? sql_to_date($form->data['Date_last']) : ''
    ));
	//-----------------------------------------------------------------------------------
    $tpl->set_var(array(
        'Sex_M' => isset($form->data['Sex']) && $form->data['Sex'] == 'м' ? ' checked' : '',
        'Sex_F' => isset($form->data['Sex']) && $form->data['Sex'] == 'ж' ? ' checked' : ''
    ));
    //-----------------------------------------------------------------------------------
    $tpl->set_var('Rightholders', get_field_select($form->data/*form*/, 'Rightholder_id'/*field*/, 'ru_rightholders'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'OrderBy'/*orderBy*/, 408/*width*/, false/*is required*/));
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $g_user, $fields;
    _record_prepare($data); $r = _record_check($data, $code == 'add' ? true : false);
    
    if (empty($r))
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields, false);
        
        if (!empty($data['Password'])) $f[] = 'Password = "' . md5($data['Password']) . '"';
        
        $f[] = 'Date_birth = "' . (!empty($data['Date_birth']) ? date_to_sql($data['Date_birth']) : '0000-00-00') . '"';
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        //add_to_log(CODE . '_' . $code, $insert_id);
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{
    $r = array();
    
    if (empty($data['Nick'])) $r[] = 'Enter login';
    if (empty($data['Fio'])) $r[] = 'Enter name';
    if (empty($data['Email'])) $r[] = 'Enter e-mail';
	
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}

?>