<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   statistics block admin                                              #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

// получение кода страницы
define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));

// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/');

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------

array_push($scripts, 'js/menu.js');
$container = get_list($page);
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($page)
{
	global $db, $g_user, $javascript;
    
    $tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_var(array(
    	'THIS_PAGE'   => THIS_PAGE,
		'BACK_HREF'   => LANG . '/'
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	$tpl->set_var(array(
		'ACT_HREF'  => LANG . '/mailings/',
		'ACT_TITLE' => 'архив рассылки'
	));
	//$tpl->parse('actions_', 'actions', true);
	//----------------------------------------------------------------------------------- 
	
	// ------------------------------ форма поиска ------------------------------
	if (isset($_GET['form'])) $form = $_GET['form'];
	$tpl->set_block('main', 'search_form', 'search_form_');
	$tpl->set_var(array(
		'Nick'          => isset($form['Nick']) ? htmlspecialchars($form['Nick']) : '',
		'Date_start'    => get_field_date(/*field*/'Date_start', /*value*/!empty($_GET['form']['Date_start']) ? htmlspecialchars($_GET['form']['Date_start']) : '', /*is required*/false, /*is_time*/true),
		'Date_end'      => get_field_date(/*field*/'Date_end', /*value*/!empty($_GET['form']['Date_end']) ? htmlspecialchars($_GET['form']['Date_end']) : '', /*is required*/false, /*is_time*/true)
	));
	$tpl->parse('search_form_', 'search_form', false);
	
	$javascript .= "document.forms[0].elements['form[Nick]'].focus();";
	
    if (empty($_GET['form']['Date_start']) || !string_is_datetime($_GET['form']['Date_start']) || empty($_GET['form']['Date_end']) || !string_is_datetime($_GET['form']['Date_end']))
    {
        $tpl->set_var('ERROR', '<div style="color:red;">Select a start and end date value</div><div class="space10"></div>');
    }
    else
    {
        $tpl->set_var('ERROR', '');
    }
	// -------------------------------------------------------------------------
    if (isset($_GET['form']['Date_start'], $_GET['form']['Date_end'], $_GET['form']['Nick']))
    {
        if (!empty($_GET['form']['Nick']))
        {
            $db->Query('SELECT ID FROM users WHERE Nick LIKE "%' . addslashes($_GET['form']['Nick']) . '%"');
            $user_id = $db->NextRecord() ? $db->F('ID') : 0;
        }
        
        $all_comments = 0; // все комментарии за период времени
        $all_cost = 0; // все деньги за период времени
        $all_yes = 0; $all_no = 0; // общее количество активных и неактивных юзеров
        $users = array();
        $db->Query('SELECT ID, Nick FROM users' . (!empty($user_id) ? (' WHERE ID = ' . intval($user_id)) : ''));
        while ($db->NextRecord()) $users[] = $db->mRecord;
        foreach ($users AS $user)
        {
        	$tpl->set_var('USER', htmlspecialchars($user['Nick']));
            
            $db->Query('SELECT Date, Cost, Days, DAY(Date) AS Day, MONTH(Date) AS Month, YEAR(Date) AS Year, HOUR(Date) AS Hour, MINUTE(Date) AS Minute, SECOND(Date) AS Second FROM ru_payments WHERE User_id = ' . $user['ID']); // здесь идет проверка активен пользователь или нет, поэтому в WHERE нет проверки на дату
            while ($db->NextRecord())
            {
                $current_time = mktime($db->F('Hour'), $db->F('Minute'), $db->F('Second'), $db->F('Month'), $db->F('Day'), $db->F('Year'));
                
                if (empty($payment_time) || $current_time > $payment_time)
                {
                    $start_date = $current_time;
                    $payment_time = mktime($db->F('Hour'), $db->F('Minute'), $db->F('Second'), $db->F('Month'), $db->F('Day') + $db->F('Days'), $db->F('Year'));
                }
                elseif ($current_time < $payment_time)
                {
                    $payment_time = mktime($ps_date['H'], $ps_date['i'], $ps_date['s'], $ps_date['m'], $ps_date['d'] + $db->F('Days'), $ps_date['Y']);
                }
                
                $ps_date = array(
                    'd' => intval(strftime("%d", $payment_time)),
                    'm' => intval(strftime("%m", $payment_time)),
                    'Y' => intval(strftime("%Y", $payment_time)),
                    'H' => intval(strftime("%H", $payment_time)),
                    'i' => intval(strftime("%M", $payment_time)),
                    's' => intval(strftime("%S", $payment_time))
                );
            }
            
            $tpl->set_var('ACTIVE', $payment_time > time() ? 'yes' : 'no');
            $all_yes += $payment_time > time() ? 1 : 0;
            $all_no += $payment_time > time() ? 0 : 1;
            
            $date_start = trim($_GET['form']['Date_start']);
            $start_time = mktime(substr($date_start, 11, 2), substr($date_start, 14, 2), 0, substr($date_start, 3, 2), substr($date_start, 0, 2), substr($date_start, 6, 4));
            $date_end = trim($_GET['form']['Date_end']);
            $end_time = mktime(substr($date_end, 11, 2), substr($date_end, 14, 2), 0, substr($date_end, 3, 2), substr($date_end, 0, 2), substr($date_end, 6, 4));
            
            $sub_time = $end_time - $start_time;
            
            $all_cost_user = 0; // все деньги за период времени для данного юзера
            $db->Query('SELECT User_id, Date, Cost, Days, DAY(Date) AS Day, MONTH(Date) AS Month, YEAR(Date) AS Year, HOUR(Date) AS Hour, MINUTE(Date) AS Minute, SECOND(Date) AS Second FROM ru_payments AS p WHERE Date <= "' . date_to_sql(trim($_GET['form']['Date_end'])) . '" AND Date >= "' . date_to_sql(trim($_GET['form']['Date_start'])) . '" AND User_id = ' . intval($user['ID']));
            while ($db->NextRecord())
            {
            	$current_time_start = mktime($db->F('Hour'), $db->F('Minute'), $db->F('Second'), $db->F('Month'), $db->F('Day'), $db->F('Year'));
                
            	$current_time_end = mktime($db->F('Hour'), $db->F('Minute'), $db->F('Second'), $db->F('Month'), $db->F('Day') + $db->F('Days'), $db->F('Year'));
                
                if ($current_time_end > $end_time)
                    $all_cost_user += 100 * ($end_time -$current_time_start) / ($current_time_end - $current_time_start);
                else
                    $all_cost_user += $db->F('Cost');
            }
            $all_cost += $all_cost_user;
            $tpl->set_var('MONEY', number_format($all_cost_user, 2));
            
            $db->Query('SELECT COUNT(*) FROM ru_comments WHERE User_id = ' . intval($user['ID']) . ' AND Date <= "' . date_to_sql(trim($_GET['form']['Date_end'])) . '" AND Date >= "' . date_to_sql(trim($_GET['form']['Date_start'])) . '"');
            $user_comments = $db->NextRecord() ? $db->F(0) : 0;
            $all_comments += $user_comments;
            $tpl->set_var('COMMENTS', $user_comments);
            
            $tpl->parse('record_', 'record', true);
        }
        
        $tpl->set_var(array(
        	'ALL_YES'      => $all_yes,
        	'ALL_NO'       => $all_no,
        	'ALL_MONEY'    => number_format($all_cost, 2),
        	'ALL_COMMENTS' => $all_comments
        ));
    }
	// -------------------------------------------------------------------------
	
    return $tpl->parse('_', 'main', true);
}

?>