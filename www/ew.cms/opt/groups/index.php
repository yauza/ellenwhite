<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   groups block admin                                                  #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);

$fields = array(
    'string' => array('Title'),
    'int'    => array(),
    'bool'   => array(),
    'text'   => array()
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '&amp;page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

foreach (array_keys($ALL_LANGS) AS $lang)
{
	$lang_pages = $lang . '_pages';
	$$lang_pages = GetPages($id, $lang);
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list($page);
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список групп',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку настроек',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
            'сисок пользователей' => LANG . '/users_a/'
        ),
        // форма поиска
        'search'    => array(
            'Title' => array(
                'title'  => 'Название',
                'type'   => 'string',
                'width'  => 700,
                'length' => 32,
                'focus'  => true,
                'auto'   => true
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить группу',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => '',
        'orderBy'   => 'Title',
        // столбцы таблицы
        'columns'   => array(
            'Title' => array(
                'head'    => 'Название',
                'type'    => 'string',
                'style'   => 'width:680px;',
                'link'    => true,
                'is_sort' => true
            ),
            'Group_id' => array(
                'head'    => '',
                'type'    => 'count',
                'title'   => 'число пользователей: [count]',
                'table'   => 'users_a',
                'style'   => 'width:200px;'
            )
        ),
        'r_icon'    => array(
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'показать',
                'title2'  => 'скрыть',
                'hide'    => "in_array([ID], array(1))"
            ),
            'delete' => array(
                'title'   => 'удалить группу',
                'message' => "Удалить группу '[Title]'",
                'hide'    => "in_array([ID], array(1))"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $g_user, $ALL_LANGS, $javascript, $fields;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_form.inc.php';
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление группы',
        'headTitleEdit' => 'Редактирование группы',
        // данные для цепочки страниц
        'chain'         => array(
            'Groups' => LANG . '/' . CODE . '/'
        ),
        'сhainLastTitle' => 'Новая группа',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку групп<br>без сохранения изменений',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
            //'cписок пользователей админки' => LANG . '/users_a/',
            //'удалить группу и всех пользователей' => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    // страницы
    $lngs = '';
    $tpl->set_block('main', 'lang_title', 'lang_title_');
    $tpl->set_block('main', 'lang', 'lang_');
	$tpl->set_block('lang', 'pages', 'pages_');
    foreach ($ALL_LANGS AS $lang => $title)
    {
    	$lang_pages = $lang . '_pages'; global $$lang_pages;
    	$lngs .= ($lang != 'ru' ? ',' : '') . '"' . $lang . '"';
		$tpl->set_var(array(
			'LANG'            => $lang,
			'LANG_TITLE'      => $title
		));
		$tpl->parse('lang_title_', 'lang_title', true);
		
		$tpl->set_var(array('pages_' => '', 'LANG' => $lang));
		$where = get_option_by_code('is_categories_news', $lang) ? '' : (' AND Code NOT LIKE "news_categories%"');
	    $db->Query('SELECT ID, Title FROM pages WHERE Lang = "' . $lang . '"' . $where . ' ORDER BY Title');
	    while ($db->NextRecord())
	    {
			$tpl->set_var(array(
				'pages_Display' => $lang == 'ru' ? 'block' : 'none',
				'pages_ID'      => $db->F('ID'),
				'pages_Title'   => htmlspecialchars($db->F('Title')),
				'pages_Active'  => in_array($db->F('ID'), $$lang_pages) ? ' checked' : ''
			));
			$tpl->parse('pages_', 'pages', true);
		}
		$tpl->parse('lang_', 'lang', true);
	}
	$tpl->set_var('LANGS', $lngs);
	$javascript .= "$('div.gr_title_langs:first').click();";
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
function GetPages($id, $lang)
{
    if (empty($_POST[$lang.'_pages']))
		$form = GetDbPages($id, $lang);
	else
		$form = $_POST[$lang.'_pages'];
		
    return $form;
}
#----------------------------------------------------------------------------------------
function GetDbPages($id, $lang)
{
    global $db;
    $r = array();
    if (string_is_id($id)) {
        $db->Query('SELECT * FROM pages AS p INNER JOIN groups_access AS ga ON p.ID = ga.Page_id WHERE p.Lang = "' . $lang . '" AND ga.Group_id = ' . $id);
        while ($db->NextRecord()) $r[$db->F('ID')] = $db->F('ID');
    }
    return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $g_user, $ALL_LANGS, $fields;
    _record_prepare($data); $r = _record_check($data);
    
    if (empty($r))
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields, false);
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
        
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        //add_to_log(CODE . '_' . $code, $insert_id);
        
        // связывание групп и страниц
        if ($code == 'edit') $db->Query('DELETE FROM groups_access WHERE Group_id = ' . $insert_id);
        
        foreach (array_keys($ALL_LANGS) AS $lang)
        {
			$lang_pages = $lang . '_pages'; global $$lang_pages;
			foreach ($$lang_pages AS $v)
				$db->Query('INSERT INTO groups_access VALUES (' . $insert_id . ', ' . $v . ')');
		}
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $isAdd = false)
{
    $r = array();
    
    if (empty($data['Title'])) $r[] = 'Enter to name';
	
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}
?>