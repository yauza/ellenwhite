<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   ban ip block admin                                                  #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);

// получение кода страницы
define ('PAGE_CODE', CODE);
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

$form = isset($_POST['form']) && is_array($_POST['form']) ? $_POST['form'] : '';

//------------------
$tpl = new Template;
$tpl->set_var(array(
	'THIS_PAGE' => THIS_PAGE,
	'LANG'      => LANG,
	'ERROR'     => empty($err) ? '' : '<br>' . $err
));
//------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['delete']) && $_GET['delete'] == '') // удаление записи
		{
			if (!$err = ban_del($id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $tpl, $db;
	
    require (_PHPSITELIB_PATH . 'psl_list.inc.php');
    
    $list = new PslList;
    $list->tpl = $tpl;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Бан-лист',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку настроек',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
            'список сообщений гостевой' => 'ru/guestbook/',
        ),
        // форма поиска
        'search'    => array(
            'IP' => array(
                'title'  => 'Поиск IP',
                'type'   => 'ip',
                'width'  => 720,
                'length' => 15,
                'focus'  => true
            )
        ),
        // ссылка для добавления
        'addTitle'  => '',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'field_id'  => 'IP',
        'where'     => '',
        'orderBy'   => 'IP DESC',
        // столбцы таблицы
        'columns'   => array(
            'IP' => array(
                'head'    => 'IP',
                'type'    => 'ip',
                'style'   => 'width:900px;',
                'is_sort' => true
            )
        ),
        'r_icon'    => array(
            'delete' => array(
                'title'   => 'удалить IP',
                'message' => 'Удалить IP: [IP]?'
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
function ban_del($id)
{
	global $db;
	if (!_is_record_exists('"'.$id.'"'))
		return 'Запись не найдена';
	elseif ($db->Query('DELETE FROM ' . TABLE . ' WHERE IP = "' . $id . '"'))
	{
		//add_to_log(CODE . '_del', $id);
		return '';
	}
	else
		return 'Ошибка при удалении записи';
}

?>