<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   statistics for banners block admin                                  #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '?page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	default:
	{
		array_push($scripts, 'js/menu.js', 'include/calendar/calendar.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require (_PHPSITELIB_PATH . 'psl_list.inc.php');
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Статистика по баннерам',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку настроек',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
            'список баннеров' => LANG . '/banners/',
            //'настройки'          => 'opt/?a=' . CODE
        ),
        // форма поиска
        'search'    => array(
            'Banner_id' => array(
                'title'   => 'Баннер',
                'type'    => 'select',
                'table'   => 'banners',
                'field'   => 'Title',
                'width'   => 430
            ),
            'Date' => array(
                'title'  => 'Месяц',
                'type'   => 'months'
            )
        ),
        // ссылка для добавления
        //'addTitle'  => 'Добавить статью',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' WHERE Banner_id = ' . (isset($_GET['form']['Banner_id']) ? intval($_GET['form']['Banner_id']) : 0),
        'orderBy'   => 'Date DESC',
        // столбцы таблицы
        'columns'   => array(
            'Date' => array(
                'type'   => 'date',
                'style'  => 'width:100px;'
            ),
            'Shows' => array(
                'type'   => 'string',
                'before' => 'кол-во просмотров: ',
                'style'  => 'width:180px;'
            ),
            'Clicks' => array(
                'type'   => 'string',
                'before' => 'кол-во кликов: '
            )
        ),
        'r_icon'    => array(
        ),
        'limits'    => array(999999999)
    ));
    
    return $list->getHTML();
}

?>