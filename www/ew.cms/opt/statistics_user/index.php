<?
#########################################################################
#                                                                       #
#   Copyright (c) 2012, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   statistics block admin                                              #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

// получение кода страницы
define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));

// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/');

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------

array_push($scripts, 'js/menu.js');
$container = get_list($page);
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($page)
{
	global $db, $g_user, $javascript;
    
    $tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_var(array(
    	'THIS_PAGE'   => THIS_PAGE,
		'BACK_HREF'   => LANG . '/'
	));
	
	//---------------------------- получение ссылок-действий ----------------------------
	$tpl->set_block('main', 'actions', 'actions_');
	$tpl->set_var(array(
		'ACT_HREF'  => LANG . '/mailings/',
		'ACT_TITLE' => 'архив рассылки'
	));
	//$tpl->parse('actions_', 'actions', true);
	//----------------------------------------------------------------------------------- 
	
	// ------------------------------ форма поиска ------------------------------
	if (isset($_GET['form'])) $form = $_GET['form'];
	$tpl->set_block('main', 'search_form', 'search_form_');
	$tpl->set_var(array(
		'Nick'          => isset($form['Nick']) ? htmlspecialchars($form['Nick']) : '',
		'Date_start'    => get_field_date(/*field*/'Date_start', /*value*/!empty($_GET['form']['Date_start']) ? htmlspecialchars($_GET['form']['Date_start']) : '', /*is required*/false, /*is_time*/true),
		'Date_end'      => get_field_date(/*field*/'Date_end', /*value*/!empty($_GET['form']['Date_end']) ? htmlspecialchars($_GET['form']['Date_end']) : '', /*is required*/false, /*is_time*/true)
	));
	$tpl->parse('search_form_', 'search_form', false);
	
	$javascript .= "document.forms[0].elements['form[Nick]'].focus();";
	
    if (empty($_GET['form']['Date_start']) || !string_is_datetime($_GET['form']['Date_start']) || empty($_GET['form']['Date_end']) || !string_is_datetime($_GET['form']['Date_end']))
    {
        $tpl->set_var('ERROR', '<div style="color:red;">Select a start and end date value</div><div class="space10"></div>');
    }
    else
    {
        $tpl->set_var('ERROR', '');
    }
	// -------------------------------------------------------------------------
    if (isset($_GET['form']['Date_start'], $_GET['form']['Date_end'], $_GET['form']['Nick']))
    {
        if (!empty($_GET['form']['Nick']))
        {
            $db->Query('SELECT ID FROM users WHERE Nick LIKE "%' . addslashes($_GET['form']['Nick']) . '%"');
            $user_id = $db->NextRecord() ? $db->F('ID') : 0;
        }
        
        $all_cost = 0;
        $db->Query('SELECT p.Date, p.Cost, u.Nick FROM ru_payments AS p LEFT JOIN users AS u ON p.User_id = u.ID WHERE p.Date <= "' . date_to_sql(trim($_GET['form']['Date_end'])) . '" AND p.Date >= "' . date_to_sql(trim($_GET['form']['Date_start'])) . '"' . (!empty($user_id) ? (' AND p.User_id = ' . intval($user_id)) : ''));
        while ($db->NextRecord())
        {
            $tpl->set_var(array(
                'USER'  => htmlspecialchars($db->F('Nick')),
            	'DATE'  => sql_to_date($db->F('Date')),
            	'MONEY' => number_format($db->F('Cost'), 2)
            ));
            $all_cost += $db->F('Cost');
            
        	$tpl->parse('record_', 'record', true);
        }
        
        $tpl->set_var(array(
        	'ALL_MONEY'    => number_format($all_cost, 2)
        ));
    }
	// -------------------------------------------------------------------------
	
    return $tpl->parse('_', 'main', true);
}

?>