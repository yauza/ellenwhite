<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   options block admin                                                 #
#                                                                       #
#########################################################################

if (!defined('IN_ADMIN')) define ('IN_ADMIN', 1);
if (!defined('PATH_TO_ADMIN')) define ('PATH_TO_ADMIN', '../');
if (!defined('PATH_TO_ROOT')) define ('PATH_TO_ROOT', PATH_TO_ADMIN . '../');
define ('LANG', 'opt');

define ('CODE', 'options');
define ('TABLE', 'options_list');

define ('ACT_LIST',      'list');
define ('ACT_EDIT',      'edit');
define ('ACT_EDIT_PROC', 'edit_proc');

$is_ajax = isset($_POST['ajax']) && $_POST['ajax'] == '' ? true : (isset($_GET['ajax']) && $_GET['ajax'] == '' ? true : false);
if ($is_ajax) header("Content-Type: text/plain; charset=utf-8");

$code = isset($_GET['a']) && preg_match('/^[a-z_]+$/i', $_GET['a']) ? $_GET['a'] : '';

$action = isset($_POST['a']) && preg_match('/^[a-z_]+$/i', $_POST['a']) ? $_POST['a'] : ($code ? $code : ACT_LIST);

$id = isset($_GET['id']) && intval($_GET['id']) >= -1 ? intval($_GET['id']) : 0;

// получение кода страницы
switch ($action)
{
	case ACT_EDIT_PROC: define ('PAGE_CODE', LANG . '_' . $code); break;
	default: define ('PAGE_CODE', LANG . ($code ? '_' . $code : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . ($code ? '?a=' . $code : '') . ($id ? '&amp;id=' . $id : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';

foreach ($LANGS AS $k => $v) $$k = GetData($k, $action);
//-----------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT_PROC:
	{
		if ($code)
		{
			$l = array();
			foreach ($LANGS AS $k => $v) $l[$k] = $$k;
			
			if (!$err = record_add_edit($code, $l)) // если все правильно
				location(PATH_TO_ADMIN . LANG . '/');
			else // иначе, если ошибка
				$action = $code;
		}
		else
			$action = ACT_LIST;
	} break;
}
//-----------------------------------------------------------------------------------
switch ($action)
{
	case ACT_LIST:
	{
		array_push($scripts, 'js/menu.js');
		$db->Query('SELECT Count_childs FROM ' . TABLE . ' WHERE Parent_id = 0');
		if ($db->NextRecord())
		{
			$container = get_list(1, $db->F('Count_childs'));
		}
	} break;
	default:
	{
		array_push($scripts, 'js/edit_form.js');
		
		$l = array();
		foreach ($LANGS AS $k => $v) $l[$k] = $$k;

		$container = get_form($action, $l, empty($err) ? '' : $err);
	}
}
//-----------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list($parent_id = 1, $count = 0)
{
	global $g_user; $db = new PslDb; $r = '';
	
    $tpl = new Template;
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '.html');
	$tpl->set_block('main', 'head', 'head_');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_block('record', 'lines', 'lines_');
	$tpl->set_block('record', 'line_plus', 'line_plus_');
	$tpl->set_block('record', 'actions', 'actions_');
    
	if ($r == '' && $parent_id == 1)
	{
		$tpl->set_var('TITLE', 'Настройки');
		$r .= $tpl->parse('head_', 'head', false);
	}
	
	$db->Query('SELECT * FROM ' . TABLE . ' WHERE Parent_id = ' . $parent_id . ' ORDER BY OrderBy');
	for ($i = 1; $db->NextRecord(); $i++)
	{
		$tpl->set_var(array('lines_' => '', 'line_plus_' => '', 'actions_' => ''));
		
		$isClilds = $db->F('Count_childs') ? true : false;
		
		$param = get_parameters($db->F('ID'), CODE);
		for ($j = 1; $j <= $param['level']; $j++)
		{
			if ($j < $param['level']) $code = '0';
			elseif ($isClilds) break;
			elseif ($i == $count) $code = '2';
			else $code = '1';
			$tpl->set_var('LINE_CODE', $code);
			$tpl->parse('lines_', 'lines', true);
		}
		
		$tpl->set_var(array(
			'LINE_CLASS' => $parent_id == 1 ? 'line' : 'line_hide',
			'ICON'       => $isClilds ? 'folder' : 'file',
			'ICON_CLICK' => $isClilds ? 'plus_minus_click(' . $param['count_childs_all'] . ', this.previousSibling.previousSibling);' : '',
			'ID'         => $db->F('ID'),
			'TITLE'      => htmlspecialchars($db->F('Title'))
		));
		
		// событие при нажатии на название
		switch ($db->F('Code'))
		{
			case 'mailings':
			case 'banners':
			case 'users_a':
			case 'groups':
			case 'users':
			case 'logs':
			case 'ban':
			case 'statistics_authors':
			case 'statistics_author':
			case 'statistics_content':
			case 'statistics_users':
			case 'statistics_user':
			{
				$tpl->set_var('HREF', THIS_PAGE . htmlspecialchars($db->F('Code')) . '/');
			} break;
			case 'open_folder':
			{
				$tpl->set_var('HREF', $isClilds ? 'javascript:plus_minus_click(' . $param['count_childs_all'] . ", document.getElementById('line_" . $db->F('ID') . "'));" : '');
			} break;
			default:
				$tpl->set_var('HREF', THIS_PAGE . '?a=' . htmlspecialchars($db->F('Code')));
		}
		
		if ($isClilds)
		{
			$tpl->set_var('LINE_CODE', $i == $count ? '2' : '1');
			$tpl->set_var('COUNT_CHILDS', $db->F('Count_childs'));
			$tpl->parse('line_plus_', 'line_plus', false);
		}
		
		// получение ссылок-действий
		$db1 = new PslDb;
		$db1->Query('SELECT oa.Title, p.Href, p.Type FROM ' . CODE . '_actions AS oa 
					INNER JOIN pages AS p ON oa.Page_id = p.ID 
					WHERE oa.Sitemap_id = ' . $db->F('ID') . ' AND oa.Hide = 0 ORDER BY OrderBy');
		while ($db1->NextRecord())
		{
			$tpl->set_var('ACT_TITLE', htmlspecialchars($db1->F('Title')));
			switch ($db1->F('Type'))
			{
				case 'list':
				{
					$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')));
				} break;
				case 'add':
				{
					$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')) . '&amp;id=' . (strpos(' '.$db1->F('Code'), CODE) ? $db->F('ID') : '-1'));
				} break;
				case 'edit':
				{
					if (strpos($db1->F('Code'), 'opt_') >= 0)
						$tpl->set_var('ACT_CLICK', htmlspecialchars($db1->F('Href')));
					else
						$tpl->set_var('ACT_CLICK', '');
				} break;
				default:
					$tpl->set_var('ACT_CLICK', '');
			}
			$tpl->parse('actions_', 'actions', true);
		}
		//ссылка на редактирование ссылок-действий (видима только для пользователя - yauza)
		if ($g_user->GetLogin() == 'yauza')
		{
			$tpl->set_var(array(
				'ACT_TITLE' => 'edit',
				'ACT_CLICK' => 'adm/actions/?lang=' . LANG . '&amp;a=edit&amp;id=' . $db->F('ID')
			));
			$tpl->parse('actions_', 'actions', true);
		}
		
		$r .= $tpl->parse('record_', 'record', true);
		
		if ($isClilds) $r .= get_list($db->F('ID'), $db->F('Count_childs'));
	}
	
    return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($code, $lngs, $err)
{
	global $db, $g_user, $LANGS;
	//-----------------------------------------------------------------------------------
    require (_PHPSITELIB_PATH . 'psl_form.inc.php');
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        //'code'           => CODE,
        //'table'          => TABLE,
        'id'             => 0,
        'thisPage'       => THIS_PAGE . '?a=' . $code,
        'headTitleEdit'  => 'Edit options',
        // данные для цепочки страниц
        'chain'          => array(
        ),
        'chainCode'      => $code,
        // ссылка для возврата на предыдущую страницу
        'backTitle'      => 'return to options list<br>without saving changes',
        'backLink'       => THIS_PAGE,
        // сообщение об ошибке
        'error'          => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_edit.html');
	//-----------------------------------------------------------------------------------
	$db->Query('SELECT Title FROM ' . TABLE . ' WHERE Code = "' . $code . '"');
	if (!$db->NextRecord()) location(PATH_TO_ADMIN . CODE . '/');
    $form->setParam('chainLastTitle', $db->F('Title'));
	//-----------------------------------------------------------------------------------
	// получение ссылок-действий
	if ($code != 'foto')
	{
		$db->Query('SELECT Title, Href, Lang FROM pages WHERE Code = "' . $code . '_list"');
		while ($db->NextRecord())
			$form->actions[mb_strtolower(htmlspecialchars($db->F('Title')), 'utf-8') . ($db->F('Lang') ? (' [' . mb_strtolower($LANGS[$db->F('Lang')], 'utf-8') . ']') : '')] = htmlspecialchars($db->F('Href'));
	}
	//----------------------------------------------------------------------------------- 
    $tpl->set_block('main', 'lang', 'lang_');
    $tpl->set_block('lang', 'int_or_string', 'int_or_string_');
    $tpl->set_block('lang', 'bool', 'bool_');
    $tpl->set_block('lang', 'text', 'text_');
    $tpl->set_block('lang', 'file', 'file_');
    
    foreach ($lngs AS $lang => $lng)
    { //print_r($lng);
	    $tpl->set_var(array(
			'int_or_string_' => '',
			'bool_'          => '',
			'text_'          => '',
			'file_'          => '',
			'LANG'           => $lang,
			'TITLE_LANG'     => $LANGS[$lang]
		));
    	foreach ($lng AS $k => $v)
    	{
    		$db->Query('SELECT Title, Type, Length FROM ' . CODE . ' WHERE Code = "' . $code . '_' . $k . '" AND Lang = "' . $lang . '" ORDER BY Title');
    		if ($db->NextRecord())
    		{
				switch (intval($db->F('Type')))
				{
					case 1: case 2: case 4: // если int или string или text
					{
						$tpl->set_var(array(
							'TITLE'  => htmlspecialchars($db->F('Title')),
							'CODE'   => htmlspecialchars($k),
							'VALUE'  => intval($db->F('Type')) == 1 ? intval($v) : htmlspecialchars($v),
							'LENGTH' => intval($db->F('Length'))
						));
						if (intval($db->F('Type')) == 4)
							$tpl->parse('text_', 'text', true);
						else
							$tpl->parse('int_or_string_', 'int_or_string', true);
					} break;
					case 3: // если bool
					{
						$tpl->set_var(array(
							'TITLE'   => htmlspecialchars($db->F('Title')),
							'CODE'    => htmlspecialchars($k),
							'VALUE_1' => $v == 1 ? ' selected' : '',
							'VALUE_0' => $v == 0 ? ' selected' : ''
						));
						$tpl->parse('bool_', 'bool', true);
					} break;
					case 5:
					{
						$tpl->set_var(array(
							'TITLE' => htmlspecialchars($db->F('Title')),
							'CODE'  => htmlspecialchars($k),
							'VALUE' => is_file(PATH_TO_ROOT . 'files/' . $v) ? htmlspecialchars($v) : 'нет',
						));
						$tpl->parse('file_', 'file', true);
					} break;
				}
			}
		}
		$tpl->parse('lang_', 'lang', true);
	}
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
function GetData($lang, $code)
{
	if (empty($_POST[$lang]) && empty($_FILES))
		$r = GetDbData($lang, $code);
	else
	{
		$r = $_POST[$lang];
		if (!empty($_FILES))
		{
			foreach($_FILES AS $lang_code => $value)
			{
				list($lng, $code) = explode('__', $lang_code);
				if ($lng == $lang && $value['error'] != 4) $r[$code] = $value;
			}
		}
	}
    return $r;
}
#----------------------------------------------------------------------------------------
function GetDbData($lang, $code)
{
    global $db;
    $r = array();
    $db->Query('SELECT Code, Value FROM ' . CODE . ' WHERE Code LIKE "' . $code . '_%" AND Lang = "' . $lang . '" ORDER BY Title');
    while ($db->NextRecord())
        $r[str_replace($code . '_', '', $db->F('Code'))] = $db->F('Value');

    return $r;
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($code, $lngs) 
{ //print_r($lngs); echo '<br>'; exit;
    global $db; $r = '';
    
	$query = array();
	foreach ($lngs AS $lang => $lng)
	{//print_r($lng); exit;
		foreach ($lng AS $k => $v)
		{
			$db->Query('SELECT Title, Type, Length FROM ' . CODE . ' WHERE Code = "' . $code . '_' . $k . '" AND Lang = "' . $lang . '"');
		    if ($db->NextRecord())
		    {
		    	$q  = 'UPDATE ' . CODE . ' SET';
		    	
		    	switch (intval($db->F('Type')))
				{
					case 1: // если int
					case 3: // если bool
					{
						$q .= ' Value = "' . intval($v) . '"';
					} break;
					case 2: // если string
					case 4: // если text
					{
						$q .= ' Value = "' . addslashes($v) . '"';
					} break;
					case 5: // если file
					{
						if (!empty($v))
						{
							//@unlink('');
							$name = translite($v['name']);
							//echo '@move_uploaded_file(' . $v['tmp_name'] . ", PATH_TO_ROOT . 'files/' . $name)";exit;
							@move_uploaded_file($v['tmp_name'], PATH_TO_ROOT . 'files/' . $name);
							$q .= ' Value = "' . addslashes($name) . '"';
						}
						else
							$q .= ' Value = ""';
					} break;
					default:
						$q .= ' Value = "' . $v . '"';
				}
				
				$q .= ' WHERE Code = "' . $code . '_' . $k . '" AND Lang = "' . $lang . '"';
				
				$query[] = $q;
			}
		}
	}
	
	foreach($query AS $v) if (!$db->Query($v)) { $r = 'Ошибка'; break; }
	
	add_to_log('options_' . $code, 0);
	
    return $r;
}
?>