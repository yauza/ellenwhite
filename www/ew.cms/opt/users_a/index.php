<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   users block admin                                                   #
#                                                                       #
#########################################################################

define ('PATH_TO_ADMIN', '../../');
require_once('../inc.php');

define ('TABLE', CODE);

$fields = array(
    'string' => array('Login', 'Email', 'Fio', 'Phone', 'Organization', 'Post', 'Comments'),
    'int'    => array('Group_id'/*, 'Rightholder_id'*/),
    'bool'   => array('Hide'),
    'text'   => array('Text')
);

// получение кода страницы
switch ($action)
{
	case ACT_ADD_PROC: define ('PAGE_CODE', CODE . '_' . ACT_ADD); break;
	case ACT_EDIT_PROC: define ('PAGE_CODE', CODE . '_' . ACT_EDIT); break;
	default: define ('PAGE_CODE', CODE . ($action ? '_' . $action : ''));
}
// получение путя к странице
define ('PAGE_PATH', LANG . '/' . CODE . '/' . (in_array($action, array(ACT_ADD, ACT_EDIT)) ? '?a=' . $action : '') . ($id ? '&amp;id=' . $id : '') . ($page != 1 ? '&amp;page=' . $page : ''));

require PATH_TO_ADMIN . 'inc/init.inc.php';
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_ADD:
	{
		$action = ACT_EDIT;
	} break;
	case ACT_ADD_PROC: // добавление записи
	case ACT_EDIT_PROC: // редактирование записи
	{
		if (!empty($_POST['form']) && !$err = record_add_edit($_POST['form'], substr($action, 0, strpos($action, '_')))) // если все правильно
			$action = ACT_LIST;
		else // иначе, если ошибка
		{
			$id = $_POST['form']['ID'];
			$action = ACT_EDIT;
		}
	} break;
}
//---------------------------------------------------------------------------------------
switch ($action)
{
	case ACT_EDIT:
	{
		if ($id != 0 && isset($_GET['field']) && string_is_login($_GET['field'])) // редактирование bool поля
		{
			echo record_change_bool_field(TABLE, $id, $_GET['field']);	exit;
		}
		elseif ($id != 0 && isset($_GET['delete'])) // удаление записи
		{
			if (!$err = record_del(TABLE, $id)) // если все правильно
				location(PATH_TO_ADMIN . THIS_PAGE . '?page=' . $page);
		}
		elseif ($id != 0) // редактирование записи
		{
			array_push($scripts, 'js/edit_form.js');
			$container = get_form($id, $page, empty($err) ? '' : $err);
			break;
		}
	}
	default:
	{
		array_push($scripts, 'js/menu.js');
		$container = get_list();
	}
}
//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo (empty($err) ? '' : $err) . $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------list------------------------------------------
#----------------------------------------------------------------------------------------
function get_list()
{
	global $db;
	//-----------------------------------------------------------------------------------
    require _PHPSITELIB_PATH . 'psl_list.inc.php';
    //-----------------------------------------------------------------------------------
    $list = new PslList;
    $list->tpl = new Template;
    $list->db = $db;
    $list->setParam(array(
        'code'      => CODE,
        'headTitle' => 'Список пользователей админки',
        // ссылка для возврата на предыдущую страницу
        'backTitle' => 'вернуться к списку настроек',
        'backLink'  => LANG . '/',
        // ссылки-действия
        'actions'   => array(
            'список групп админки' => LANG . '/groups/'
        ),
        // форма поиска
        'search'    => array(
            'Fio' => array(
                'title'   => 'Имя',
                'type'    => 'string',
                'width'   => 160,
                'length'  => 32,
                'focus'   => true,
                'auto'    => true
            ),
            'Group_id' => array(
                'title'   => 'Группа',
                'type'    => 'select',
                'table'   => 'groups',
                'field'   => 'Title',
                'width'   => 160
            ),
            'Email' => array(
                'title'   => 'E-mail',
                'type'    => 'string',
                'width'   => 140,
                'length'  => 32,
                'auto'    => true
            ),
            'Hide' => array(
                'title'   => 'Заблокированные',
                'type'    => 'bool'
            )
        ),
        // ссылка для добавления
        'addTitle'  => 'Добавить пользователя',
        //'addLink'   => '',
        'addHide'   => '',
        // данные для вывода записей
        'table'     => TABLE,
        'where'     => ' LEFT JOIN groups AS g ON g.ID = r.Group_id WHERE r.ID != 1',
        'orderBy'   => 'ID',
        // столбцы таблицы
        'columns'   => array(
            'Fio' => array(
                'head'    => 'Имя',
                'type'    => 'string',
                'after'   => ' ([Login])',
                'style'   => 'width:440px;',
                'link'    => true,
                'is_sort' => true
            ),
            'g.Title' => array(
                'head'    => 'Группа',
                'type'    => 'string',
                'is_sort' => true,
                'style'   => 'width:200px;'
            ),
            'Email' => array(
                'head'    => 'E-mail',
                'type'    => 'string',
                'is_sort' => true,
                'style'   => 'width:240px;'
            )
        ),
        'r_icon'    => array(
            'Hide' => array(
                'image'   => 'eye.gif',
                'image2'  => 'eyesleep.gif',
                'title'   => 'разблокировать',
                'title2'  => 'заблокировать'
            ),
            'delete' => array(
                'title'   => 'Удалить пользователя',
                'message' => "Удалить пользователя: '[Login]'?"
            )
        )
    ));
    
    return $list->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id, $page, $err)
{
	global $db, $g_user, $javascript, $fields;
	//-----------------------------------------------------------------------------------
    require (_PHPSITELIB_PATH . 'psl_form.inc.php');
    //-----------------------------------------------------------------------------------
    $form = new PslForm;
    $form->setParam(array(
        'code'          => CODE,
        'table'         => TABLE,
        'id'            => $id,
        'headTitleAdd'  => 'Добавление пользователя',
        'headTitleEdit' => 'Редактрирование пользователя',
        // данные для цепочки страниц
        'chain'         => array(
            'Users' => LANG . '/' . CODE . '/'
        ),
        'fieldForChain' => 'Login',
        'сhainLastTitle' => 'Новый пользователь',
        // ссылка для возврата на предыдущую страницу
        'backTitle'     => 'вернуться к списку пользователей<br>без сохранения изменений',
        'backLink'      => LANG . '/' . CODE . '/',
        // ссылки-действия
        'actions'       => array(
            //'список групп админки' => LANG . '/groups/',
            //'добавить группу'      => LANG . '/groups/?a=add&id=-1',
            //'удалить пользователя' => $id != -1 ? (THIS_PAGE . "?page=$page&a=edit&id=$id&delete") : false
        ),
        // сообщение об ошибке
        'error'         => $err
    ));
    //-----------------------------------------------------------------------------------
    // выводим поля формы
    $form->db = $db;
    $form->data = $form->getData();
    $tpl = new Template;
    //-----------------------------------------------------------------------------------
	$tpl->set_file('main', PATH_TO_TEMPLATES . CODE . '_' . ACT_EDIT . '.html');
    //-----------------------------------------------------------------------------------
    to_replace_fields($tpl, $form->data, $fields);
    //-----------------------------------------------------------------------------------
    $tpl->set_var('Group', get_field_select($form->data/*form*/, 'Group_id'/*field*/, 'groups'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'Title'/*orderBy*/, 378/*width*/, true/*is required*/));
    //-----------------------------------------------------------------------------------
   // $tpl->set_var('Rightholders', get_field_select($form->data/*form*/, 'Rightholder_id'/*field*/, 'ru_rightholders'/*table*/, 'Title'/*title*/, 0/*parent*/, ''/*where*/, 'OrderBy'/*orderBy*/, 378/*width*/, false/*is required*/));
	//-----------------------------------------------------------------------------------
	$tpl->set_block('main', 'timezones', 'timezones_');
	foreach (timezone_identifiers_list() AS $zone)
	{
		if (strpos($zone, 'Europe/') !== false)
		{
			$tpl->set_var(array(
				'Timezone'        => $zone,
				'Timezone_title'  => str_replace('Europe/', '', $zone),
				'Timezone_action' => isset($form->data['Timezone']) && $form->data['Timezone'] == $zone ? ' selected' : ''
			));
			$tpl->parse('timezones_', 'timezones', true);
		}
	}
    //-----------------------------------------------------------------------------------
    if ($id == -1) $javascript .= "$('#psdw_edit').click();";
	//-----------------------------------------------------------------------------------
    $form->setParam('fields', $tpl->parse('C', 'main', false));
    return $form->getHTML();
}
#----------------------------------------------------------------------------------------
#------------------------------------add/edit process------------------------------------
#----------------------------------------------------------------------------------------
function record_add_edit($data, $code) 
{ //print_r($data); exit;
    global $db, $g_user, $fields;
    _record_prepare($data); $r = _record_check($data, $code);
    
    if ($r == '')
	{
		$f = array();
		if ($code == 'add')
			$q1  = 'INSERT INTO ' . TABLE . ' SET ';
		else
        	$q1  = 'UPDATE ' . TABLE . ' SET ';
        
		$f = processing_edit_record($data, $fields, false);
		
		if (!empty($data['Password'])) $f[] = 'Password = "' . md5($data['Password']) . '"';
		
        $q2 = $code == 'edit' ? (' WHERE ID = ' . $data['ID']) : '';
		
        $db->Query($q1 . implode(', ', $f) . $q2);
        $insert_id = $code == 'add' ? $db->GetInsertId() : $data['ID'];
        //add_to_log(CODE . '_' . $code, $insert_id);
    }
    return $r;
}
#----------------------------------------------------------------------------------------
function _record_check($data, $code)
{
	global $db;
    $r = array();
    
    if (empty($data['Group_id']) || intval($data['Group_id']) <= 0) $r[] = 'Select group';
    if (empty($data['Login'])) $r[] = 'Enter to login';
	//if (empty($data['Password'])) $r[] = 'Введите пароль';
    if (!(isset($data['Email']) && string_is_email($data['Email']))) $r[] = 'Enter correct e-mail';
	
	if ($code == 'add' && empty($r))
	{
		$db->Query('SELECT ID FROM ' . TABLE . ' WHERE Login = "' . addslashes($data['Login']) . '" OR Email = "' . addslashes($data['Email']) . '"');
		if ($db->NextRecord())	$r[] = 'A user with the same username or e-mail already exists';
	}
	
    return implode('<br>', $r);
}
#----------------------------------------------------------------------------------------
function _record_prepare(&$data)
{
	global $fields;
    
	foreach ($fields['string'] AS $val) { $data[$val] = isset($data[$val]) ? trim($data[$val]) : ''; }
}

?>