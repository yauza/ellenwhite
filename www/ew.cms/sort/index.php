<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   sort block admin                                                    #
#                                                                       #
#########################################################################

if (!defined('IN_ADMIN')) define ('IN_ADMIN', 1);
if (!defined('PATH_TO_ADMIN')) define ('PATH_TO_ADMIN', '../');
if (!defined('PATH_TO_ROOT')) define ('PATH_TO_ROOT', PATH_TO_ADMIN .  '../');
define ('LANG', isset($_GET['lang']) && preg_match('/^[a-z_]+$/i', $_GET['lang']) ? $_GET['lang'] : 'ru');

define ('CODE', 'sort');
define ('TABLE', LANG . '_' . (isset($_POST['table']) && preg_match('/^[a-z_]+$/i', $_POST['table']) ? $_POST['table'] : (isset($_GET['table']) && preg_match('/^[a-z_]+$/i', $_GET['table']) ? $_GET['table'] : 'sitemap')));

define ('ACT_SORT', 'sort_proc');

$is_ajax = isset($_POST['ajax']) && $_POST['ajax'] == '' ? true : (isset($_GET['ajax']) && $_GET['ajax'] == '' ? true : false);

$action = isset($_POST['a']) && preg_match('/^[a-z_]+$/i', $_POST['a']) ? $_POST['a'] : (isset($_GET['a']) && preg_match('/^[a-z_]+$/i', $_GET['a']) ? $_GET['a'] : '');

$id = isset($_GET['id']) && intval($_GET['id']) ? intval($_GET['id']) : 0;

define ('PAGE_CODE', CODE . '_content');
define ('PAGE_PATH', CODE . '/?lang=' . LANG . '&amp;id=' . $id);

require_once(PATH_TO_ADMIN . 'inc/init.inc.php');

switch ($action)
{
	case ACT_SORT: // обновление порядка сортировки
	{
		if (isset($_GET['f']) && is_array($_GET['f']))
		{
			$mes = str_replace(LANG . '_', '', TABLE);
			foreach($_GET['f'] AS $id => $order)
			{
				if ($db->Query('UPDATE LOW_PRIORITY ' . TABLE . ' SET OrderBy  = ' . $order . ' WHERE ID = ' . $id))
					continue;
				else { echo $mes = 'Ошибка!'; break; }
			}
			add_to_log(CODE . '_content', 0);
			echo $mes;
		}
		else
			echo 'Ошибка!';
	} exit;
}


if ($is_ajax)
{
	header("Content-Type: text/plain; charset=utf-8");
	echo get_form($id);
    exit;
}
else
{
	array_push($scripts, 'js/sort.js');
    $container = get_form($id);
}


//---------------------------------------------------------------------------------------
require PATH_TO_ADMIN . 'include/top.inc.php';
echo $container;
require PATH_TO_ADMIN . 'include/bottom.inc.php';
require _PHPSITELIB_PATH . 'psl_finish.inc.php';
#----------------------------------------------------------------------------------------
#------------------------------------------page------------------------------------------
#----------------------------------------------------------------------------------------
function get_form($id)
{
    global $db;
    $r = '';
	
	$tpl = new Template;
	$tpl->set_file('main', PATH_TO_ROOT . 'temp_admin/' . CODE . '.html');
	$tpl->set_block('main', 'record', 'record_');
	$tpl->set_block('main', 'record_empty', 'record_empty_');
	
	$tpl->set_var(array(
		'CODE'      => CODE,
		'BACK_HREF' => LANG . '/content/',
        'TABLE'     => str_replace(LANG . '_', '', TABLE)
	));
	
	$db->Query('SELECT Title, Count_childs FROM ' . TABLE . ' WHERE ID = ' . $id);
	if ($db->NextRecord())
	{
		$tpl->set_var('MAIN_TITLE', $id != 1 ? htmlspecialchars($db->F('Title')) : _PHPSITELIB_SITE);
		$cnt = $db->F('Count_childs');
		if ($cnt < 1) return 'ошибка';
	}
	
	$cnt_all = $cnt + 6;
	while (!is_int($cnt_all / 6)) $cnt_all++;
	
	$db->Query('SELECT ID, Title, Count_childs FROM ' . TABLE . ' WHERE Parent_id = ' . $id . ' AND ID != 6 ORDER BY OrderBy');
	
	for ($i = 1; $i <= $cnt && $db->NextRecord(); $i++)
	{
		$tpl->set_var(array(
			'NUM'   => $i,
			'ID'    => $db->F('ID'),
			'TITLE' => htmlspecialchars($db->F('Title')),
			'ICON'  => $db->F('Count_childs') ? 'folder' : 'file'
		));
		$tpl->parse('record_', 'record', true);
	}
	for ($i = $cnt + 1; $i <= $cnt_all; $i++)
	{
		$tpl->set_var('NUM', $i);
		$tpl->parse('record_empty_', 'record_empty', true);
	}
	
	return $tpl->parse('C', 'main', false);
}

?>