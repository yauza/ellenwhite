//---------------------------------------------------------------------------------
$(document).ready(function(){//----------------------------------------------------
//---------------------------------------------------------------------------------
// отправка форм по нажатию на enter
$('form.login input, form.search input, form.search select').keyup(function(event)
{
	if (event.keyCode == 13) 
		$(this).parents('form').submit();
});
//---------------------------------------------------------------------------------
// изменение булевых полей с 1 на 0 и наоборот
$('div.line > div.r_icon[src!=][alt], div.line_hide > div.r_icon[src!=][alt], div.foto > div.icons > div.icon[src!=][alt]').click(function()
{
	loading(true);
	var obj = $(this);
	
	$.get(location.href, { ajax: '', a: 'edit', id: $(this).parent('div').attr('_id'), field: $(this).attr('field') ? $(this).attr('field') : 'Hide' }, function(txt)
	{
		if (txt == 'ok')
		{
			// меняем иконки
			var temp = obj.css('background-image');
			obj.css('background-image', obj.attr('src')).attr('src', temp);			
			// меняем title у иконок
			temp = obj.attr('title');
			obj.attr('title', obj.attr('alt')).attr('alt', temp);
			
			loading(false);
		}
		else
			alert('Обнаружена ошибка: ' + txt);
	}, 'text');
});
//---------------------------------------------------------------------------------
// удаление записей
$('div.line > div.r_icon[del], div.line_hide > div.r_icon[del], div.foto > div.icons > div.icon[del]').click(function()
{
	if ($(this).css('background-image').indexOf('_none.gif') != -1) return;
	$('<div id="dialog" title="Внимание">' + $(this).attr('del') + '</div>').appendTo('body');
	var obj = $(this);
	$("#dialog").dialog(
	{ 
		bgiframe: true, 
		resizable: false, 
		modal: true, 
		overlay: { backgroundColor:'#000', opacity:0.5 }, 
		close: function() { $("#dialog").remove(); }, 
		buttons: 
		{ 
			'Отмена': function()
			{
				$(this).dialog('close');
				$('#dialog').remove();
			},
			'Удалить': function()
			{
				window.location = location.href + (location.search ? '&' : '?') + 'a=edit&id=' + (obj.attr('_id') ? obj.attr('_id') : obj.parent('div').attr('_id')) + '&delete';
			}
		}
	});
});
//---------------------------------------------------------------------------------
// выделение обязательного поля при недопустимом значении (страницы редактирования записей)
$('input.required, textarea.required').keyup(function() // проверка для пустых полей
{
	$(this).toggleClass('wrong', $(this).val() == '');
	btnsaveDisabled();
});
$('select.required').change(function() // проверка для пустых селектов
{
	$(this).toggleClass('wrong', $(this).val() == 0);
	btnsaveDisabled();
});
$('input.required_date').change(function() { $(this).keyup(); });
$('input.required_date').keyup(function() // проверка для поля даты
{
    var date = $(this).val();
	if (date.match(/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}/))
	{
		var d = parseInt(date.substring((date.substring(0, 1) == '0' ? 1 : 0), date.indexOf('.')));
		var m = parseInt(date.substring(date.indexOf('.') + (date.substring(date.indexOf('.') + 1, date.indexOf('.') + 2) == '0' ? 2 : 1), date.lastIndexOf('.')));
		var y = parseInt(date.substring(date.lastIndexOf('.') + 1));
		var mMax = new Array(0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31); // хорошо бы доделать для високосных годов
		
        $(this).toggleClass('wrong', !(m > 0 && m < 13 && d > 0 && d <= mMax[m]));
	}
	else
		$(this).addClass('wrong');
	
	btnsaveDisabled();
});
$('input.required_datetime').change(function() { $(this).keyup(); });
$('input.required_datetime').keyup(function() // проверка для поля даты со временем
{
	var date = $(this).val();
	if (date.match(/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4} [0-9]{1,2}\:[0-9]{1,2}/))
	{
		var d = parseInt(date.substring((date.substring(0, 1) == '0' ? 1 : 0), date.indexOf('.')));
		var m = parseInt(date.substring(date.indexOf('.') + (date.substring(date.indexOf('.') + 1, date.indexOf('.') + 2) == '0' ? 2 : 1), date.lastIndexOf('.')));
		var y = parseInt(date.substring(date.lastIndexOf('.') + 1, date.lastIndexOf('.') + 5));
		var h = parseInt(date.substring(date.lastIndexOf('.') + (date.substring(date.lastIndexOf('.') + 6, date.lastIndexOf('.') + 7) == '0' ? 7 : 6), date.indexOf(':')));
		var i = parseInt(date.substring(date.indexOf(':') + (date.substring(date.indexOf(':') + 1, date.indexOf(':') + 2) == '0' ? 2 : 1)));
		var mMax = new Array(0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		
		$(this).toggleClass('wrong', !(m > 0 && m < 13 && d > 0 && d <= mMax[m] && h >= 0 && h < 24 && i >= 0 && i < 60));
	}
	else
		$(this).addClass('wrong');
	
	btnsaveDisabled();
});
//---------------------------------------------------------------------------------
// обработка нажатий на кнопки
$('input.btnsave, input.btnsearch, input.btn_grey, input.btn_orange').click(function()
{
	var root = window.addEventListener || window.attachEvent ? window : document.addEventListener ? document : null;
	$(this).attr('disabled', 'disabled');
	root.onbeforeunload = null;
	$(this).closest('form').submit();
});
//---------------------------------------------------------------------------------
$('div.popup_image').click(function()
{
	var div = $(this).find('div');
    div.css('left', getElementPosition($(this).get(0)).left - parseInt($(this).find('div > img').css('width')) / 2 + 20 + 'px');
	var top = getElementPosition($(this).get(0)).top - parseInt($(this).find('div > img').css('height')) / 2 + 10;
	if (top < 0) top = 0;
	div.css('top', top + 'px');
	$(this).find('div').toggle('scale', {}, 500);
});
//---------------------------------------------------------------------------------
// переключение списка подписчиков и текста рассылки
$('a.users').click(function()
{
	$('#users_emails').toggle();
	$('#users_text').toggle();
	// изменим название ссылки
	var temp = $(this).html();
	$(this).html($(this).attr('title')).attr('title', temp);
	// выводим количество выбранных подписчиков
	if ($('#users_text').is(':visible'))
	{
		$('#users_count').html($('input[name^=users]:checked').size());
		$('#users_text > input[type=button]').attr('disabled', $('input[name^=users]:checked').size() < 1 ? 'disabled' : '');
	}
});
// выбор всех подписчиков для рассылки
$('#users_all').click(function()
{
	$('div.line input:checkbox').attr('checked', $(this).attr('checked') ? true : false);
});
//---------------------------------------------------------------------------------
// преобразование textarea.tinymce в редактор
$('textarea.tinymce').each(function()
{
    tiny_width = $(this).attr('tiny_width') == undefined ? '650' : $(this).attr('tiny_width');
    tiny_height = $(this).attr('tiny_height') == undefined ? '200' : $(this).attr('tiny_height');
    $(this).tinymce(
    {
    	theme: 'advanced',
    	language: 'en',
    	width: tiny_width,
    	height: tiny_height,
    	element_format : "html",
    
    	plugins: "safari, table, inlinepopups, contextmenu, paste, media, advhr, advimage",
    	
    	theme_advanced_buttons1 : "tablecontrols, |, hr, advhr, |, link, unlink, anchor, |, image, media, charmap, |, removeformat, cleanup, visualaid, |, code",
    	theme_advanced_buttons2 : "formatselect, fontsizeselect, bold, italic, underline, strikethrough, |, justifyleft, justifycenter, justifyright, justifyfull, |, forecolor, backcolor, |, bullist, numlist, |, sub, sup, |, outdent, indent, |, blockquote",
    	theme_advanced_buttons3 : "",
    				
    	theme_advanced_toolbar_location: "top",
    	theme_advanced_toolbar_align: "left",
    	
    	theme_advanced_font_sizes: "Обычный=100%, Большой=120%, Очень большой=140%",
        
        extended_valid_elements: 'iframe[name|src|framespacing|border|frameborder|scrolling|title|height|width|marginheight|marginwidth|allowfullscreen]',
    	
    	content_css: "/css/tiny.css",
    	
    	file_browser_callback : "imgLibManager.open"
    });
});
imgLibManager.init({url: '/include/tinymce/imglib/index.html'});
//---------------------------------------------------------------------------------
});//------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//отсылка команды серверу
function sendAction(vars)
{
	$.get(path_to_admin + 'actions.php', vars, function(txt)
	{
		if (txt != 'ok') alert('Ошибка при выполнении sendAction: ' + txt);
	}, 'text');
}
//---------------------------------------------------------------------------------
// включение фоновго дива, пока происходит к-либо загрузка
function loading(is)
{
	if (is) // включение фонового дива
	{
		$('#load_img').css('margin-top', getElementPosition('main').height / 2 - 25 + 'px');
		$('#loading').css('height', getElementPosition('main').height).show();
		$('#main').hide();
	}
	else // выключение фонового дива
	{
		$('#loading').hide();
		$('#main').show();
	}
}
//---------------------------------------------------------------------------------
// блокировка и разблокировка кнопки "Сохранить"
function btnsaveDisabled()
{
    $('input.btnsave').attr('disabled', ($('input.required, textarea.required, select.required, input.required_date, input.required_datetime').hasClass('wrong') ? 'disabled' : false));
}
//
//
function _alert(title, text)
{
	$('<div id="dialog" title="' + title + '">' + text + '</div>').appendTo('body');
	$("#dialog").dialog( { bgiframe: true, resizable: false, modal: true, close: function() { $("#dialog").remove(); } } );
}

//
function field_autocomplete(obj)
{
    field = obj.attr('field') ? obj.attr('field') : obj.attr('name').replace('form[', '').replace(']', '');
	table = obj.attr('table');
	if (table == '') return;
	
	obj.autocomplete(path_to_admin + 'actions.php',
	{
		minChars: 2,
		max: 9,
		extraParams: { a: 'autocomplete', table: table, field: field }
	});
}