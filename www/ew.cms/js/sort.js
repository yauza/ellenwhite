var sort_flag = false; // true - идет перетаскивание объекта; false - не идет
var def_obj_in = ''; // innerHTML области, из которой перетаскивается объект
var def_obj_id = ''; // id области, из которой перетаскивается объект
var def_obj_bg = ''; // background области, из которой перетаскивается объект
//---------------------------------------------------------------------------------
$(document).ready(function(){//----------------------------------------------------
//---------------------------------------------------------------------------------
// обрабатываем движение мышки
$(document).mousemove(function(event)
{
	if (sort_flag) // если идет процесс перетаскивания объекта
	{
		// определяем координаты области для сортировки
		x1_div = getElementPosition('main_sort').left;
		y1_div = getElementPosition('main_sort').top;
		x2_div = x1_div + getElementPosition('main_sort').width;
		y2_div = y1_div + getElementPosition('main_sort').height;
		
		// определяем координаты мышки
		var event = event || window.event;
		x_mouse = getMousePosition(event).x;
		y_mouse = getMousePosition(event).y;
		
		// если мышка выходит за границы области сортировки
		if (x1_div >= x_mouse || y1_div >= y_mouse || x2_div <= x_mouse || y2_div <= y_mouse)
			setTimeout('sort_up(document.getElementById(def_obj_id))', 100);
		
		// для тестирвоания
		//$('#test').html(x1_div + ' >= ' + x_mouse + ' || ' + y1_div + ' >= ' + y_mouse + ' || ' + x2_div + ' <= ' + x_mouse + ' || ' + y2_div + ' <= ' + y_mouse);
	}
});
//---------------------------------------------------------------------------------
// наведение мышки на квадратик
$('div.field_sort, div.field_sort_empty').mouseover(function()
{
	if (sort_flag) // если идет процесс перетаскивания объекта
	{
		if ($(this).hasClass('field_sort')) // если данный квадратик занят объектом
		{
			var def_obj_id_temp = def_obj_id;
			var def_obj_in_temp = def_obj_in;
			var def_obj_bg_temp = def_obj_bg;
			
			var id1 = parseInt($(this).attr('id').substr(6));
			var id2 = parseInt(def_obj_id.substr(6));
			if (id1 < id2)
			{
				for (i = id2; i > id1; i--)
				{
					sort_down($('#field_' + (i - 1)).get(0));
					sort_up($('#field_' + i).get(0));
				}
			}
			else
			{
				for (i = id2; i < id1; i++)
				{
					sort_down($('#field_' + (i + 1)).get(0));
					sort_up($('#field_' + i).get(0));
				}
			}
			
			setDefObjParam(def_obj_id_temp, def_obj_in_temp, def_obj_bg_temp);
			sort_flag = true;
			sort_up($(this).get(0));
			sort_down($(this).get(0));
		}
		else // если данный квадратик пустой
			$(this).css('cursor', 'move').css('background', def_obj_bg).html(def_obj_in);
		
		$(this).css('border', '1px solid #fff');
	}
	else // простое наведение мышки, без перетаскивания объекта
	{
		if ($(this).hasClass('field_sort')) // если данный квадратик занят объектом
		{
			$(this).css('border', '1px solid #fff');
		}
		else // если данный квадратик пустой
		{
			$(this).css('cursor', 'default').css('border', '1px solid #ccc');
		}
	}
});
//---------------------------------------------------------------------------------
// увод мышки с квадратика
$('div.field_sort, div.field_sort_empty').mouseout(function()
{
	if (sort_flag) // если идет процесс перетаскивания объекта
	{
		if ($(this).hasClass('field_sort')) // если данный квадратик занят объектом
		{
			$(this).css('cursor', 'default');
		}
		else // если данный квадратик пустой
		{
			$(this).css('background', '').html('');
		}
	}
	$(this).css('border', '1px solid #ccc');
});
//---------------------------------------------------------------------------------
// отпускание мышки на квадратике
$('div.field_sort, div.field_sort_empty').mouseup(function()
{
	sort_up($(this).get(0));
});
//---------------------------------------------------------------------------------
// нажатие мышки на квадратике
$('div.field_sort,div.field_sort_empty').mousedown(function()
{
	sort_down($(this).get(0));
});
//---------------------------------------------------------------------------------
// скрытие и показ справки
$('div.help_head').click(function()
{
	$('div.help_text').toggle(1000);
});
//---------------------------------------------------------------------------------
// скрытие и показ справки
$('input.btnsave').click(function()
{
	var fields = reCountNumbers(true);	
	var temp = location.search.replace('?lang=', '');
	var lang = temp.substr(0, temp.indexOf('&'));
	
	$.get(location.pathname + '?' + fields.join('&'), { ajax: '', a: 'sort_proc', lang: lang, table: $('#main_sort').attr('table') }, function(txt)
	{
		if (txt == 'sitemap')
			window.location = path_to_admin + lang + '/content/';
		else if (txt == 'store')
			window.location = path_to_admin + lang + '/' + txt + '/';
		else if (txt == 'ok_foto')
			window.location = location.href.replace('&a=sort', '');
		else
			alert('Ошибка при выполнении сортировки: ' + txt);
	}, 'text');
});
//---------------------------------------------------------------------------------
});//------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

function sort_down(obj)
{//alert('sort_down');
	var obj_id = '#'+obj.id;
	if ($(obj_id).hasClass('field_sort')) // если квадратик занят объектом
	{
		sort_flag = true;
		// запоминаем все данные квадратика с объектом
		setDefObjParam(obj.id, $(obj_id).html(), $(obj_id).css('background'))
		// очищаем квадратик
		$(obj_id).html('').css('background', '').removeClass('field_sort').addClass('field_sort_empty');
	}
	else // если квадратик пустой
	{
		setDefObjParam('', '', '');
		sort_flag = false;
	}
}
//---------------------------------------------------------------------------------
function sort_up(obj)
{//alert('sort_up');
	var obj_id = '#'+obj.id;
	if (sort_flag) // если идет процесс перетаскивания объекта
	{
		sort_flag = false;
		if ($(obj_id).hasClass('field_sort')) // если данный занятом объектом
		{
			sort_up($('#'+def_obj_id).get(0));
		}
		else // если данный квадратик пустой
		{
			$(obj_id).removeClass('field_sort_empty').addClass('field_sort').css('background', def_obj_bg).html(def_obj_in);
			reCountNumbers(false);
		}
		$(obj_id).css('cursor', 'default');
	}
	else if (obj.id == def_obj_id) // возврат, перетаскиваемого объекта, на его изначальное место
	{
		$(obj_id).removeClass('field_sort_empty').addClass('field_sort').css('background', def_obj_bg).html(def_obj_in);
	}
	
	setDefObjParam('', '', '');
}
//---------------------------------------------------------------------------------
// временное сохранение данных перетаскиваемого объекта
function setDefObjParam(id, inner, bg)
{
	def_obj_id = id;
	def_obj_in = inner;
	def_obj_bg = bg;
}
//---------------------------------------------------------------------------------
// процесс сортировки
function reCountNumbers(to_server)
{
	var i = 1, j = 1;
	var fields = new Array();
	
	var obj = document.getElementById('field_1');
	while (obj)
	{
		if (obj.className == 'field_sort')
		{
			obj.firstChild.innerHTML = j;
			if (to_server)
				fields.push(encodeURIComponent('f['+obj.firstChild.id+']') + '=' + encodeURIComponent(j));
			j++;
		}
		i++;
		obj = document.getElementById('field_'+i);
	}
	
	/*var obj = $('#field_1');
	while (obj)
	{
		if (obj.hasClass('field_sort'))
		{
			obj.children('div.field_num').html(j);
			if (to_server)
				fields.push(encodeURIComponent('f['+obj.children('div.field_num').attr('id')+']') + '=' + encodeURIComponent(j));
			j++;
		}
		i++;
		obj = $('#field_' + i);
	}*/
	if (to_server) return fields;
}
//---------------------------------------------------------------------------------