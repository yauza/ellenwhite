//---------------------------------------------------------------------------------
$(document).ready(function(){//----------------------------------------------------
//---------------------------------------------------------------------------------
// клик на ссылке "изменить название и описание фотоальбома" (редактирование фотоальбомов)
$('#foto_change').click(function()
{
	// меняем название данной ссылки
	var temp = $(this).html();
	$(this).html($(this).attr('title'));
	$(this).attr('title', temp);
	
	//foto_change_text();
});
//---------------------------------------------------------------------------------
// наведение мышки на квадратик с фото
$('.foto').mouseover(function()
{
	$(this).removeClass().addClass('foto_over');
	// меняем src у иконок внизу квадратика
	$(this).children('img.icon').attr('src', function(){
		return $(this).attr('src').replace('_hide', '');
	});
});
//---------------------------------------------------------------------------------
// уведение мышки с квадратика с фото
$('.foto').mouseout(function()
{
	$(this).removeClass().addClass('foto');
	// меняем src у иконок внизу квадратика
	$(this).children('img.icon').attr('src', function(){
		return $(this).attr('src').replace('.gif', '_hide.gif');
	});
});
//---------------------------------------------------------------------------------
// добавление нескольких фото
$('#add_photos').uploadify(
{
	'uploader'   : '/include/uploadify/uploadify.swf',
	'script'     : $('#add_photos').attr('path') + '?foto=' + $('#add_photos').attr('foto'),
	'buttonImg'  : '/include/uploadify/select_photos.png',
	'buttonText' : 'Добавить фотографии',
	'cancelImg'  : '/include/uploadify/cancel.png',
	'width'      : 240,
	'height'     : 48,
	'fileDataName' : 'photo',
	'auto'       : false,
	'multi'      : true,
	'fileDesc'   : 'jpg;png;gif',
	'fileExt'    : '*.jpg;*.png;*.gif',
	'scriptData' : { a: 'add_few_proc', key: $('#add_photos').attr('key') },
	'onSelectOnce' : function(event, queueID, fileObj)
	{
		//$('#user_links').before(get_loading('loading_hor.gif'));
	},
	'onComplete' : function(event, queueID, fileObj, response, data)
	{
		//alert(response);
	},
	'onAllComplete' : function(event, data)
	{
		//alert('all');
	},
	'onError' : function(event, queueID, fileObj, errorObj)
	{
		alert("type:\n" + errorObj.type + "\n\ninfo:\n" + errorObj.info);
	}
});
//---------------------------------------------------------------------------------
});//------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
function foto_change_text()
{
	// запрещаем или разрешаем редактировать поля фотоальбома
	$('.foto_text').toggleClass('foto_text_hide');
	//$('.foto_text').attr('disabled', $('.btn_grey').is(':hidden') ? '' : 'disabled');
	// показываем или скрываем кнопку "Сохранить"
	$('.btn_grey').toggle();
}
//---------------------------------------------------------------------------------