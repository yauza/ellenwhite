//---------------------------------------------------------------------------------
$(document).ready(function(){//----------------------------------------------------
//---------------------------------------------------------------------------------
// выберем все поля формы и сохраним начальные значения
$('div.field input, div.field select, div.field textarea').each(function()
{
	if ($(this).attr('type') == 'checkbox')
		$(this).attr('def', $(this).attr('checked') ? 1 : 0);
	else
		$(this).attr('def', $(this).val());
});
// проверка ухода со страницы без сохранения изменений
var root = window.addEventListener || window.attachEvent ? window : document.addEventListener ? document : null;
root.onbeforeunload = function()
{
	var fields = new Array();
	// выберем все поля формы
	$('div.field input, div.field select, div.field textarea').each(function()
	{
		// сравнение строк
		//if ($(this).attr('tiny_width'))
		//	$('#error').html('<PLAINTEXT>' + $(this).html() + "\n\n" + $(this).attr('def') + '</PLAINTEXT>');
		if ($(this).attr('def') != undefined && (($(this).attr('type') == 'checkbox' && ($(this).attr('checked') ? 1 : 0) != $(this).attr('def')) || ($(this).attr('type') != 'checkbox' && $(this).val() != $(this).attr('def')))) // остальные поля
		{
			var field = $(this).parent().prev().text().replace(/(\*\s)|[\:]/g, '');
			// проверка сделана для раздела Группы, чтобы не дублировалось поле "Права"
			if (fields.join().indexOf(field) == -1) fields.push(field);
		}
	});
	
	if (fields.length > 0) { return "\nБыли внесены изменения в следующие поля\n\n- " + fields.join("\n- "); }
};

//---------------------------------------------------------------------------------
// редактирование поля "или добавить новую" для категорий (редактирвоание новостей)
$('div.field > input.add_new').keyup(function()
{
	// если поле не пусто, то запрещаем выбирать другие категории
	$(this).siblings('select').attr('disabled', $(this).val() != '' ? 'disabled' : '');
});
//---------------------------------------------------------------------------------
// проверка сложности пароля
$('#psdw_edit').click(function()
{
	$(this).parent('div.field').addClass('hide');
	$(this).parent('div.field').nextAll('div.field').eq(0).removeClass('hide');
});
//---------------------------------------------------------------------------------
// проверка сложности пароля
$('#psdw').keyup(function()
{
	var sum = 0, size = 0;
	var val = $(this).val();
	size = val.split('').length;
	if (RegExp('[A-Z]', 'g').test(val)) sum++;
	if (RegExp('[-a-z_]', 'g').test(val)) sum++;
	if (RegExp('[0-9]', 'g').test(val)) sum++;
	if (size < 5 && sum > 2) sum = 2;
	else if (size < 8 && sum > 2) sum--;
	else if (size >= 8 && sum <= 3) sum++;
	$('#psdw_indicate').css('background-image', 'url(' + path_to_img + 'pass_' + sum + '.gif)').css('width', size + '0px');
});
//---------------------------------------------------------------------------------
// клик на разделе страниц в списке (редактирование групп)
$('div.gr_title_langs').click(function()
{
	// выделяем раздел ("Русский", "Английский", "Настройки")
	$(this).addClass('gr_title_langs_act');
	$(this).siblings('div').removeClass('gr_title_langs_act');
	// показываем страницы, относящиеся к данному разделу (остальные скрываем)
	$('#pages_'+$(this).attr('title')).show();
	$('#pages_'+$(this).attr('title')).siblings('div').hide();
});
//---------------------------------------------------------------------------------
// клик на названии баннера в списке (редактирование баннеров)
$('a.ban_title').click(function()
{
	// подставляем html-код с полями формы нужного баннера в видимую область
	$('#ban_active').html($('#ban_'+$(this).attr('title')).html());
	// выделяем название баннера в списке
	$(this).addClass('ban_title_act');
	$(this).siblings('a').removeClass('ban_title_act');
	// очищаем область сообщений (ошибок)
	$('#error').html('');
	// если клик по "добавить"
	if ($(this).attr('title') == 'add')
	{
		$('#ban_type').change(function()
		{ // смена типа баннера (картинка \ текст)
			$('#ban_type_img').toggle();
			if ($('#ban_type_txt').hasClass('hide')) $('#ban_type_txt').removeClass('hide');
			else $('#ban_type_txt').addClass('hide');
			//$('#ban_type_txt').toggle(); не работает
		});
	}
});
//---------------------------------------------------------------------------------
// добавление варианта ответа для опросов
$('a.add_answer').click(function()
{
	// получим следующий номер ответа
	var num = parseInt($(this).prev('div').attr('next_num'));
	// создадим поле для нового варианта ответа
	$('<div></div>').appendTo('#answers').attr('num', num).html(
	'<div num=\'' + num + '\'>' +
	 '<div class="field w750"><input class="w720" name="answers_title[' + num + ']" value="" maxlength="64"></div>' +
     '<div class="field w30">0<input type="hidden" name="answers_count[' + num + ']" value="0"></div>' +
     '<div class="field">голосов</div>' +
     '<div class="space0"></div>' +
     '<div class="field w120"></div>' +
    '</div>');
	// увеличим на 1 номер для следующего ответа
	$(this).prev('div').attr('next_num', num + 1);
});
//---------------------------------------------------------------------------------
// добавление варианта для расширенного select
$('a.add_adv_select').click(function()
{
	// получим следующий номер
	var next_num = parseInt($(this).attr('num'));
	// создадим поле для нового варианта ответа
    if ($(this).prevAll('select').length)
    {
        var code = $(this).prevAll('select').eq(0).attr('name').match(/^([a-z]+)\[\d\]$/)[1];
        $(this).prevAll('select').eq(0).clone(true).val(0).insertBefore(this).attr('name', code + '[' + next_num + ']').after('<br>');
    }
    else
    {
        var code = $(this).prevAll('input[type!=hidden]').eq(0).attr('name').match(/^([a-z]+)\[\d\]$/)[1];
        $(this).prevAll('input[type!=hidden]').eq(0).clone(true).val('').insertBefore(this).attr('name', code + '[' + next_num + ']').after('<br>').focus(function(){autocomlete($(this));}).focus();
    }
	// увеличим на 1 номер для следующего варианта
    $(this).attr('num', parseInt(next_num + 1));
});
//---------------------------------------------------------------------------------
// изменение секции при редактировании товара
$('#store_section').change(function()
{//alert($(this).val());
    $('div.section_filters').hide();
    if ($(this).val())
        $('#section_filter_' + $(this).val()).show();
}).change();
//---------------------------------------------------------------------------------
// включение функции авто-заполнения
$('input[table]').each(function()
{
    autocomlete($(this));
});
//---------------------------------------------------------------------------------
});//------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// добавление инпута для свойства товара
function addProp()
{
    var i = parseInt($('input.new_prop').length) + 1;
    $('#add_prop').parents('div.field').prev('div.field').before('<div class="field w300" style="text-align:right; margin-right:10px;"><input class="new_prop w280" name="new_props[' + i + ']" value="" style="text-align:right;"></div><div class="field w300"><input class="w280" name="" value="" maxlength="32"></div><div class="space0"></div>');
}
//---------------------------------------------------------------------------------
// добавление разновидности товара
function addItemProp(item_id)
{
    var props = '{ ';
    
    i = 0;
    $('input.add_prop').each(function()
    {
        props += (i > 0 ? ', "' : '"') + $(this).attr('name') + '": "' + $(this).val() + '"';
        i++;
    });
    $('input.new_prop').each(function()
    {
        props += (i > 0 ? ', "' : '"') + $(this).val() + '": "' + $(this).parent('div').next('div').children('input').val() + '"';
        i++;
    });
    
    props += ' }';
    
    $('#items').html('<img src="' + path_to_admin + 'img/loading_hor.gif" alt="загрузка" title="загрузка">');
    
    $.post(path_to_admin + 'ru/store_items/', { a: 'add_item_prop', id: item_id, cost: parseInt($('#add_cost').val()), count: parseInt($('#add_count').val()), props: props }, function(txt)
    {
        $('#items_props').html(txt);
    }, 'text');
}
//---------------------------------------------------------------------------------
// редактирование разновидности товара
function openEditItemProp(item_id)
{
    i = 0;
    $('#item_prop_' + item_id + '_edit input').each(function()
    {
        $(this).width($('#item_prop_' + item_id).children('td').eq(i).width());
        i++;
    });
    
    $('#item_prop_' + item_id).addClass('hide');
    $('#item_prop_' + item_id + '_edit').removeClass('hide');
}
function editItemProp(item_id)
{
    var props = '{ ';
    
    i = 0;
    $('#item_prop_' + item_id + '_edit input').each(function()
    {
        props += (i > 0 ? ', "' : '"') + $(this).attr('name') + '": "' + $(this).val() + '"';
        
        $('#item_prop_' + item_id).children('td').eq(i).html($(this).val());
        i++;
    });
    
    props += ' }';
    
    $('#item_prop_' + item_id + '_edit').addClass('hide');
    $('#item_prop_' + item_id).removeClass('hide');
    
    $.post(path_to_admin + 'ru/store_items/', { a: 'edit_item_prop', id: item_id, props: props }, function(txt)
    {
        if (txt != '')
            alert(txt);
    }, 'text');
}
//---------------------------------------------------------------------------------
// удаление
function delItem(id)
{
    $.post(path_to_admin + 'ru/store_items/', { a: 'del_item_prop', id: id }, function(txt)
    {
        if (txt == '')
            $('#item_prop_' + id).remove();
        else
            alert(txt);
    }, 'text');
}
//---------------------------------------------------------------------------------
function autocomlete(obj)
{
    field = obj.attr('field');
	table = obj.attr('table');
	if (table == '' || field == '') return;
	
	obj.autocomplete(path_to_admin + 'actions.php',
	{
		minChars: 2,
		max: 9,
		extraParams: { a: 'autocomplete', table: table, field: field }
	});
}