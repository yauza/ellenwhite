﻿
var rnd=1000;//Округление
 
function convert(f) {
  f.kb.value=Math.round(f.byte.value/1024*rnd)/rnd
  f.mb.value=Math.round(f.byte.value/1048576*rnd)/rnd
  f.gb.value=Math.round(f.byte.value/1073741824*rnd)/rnd
}
 
function convertkb(f) {
  f.byte.value=Math.round(f.kb.value*1024*rnd)/rnd
  f.mb.value=Math.round(f.kb.value/1024*rnd)/rnd
  f.gb.value=Math.round(f.kb.value/1048576*rnd)/rnd
}
 
function convertmb(bytes) {
	var mb = Math.round(bytes / 1048576 * rnd) / rnd;
  
	if (mb && mb > 1)
		return mb + ' мб.';
	else
		return Math.round(bytes / 1024 * rnd) / rnd + ' кб.';
}
 
function convertgb(f) {
  f.byte.value=Math.round(f.gb.value*1073741824*rnd)/rnd
  f.kb.value=Math.round(f.gb.value*1048576*rnd)/rnd
  f.mb.value=Math.round(f.gb.value*1024*rnd)/rnd
}

$(document).ready(function(){
	selected_files = [];


	
	 // Проверка поддержки File API в браузере  и DnD
    if(window.FileReader == null || !('draggable' in document.createElement('span')))
	
	/* если file api не поддерживается, реализуем загрузку файлов постаринке */
	{

					
    }
	/* end если file api не поддерживается */
	
	
	/* если поддерживется file api и DnD */
	else {
    
   		// Контейнер, куда можно помещать файлы методом drag and drop
    	dropBox = $('#file-container'),

		uploadedCount = 0,
		
		result = false;
		
	
	/* 
		обработчики событий интрфейса
	*/
	
		// удаление выбранного фото
	$('.delele_selected_file').live('click', function(){
			var file_number = $(this).attr('file_number');
			
			if (file_number && selected_files[file_number])
			{
				delete selected_files[file_number];
				displayFiles();
			}
	});

    // Обновление progress bar'а
    function updateProgress(bar, value) {
        var width = bar.width();
        var bgrValue = -width + (value * (width / 100));
        bar.attr('rel', value).css('background-position', bgrValue+'px center').text(value+'%');
    }

    // Отображение выбраных файлов и создание миниатюр
    function displayFiles() {
        var fileListContent = '<table border="0" width="400" style="margin-left:50px;border-collapse: collapse;">';
		
		if (sizeOf(selected_files))
		{
			fileListContent += '<tr><td align="left" height="30" style="border-bottom:1px solid black;">Имя файла</td><td style="border-bottom:1px solid black;" align="right">Размер</td><td style="border-bottom:1px solid black;">Удалить</td><td style="border-bottom:1px solid black;">Загружено</td></tr>';
			
			$.each(selected_files, function(i, file) {
				if (file && !file.sended)
					fileListContent += '<tr><td align="left">' + file.name + '</td><td align="right">' + convertmb(file.size) + '</td><td class="delele_selected_file" file_number="' + i +'">X</td><td id="progress' + i + '">0%</td></tr>';
			});
		}
		
		fileListContent += '</table>';
		
		dropBox.html(fileListContent);
		//$(fileListContent).appendTo(dropBox);
    }
   
    ////////////////////////////////////////////////////////////////////////////



    // Обработка событий drag and drop при перетаскивании файлов на элемент dropBox
    // (когда файлы бросят на принимающий элемент событию drop передается объект Event,
    //  который содержит информацию о файлах в свойстве dataTransfer.files. В jQuery "оригинал"
    //  объекта-события передается в св-ве originalEvent)
    dropBox.bind({
        dragenter: function() {
            $(this).addClass('highlighted');
            return false;
        },
        dragover: function() {
            return false;
        },
        dragleave: function() {
            $(this).removeClass('highlighted');
            return false;
        },
        drop: function(e) {
            var dt = e.originalEvent.dataTransfer;
			$.each(dt.files, function(i, file) { 
				file.sended = false;
				selected_files[selected_files.length] = file;
			//console.log(selected_files.length);
			
			});
			
			displayFiles();
            return false;
        }
    });
		
	    $('#my_file_up').live('change', function(){ 
		var control = $('#my_file_up');
		var files = this.files[0];
		var file_arr = new Array();
		file_arr.push(files);
	//	file.sended = false;
	//	selected_files[1] = file;
		$.each(file_arr, function(i, file) { 
				file.sended = false;
				selected_files[selected_files.length] = file;
			
			});
		//console.log(file);
		displayFiles();
		control.replaceWith( control = control.clone( true ) );
            return false;
		});	
		
	}
	/* end если поддерживется file api */
  
    
    ////////////////////////////////////////////////////////////////////////////

function dump(obj) {
    var out = "";
    if(obj && typeof(obj) == "object"){
        for (var i in obj) {
            out += i + ": " + obj[i] + "\n";
        }
    } else {
        out = obj;
    }
    alert(out);
}


    // Обаботка события нажатия на кнопку "Загрузить". Проходим по всем миниатюрам из списка,
    // читаем у каждой свойство file (добавленное при создании) и начинаем загрузку, создавая
    // экземпляры объекта uploaderObject. По мере загрузки, обновляем показания progress bar,
    // через обработчик onprogress, по завершении выводим информацию
	
	/*
		переменная, которая когда становится 1 субмитит форму
	 	данная форма изначально отправляет картинки на сервер, а уже после того, очистив поле file, субмитит нашу форму

	  */
	var alldone = 0;
	

	function checkSendState()
	{
		//alert(sizeOf(selected_files));
		var result = true;
		
		$.each(selected_files, function(i, file) {
			if (file && !file.sended)
				result = false;
			else
				delete selected_files[i];
		});			
		
		//alert(sizeOf(selected_files));
		displayFiles();
		
		if (result)
		{
			uploadedCount = 0;
			alert('Файлы закачались.');
			updateFileList();
		}
	}
	
	$('#upload_files').click(function() {
        
			if (sizeOf(selected_files))
			{
				$.each(selected_files, function(i, file) {

					new uploaderObject({
						file:       file,
						
						/*переменная ulr - адрес скрипта, который будет принимать фото со стороны сервера (в моём случае это значение action нашей формы)*/
						
						url:        $('#upload_files').attr('folder') + '/file.php?action=add_file&id=' + $('input[name="form[ID]"]').val(),
						fieldName:  'my_file',
						
						
						onprogress: function(percents) {
							$('#progress' + i).html(percents + '%');
						},
						
						oncomplete: function(done, data) {
							if (done) {
								selected_files[i].sended = true;
							}

							uploadedCount++;

							if (uploadedCount == sizeOf(selected_files))
								checkSendState();						
						}
					});		
				
				});
			}
			else
				alert('Вы не перетащили файлы.');
    });

});

/*
 * Объект-загрузчик файла на сервер.
 * Передаваемые параметры:
 * file       - объект File (обязателен)
 * url        - строка, указывает куда загружать (обязателен)
 * fieldName  - имя поля, содержащего файл (как если задать атрибут name тегу input)
 * onprogress - функция обратного вызова, вызывается при обновлении данных
 *              о процессе загрузки, принимает один параметр: состояние загрузки (в процентах)
 * oncopmlete - функция обратного вызова, вызывается при завершении загрузки, принимает два параметра:
 *              uploaded - содержит true, в случае успеха и false, если возникли какие-либо ошибки;
 *              data - в случае успеха в него передается ответ сервера
 *              
 *              если в процессе загрузки возникли ошибки, то в свойство lastError объекта помещается
 *              объект ошибки, содержащий два поля: code и text
 */

var uploaderObject = function(params) {

    if(!params.file || !params.url) {
        return false;
    }

    this.xhr = new XMLHttpRequest();
    this.reader = new FileReader();

    this.progress = 0;
    this.uploaded = false;
    this.successful = false;
    this.lastError = false;
    
    var self = this;    

    self.reader.onload = function() {
        self.xhr.upload.addEventListener("progress", function(e) {
            if (e.lengthComputable) {
                self.progress = (e.loaded * 100) / e.total;
                if(params.onprogress instanceof Function) {
                    params.onprogress.call(self, Math.round(self.progress));
                }
            }
        }, false);

        self.xhr.upload.addEventListener("load", function(){
            self.progress = 100;
            self.uploaded = true;
        }, false);

        self.xhr.upload.addEventListener("error", function(){            
            self.lastError = {
                code: 1,
                text: 'Error uploading on server'
            };
        }, false);

        self.xhr.onreadystatechange = function () {
            var callbackDefined = params.oncomplete instanceof Function;
            if (this.readyState == 4) {
                if(this.status == 200) {
                    if(!self.uploaded) {
                        if(callbackDefined) {
                            params.oncomplete.call(self, false);
                        }
                    } else {
                        self.successful = true;
                        if(callbackDefined) {
                            params.oncomplete.call(self, true, this.responseText);
                        }
                    }
                } else {
                    self.lastError = {
                        code: this.status,
                        text: 'HTTP response code is not OK ('+this.status+')'
                    };
                    if(callbackDefined) {
                        params.oncomplete.call(self, false);
                    }
                }
            }
        };

        self.xhr.open("POST", params.url);

        var boundary = "xxxxxxxxx";
        self.xhr.setRequestHeader("Content-Type", "multipart/form-data, boundary="+boundary);
        self.xhr.setRequestHeader("Cache-Control", "no-cache");

        var body = "--" + boundary + "\r\n";
        body += "Content-Disposition: form-data; name='"+(params.fieldName || 'file')+"'; filename='" + params.file.name + "'\r\n";
        body += "Content-Type: application/octet-stream\r\n\r\n";
        body += self.reader.result + "\r\n";
        body += "--" + boundary + "--";
		//console.log(self.reader.result);
        if(self.xhr.sendAsBinary) {
            // firefox
            self.xhr.sendAsBinary(body);
        } else {
            // chrome (W3C spec.)
            self.xhr.send(body);
        }

    };
	self.reader.readAsDataURL(params.file);
    //self.reader.readAsBinaryString(params.file);
};