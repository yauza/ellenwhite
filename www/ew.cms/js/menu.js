//---------------------------------------------------------------------------------
$(document).ready(function(){//----------------------------------------------------
//---------------------------------------------------------------------------------
// наведение мышки на строку в древовидном списке
$('div.line, div.line_hide').mouseover(function()
{
	$(this).removeClass().addClass('line_over');
});
//---------------------------------------------------------------------------------
// уведение мышки с строки в древовидном списке
$('div.line, div.line_hide').mouseout(function()
{
	$(this).removeClass().addClass('line');
});
//---------------------------------------------------------------------------------
//
$('div.r_icon[title=]').css('cursor', 'default');
//---------------------------------------------------------------------------------
// включении функции авто-заполнения
$('input[table]').each(function()
{
	field = $(this).attr('name').replace('form[', '').replace(']', '');
	table = $(this).attr('table');
	if (table == '') return;
	
	$(this).autocomplete(path_to_admin + 'actions.php',
	{
		minChars: 2,
		max: 9,
		extraParams: { a: 'autocomplete', table: table, field: field }
	});
});
//---------------------------------------------------------------------------------
// включение сортировки в обычном списке путем перетаскивания строк
$('#sort_lines').sortable({
	items: 'div.line',
	update: function(event, ui)
	{
		data = '';
		$('#sort_lines').children('div').each(function()
		{
            if (!$(this).hasClass('line_head'))
                data += ',' + $(this).attr('_id');
		});
		//alert(data);
		vars = { a: 'sort', code: $('#sort_lines').attr('table'), data: data.substr(1) };
		sendAction(vars);
	}
});
//---------------------------------------------------------------------------------
});//------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// нажание на +\- в древовидном списке
function plus_minus_click(num, img_pm, send, code)
{
	if (num < 1) return;
	
	img_folder = img_pm.nextSibling.nextSibling;
	if (img_folder.src.indexOf('folder.gif') != -1) // если закрыто
	{
		img_folder.src = path_to_img + 'folder_open.gif';
		img_pm.src = path_to_img + 'line_minus.gif';
		div_class = 'line';
		if (lang == 'ru' && send == true)
		{
			vars = { ajax: '', a: 'addOpenMenuId', code: code, id: img_pm.getAttribute('_id'), lang: lang };
			sendAction(vars);
		}
	}
	else // иначе, если открыто
	{
		img_folder.src = path_to_img + 'folder.gif';
		img_pm.src = path_to_img + 'line_plus_1.gif';
		div_class = 'line_hide';
		if (lang == 'ru' && send == true)
		{
			vars = { ajax: '', a: 'delOpenMenuId', code: code, id: img_pm.getAttribute('_id'), lang: lang };
			sendAction(vars);
		}
	}
	
	divline = img_pm.parentNode;
	i = 0; j = 0;
	while (divline = divline.nextSibling)
	{
		if (divline.nodeName == 'DIV')
		{
			if (j > 0)
				j--;
			else
			{
				if (div_class == 'line') j = divline.getAttribute('count_childs');
				divline.className = div_class;
				
				if (div_class == 'line_hide')
				{
					img_pm = divline.firstChild;
					while (img_pm = img_pm.nextSibling)
					{
						if (img_pm.nodeName == 'IMG' && img_pm.src.indexOf('line_minus.gif') != -1)
						{
							img_pm.src = path_to_img + 'line_plus_1.gif';
							img_pm.nextSibling.nextSibling.src = path_to_img + 'folder.gif';
							break;
						}
					}
				}
			}
			i++; if (i >= num) break;
		}
	}
}