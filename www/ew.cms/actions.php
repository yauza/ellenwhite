<?
#########################################################################
#                                                                       #
#   Copyright (c) 2010, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   actions block admin                                                 #
#                                                                       #
#########################################################################

define ('IN_ADMIN', 1);
define ('PATH_TO_ADMIN', './');
define ('PATH_TO_ROOT', '../');
define ('LANG', isset($_GET['lang']) && in_array($_GET['lang'], array('ru', 'en')) ? $_GET['lang'] : 'ru');

define ('CODE', 'actions');

define ('ACT_ADD_OPEN_MENU_ID', 'addOpenMenuId');
define ('ACT_DEL_OPEN_MENU_ID', 'delOpenMenuId');
define ('ACT_SORT', 'sort');
define ('ACT_AUTOCOMPLETE', 'autocomplete');

$is_ajax = isset($_GET['ajax']) ? true : false;

$action = isset($_GET['a']) ? $_GET['a'] : '';
$code = isset($_GET['code']) ? $_GET['code'] : '';
$data = isset($_GET['data']) ? $_GET['data'] : '';
$id = isset($_GET['id']) ? intval($_GET['id']) : 0;

define ('PAGE_CODE', 'content');
define ('PAGE_PATH', LANG . '/' . PAGE_CODE . '/');

require_once(PATH_TO_ADMIN . 'inc/init.inc.php');
//print_r($_GET);
switch ($action)
{
	case ACT_ADD_OPEN_MENU_ID:
	{
		$coo = (isset($_COOKIE['open_' . $code]) ? ($_COOKIE['open_' . $code] . '|') : '') . $id;
		setcookie('open_' . $code, $coo, time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
		echo 'ok';
	} exit;
	case ACT_DEL_OPEN_MENU_ID:
	{
		$coo = isset($_COOKIE['open_' . $code]) ? $_COOKIE['open_' . $code] : '';
		$opens = explode('|', $coo);
		array_unique($opens);
		
		_all($id);
		
		$coo = implode('|', $opens);
		setcookie('open_' . $code, $coo, time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
		echo 'ok';
	} exit;
    case ACT_SORT:
    {
        $db->Query('SELECT MIN(OrderBy) AS min, MAX(OrderBy) AS max FROM ' . $code . ' WHERE ID IN (' . $data . ')');
        if ($db->NextRecord())
        {
        	$ids = explode(',', $data);
            if ($db->F('max') - $db->F('min') + 1 < count($ids))
            {
                echo 'Невозможно произвести сортировку (max = ' . $db->F('max') . '; min = ' . $db->F('min') . '; count = ' . count($ids) . ')';
                exit;
            }
            $orderBy = $db->F('min');
            foreach ($ids AS $id)
            {
                $db->Query('UPDATE ' . $code . ' SET OrderBy = ' . $orderBy++ . ' WHERE ID = ' . $id);
            }
                
            echo 'ok';
        }
    } exit;
    case ACT_AUTOCOMPLETE:
    {
        $return = array();
        if (isset($_GET['table'], $_GET['field'], $_GET['q']))
        {
            $db->Query('SELECT ' . $_GET['field'] . ' FROM ' . $_GET['table'] . ' WHERE ' . $_GET['field'] . ' LIKE "%' . addslashes($_GET['q']) . '%" ORDER BY ' . $_GET['field'] . ' LIMIT 10');
            while ($db->NextRecord())
                echo $db->F($_GET['field']) . "\n";
        }
    } exit;
	default:
	{
		
	}
}

function _all ($id)
{
	global $db, $g_user, $opens; $ids = array();
	$db->Query('SELECT ID FROM ' . LANG . '_sitemap WHERE Parent_id = ' . intval($id));
	while ($db->NextRecord()) $ids[] = $db->F('ID');
	foreach ($ids AS $v)
	{		
		_all ($v);
		
		//$key = array_search($v, $opens);
		//if ($key && isset($opens[$key])) unset($opens[$key]);
	}
	$key = array_search($id, $opens);
	if ($key !== false && isset($opens[$key])) unset($opens[$key]);
}

//require (_PHPSITELIB_PATH . 'psl_finish.inc.php');

?>