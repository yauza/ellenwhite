<?php
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Events page                                                         #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT',  './');
define ('CODE', '404');

require (PATH_TO_ROOT . 'inc/init.inc.php');

////////////////////////////////////////////////////////////////////////
$t = new Template;
$t->set_file('main', PATH_TO_TEMPLATE . '404.html');
require (PATH_TO_ROOT . 'include/top.inc.php');
$t->pparse('C', 'main', false);
require (PATH_TO_ROOT . 'include/bottom.inc.php');
exit;
////////////////////////////////////////////////////////////////////////

?>