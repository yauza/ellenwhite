<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   index.php                                                           #
#   Main on front page                                                  #
#                                                                       #
#########################################################################

define ('LANG', 'ru');
define ('PATH_TO_ROOT', './');
define ('CODE', 'home');

require (PATH_TO_ROOT . 'inc/init.inc.php');

//---------------------------------------------------------------------------
$err = array(); $ok = 0; $text = '';
//---------------------------------------------------------------------------


if (isset($_GET['add_index_records'], $_GET['count']))
{
    
    $return = '';
    $section_id = isset($_GET['section_id']) && intval($_GET['section_id']) ? intval($_GET['section_id']) : 0;
    
    $type_page = 'block';
    if ((!empty($_COOKIE['index']) && $_COOKIE['index'] == 'block') || (!empty($_SESSION['index']) && $_SESSION['index'] == 'block'))
    {
        $type_page = 'block';
    }
    elseif ((!empty($_COOKIE['index']) && $_COOKIE['index'] == 'list') || (!empty($_SESSION['index']) && $_SESSION['index'] == 'list'))
    {
        $type_page = 'list';
    }
    
    $t = new Template;
    $t->set_file('main', PATH_TO_TEMPLATE . 'index.html');
    
    $clases = array('blog', 'nuart', 'fashion', 'erotica', 'mc');

    $where1_list = array('v.Hide = 0');
    $where2_list = array('p.Hide = 0');
    
    if (in_array($section_id, array(1, 2, 3, 4)))
    {
        $where1_list[] = 'v.Section_id = ' . intval($section_id);
        $where2_list[] = 'p.Section_id = ' . intval($section_id);
    }

    $videos = array();
    $t->set_block('main', 'record_block', 'record_block_');
    $t->set_block('main', 'record_list', 'record_list_');

    $db->Query('(SELECT "videos" AS Code, v.ID, v.Date, v.TitleRus, v.TitleOriginal, v.Section_id, v.IsFree, DAY(v.Date) AS Day, MONTH(v.Date) AS Month, YEAR(v.Date) AS Year, v.Description, v.Description_eng, s.Title AS Section, s.Title_eng AS Section_eng FROM ru_videos AS v INNER JOIN ru_sections AS s ON v.Section_id = s.ID WHERE ' . implode(' AND ', $where1_list) . ') UNION 
            (SELECT "photos" AS Code, p.ID, p.Date, p.TitleRus, p.TitleOriginal, p.Section_id, p.IsFree, DAY(p.Date) AS Day, MONTH(p.Date) AS Month, YEAR(p.Date) AS Year, p.Description, p.Description_eng, s.Title AS Section, s.Title_eng AS Section_eng FROM ru_photos AS p INNER JOIN ru_sections AS s ON p.Section_id = s.ID WHERE ' . implode(' AND ', $where2_list) . ')
            ORDER BY Date DESC LIMIT ' . $_GET['count'] . ', 5');
    while ($db->NextRecord()) $videos[] = $db->mRecord;
    for ($i = 0; $i < count($videos); $i++)
    {
        // люди
        $peoples = array();
        $db->Query('SELECT p.ID, p.IsPage, p.FioRus, p.FioOriginal, p2.Title AS Profession, p2.Title_eng AS Profession_eng, pr.Profession_id FROM ru_peoples AS p INNER JOIN ru_peoples_records AS pr ON p.ID = pr.People_id INNER JOIN ru_professions AS p2 ON pr.Profession_id = p2.ID WHERE pr.Record_id = ' . intval($videos[$i]['ID']) . ' AND pr.Record_code = "' . $videos[$i]['Code'] . '" AND p.Hide = 0' . ($type_page == 'list' ? '' : ' AND pr.Profession_id IN (1, 2, 5)') . ' GROUP BY p.ID' . ($type_page == 'list' ? '' : ' LIMIT 4'));
        while ($db->NextRecord())
        {
            $prof = $lang == 'ru' ? 'Profession' : 'Profession_eng';
            $fio = $lang == 'ru' ? 'FioRus' : 'FioOriginal';
            
            if ($db->F('IsPage'))
                $fio = '<a href="' . ($db->F('Profession_id') == 1 ? 'faces' : 'authors') . '/' . $db->F('ID') . '">' . htmlspecialchars($db->F($fio)) . '</a>';
            else
                $fio = htmlspecialchars($db->F($fio));
            
        	$peoples[] = htmlspecialchars($db->F($prof)) . ': ' . $fio;
        }
         
    	$t->set_var(array(
         	'RECORD_CLASS'     => $clases[$videos[$i]['Section_id']],
         	'RECORD_LINK'      => $videos[$i]['Code'] . '/' . $videos[$i]['ID'],
         	'RECORD_TITLE'     => $lang == 'ru' ? htmlspecialchars($videos[$i]['TitleRus']) : htmlspecialchars($videos[$i]['TitleOriginal']),
         	'RECORD_IMG_BIG'   => is_image(PATH_TO_ROOT . 'img/ru/' . $videos[$i]['Code'] . '/' . $videos[$i]['ID'] . '_big'),
         	'RECORD_IMG_SMALL' => is_image(PATH_TO_ROOT . 'img/ru/' . $videos[$i]['Code'] . '/' . $videos[$i]['ID'] . '_small'),
         	'RECORD_PEOPLES'   => implode('<br>', $peoples),
         	'RECORD_DATE'      => $videos[$i]['Day'] . ' ' . get_month_name_low($videos[$i]['Month']) . ' ' . $videos[$i]['Year'] . 'г.',
            'RECORD_LOCK'      => !$videos[$i]['IsFree'] ? 'f-lock' : 'clear',
            'RECORD_SECTION_LINK'  => '/?section_id=' . intval($videos[$i]['Section_id']),
            'RECORD_SECTION_TITLE' => $lang == 'ru' ? htmlspecialchars($videos[$i]['Section']) : htmlspecialchars($videos[$i]['Section_eng']),
            'RECORD_DESCRIPTION' => $lang == 'ru' ? get_title_formated($videos[$i]['Description'], 50, 200) : get_title_formated($videos[$i]['Description_eng'], 50, 200)
        ));
        
        
        if ($type_page == 'list')
            $return .= $t->parse('record_list_', 'record_list', true);
        else
            $return .= $t->parse('record_block_', 'record_block', true);
    }
    
    echo json_encode(array('records' => $return));
    exit;
}

if (isset($_GET['ads_id']))
{
    // добавление +1 к показам рекламного ролика
    $db->Query('UPDATE ru_ads SET Shows = Shows + 1 WHERE ID = ' . intval($_GET['ads_id']));
    
    // добавление +1 к показам для данного пользователя
    if (!empty($user['ID']))
    {
        $db->Query('SELECT Count FROM ru_ads_users WHERE Ads_id = ' . intval($_GET['ads_id']) . ' AND User_id = ' . intval($user['ID']));
        if ($db->NextRecord())
        	$db->Query('UPDATE ru_ads_users SET Count = Count + 1 WHERE Ads_id = ' . intval($_GET['ads_id']) . ' AND User_id = ' . intval($user['ID']));
        else
            $db->Query('INSERT INTO ru_ads_users SET Count = 1, Ads_id = ' . intval($_GET['ads_id']) . ', User_id = ' . intval($user['ID']));
    }
    
    exit;
}

if (!empty($_POST['a']) && string_is_login($_POST['a']))
{
    switch ($_POST['a'])
    {
        case 'video_view':
        {
            if (isset($_POST['video_id']))
            {
                $db->Query('UPDATE ru_videos SET Viewers = Viewers + 1, Date_last_see = NOW() WHERE ID = ' . intval($_POST['video_id']));
                $ok = 1;
                $text = 'UPDATE ru_videos SET Viewers = Viewers + 1 WHERE ID = ' . intval($_POST['video_id']);
                
                $db->Query('INSERT INTO ru_viewers SET User_id = ' . intval($_POST['user_id']) . ', Record_id = ' . intval($_POST['video_id']) . ', Record_type = "videos", Date = NOW()');
            }
        } break;
        case 'video_rating':
        {
            if (!empty($user['ID']) && isset($_POST['video_id'], $_POST['rating']))
            {
                $db->Query('SELECT Video_id FROM ru_videos_ratings WHERE Video_id = ' . intval($_POST['video_id']) . ' AND User_id = ' . intval($user['ID']));
                if ($db->NextRecord())
                {
                    $err[] = 'Вы уже оценивали данное видео';
                }
                else
                {
                    $db->Query('INSERT INTO ru_videos_ratings SET Video_id = ' . intval($_POST['video_id']) . ', User_id = ' . intval($user['ID']) . ', Rating = ' . intval($_POST['rating']));
                    
                    $rating = get_video_stars(intval($_POST['video_id']));
                    $text = $rating['nums'];
                    $ok = 1;
                }
            }
        } break;
        case 'registration':
        {
            if (empty($_POST['email']) || !string_is_email($_POST['email']))
                $err[] = 'Введите корректный e-mail.';
            else
            {
                $db->Query('SELECT Email FROM users WHERE Email = "' . addslashes($_POST['email']) . '"');
                if ($db->NextRecord())
                    $err[] = 'Введнный e-mail уже есть в базе.';
            }
            
            if (empty($_POST['pass']) || !string_is_login($_POST['pass']))
                $err[] = 'Пароль должен состоять минимум из 6 символов (a-z_-).';
            
            
            include PATH_TO_ROOT . 'include/formvalidator/SPAF_FormValidator.class.php';
        	$obj = new SPAF_FormValidator();
        	if (empty($_POST['code']) || !$obj->validRequest($_POST['code']))
                $err[] = 'Введите правильный код с картинки.';
            else
        		$obj->destroy();
        
            if (empty($err))
            {
                $db->Query('INSERT INTO users SET Date_reg = NOW(), Email = "' . addslashes($_POST['email']) . '", Password = "' . md5($_POST['pass']) . '"');
                $ok = 1;
            }
        } break;
        case 'login':
        {
            if (empty($_POST['email']) || empty($_POST['pass']))
                $err[] = 'Введите e-mail и пароль.';
            else
            {
                $db->Query('SELECT ID, Password, Hide FROM users WHERE Email = "' . addslashes($_POST['email']) . '" AND Password = "' . md5($_POST['pass']) . '"');
                if ($db->NextRecord())
                {
                    if ($db->F('Hide') == 1)
                        $err[] = 'Данный аккаунт заблокирован.';
                    else
                    {
                        $_SESSION['user_id'] = $db->F('ID');
                        $_SESSION['user_pass'] = $db->F('Password');
                        
                        setcookie('user_id', $db->F('ID'), time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
                        setcookie('user_pass', $db->F('Password'), time() + 60*60*24*30, '/', '.' . $_SERVER['HTTP_HOST']);
                        
                        $ok = 1;
                    }
                }
                else
                    $err[] = 'Пользователя с такими входными данными не обнаружено.';
            }
            
        } break;
        case 'get_code_video':
        {
            if (empty($_POST['video_id']))
                $err[] = 'Нет ID видео.';
            elseif (empty($user['IsPartner']))
                $err[] = 'Пользователь не является партнером.';
            else
            {
                $db->Query('SELECT ID, TitleRus, TitleOriginal, Description, Imdb FROM ru_videos WHERE ID = ' . intval($_POST['video_id']) . ' AND Hide = 0');
                if ($db->NextRecord())
                {
                    $ok = 1;
                    
                    $video = $db->mRecord;
                    
                    $text .= '<script type="text/javascript" src="http://vp.yauza.com/include/player/jwplayer.js"></script>';
                    $text .= '<div id="mediaplayer"></div>';
                    $text .= '<script type="text/javascript">';
                    $text .= 'jwplayer("mediaplayer").setup(';
                    $text .= '{';
                    $text .= "id: 'playerID',";
                    $text .= "width: '777',";
                    $text .= "height: '416',";
                    $text .= "file: 'http://vp.yauza.com/files/videos/1/DV0556.mp4',";
                    $text .= 'logo: false,';
                    $text .= "skin: 'http://vp.yauza.com/include/player/modieus/modieus.zip',";
                    $text .= 'modes: [';
                    $text .= '{';
                    $text .= "type: 'html5'";
                    $text .= '},';
                    $text .= '{';
                    $text .= "type: 'flash',";
                    $text .= "src: 'http://vp.yauza.com/include/player/player.swf'";
                    $text .= '}';
                    $text .= ']';
                    $text .= '});';
                    $text .= '</script>';
                    
                    
                	/*$text .= 'Идентификатор партнера: ' . $user['ID'];
                	$text .= "\n" . 'Идентификатор фильма в базе: ' . $video['ID'];
                	$text .= "\n" . 'Локализованное название: ' . htmlspecialchars($video['TitleRus']);
                	$text .= "\n" . 'Название фильма оригинальное: ' . htmlspecialchars($video['TitleOriginal']);
                    
                    $peoples = array();
                    $db->Query('SELECT p.FioRus FROM ru_peoples AS p INNER JOIN ru_videos_directors AS vd ON p.ID = vd.People_id WHERE vd.Video_id = ' . intval($_POST['video_id']) . ' AND p.Hide = 0');
                    while ($db->NextRecord()) $peoples[] = htmlspecialchars($db->F('FioRus'));
                	$text .= "\n" . 'Режиссер: ' . implode(', ', $peoples);
                    
                	$text .= "\n" . 'Краткое описание фильма: ' . htmlspecialchars($video['Description']);
                	$text .= "\n" . 'Идентификатор IMDb: ' . $video['Imdb'];*/
                }
                else
                    $err[] = 'Видео не найдено.';
            }
        } break;
    }
}

echo json_encode(array('err' => implode('<br>', $err), 'ok' => $ok, 'text' => $text));

?>