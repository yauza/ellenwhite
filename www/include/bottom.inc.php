<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   bottom.inc.php                                                      #
#   bottom page                                                         #
#                                                                       #
#########################################################################

$t_bot = new Template();
$t_bot->set_file('main', PATH_TO_TEMPLATE . 'bottom.html');

$t_bot->set_var(array(
	'PATH_TO_ROOT'      => PATH_TO_ROOT,
	'PATH_TO_IMG'       => PATH_TO_IMG,
    'BACK_TO_TOP_LINK'  => $_SERVER['REQUEST_URI'] . '#',
    'BACK_TO_TOP_TITLE' => $lang == 'ru' ? 'вернуться в начало страницы' : 'back to top',
	'LANG'         => $lang == 'ru' ? 'ru' : 'en',
	'Copyright'	=>	get_option_by_code("site_copyright"),
	'Facebook'	=>	get_option_by_code("site_facebook_link"),
	'Delicious'	=>	get_option_by_code("site_delicious_link"),
	'Stumble'	=>	get_option_by_code("site_stumble_link"),
	'Twitter'	=>	get_option_by_code("site_twitter_link"),
	'Linkedin'	=>	get_option_by_code("site_linkedin_link"),
	'Reddit'	=>	get_option_by_code("site_reddit_link")
));
$t_bot->pparse('_', 'main');
unset($t_bot);

//---------------------------------------------------------------------------

?>