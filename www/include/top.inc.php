<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   top.inc.php                                                         #
#   print top of pages                                                  #
#                                                                       #
#########################################################################

$t_top = new Template();

$t_top->set_file('main', PATH_TO_TEMPLATE . 'top.html');
$t_top->set_block('main', 'slider', 'slider_');
$t_top->set_var(array(
    'BASE_HREF'        => 'http://' . $_SERVER['HTTP_HOST'] . '/',
	'PAGE_TITLE'       => defined('PAGE_TITLE') ? htmlspecialchars(PAGE_TITLE) : ''
));
/*echo "<pre style='color:white;'>";
print_r($_SERVER);
echo "</pre>";*/
if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['PHP_SELF'] == '/ru/folio/index.php')
	{
		$t_top->set_block('slider', 'header_sl', 'header_sl_');
		$db->Query('SELECT * FROM ru_folio WHERE Hide = 0 AND IsSlide = 1');
		while($db->NextRecord())
		{
			$img = is_image(($_SERVER['PHP_SELF'] == '/ru/folio/index.php' ? '../../' : '') . 'img/ru/folio/' . $db->F('ID'));
			if ($img)
			{
				$t_top->set_var("SL_IMG", $img);
				$t_top->parse('header_sl_', 'header_sl', true);
			}
		}
				$t_top->parse('slider_', 'slider', false);
				$t_top->set_var('header_sl_', '');
	}
switch($_SERVER['PHP_SELF'])
{
	case "/ru/folio/index.php" : $t_top->set_var("BODY_ID", "page3"); break;
	case "/ru/about/index.php" : $t_top->set_var("BODY_ID", "page2"); break;
	case "/ru/services/index.php" : $t_top->set_var("BODY_ID", "page4"); break;
	case "/ru/news/index.php" : $t_top->set_var("BODY_ID", "page5"); break;
	case "/ru/comingsoon/index.php" : $t_top->set_var("BODY_ID", "page5"); break;
	case "/ru/contact/index.php" : $t_top->set_var("BODY_ID", "page6"); break;
	case "/ru/content/index.php" : $t_top->set_var("BODY_ID", "page6"); break;
	case "/ru/search/index.php" : $t_top->set_var("BODY_ID", "page6"); break;
	case "/404.php" : $t_top->set_var("BODY_ID", "page6"); break;
	default: $t_top->set_var("BODY_ID", "page1"); break;
}

/*$top_menu = array(
	"Home"	=>	array("link" => '/', "self"	=>	"/index.php"),
	"About"	=>	array("link" => '/about/', "self"	=>	"/ru/about/index.php"),
	"Folio"	=>	array("link" => '/folio/', "self"	=>	"/ru/folio/index.php"),
	"Services"	=>	array("link" => '/services/', "self"	=>	"/ru/services/index.php"),
	"News"	=>	array("link" => '/news/', "self"	=>	"/ru/news/index.php"),
	"Contact"	=>	array("link" => '/contact/', "self"	=>	"/ru/contact/index.php")
);*/
$top_menu ["Home"]= array("link" => '/', "self"	=>	"/index.php");
$db->Query('SELECT * FROM ru_sitemap WHERE Hide = 0 AND Parent_id = 411 ORDER BY OrderBy');
while($db->NextRecord())
	$top_menu [$db->F('Title')]= array("link" => '/'.($db->F('Code') == "../opt/?a=site" ? 'contact' : $db->F('Code')).'/', "self"	=>	"/ru/". $db->F('Code') ."/index.php");
	

/*echo "<pre style='color:white'>";
print_r($top_menu);
echo "</pre>";*/
$cnt = count($top_menu);
$i = 1;
$t_top->set_block('main', 'top_menu', 'top_menu_');
foreach($top_menu AS $top => $tm)
{
	$t_top->set_var(array(
		"MENU_CLASS"	=>	$i == $cnt ? 'class="end"' : '',
		"MENU_ID"	=>	$_SERVER['PHP_SELF'] == $tm['self'] ? 'id="active"' : '',
		"MENU_LINK"	=>	$tm['link'],
		"MENU_TITLE"	=>	$top
	));
	$t_top->parse('top_menu_', 'top_menu', true);
	$i++;
	
	//echo "<p style='color:white'>".$top."</p>";
}

$t_top->pparse('_', 'main');
unset($t_top);

?>