<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   left.inc.php                                                        #
#   print left column content                                           #
#                                                                       #
#########################################################################

$t_right = new Template();
$t_right->set_file('main', PATH_TO_TEMPLATE . 'right.html');

$t_right->set_var(array(
	'PATH_TO_ROOT' => PATH_TO_ROOT,
	'PATH_TO_IMG'  => PATH_TO_IMG
));
//-----------------------------------------------------------------------------
$t_right->pparse('_', 'main');
unset($t_right);

?>