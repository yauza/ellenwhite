<?
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   left.inc.php                                                        #
#   print left column content                                           #
#                                                                       #
#########################################################################

$t_left = new Template();
$t_left->set_file('main', PATH_TO_TEMPLATE . 'left.html');

$t_left->set_var(array(
	'PATH_TO_ROOT' => PATH_TO_ROOT,
	'PATH_TO_IMG'  => PATH_TO_IMG
));
//-----------------------------------------------------------------------------
$t_left->pparse('_', 'main');
unset($t_left);

?>