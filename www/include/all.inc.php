<?php
#########################################################################
#                                                                       #
#   Copyright (c) 2011, Yauza Software (http://www.yauza.com)           #
#                                                                       #
#   all.inc.php                                                         #
#                                                                       #
#########################################################################

if ((CODE == 'videos' && !empty($video)) || (CODE == 'photos' && !empty($photo)))
{
    $_SESSION['last_page'] = $_SERVER['REQUEST_URI'];
}
elseif (CODE != 'registration')
    $_SESSION['last_page'] = false;


require PATH_TO_ROOT . 'include/top.inc.php';

//require PATH_TO_ROOT . 'include/left.inc.php';

$t->pparse('C', 'main', false);

//require PATH_TO_ROOT . 'include/right.inc.php';

require PATH_TO_ROOT . 'include/bottom.inc.php';

require _PHPSITELIB_PATH . 'psl_finish.inc.php';

?>